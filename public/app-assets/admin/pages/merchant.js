$(function () {

    if($('#admin-users-list-wrap').length) {

        const manageUsers = $('#table-manage-users');
        if (manageUsers.length > 0) {

            $.extend( $.fn.dataTableExt.oStdClasses, {
                "sFilterInput": "form-control",
                "sLengthSelect": "form-control"
            });

            const tableManageUsers = manageUsers.DataTable({
                responsive: true,
                processing: true,
                serverSide: true,
                autoWidth: false,
                ajax: {
                    url: manageUsers.data('url'),
                },
                columns: [
                    {data: 'merchantId', name: 'merchantId'},
                    {data: 'merchant_business_name', name: 'merchant_business_name'},
                    {data: 'merchant_company_name', name: 'merchant_company_name'},
                    {data: 'merchant_name', name: 'merchant_name'},
                    {data: 'email', name: 'email'},
                    {data: 'mobile', name: 'mobile'},
                    {data: 'address', name: 'address'},
                    {data: 'postcode', name: 'postcode'},
                    //{data: 'seller_status', name: 'seller_status', className: 'text-capitalize', orderable: false},
                    {data: 'action', className: 'text-center', orderable: false}
                ]
            });




            /* $(document.body).on('click','.act-delete-storelocations',function (e) {
                e.preventDefault();

                var url = $(this).attr('data-url');
                var name = $(this).attr('data-name');
                bootbox.confirm({
                    message: "Are you sure want to delete <strong>"+name+"</strong> store?",
                    centerVertical : true,
                    callback: function(result){
                        if(result){
                            window.location.href = url;
                        }
                        else {
                            return;
                        }
                    }
                })


            }); */


        }
    }

});
