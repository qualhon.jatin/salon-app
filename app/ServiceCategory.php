<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    protected $guard = 'merchants';
    protected $table = 'service_categories';

    protected $fillable = [
        'category_name','status'
    ];

    public function services(){
    	return $this->hasMany('App\Service','service_categories_id');
    }
}
