<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merchant_secondary_working_hour extends Model
{
    protected $table = 'merchant_secondary_working_hours';

    protected $fillable = [
        'merchant_id','date','week_day','start_time', 'end_time','break_start_time','break_end_time','status'
    ];
}
