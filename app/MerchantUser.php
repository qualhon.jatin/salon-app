<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantUser extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_users';

    protected $fillable = [
        'merchant_id','user_id'
    ];

    public function detail(){
        return $this->hasOne('App\UsersDetail','user_id','user_id');
    }

    public function appointments(){
    	return $this->hasMany('App\Appointment','merchant_id','merchant_id');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
