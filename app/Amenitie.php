<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenitie extends Model
{
    protected $guard = 'merchants';
    protected $table = 'amenities';

    protected $fillable = [
        'name','status'
    ];
}
