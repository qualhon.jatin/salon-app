<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantOffer extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_offers';

    protected $fillable = [
        'merchant_id','service_id','discount','max_discount_price','start_time','end_time','status','max_offer_limit'
    ];

    public function merchantServices(){
        $columns = ['id','service_id','merchant_staff_id','service_category_id','service_price','service_duration','service_buffer_time','service_image'];
        return $this->hasMany('App\MerchantService','merchant_staff_id','merchant_id')->select($columns);
    }

    public function merchant(){
    	$columns = ['id','merchant_id','business_cover_image','merchant_business_name','merchant_company_name','landline','merchant_name','salon_type'];
        return $this->hasMany('App\MerchantDetail','merchant_id','merchant_id')->select($columns);	
    }

    public function merchants(){
    	$columns = ['id','is_verified','status'];
        return $this->hasOne('App\Merchant','id','merchant_id')->select($columns);	
    }


    public function category(){        
        return $this->hasOne('App\ServiceCategory');
    }
}
