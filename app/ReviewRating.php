<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewRating extends Model
{
    protected $table = 'review_ratings';

    protected $fillable = [
        'user_id','merchant_id','appointment_id', 'rating','review'
    ];


    public function userDetail (){
        $columns = ['user_id','name','user_image'];
        return $this->hasOne('App\UsersDetail','user_id','user_id')->select($columns);
    }
    
    public function appointment(){
        $columns = ['user_id','name','user_image'];
        return $this->hasOne('App\Appointment','merchant_id','merchant_id');
    }    


}
