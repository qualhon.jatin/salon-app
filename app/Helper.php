<?php

function get_local_time()
{

    $ip = file_get_contents("http://ipecho.net/plain");
    $url = 'http://ip-api.com/json/' . $ip;
    $tz = file_get_contents($url);
    $tz = json_decode($tz, true)['timezone'];

    return $tz;

}

function getWeekDayFromDate($week_dates)
{
    $list = [];
    $list_data = [];
    foreach ($week_dates as $dates) {
        $list[date('D', strtotime($dates))] = $dates;
    }

    $week_day = [
        "Mon" => 1,
        "Tue" => 2,
        "Wed" => 3,
        "Thu" => 4,
        "Fri" => 5,
        "Sat" => 6,
        "Sun" => 7,
    ];

    foreach ($week_day as $key => $values) {
        foreach ($list as $key_list => $list_values) {
            if (($key == $key_list)) {

                $list_data[$list_values] = $values;
                break;
            }
        }

    }
    ksort($list_data);
    return $list_data;

}

function compareWorkingHour($default_working_hour,$temp_working_hour){
    foreach($default_working_hour as $default_hour){
        foreach($temp_working_hour as $temp_hour){
            if($default_hour['week_day'] == $temp_hour['week_day']){
                $data['status'] = $temp_hour['status'];
                $data['start_time'] = $temp_hour['start_time'];
                $data['end_time'] = $temp_hour['end_time'];
                $data['week_day'] = $temp_hour['week_day'];
            }

            
        }
        return($data);
    }
    
    // pr($default_working_hour);
    // pr($temp_working_hour);
}

function pr($data){
    echo "<pre>"; 
    echo "<pre>"; 
    print_r($data);
}

function ej($data){
    echo json_encode($data);
}


