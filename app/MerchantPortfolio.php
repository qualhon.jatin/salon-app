<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantPortfolio extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_portfolios';

    protected $fillable = [
        'merchant_id','image'
    ];
}
