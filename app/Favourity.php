<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourity extends Model
{
    protected $table = 'favourities';

    protected $fillable = [
        'merchant_id','user_id'
    ];
}
