<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffWorkingDay extends Model
{
    protected $guard = 'merchants';
    protected $table = 'staff_working_days';

    protected $fillable = [
       'merchant_id', 'day',  'status','start_time','end_time','breakstart','breakend'
    ];

    public function merchants()
    {
        return $this->belongsToMany(MerchantUser::class);
    }
   
    
}
