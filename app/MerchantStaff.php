<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantStaff extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_staff';

    protected $fillable = [
        'merchant_id','staff_id','status'
    ];

    public function staffServices(){
        $columns = ['id','service_id','merchant_staff_id','service_category_id','service_price','service_duration','service_buffer_time','service_image'];
    	return $this->hasMany('App\MerchantService','merchant_staff_id','staff_id')->select($columns);
    }

    public function staffDetail(){
        $columns = ['merchant_id','merchant_image','merchant_name','gender','email','landline'];
        return $this->hasOne('App\MerchantDetail','merchant_id','staff_id')->select($columns);
    }

    public function workingHours(){
        $columns = ['id','merchant_id','week_day','start_time','end_time','break_start_time','break_end_time','status'];
        return $this->hasMany('App\MerchantsWorkingHour','merchant_id','staff_id')->select($columns);
    }

    public function staffAppointment(){
        $columns = ['id','merchant_id','staff_id','user_id','appointment_date_time'];
        return $this->hasMany('App\Appointment','staff_id','staff_id')->select($columns);
    }
    public function merchant(){
        $columns = ['date_of_joining'];
        return $this->hasOne('App\Merchant', 'id','staff_id')->select($columns);
    }

    public function workingHoursSecondary(){
        $columns = ['id','merchant_id','week_day','start_time','end_time','break_start_time','break_end_time','status'];
        return $this->hasMany('App\Merchant_secondary_working_hour','merchant_id','staff_id')->select($columns);
    }
}
