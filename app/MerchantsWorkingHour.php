<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantsWorkingHour extends Model
{
   
    protected $guard = 'merchants';
    protected $table = 'merchants_working_hours';

    protected $fillable = [
        'merchant_id','week_day','start_time','end_time','break_start_time','break_end_time','status'
    ];
}
