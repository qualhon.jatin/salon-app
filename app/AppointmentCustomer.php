<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentCustomer extends Model
{
    protected $table = 'appointment_customers';
    protected $fillable = [
        'appointment_id','customer_name','customer_mobile', 'customer_address','customer_landmark','customer_postcode', 'customer_lat','customer_lng'
    ];
}
