<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantDetail extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_details';

    protected $fillable = [
        'merchant_id','business_cover_image','merchant_business_name','merchant_company_name','landline','merchant_name','merchant_service_type','salon_type','email'
    ];

    public function review(){
        $columns = ['id','merchant_id','rating','review'];
        return $this->hasMany('App\ReviewRating','merchant_id','merchant_id')->select($columns);
    }

    public function address(){
    	$columns = ['id','merchant_id','address1','address2','lat','lng','city','postcode'];
    	return $this->hasOne('App\MerchantAddress','merchant_id','merchant_id')->select($columns);	
    }

    public function merchant(){
    	$columns = ['id','merchant_id','address1','address2','lat','lng','city','postcode'];
    	return $this->belongsTo('App\Merchant','id');	
    }
  
}
