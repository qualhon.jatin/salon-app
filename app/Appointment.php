<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes;
    protected $table = 'appointments';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id','merchant_id','staff_id', 'appointment_date_time','appointment_type','appointment_status', 'actual_price','discount','final_price', 'client_message','appointment_id','timezone','cancel_message','cancel_by','cancel_by_id','merchant_address','merchant_lat','merchant_lng','user_address','user_lat','user_lng','appointment_service_type'
    ];

    public function detail(){
    	$columns = ['id','appointment_id','price','name','service_duration','service_buffer_time','discount','package_services'];
    	return $this->hasMany('App\AppointmentDetail')->select($columns);
    }
    public function merchantDetail(){
        $columns = ['id','merchant_id','business_cover_image','merchant_business_name','merchant_company_name','landline','merchant_name','merchant_image','merchant_service_type','salon_type','email','gender','detail_image'];
        return $this->hasOne('App\MerchantDetail','merchant_id','merchant_id')->select($columns);
    }

    public function staffDetail(){
        $columns = ['id','merchant_id','business_cover_image','merchant_business_name','merchant_company_name','landline','merchant_name','merchant_image','merchant_service_type','salon_type','email','gender','detail_image'];
        return $this->hasOne('App\MerchantDetail','merchant_id','staff_id')->select($columns);
    }

  	public function merchantAddress(){
        return $this->hasOne('App\MerchantAddress','merchant_id','merchant_id');
    }

    public function userDetail(){
        $columns = ['id','user_id','email','name','gender','user_image','allergies'];
    	return $this->hasOne('App\UsersDetail','user_id','user_id')->select($columns);
    }

    public function customerDetail(){
        return $this->hasOne('App\AppointmentCustomer');
    	
    }
}
