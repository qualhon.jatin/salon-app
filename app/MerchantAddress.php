<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantAddress extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_addresses';

    protected $fillable = [
        'merchant_id','address1','address2','city','postcode','lat','lng'
    ];
}
