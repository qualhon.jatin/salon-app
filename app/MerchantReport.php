<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantReport extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_reports';

    protected $fillable = [
        'merchant_id','total_revenue','current_month_revenue','last_month_revenue',
    ];
}
