<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantAmenitie extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_amenities';
    public $timestamps = true;
    protected $fillable = [
        'amenitie_id','merchant_id'
    ];
}
