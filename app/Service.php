<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $guard = 'merchants';
    protected $table = 'services';

    protected $fillable = [
        'service_categories_id','service_name','service_price','service_duration','service_buffer_time','service_image','status'
    ];
}
