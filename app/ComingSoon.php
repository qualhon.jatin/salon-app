<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComingSoon extends Model
{
    protected $table = 'coming_soons';

    protected $fillable = [
        'user_id','user_type','name','phone','company','address'
    ];
}
