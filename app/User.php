<?php

namespace App;



use Laravel\Passport\HasApiTokens;


use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable 
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */
    
    protected $fillable = [
        'mobile','otp', 'is_user_verified','country_code','pin','currency'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password', 'remember_token',
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
      */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    public function detail(){
        $columns = ['user_id','email','gender','user_image','name','push_status','email_push_status'];
        return $this->hasOne('App\UsersDetail')->select($columns);
    }
}
