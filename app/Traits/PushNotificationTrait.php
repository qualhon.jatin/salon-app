<?php

namespace App\Traits;

use App\PushToken;
use App\Notification;
use Illuminate\Http\Request;


trait PushNotificationTrait {

    public function pushNotification($data,$type = 2,$ref_id = null) {

        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = 'AAAAMTmZpRw:APA91bHsULbs5AdxS0mv_x8d1vpPkYkNy2IqcZvhdc4XdOQi9BAO3m5oLtB-n7oNwiBdBlBlp6jeLrdeUtwYm87Walfh-LlID-M0qnSnBovO9eMYy9Q3v0llPD9xhabv19onuqcoxBN4';

        $title = $data['title'];
        $body = $data['body'];
        $notification = array('title' =>$title , 'body' => $body, 'sound' => 'default');

        if(@$data['type']) {
            $data_type = array('type'=>$data['ntype']);
        } else {
            $data_type = array('type'=>1);
        }

        // $userType    =   (isset($data['user_type']) && $data['user_type'] != null) ? $data['user_type'] : 1;
        $userType    =  $data['user_type'];

        $tokens  =   PushToken::where('user_id', $data['user_id'])->where('user_type', $userType)->get();

        foreach($tokens as $token){

            $arrayToSend = array('to' => $token->token, 'notification' => $notification,'priority'=>'high','data'=>$data_type);
            $json = json_encode($arrayToSend);
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key='. $serverKey;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
            $response = curl_exec($ch);
            curl_close($ch);
        }

        // $this->saveNotification($data['user_id'],$userType,$body,$type,$ref_id);
    }

    protected function saveNotification($user_id, $user_type, $message, $type, $ref_id) {
        $data = [
                    'user_id'=>$user_id, 
                    'user_type'=>$user_type, 
                    'type' => $type,
                    'ref_id' => $ref_id,
                    'message'=>$message,                
                ];
        Notification::create($data);
    }


}
