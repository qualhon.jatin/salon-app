<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersDetail extends Model
{
    protected $table = 'users_details';

    protected $fillable = [
        'user_id','email','name','gender','user_address1','user_address2','user_city','user_postcode','user_image','added_by','added_user_type','push_status','email_push_status','nickname','birthday','anniversary','allergies','phone_preference','text_preference'
    ];


    public function user(){
    	return $this->belongsTo('App\User');
    }
}
