<?php

namespace App\Console\Commands;

use App\Appointment;
use Illuminate\Console\Command;
use App\Http\Controllers\Controller;
use App\Traits\PushNotificationTrait;

class NotificationCron extends Command
{
    use PushNotificationTrait;    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron is working fine!");

        // $appointments = Appointment::where('appointment_status',1)->get()->toArray();
        // foreach ($appointments as $appointment) {           
        //     $appointmentDatetime = $appointment['appointment_date_time'];
        //     $currentDatetime = $this->getCurrentTime($appointment['timezone']);
        //     if($appointmentDatetime > $currentDatetime){                                
        //         $seconds = strtotime($appointmentDatetime) - strtotime($currentDatetime);
        //         $hours = $seconds / 60 / 60; // get hours
        //         if(round($hours,2) == 1.0){
        //             $data_list =[
        //                 [
        //                     'title'     => "Appointment Reminder",
        //                     'body'      => "You have and appointment after 1 hour",
        //                     'user_id'   =>  $appointment['user_id'],
        //                     'user_type' => 0
        //                 ],
        //                 [
        //                     'title'     => "Appointment Reminder",
        //                     'body'      => "You have and appointment after 1 hour",
        //                     'user_id'   =>  $appointment['merchant_id'],
        //                     'user_type' => 1
        //                 ],
        //                 [
        //                     'title'     => "Appointment Reminder",
        //                     'body'      => "You have and appointment after 1 hour",
        //                     'user_id'   =>  $appointment['staff_id'],
        //                     'user_type' => 2
        //                 ]
        //             ];                          

        //             foreach($data_list as $data){
        //                 $this->pushNotification($data,1,$appointment['id']);
        //             }
        //         \Log::info("Appointment notification sent");
        //         }
        //         dump(round($hours,2));
        //     }
        // }

        /*
           Write your database logic we bellow:
           Item::create(['name'=>'hello new']);
        */
      
        $this->info('Demo:Cron Cummand Run successfully!');
    }
}
