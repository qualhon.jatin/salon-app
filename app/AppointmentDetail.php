<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentDetail extends Model
{
    protected $table = 'appointment_details';

    protected $fillable = [
        'appointment_id','name','price', 'service_duration','discount','service_buffer_time','package_services'
    ];

    public function appointmnet(){
        return $this->belongsTo('App\Appointment');
    }
    
}
