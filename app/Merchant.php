<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Auth;


use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;

class Merchant extends Authenticatable
{

    use HasApiTokens, Notifiable;

    protected $guard = 'merchants';
    protected $table = 'merchants';

    protected $fillable = [
        'mobile', 'otp', 'remember_token', 'verification_level', 'merchant_user_type', 'date_of_joining','merchant_image','country_code','pin','currency'
    ];

    public function address(){
        return $this->hasOne('App\MerchantAddress');
    }

    public function detail(){
        $columns = ['id','merchant_id','business_cover_image','merchant_business_name','merchant_company_name','landline','merchant_name','merchant_image','merchant_service_type','salon_type','email','gender','detail_image','push_status','email_push_status'];
        return $this->hasOne('App\MerchantDetail')->select($columns);
    }

    public function rating(){
        $columns = ['id','merchant_id','appointment_id','rating','review','created_at','user_id'];
        return $this->hasMany('App\ReviewRating')->select($columns);
    }

    public function services(){
        $columns = ['id','service_id','merchant_staff_id','service_category_id','service_price','service_duration','service_buffer_time','service_image'];
        return $this->hasMany('App\MerchantService','merchant_staff_id')->select($columns);
    }
    public function merchantServices(){
        return $this->hasMany('App\MerchantService','merchant_staff_id','id')->where('user_type',1);
    }

    public function staffServices(){
        return $this->hasMany('App\MerchantService','merchant_staff_id','id')->select(['service_category_id','merchant_staff_id','service_id'])->where('user_type',2);
    }

    public function amenities (){
        return $this->hasMany('App\MerchantAmenitie');
    }

    public function portfolio(){
        $columns = ['id','merchant_id','image'];
        return $this->hasMany('App\MerchantPortfolio')->select($columns);
    }

    public function staff(){
        $columns = ['id','merchant_id','staff_id'];
        return $this->hasMany('App\MerchantStaff')->select($columns);
    }

    // public function signleStaff(){
    //     $columns = ['id','merchant_id','staff_id'];
    //     return $this->hasOne('App\MerchantStaff','merchant_id',)->select($columns);
    // }

    public function workingHours(){
        $columns = ['id','merchant_id','week_day','start_time','end_time','break_start_time','break_end_time','status'];
        return $this->hasMany('App\MerchantsWorkingHour')->select($columns);
    }

    public function offers(){
        $columns = ['id','merchant_id','service_id','discount','max_discount_price','start_time','end_time'];
        return $this->hasMany('App\MerchantOffer')->select($columns);
    }

    public function packages(){
        $columns =  ['id','merchant_id','name','price','start_time','end_time'];
        return $this->hasMany('App\MerchantPackage')->select($columns);
    }

    public function staffAppointment(){
        $columns =  ['id','merchant_id','staff_id','user_id','appointment_date_time'];
        return $this->hasMany('App\Appointment','staff_id')->select($columns)->where('appointment_status',1);
    }

    public function workingHoursSecondary(){
        $columns = ['id','merchant_id','week_day','start_time','end_time','break_start_time','break_end_time','status'];
        return $this->hasMany('App\Merchant_secondary_working_hour','merchant_id','merchant_id')->select($columns);
    }

}
