<?php

namespace App\Http\Controllers;

use Twilio\Rest\Client;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DateTime;
use DatePeriod;
use DateInterval;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $success_code = 200;    
    protected $error_code = 400;    
    protected $unauthenticate_code = 401;

    protected $default_image = '';
    protected $default_profile_image = '';
    protected $default_cover_image = '';
    protected $default_detail_image = '';
    protected $days = [1=>'Monday',2=>'Tuesday',3=>'Wednesday',4=>'Thursday',5=>'Friday',6=>'Saturday',7=>'Sunday'];
    

    public function __construct(){
        $this->default_image = url('app-assets/image/salon_pic_demo.png');        
        $this->default_profile_image = url('app-assets/image/default_profile_image.png');        
        $this->default_cover_image = url('app-assets/image/default-cover-image.png');
        $this->default_detail_image = url('app-assets/image/default-square-detail-image.png');
        $this->default_salon_image = url('app-assets/image/salon_pic_demo.png');
    }

    protected function paginate($items, $perPage = 1, $page = null, $options = []) {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    protected function removePaginatorParam($data) {          
        $unset_vars = ['current_page','first_page_url','from','last_page_url','next_page_url','path','total','prev_page_url','to','per_page'];
        foreach ($unset_vars as $key) {        
            unset($data[$key]);
        }            
        return $data;
    }

    protected function convertToTimestamp($value,$timezone, $millisecods = true){
        date_default_timezone_set($timezone);
        if($millisecods){
            return strtotime($value)*1000;
        }
        return strtotime($value);
    }

    protected function convertToTimestampLocal($value, $millisecods = true){
        if($millisecods){
            return strtotime($value)*1000;
        }
        return strtotime($value);   
    }

    protected function convertToDateTime($value){
        return date("Y-m-d H:i:s", ($value)/1000);
    }

    protected function convertToDate($timestamp, $format = "Y-m-d"){
        $dateTime = $this->convertToDateTime($timestamp);
        return date($format,strtotime($dateTime));
    }

    protected function convertToTime($timestamp){
        $dateTime = $this->convertToDateTime($timestamp);
        return date('Gi.s',strtotime($dateTime));
    }

    protected function getCurrentTime($timezone){
        $date = new \DateTime("now", new \DateTimeZone($timezone) );
        return $date->format('Y-m-d H:i:s');
    }   

    protected function minsToMiliseconds($mins){
        return $mins * 60000;
    }

    protected function getDay($timestamp,$timezone){    
        date_default_timezone_set($timezone);    
        return date('l', strtotime($this->convertToDate($timestamp)));
    }
    
    protected function getDayShort($timestamp,$timezone){    
        date_default_timezone_set($timezone);    
        return date('D', strtotime($this->convertToDate($timestamp)));
    }

    protected function UTC(){
        $utc_time = date("Y-m-d H:i:s", time());                
        return $this->convertToTimestamp($utc_time);     
    }
    protected function valueExsist($array_data,$val,$column){        
        foreach ($array_data as $data) {
            if($data[$column] == $val) {
                return true;
            }     
        }
    }

    protected function requestValidator($req,$fields){
        $request_fields = [];
        foreach ($fields as $field) {
            $data  = [$field => 'required'];
            $request_fields[$field] = 'required';            
        }        ;        
        $validator = Validator::make(
            $req->all(),
            $request_fields
        );
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }
    }

    /*
    *
    *        Get nearby Salon Listing as Per lat lng
    *
    */


    protected function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Mi') {
       
        $theta = $longitude1 - $longitude2;
        $distance = sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta));

        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;
        
        switch($unit)
        {
            case 'Mi': break;
            case 'Km' : $distance = $distance * 1.609344;
        }
        return (round($distance,2));
    }

    protected function uploadFile($file,$folder){                
        $imageName = $folder.'/' . time() . '.' . $file->getClientOriginalExtension();
        $t = Storage::disk('s3')->put($imageName, file_get_contents($file), 'public');
        $imageName = Storage::disk('s3')->url($imageName);        
        return $imageName;
    }

    protected function addInterval($timestamp, $miliseconds = 1800000 /* 30 mins */){
        return $timestamp + $miliseconds;
    }        

    protected function randomNo($digits){
        return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

   

    protected function sendMessage($message, $recipients) {
        $account_sid = env("TWILIO_SID");
        $auth_token = env("TWILIO_AUTH_TOKEN");
        $twilio_number = env("TWILIO_NUMBER");
        $client = new Client($account_sid, $auth_token);
        
        try{
            $client->messages->create($recipients, ['from' => $twilio_number, "messagingServiceSid" => "MG7d0ca3deed76d2935d290f19d8dfdf90",'body' => $message] );        
        }
        catch(\Exception $e){
            if($e->getCode() == 21211){
                return ['status'=>$this->error_code];
            }        
        }
        
    }

    protected function sendOTP($mobile_number){
        $OTP = $this->randomNo(4);        
        $msg = "<#> Your OTP is ".$OTP." This is confidential and we don't recommend to share it with anyone";
        $opt_response = $this->sendMessage($msg,$mobile_number);                
        
        if($opt_response == null){
            return $OTP;    
        }else{            
            return $opt_response;    
        }
       
    }
    protected function sendPin($mobile_number,$name){
        $pin = $this->randomNo(4);        
        $msg = "Hello ".$name.", your temporary  pin is ".$pin.".We recommend you to please update your PIN after login with temporary PIN.";
        
        
        $opt_response = $this->sendMessage($msg,$mobile_number);                
        
        if($opt_response == null){
            return $pin;    
        }else{            
            return $opt_response;    
        }
       
    }

    protected function convertToLocalTime($timestamp, $timezone){
        $date = $this->convertToDateTime($timestamp);
        
        $l10nDate = new \DateTime($date, new \DateTimeZone('UTC'));
        $l10nDate->setTimeZone(new \DateTimeZone($timezone));
        return $final_date = $l10nDate->format('Y-m-d H:i:s');
    }

    protected function getDateTimeDiff($datetime1, $datetime2, $unit = 'min'){
        switch ($unit) {
            case 'min':
                    $seconds = strtotime($datetime1) - strtotime($datetime2);
                    $response = $mintues = $seconds / 60; // get mintues
                break;
            case 'hrs':
                    $seconds = strtotime($datetime1) - strtotime($datetime2);
                    $response = $mintues = $seconds / 60 / 60; // get hours
            default:
                $response = null;
                break;
        }
        return $response;
    }

    protected function sendMail($to, $from, $subject, $message){        
        $mail = new PHPMailer(true);
        try {        

                // Server settings
                $mail->SMTPDebug = 0;                                   // Enable verbose debug output
                $mail->isSMTP();                                        // Set mailer to use SMTP
                $mail->Host = 'smtp.gmail.com';                         // Specify main and backup SMTP servers
                $mail->SMTPAuth = true;                                 // Enable SMTP authentication
                $mail->Username = 'info@opayn.com';                     // SMTP username
                $mail->Password = 'infoOp@yn$21';                        // SMTP password
                $mail->SMTPSecure = 'ssl';                              // Enable TLS encryption, `ssl` also accepted
                $mail->Port = 587;                                      // TCP port to connect to

                //Recipients
                $mail->setFrom('no-reply@opayn.com', 'Mailer');
                $mail->addAddress($to);  // Add a recipient, Name is optional
                // $mail->addReplyTo('gurpreetsingh.qualhon@gmail.com', 'Mailer');
                // $mail->addCC('his-her-email@gmail.com');
                // $mail->addBCC('his-her-email@gmail.com');

                //Attachments (optional)
                // $mail->addAttachment('/var/tmp/file.tar.gz');            // Add attachments
                // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');   // Optional name

                //Content
                $mail->isHTML(true);                            // Set email format to HTML
                $mail->Subject = $subject;                
                $mail->Body    = $message;                      // message                
                $mail->send();

                $response = response()->json(['status'=>'true','message'=>'Mail sent successfully'], $this->success_code);                
            } 
        catch (Exception $e) {              
            $response = response()->json(['status'=>'false','message'=>'Mail not sent','exception'=>$e], $this->error_code);
        }
        return $response;
    }

    protected function DateTimeFormat($dateTime){
        return date("d-M-Y h:i A", strtotime($dateTime));
    }

    protected function getDatesFromRange($start, $end, $format = 'Y-m-d') { 
      
            // Declare an empty array 
            $array = array(); 
              
            // Variable that store the date interval 
            // of period 1 day 
            $interval = new DateInterval('P1D'); 
          
            $realEnd = new DateTime($end); 
            $realEnd->add($interval); 
          
            $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
          
         
            // Use loop to store date into array 
            foreach($period as $date) {                  
                $array[] = $date->format($format);  
            } 
          
            // Return the array elements 
            return $array; 
         
    }

    protected function getDayNo($day,$timezone){
        $day = $this->getDayShort($day,$timezone);
        $week_day = [
            "Mon" => 1,
            "Tue" => 2,
            "Wed" => 3,
            "Thu" => 4,
            "Fri" => 5,
            "Sat" => 6,
            "Sun" => 7
        ];

        foreach($week_day as $key => $values){
            if(($day == $key)){
                return $values;
                break;
            }
            
        } 
    }


    protected function getStartAndEndDate($date,$timezone){
    $date = date("Y-m-d", strtotime($this->convertToLocalTime($date, $timezone)));

    $start_date = date("Y-m-d", strtotime($date));
    $start = date("Y-m-d", strtotime($date));
    $start_date = $start_date . " 00:00:00";

    $end_date = $start_date;
    $end_date = strtotime($end_date);
    $end_date = strtotime("+6 day", $end_date);
    $end = date("Y-m-d", $end_date);
    $end_date = date("Y-m-d", $end_date) . " 23:59:59";


    return ['date' => $date,'start_time' => $start_date,'end_time'=> $end_date];
    }
}
