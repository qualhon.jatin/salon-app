<?php

namespace App\Http\Controllers\v1;

use Auth;
use App\User;
use App\Service;
use App\Merchant;
use App\Appointment;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\PushNotificationTrait;
use Illuminate\Support\Facades\Validator;

class MainController extends Controller {
    
    use PushNotificationTrait;

	public function updateNumber(Request $request){    
        if(Auth::user()){                
    		$validator = Validator::make(
                $request->all(),
                [
                    'mobile' => 'required',
                    'new_mobile' => 'required',
                    'user_type' => 'required'
                ],
                [
                    'mobile.required' => 'Mobile number is Required',
                    'mobile_number.digits' => 'Mobile number is not valid',                    
                ]
            );

    		if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }
            
            $mobile_number = $request->mobile;
            $new_mobile_number = $request->new_mobile;
            $user_type = $request->user_type;       
            $login_user  = Auth::user();            

            if(isset($request->status) && $request->status == true){     
                $options = ($user_type != 0) ? ['is_verified'=>1] : ['is_user_verified'=>1];     
                $options['mobile'] = $new_mobile_number;                
                $options['otp'] = '';                

                $data = array(
                                    'title'     => "Contact number update",
                                    'body'      => "Contact number updated successfully",
                                    'user_id'  =>  Auth::id(),
                                ); 
                $token = $login_user->token();

                if($user_type != 0){
                    if(Merchant::where('id',Auth::id())->update($options)){                        
                        
                        $token->revoke();
                        $new_token =  $login_user->createToken('MyApp')->accessToken;
                                    
                        $this->pushNotification($data);
                        $response = response()->json(['message'=>'Mobile number updated successfully','token'=>$new_token], $this->success_code);
                    }
                }
                else{
                    if(User::where('id',Auth::id())->update($options)) {                        
                        
                        $token->revoke();
                        $new_token =  $login_user->createToken('MyApp')->accessToken;
                        
                        $this->pushNotification($data);
                        $response = response()->json(['message'=>'Mobile number updated successfully','token' => $new_token], $this->success_code);      
                    }
                }                
                
                return $response;
            }

            $otp = $this->sendOTP($new_mobile_number);
            // $otp = '0000';
            switch ($user_type) {
            	case 0:
            		# for frontend user
            		$user = User::where(['mobile'=>$mobile_number,'id'=>Auth::id()])->first();
            		if( $user != null){   
                        if(User::where('mobile',$new_mobile_number)->first() == null){
                			if(User::where(['mobile'=>$mobile_number,'id'=>Auth::id()])->update(['otp'=>$otp,'is_user_verified'=>0])){                                
                				$response =  response()->json(['message'=>'OTP sent on your mobile','otp'=>$otp], $this->success_code);	
                			} 
                			else{
                				$response =  response()->json(['message'=>'Unable to send OTP'], $this->error_code);
                			}
                        }
                        else{
                            $response =  response()->json(['message'=>'New mobile number already exsit'], $this->error_code);
                        }
            		}
            		else{
            			$response =  response()->json(['message'=>'You are not authorize to update number. Check your phone number'], $this->error_code);
            		}        		
            		break;
            	case 1:
            		# merchant user
            		$user = Merchant::where(['mobile'=>$mobile_number,'id'=>Auth::id()])->where('merchant_user_type',1)->first();        				
                    
                    if( $user != null){     
                        if(User::where('mobile',$new_mobile_number)->first() == null){   			        			
                			if(Merchant::where(['mobile'=>$mobile_number,'id'=>Auth::id()])->where('merchant_user_type',1)->update(['otp'=>$otp,'is_verified'=>0])){
                				$response =  response()->json(['message'=>'OTP sent on your mobile','otp'=>$otp], $this->success_code);	
                			} 
                			else{
                				$response =  response()->json(['message'=>'Unable to send OTP'], $this->success_code);
                			}
                        }
                        else{
                            $response =  response()->json(['message'=>'New mobile number already exsit'], $this->error_code);
                        }
            		}
            		else{
            			$response =  response()->json(['message'=>'You are not authorize to update number. Check your phone number'], $this->error_code);
            		}
            		break;
            	case 2:
            		# staff users
            		$user = Merchant::where(['mobile'=>$mobile_number,'id'=>Auth::id()])->where('merchant_user_type',2)->first();        				
            		if( $user != null){        
                        if(User::where('mobile',$new_mobile_number)->first() == null){
                			if(Merchant::where(['mobile'=>$mobile_number,'id'=>Auth::id()])->where('merchant_user_type',2)->update(['otp'=>$otp,'is_verified'=>0])){
                				$response =  response()->json(['message'=>'OTP sent on your mobile','otp'=>$otp], $this->success_code);	
                			} 
                			else{
                				$response =  response()->json(['message'=>'Unable to send OTP'], $this->success_code);
                			}
                        }
                        else{
                            $response =  response()->json(['message'=>'New mobile number already exsit'], $this->error_code);
                        }
            		}
            		else{
            			$response =  response()->json(['message'=>'You are not authorize to update number. Check your phone number'], $this->error_code);
            		}
            		break;
            	default:
                	$response =  response()->json(['message'=>'Invalid input'], $this->error_code);        		
            		break;
            }
        }
        else{
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;        
    }

    public function timestampToLocal(Request $request){
        $userTimezone = $myDateTime = '';
        // Asia/Kolkata
        // dd($request->all());
        $timestamp = $request->timestamp;
        $timezone = $request->timezone;

        $response = response()->json(['message' => 'local time','data'=>$this->convertToLocalTime($timestamp, $timezone)], $this->success_code);
        return $response;


        // $userTimezone = new \DateTimeZone($timezone);
        $datetime = $this->convertToDateTime($timestamp);
        // $myDateTime = new \DateTime($datetime);

        // $dt = new \DateTime($datetime);
        // $tz = new \DateTimeZone('Asia/Kolkata'); // or whatever zone you're after

        // $dt->setTimezone($tz);
        // $final_date =  $dt->format('Y-m-d H:i:s');

        $date = $datetime;
        $l10nDate = new \DateTime($date, new \DateTimeZone('UTC'));
        $l10nDate->setTimeZone(new \DateTimeZone($timezone));
        $final_date = $l10nDate->format('Y-m-d H:i:s');

        $data = ['timezone'=>$userTimezone,'timestamp'=>$timestamp,'datetime'=>$datetime,'final_date'=>$final_date];
        $response = response()->json(['message' => 'local timeZone','data'=>$data], $this->success_code);
        return $response;
    }

    public function convertSeconds(){
        $services = Service::all();
        foreach($services as $service){
            if($service->service_buffer_time == 300){                    
                if(Service::where('id',$service->id)->update(['service_buffer_time'=>$service->service_buffer_time / 60])) {
                    dump('true');
                }
                else{                    
                    dump('false');
                }
            }
        }
    }

    public function notificationList(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'timezone' => 'required',             
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }
        $id = Auth::id();
        
        $timezone = $request->timezone;        
        $user_type = (strpos($request->route()->uri, 'merchant') !== false) ? 1 : 0;        
        
        $columns = ['id','user_id','message','type','ref_id','user_type','created_at'];
        $notifications = Notification::select($columns)->where(['user_id'=>$id,'user_type'=>$user_type])->get();
        
        if(count($notifications) > 0 ){            
            foreach($notifications as $notification){                
                $notification['notificationTime']= $this->convertToTimestamp($notification['created_at'],$timezone);
                $data = $notification;               
                $notification['appoientment_id'] = $notification['ref_id'];
                $notification['notification_type'] = $notification['type'];


                $notification['appointment_date_time'] = null;
                $notification['appointment_end_time'] = null;
                $notification['services'] =  null;
                $notification['user_name'] = null;
                $notification['staff_name'] = $this->default_detail_image;
                $notification['user_image'] = $this->default_detail_image;;
                $notification['staff_image'] = null;
                $notification['appoientment_status'] = null;
               


                switch ($notification->type) {
                    case 1:               
                            $columns = ['id','merchant_id','staff_id','user_id','appointment_date_time','appointment_status','appointment_id'];
                            $appoientment = Appointment::select($columns)->where('id',$notification->ref_id)->with('detail','userDetail','staffDetail')->first();
                            
                            $data['appointment_date_time'] = $this->convertToTimestamp($appoientment['appointment_date_time'],$timezone);
                            $data['appointment_end_time'] = $appoientment['appointment_date_time'];
                            $services = $appoientment['detail']->toArray();
                            $service_list = array_map(function($services){                                              
                                        return $services['name'];
                                    },$services);

                            $end_time = 0;
                            foreach ($services as $service) {
                                $end_time += $service['service_duration'];
                            }                           
                
                            $data['appoientment_status'] = $appoientment['appointment_status'];
                            $data['appoientment'] = $appoientment;
                            $notification['appointment_end_time'] = $appoientment['appointment_end_time'] + $this->minsToMiliseconds($end_time);
                            $notification['services'] =  implode(", " ,$service_list);
                            $notification['user_name'] = $appoientment['userDetail']['name'];
                            $notification['staff_name'] = $appoientment['staffDetail']['merchant_name'];
                            $notification['user_image'] = ($appoientment['userDetail']['user_image'] != null ) ? $appoientment['userDetail']['user_image'] : $this->default_profile_image;
                            $notification['staff_image'] = ($appoientment['staffDetail']['merchant_image'] != null) ? $appoientment['staffDetail']['merchant_image'] : $this->default_profile_image;
                            $notification['appoientment_status'] = $appoientment['appointment_status'];
                            $notification['appoientment'] = $appoientment;
                            
                            $unsetVars = ['detail','staff_id','merchant_id','user_id','appointment_id','userDetail','staffDetail'];
                            foreach($unsetVars as $key){
                                unset($appoientment[$key]);
                            }                            
                        break;
                    case 2: 
                        
                    break;
                    default:
                        # code...
                        break;
                }    

                $unsetVars = ['appoientment','type','ref_id','user_type','user_id','created_at'];
                foreach($unsetVars as $key){
                    unset($data[$key]);
                    unset($notification[$key]);
                }
            }
            $response = response()->json(['message'=>'Notification List', 'data'=>$notifications], $this->success_code);            
        }
        else{
            $response = response()->json(['message'=>'No notifications found', 'data'=>$notifications], $this->success_code);   
        }
        return $response;
    }

    public function notificationRead(Request $request){
        if(Notification::where('id',$request->id)->update(['is_read'=>1])) {
            $response = response()->json(['message'=>'Notification read by user'], $this->success_code);
        }
        else{
            $response = response()->json(['message'=>'Something went wrong'], $this->error_code);      
        }
        return $response;
    }
    
    public function notificationDelete(){
        if(Notification::where('user_id',Auth::id())->delete()) {
            $response = response()->json(['message'=>'Notification delete successfully'], $this->success_code);
        }
        else{
            $response = response()->json(['message'=>'Something went wrong'], $this->error_code);      
        }
        return $response;
    }

    public function notificationCron(){
        
        $appointments = Appointment::where('appointment_status',1)->where('timezone','!=','')->get()->toArray();        
        foreach ($appointments as $appointment) {           
            $appointmentDatetime = $appointment['appointment_date_time'];
            $currentDatetime = $this->getCurrentTime($appointment['timezone']);
            if($appointmentDatetime > $currentDatetime){    
                
                $seconds = strtotime($appointmentDatetime) - strtotime($currentDatetime);
                $mintues = $seconds / 60; // get mintues
                    
                if(round($mintues,2) <= 60 && round($mintues,2) >= 50){
                    $data_list =[
                        [
                            'title'     => "Appointment Reminder",
                            'body'      => "You have an appointment after 1 hour",
                            'user_id'   =>  $appointment['user_id'],
                            'user_type' => 0
                        ],
                        [
                            'title'     => "Appointment Reminder",
                            'body'      => "You have an appointment after 1 hour",
                            'user_id'   =>  $appointment['merchant_id'],
                            'user_type' => 1
                        ],
                        [
                            'title'     => "Appointment Reminder",
                            'body'      => "You have an appointment after 1 hour",
                            'user_id'   =>  $appointment['staff_id'],
                            'user_type' => 2
                        ]
                    ];                          

                    foreach($data_list as $data){
                        $this->pushNotification($data,1,$appointment['id']);
                    }
                \Log::info("Appointment notification sent");                
                }
                \Log::info("Appointment id ".$appointment['id']);
                \Log::info("Appointment time left ".round($mintues,2));
            }
            \Log::info("cron running");
        }        
    }

    public function merchantnotificationList(Request $request){

        $validator = Validator::make(
            $request->all(),
            [
                'timezone' => 'required',             
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }
        $id = Auth::id();
        // dd($id);
        $timezone = $request->timezone;
        $current_date_time = $this->getCurrentTime($timezone);
        $current_date_time = date("Y-m-d",strtotime($current_date_time));
        
        $end_date = $current_date_time. " 23:59:59";
        
        $start_date = $end_date;
		$start_date = strtotime($start_date);
		$start_date = strtotime("-7 day", $start_date);
		$start_date =  date("Y-m-d", $start_date)." 00:00:00";

        $user_type = (strpos($request->route()->uri, 'merchant') !== false) ? 2 : 1;        
        $columns = ['id','user_id','message','type','ref_id','user_type','created_at'];
        $notifications = Notification::select($columns)
                                    ->where(['user_id'=>$id,'user_type'=>$user_type])
                                    ->whereBetween('created_at',[$start_date,$end_date])
                                    ->get();
        // echo json_encode($notifications);die();
        if(count($notifications) > 0 ){            
            foreach($notifications as $notification){                
                $notification['notificationTime']= $this->convertToTimestamp($notification['created_at'],$timezone);
                $data = $notification;               
                $notification['appoientment_id'] = $notification['ref_id'];
                $notification['notification_type'] = $notification['type'];


                $notification['appointment_date_time'] = null;
                $notification['appointment_end_time'] = null;
                $notification['services'] =  null;
                $notification['user_name'] = null;
                $notification['staff_name'] = null;
                $notification['user_image'] = null;
                $notification['staff_image'] = null;
                $notification['appoientment_status'] = null;
               


                switch ($notification->type) {
                    case 1:               
                            $columns = ['id','merchant_id','staff_id','user_id','appointment_date_time','appointment_status','appointment_id'];
                            $appoientment = Appointment::select($columns)->where('id',$notification->ref_id)->with('detail','userDetail','staffDetail')->first();
                            
                            $data['appointment_date_time'] = $this->convertToTimestamp($appoientment['appointment_date_time'],$timezone);
                            $data['appointment_end_time'] = $appoientment['appointment_date_time'];
                            $services = $appoientment['detail']->toArray();
                            $service_list = array_map(function($services){                                              
                                        return $services['name'];
                                    },$services);

                            $end_time = 0;
                            foreach ($services as $service) {
                                $end_time += $service['service_duration'];
                            }                           
                        
                            $data['appoientment_status'] = $appoientment['appointment_status'];
                            $data['appoientment'] = $appoientment;
                            $notification['appointment_end_time'] = $appoientment['appointment_end_time'] + $this->minsToMiliseconds($end_time);
                            $notification['services'] =  implode(", " ,$service_list);
                            $notification['user_name'] = $appoientment['userDetail']['name'];
                            $notification['staff_name'] = $appoientment['staffDetail']['merchant_name'];
                            $notification['user_image'] = ($appoientment['userDetail']['user_image'] != null ) ? $appoientment['userDetail']['user_image'] : $this->default_detail_image;
                            $notification['staff_image'] = ($appoientment['staffDetail']['merchant_image'] != null) ? $appoientment['staffDetail']['merchant_image'] : $this->default_detail_image;
                            $notification['appoientment_status'] = $appoientment['appointment_status'];
                            $notification['appoientment'] = $appoientment;
                            
                            $unsetVars = ['detail','staff_id','merchant_id','user_id','appointment_id','userDetail','staffDetail'];
                            foreach($unsetVars as $key){
                                unset($appoientment[$key]);
                            }                            
                        break;
                    
                    default:
                        # code...
                        break;
                }    

                $unsetVars = ['appoientment','type','ref_id','user_type','user_id','created_at'];
                foreach($unsetVars as $key){
                    unset($data[$key]);
                    unset($notification[$key]);
                }
            }
            $notifications = $notifications->toArray();
         
            $data = array_column($notifications, 'notificationTime');                     
		    array_multisort($data, SORT_DESC, $notifications);
            $response = response()->json(['message'=>'Notification List', 'data'=>$notifications], $this->success_code);            
        }
        else{
            
            $response = response()->json(['message'=>'No notifications found', 'data'=>$notifications], $this->success_code);   
        }
        return $response;
    }    
}

