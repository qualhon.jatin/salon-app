<?php

namespace App\Http\Controllers\v1\Api\Merchant;

use App\Merchant;
use App\MerchantOffer;
use App\MerchantPackage;
use App\PackageService;
use App\MerchantService;
use App\AppointmentDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OffersPackagesController extends Controller {


    /*
    * 	This Api for the merchant to create and update the offer for the single service. If Offer already exist than same API will update the offer
    */
	public function createOffer(Request $req){

		if(Auth::user() ){
			$validator = Validator::make(
	            $req->all(),
	            [
	                'service_id' => 'required',
	                'discount' => 'required',
	                'start_time' => 'required',
	                'end_time' => 'required',
	            ]
	        );

	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
	        }
			$merchant_id = Auth::id();
			$service_id = $req->service_id;
			$discount = $req->discount;
			$max_discount = (isset($req->max_discount)) ? $req->max_discount : 0;
			$max_offer_limit = 0;
			$start_time = $this->convertToDateTime($req->start_time);
			$end_time = $this->convertToDateTime($req->end_time);
			if($end_time <= $start_time ){
				return response()->json(['message' => 'End time should be greater than start time'], $this->error_code);
			}
			$data = [
						'merchant_id' => $merchant_id,
						'service_id' => $service_id,
						'discount' => $discount,
						'max_discount_price' => $max_discount,
						'status'=>1,
						// 'max_offer_limit' => $max_offer_limit,
						'start_time' => $start_time,
						'end_time'=> $end_time,

					];
			$merchant_offer = MerchantOffer::where(['service_id'=>$service_id,'merchant_id'=>$merchant_id])->first();
			if($merchant_offer == null){
				if(MerchantOffer::create($data))
					$response = response()->json(['message' => 'Offer Created Successfully'], $this->success_code);
				else
					$response = response()->json(['message' => 'Something went wrong'], $this->error_code);
			}
			else{
				if(MerchantOffer::where(['service_id'=> $service_id,'merchant_id'=>$merchant_id])->update(['discount' => $discount,'max_discount_price'=>$max_discount,'start_time'=>$start_time,'end_time'=>$end_time]))
					$response = response()->json(['message' => 'Offer Updated Successfully'], $this->success_code);
				else{
					$response = response()->json(['message' => 'You are not authorized to create/update offer!'], $this->unauthenticate_code);
				}
			}
		}
		else{
			$response = response()->json(['message' => 'Please login!'], $this->unauthenticate_code);
		}
		return $response;
	}

	/*
    * 	This Api for the merchant to delete the single offer at a time.
    */
	public function deleteOffer(Request $req){
		if(Auth::user()){
			$validator = Validator::make(
	            $req->all(),
	            [
	                'offer_id' => 'required',
	            ]
	        );

	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
	        }
	        $merchant_id = Auth::id();
	        $offer_id = $req->offer_id;

	        $merchant_offer = MerchantOffer::where(['id'=>$offer_id,'merchant_id'=>$merchant_id])->first();
	        if($merchant_offer != null){
		        if(MerchantOffer::where(['id'=>$offer_id,'merchant_id'=>$merchant_id])->delete()){
		        	$response = response()->json(['message' => 'Offer Deleted Successfully'], $this->success_code);
		        }
		        else{
		        	$response = response()->json(['message' => 'Something went wrong'], $this->error_code);
		        }
		    }
		    else{
		    	$response = response()->json(['message' => 'You are not authorized to delete this offer!'], $this->unauthenticate_code);
		    }
		}
		else{
			$response = response()->json(['message' => 'Please login!'], $this->unauthenticate_code);
		}
		return $response;
	}
	/*
    * 	This Api for the merchant to create and packages along with the services.
    */
	public function createPackage(Request $req){
		if(Auth::user()){

			$validator = Validator::make(
	            $req->all(),
	            [
	                'name' => 'required',
	                'price' => 'required',
	                'service_id' => 'required',
	                'start_time' => 'required',
	                'end_time' => 'required',
	            ]
	        );

	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
	        }

			$merchant_id = Auth::id();
			$service_id = $req->service_id;
			$package_name = $req->name;
			if($req->end_time <= $req->start_time ){
				return response()->json(['message' => 'End time should be greater than start time'], $this->error_code);
			}
			$data = [
						'name' => $package_name,
						'price' => $req->price,
						'status'=>1,
						'merchant_id' => $merchant_id,
						'start_time' => $this->convertToDateTime($req->start_time),
						'end_time'=> $this->convertToDateTime($req->end_time)

					];
			$service_ids = explode(',', $service_id);
			$result = MerchantService::where('merchant_staff_id',$merchant_id)->whereIn('service_id',$service_ids)->get();
			$merchant_package = MerchantPackage::where(['name'=>$package_name,'merchant_id'=>$merchant_id])->first();
			if($merchant_package == null){
				if($id = MerchantPackage::create($data)->id){
					$package_services = [];
					foreach ($service_ids as $s_id) {
						$package_service_data = [
													'package_id' => $id,
													'service_id' => (integer) $s_id
												];
						array_push($package_services,$package_service_data);
					}

					PackageService::insert($package_services);
					$response = response()->json(['message' => 'Package Created Successfully'], $this->success_code);
				}
				else{
					$response = response()->json(['message' => 'Something went wrong'], $this->error_code);
				}
			}
			else{
				$response = response()->json(['message' => 'Package is already exist'], $this->unauthenticate_code);
			}
		}
		else{
			$response = response()->json(['message' => 'Please login!'], $this->unauthenticate_code);
		}
		return $response;
	}

	/*
	* This Api for the merchant to update the existed packages.
	*/
	public function updatePackage(Request $req){
		if(Auth::user()){

			$validator = Validator::make(
	            $req->all(),
	            [
	                'name' => 'required',
	                'price' => 'required',
	                'service_id' => 'required',
	                'start_time' => 'required',
	                'end_time' => 'required',
	                'package_id' => 'required'
	            ]
	        );

	        if ($validator->fails()) {
	            return response()->json(['error' => $validator->errors()], $this->error_code);
	        }

	        $merchant_id = Auth::id();			
			$service_id = $req->service_id;
			$package_name = $req->name;
			$package_id= $req->package_id;
			if($req->end_time <= $req->start_time ){
				return response()->json(['message' => 'End time should be greater than start time'], $this->error_code);
			}
			$data = [
						'name' => $package_name,
						'price' => $req->price,
						'status'=>1,
						'merchant_id' => $merchant_id,
						'start_time' => $this->convertToDateTime($req->start_time),
						'end_time'=> $this->convertToDateTime($req->end_time)

					];
				if(MerchantPackage::where(['id'=>$package_id,'merchant_id'=>$merchant_id])->update($data)){
					$package_services = [];
					foreach (explode(',', $service_id) as $s_id) {
						$package_service_data = [
													'package_id' => $package_id,
													'service_id' => (integer) $s_id
												];
						array_push($package_services,$package_service_data);
					}
					PackageService::where('package_id', $package_id)->delete();
					PackageService::insert($package_services);
					$response = response()->json(['message' => 'Package Updated Successfully'], $this->success_code);
				}
				else{
					$response = response()->json(['message' => 'You are not authorized to update this package!'], $this->unauthenticate_code);
				}			
	    }
	   	else{
			$response = response()->json(['message' => 'Please login!'], $this->unauthenticate_code);
		}
		return $response;
	}

	/*
	* This Api for the merchant to update the existed packages.
	*/
	public function deletePackage(Request $req){
		if(Auth::user()){
			
			$validator = Validator::make(
	            $req->all(),
	            [
	                'package_id' => 'required'
	            ]
	        );

	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
	        }
	        $package_id = $req->package_id;
	        $merchant_id = Auth::id();
	        $merchant_package = MerchantPackage::where(['id'=>$package_id,'merchant_id'=>$merchant_id])->first();
	        if($merchant_package != null){
		        if(MerchantPackage::where(['id'=>$package_id,'merchant_id'=>$merchant_id])->delete()){
		        	$response = response()->json(['message' => 'Package Deleted Successfully'], $this->success_code);
		        }
		        else{
		        	$response = response()->json(['message' => 'Something went wrong'], $this->error_code);
		        }
		    }
		    else{
		    	$response = response()->json(['message' => 'You are not authorized to delete this package!'], $this->unauthenticate_code);
		    }
		}
		else{
			$response = response()->json(['message' => 'Please login!'], $this->unauthenticate_code);
		}
		return $response;

	}
	/*
    * 	This Api for the merchant to get the list of the created offers.
    */
	public function getOffers($timezone, $merchant_id = null){				
		if(Auth::user() || $merchant_id != null){
			// dd(Auth::user());
			$m_id = ($merchant_id == null) ? Auth::id() : $merchant_id;
			$timezone = str_replace("-", "/", $timezone);
			$merchant = Merchant::where('id',$m_id)->select('id')->with(['offers.merchantServices.serviceDetail','offers.merchantServices.category'])->first();

			$list = [];
			foreach ($merchant['offers'] as $offer) {
				$offer['offer_id'] = $offer->id;
				$offer['start_time'] = $this->convertToTimestamp($offer->start_time,$timezone);
				$offer['end_time'] = $this->convertToTimestamp($offer->end_time,$timezone);

				unset($offer->id);
				foreach ($offer->merchantServices as $service) {
					if($offer['service_id'] == $service['service_id']){
						$offer['category_name'] = $service['category']['category_name'];
						$offer['category_id'] = $service['category']['id'];
						$offer['service_name'] = $service->serviceDetail['service_name'];
						$offer['service_price'] = $service['service_price'];
						$offer['service_id'] = $service['service_id'];
						$offer['service_duration'] = $service['service_duration'];
						$offer['service_buffer_time'] = $service['service_buffer_time'];
						$offer['service_category_id'] = $service['service_category_id'];
						$offer['service_image'] = ($service['service_image'] == null) ? $this->default_image : $service['service_image'];
						$selling_price = $this->discountAmount($service['service_price'],$offer['discount']);
						$offer['after_discount_price'] =  (($selling_price) < ($service['service_price'] - $offer['max_discount_price'])) ? ($service['service_price'] - $offer['max_discount_price']) : $selling_price;
					}
				}
				unset($offer->merchantServices);
				array_push($list, $offer);
			}
			// exit;
			$response = response()->json(['message' => 'Merchant Offer List','data'=>$list], $this->success_code);
		}
		else{
			$response = response()->json(['message' => 'Please login!'], $this->unauthenticate_code);
		}

		return  ($merchant_id != null) ? $list : $response;
	}
	public function getOfferList(Request $request){
		$validator = Validator::make(
	            $request->all(),
	            [
	                'timezone' => 'required'
	            ]
	        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], $this->error_code);
        }		
		return $this->getOffers($request->timezone);
	}
	/*
    * 	This Api for the merchant to get the list of the created packages.
    */
	public function getPackages($timezone, $merchant_id = null){
		if(Auth::user() || $merchant_id == null){			
			$m_id = ($merchant_id == null) ? Auth::id() : $merchant_id;					
			$timezone = str_replace("-", "/", $timezone);
			$merchant = Merchant::where('id',$m_id)->select('id')->with(['packages.packageServices.service.serviceDetail','packages.packageServices.service.category'])->first();
			$list = [];
			foreach ($merchant->packages as $package) {
				$package['package_id'] = $package['id'];
				$package['package_name'] = $package['name'];
				$package['package_price'] = $package['price'];
				$package['start_time'] = $this->convertToTimestamp($package['start_time'],$timezone);
				$package['end_time'] = $this->convertToTimestamp($package['end_time'],$timezone);
				$unset_vars = ['id','name','price'];
				foreach($unset_vars as $key){
					unset($package[$key]);
				}
				$services = [];
				foreach ($package->packageServices as $service) {
					$service['service_name'] =  $service['service']->serviceDetail['service_name'];
					$service['service_img'] = ($service['service']['service_img'] != null) ? $service['service']['service_img'] : $this->default_image;

					$service['category_name'] = $service['service']['category']['category_name'];
					$service['category_id'] = $service['service']['category']['id'];

					unset($service['service']);
					unset($service['package_id']);
					array_push($services, $service);
				}
				unset($package->packageServices);
				$package['services'] = $services;
				array_push($list,$package);
			}
			$response = response()->json(['message' => 'Merchant packages List','data'=>$list], $this->success_code);
		}
		else{
			$response = response()->json(['message' => 'Please login!'], $this->unauthenticate_code);
		}
		return  ($merchant_id != null) ? $list : $response;
	}
	public function getPackageList(Request $request){
		$validator = Validator::make(
	            $request->all(),
	            [
	                'timezone' => 'required'
	            ]
	        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], $this->error_code);
        }		
		return $this->getPackages($request->timezone);
	}
	/*
	*	This method will return price the after discount.
	*/
	private function discountAmount($actualPrice,$discount){
		return $actualPrice - ($actualPrice * ($discount / 100));
	}

	public function trendingPackages(Request $request){
		if(Auth::user()){

			$validator = Validator::make($request->all(),
	            [
					'timezone' => 'required',
					'location' => 'required'
	            ]
	        );

	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
			}
			
			$current_lat = explode(',', $request->location)[0];
			$current_lng = explode(',', $request->location)[1];

			$merchants = Merchant::where(['merchant_user_type'=>1,'status'=>1])->with('address')->get()->toArray();
			$current_date = $this->getCurrentTime($request->timezone);
			$current_date = date("Y-m-d",strtotime($current_date));
			// echo $current_date;die();
			$merchant_ids = [];
			foreach($merchants as $merchant){
				if($this->getDistanceBetweenPointsNew($current_lat,$current_lng,$merchant['address']['lat'],$merchant['address']['lng']) <= 20){
						array_push($merchant_ids,$merchant['id']);
				}	
			}		
			// echo json_encode($merchant_ids);die();
			$all_packages_status = (isset($request->all_packages) && ($request->all_packages == "true")) ? true : false;
			
			if(!$all_packages_status){

			$trending_services = AppointmentDetail::leftJoin('appointments', 'appointments.id', '=', 'appointment_details.appointment_id')
								->whereIn('appointments.merchant_id',$merchant_ids)
								->where('appointment_details.package_services','!=',null)	
								->where('appointment_details.package_services','!=','')	
								->where('appointments.appointment_status', 2)
								->selectRaw('count(appointment_details.name) as service_total, appointment_details.name as package_name')	
								->groupBy('appointment_details.name')
								->orderBy('service_total','DESC')
								->limit(4)
								->get();	




			// $trending_package_ids = MerchantPackage										
			// echo json_encode($trending_services);
			// die();
			$trending_ids = [];
			foreach($trending_services as $trending){
				$merchant = MerchantPackage::where('name',$trending['package_name'])
											->where('status',1)
											->whereDate('start_time', '>=', $current_date)
											->whereHas('packageServices')
											->with('detail','packageServices.service.serviceDetail')->get();
				if($merchant != null){
					$merchant['used_count'] = $trending['service_total'];
					array_push($trending_ids,$merchant);							
				}
		
			}
			
			$packages = $trending_ids;
			
			}
			else{
				$packages = MerchantPackage::select('id','merchant_id','name','price','start_time','end_time')
											->whereIn('merchant_id',$merchant_ids)
											->where('status',1)
											->whereDate('start_time', '>=', $current_date)
											->whereHas('packageServices')
											->with('detail','packageServices.service.serviceDetail')
											->get();
												
			}
			
			$timezone = $request->timezone;
			if($packages != null){
				$packages = $packages->toArray();
				$package_list = array_map(function($packages) use ($request){
									$services = $packages['package_services'];
									$service_list = array_map(function($services){
										return $services['service']['service_detail']['service_name'];								
									},$services);
									$packages['services'] = implode(',', $service_list);
									$packages['start_time'] = $this->convertToTimestamp($packages['start_time'],$request->timezone);
									$packages['end_time'] = $this->convertToTimestamp($packages['end_time'],$request->timezone);									
									$packages['merchant_business_name'] = $packages['detail']['merchant_business_name'];									
									$packages['detail_image'] = ($packages['detail']['detail_image'] != null ) ? $packages['detail']['detail_image']  : $this->default_detail_image ;									
									 									
									unset($packages['package_services']);
									unset($packages['detail']);
								return $packages;
				},$packages);
				
				$current = array_column($package_list, 'start_time');                     
				array_multisort($current, SORT_ASC, $package_list);

				$message = ($all_packages_status)	? 'packages list' : 'Trending packages list';
				$response = response()->json(['message' => $message,'data'=>$package_list], $this->success_code);
			}
			else{
				$message = ($all_packages_status)	? 'No packages found' : 'No trending packages found';
				$response = response()->json(['message' => $message,'data'=>[]], $this->success_code);	
			}
		}
		else{
			$response = response()->json(['message' => 'Please login!'], $this->unauthenticate_code);
		}				

		return $response;
	}
}
