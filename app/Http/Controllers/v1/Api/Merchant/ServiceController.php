<?php

namespace App\Http\Controllers\v1\Api\Merchant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use App\Category;
use App\Service;
use App\Merchant;
use App\MerchantStaff;
use App\MerchantService;
use App\MerchantDetail;
// use App\ServiceWorkingHours;
// use App\ServiceWorkingDays;
use App\MerchantsWorkingHour;
use App\ServiceCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;

class ServiceController extends Controller
{
    public $successStatus = 200;

    /*
     *
     * select merchant service while register company screen select_service
     *
     *
    */


    public function getServiceInfo(Request $request)
    {
        if (Auth::guard('merchants')) {
            $category = ServiceCategory::get()->toArray();
            $categ = array();

            foreach ($category as $key => $cat) {
                unset($cat['created_at']);
                unset($cat['updated_at']);
                unset($cat['status']);
                $categ[] = $cat;
            }

            $all_service = array();
            
            foreach ($categ as $key => $cat1) {

                $service = Service::select('id as service_id', 'service_categories_id as category_id', 'service_name', 'service_price', 'service_duration', 'service_buffer_time')
                        ->where('service_categories_id', '=', $cat1['id'])
                        ->get();
                $cat1['services'] = $service;
                $all_service[] = $cat1;
            }
            
            $merchant_services = MerchantService::where('merchant_staff_id',Auth::id())->get()->toArray();
            // echo json_encode($all_service);
            // die();
            $service_ids = array_map(function($merchant_services){
                return $merchant_services['service_id'];
            },$merchant_services);
            
            $final_list = $all_service;
            
            if($request->status){
                $categories = $all_service;

                $final_list = [];
                $cat_ids = [];
                $temp_array = [];
                $tmp_service_arr = [];
                $tmp_services = [];
                
            foreach($categories as $key => $value){
                $tmp_service_arr['id'] = $value['id'];
                $tmp_service_arr['category_name'] = $value['category_name'];
                foreach($value['services'] as $key_val){
                    if(!in_array($key_val['service_id'],$service_ids)){
                            
                            array_push($temp_array,$key_val); 
                        }
                    }
                    $tmp_service_arr['services'] = $temp_array;
                    
                    if(!empty($temp_array)){
                        $tmp_services[] = $tmp_service_arr;
                    }
                    $temp_array = [];
                    
                }
                
           
                
                $final_list = $tmp_services;
          
            
                
            }
              
            return response()->json(['message' => true,'data' => array_values($final_list)]);

        }else {

            return response()->json(['message' => "Merchant doesn't exist, Please login!"], 401);

        }
    }


     /*
     *
     * Add new service by merchant while register company screen add_service
     *
     *
    */

    public function addNewService(Request $request)
    {
        if (Auth::guard('merchants')) {

            $user = Auth::user();
            $id =  Auth::id();
            // dd($id);

            $validator = Validator::make(
                $request->all(),
                [
                    'service_name' => 'required',
                    'service_price' => 'required|integer',
                    'service_duration' => 'required|integer',
                    'service_buffer_time' => 'required|integer',

                ],
                [
                    'service_name.required' => ' Service Name is Required',
                    'service_price.required' => ' Service Price is Required',
                    'service_duration.required' => 'Service Duration is Required',
                    'service_buffer_time.required' => 'Service Buffer Time is Required',
                    'service_price.integer' => ' Please enter a Valid  Price',
                    'service_duration.integer' => ' Please enter a Valid  Duration',
                    'service_buffer_time.integer' => ' Please enter a Valid Buffer Time',
                ]
            );


            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }


            if (!empty($user)) {

                $service = new Service();
                $service->service_categories_id = "13";
                $service->service_name = $request->service_name;
                $service->service_price = $request->service_price;
                $service->service_duration = $request->service_duration;
                $service->service_buffer_time = $request->service_buffer_time;

                $check =  Service::select('*')->where('service_name', '=', $request->service_name)->first();

                if (empty($check)) {

                    $service->save();
                    return response()->json(['message' => $request->service_name." service added successfully"], $this->successStatus);

                }else {

                    return response()->json(['message' => 'Service already exist'], 400);

                }

            }else {

                return response()->json(['message' => 'Service not added. Please login'], 401);

            }
        }else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    }



     /*
     *
     * Added service by merchant (choose by merchant) while register company screen added_services
     *
     *
    */

    public function merchantServices(Request $request)
    {

        if (Auth::guard('merchants')) {

            $user = Auth::user();

            $validator = Validator::make($request->all(),
                [
                    'services' => 'required',
                ],
                [
                    'services.required' => ' Services is required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }

            if (!empty($user)) {

                $id =  Auth::id();

                 DB::table('merchant_services')->where('merchant_staff_id', '=', $id)->delete();

                $categoryJSON = $request->services;


                if($categoryJSON != "[]" ){
                    $categoryArray = json_decode($categoryJSON, true);

                    $name = array();
                    foreach ($categoryArray as $key => $value) {

                        $category_id =  Service::select('service_categories_id')->where('id', '=', $value)->first();

                        $category = $category_id['service_categories_id'];

                        $services_values =  Service::select('service_name', 'service_price', 'service_duration', 'service_buffer_time', 'service_image', 'status')->where('service_categories_id', '=', $category)
                        ->where('id', '=', $value)
                        ->first();

                        array_push($name,$services_values->service_name);

                        $user_type =  Merchant::select('merchant_user_type')->where('id', '=', $id)->first();

                        $merchantservices = new MerchantService();
                        $merchantservices->merchant_staff_id = $id;
                        $merchantservices->service_category_id = $category;
                        $merchantservices->service_id = $value;
                        $merchantservices->service_image = $services_values['service_image'];
                        $merchantservices->service_price = $services_values['service_price'];
                        $merchantservices->service_duration = $services_values['service_duration'];
                        $merchantservices->service_buffer_time = $services_values['service_buffer_time'];

                        $merchantservices->user_type = $user_type['merchant_user_type'];

                        $merchantservices->status = "1";

                        if ($category == NULL) {
                            // $response['message'] = "Category cannot be empty";
                            return response()->json(['message' => 'Category cannot be empty'], 400);


                        }
                        $merchantservices->service_id = $value;

                        if ($value == NULL) {

                            $response['message'] = "Service cannot be empty ";
                            return response()->json($response);

                        }

                        $merchantservices->save();
                        $update = DB::table('merchants')->where('id', $id)->update(['verification_level' => '4']);

                    }
                    // $name = implode(",",$name);
                    $response['message'] = " Service added successfully";
                    return response()->json($response);
                }
                else{
                    return response()->json(['message' => 'Merchant list can not be empty'], 401);
                }

            }

        }else {

            return response()->json(['message' => 'Merchant Not Exist'], 401);

        }
    }


    /*
     *
     *  Merchant working hours while register company screen working_hours
     *  Modified by : Gurpreet singh
        updated on 29-oct-2020
     *
    */

    public function ServiceWorkingTime(Request $request)
    {        
        $validator = Validator::make($request->all(),
                [
                    'timezone' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }
        $days = (gettype($request['day']) == 'string') ? json_decode($request['day'],true) : $request['day'];
        
        $timezone = $request->timezone;
        $working_hours =  array_filter($days,function($day) {             
                                $start_time = $day['start_time'];
                                $end_time = $day['end_time'];
                                $break_start = $day['break_start_time'];
                                $break_end =  $day['break_end_time'];
                                if($start_time < $end_time){
                                    if($break_start > 0 && $break_end > 0){
                                        // if($break_start > $start_time && $break_end < $end_time && $break_end > $break_start){
                                        //     return true;
                                        // }
                                        return true;
                                    }                                    

                                    return true;
                                }
                            });    
        if(count($working_hours) == count($days)){
            $working_hours_list = array_map(function($working_hours) use ($timezone){
                                        $working_hours['merchant_id'] = Auth::id();
                                        $working_hours['status'] = 1;
                                        $working_hours['start_time'] = $this->convertToLocalTime($working_hours['start_time'],$timezone);
                                        $working_hours['end_time'] = $this->convertToLocalTime($working_hours['end_time'],$timezone);
                                        $working_hours['break_start_time'] = (($working_hours['break_start_time'] == 0) || empty($working_hours['break_start_time'] )) ? null : $this->convertToLocalTime($working_hours['break_start_time'],$timezone);
                                        $working_hours['break_end_time'] = (($working_hours['break_end_time'] == 0) || empty($working_hours['break_end_time'] )) ? null : $this->convertToLocalTime($working_hours['break_end_time'],$timezone);
                                        return $working_hours;
                                    }, $working_hours);          
                      $working_hous  = MerchantsWorkingHour::where('merchant_id',Auth::id())
                                                            ->where('status',1)
                                                            ->first();
                        if($working_hous != null){
                            $remove_working_hour = MerchantsWorkingHour::where('merchant_id',Auth::id())->delete();
                            if(MerchantsWorkingHour::insert($working_hours_list)){
                                Merchant::where('id', Auth::id())->update(['verification_level' => '5']);                
                                $response = response()->json(['message' => 'Working hour set successfully'], $this->success_code);
                            }else{
                                $response = response()->json(['message' => 'Some error occured'], $this->error_code);
                            }           
                         }
                        else{
                            if(MerchantsWorkingHour::insert($working_hours_list)){
                                Merchant::where('id', Auth::id())->update(['verification_level' => '5']);                
                                $response = response()->json(['message' => 'Working hour set successfully'], $this->success_code);
                            }
                            else{
                                $response = response()->json(['message' => 'Some error occured'], $this->error_code);
                            }
                   
                        }
        }


        else{
            $response = response()->json(['message' => 'Your start time should be less than your endtime'], $this->error_code);
        }


        return $response;
        /*
        if (Auth::guard('merchants')) {

            $user = Auth::user();

            $validator = Validator::make($request->all(),
                [
                    'day' => 'required',
                ],
                [
                    'day.required' => ' Day is required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }

            if (!empty($user)) {

                $id =  Auth::id();
                $dayJSON = $request->day;

                $dayArray = json_decode($dayJSON, true);

                foreach ($dayArray as $key1 => $value1) {


                    if ($value1["day"] == NULL) {
                        $response['message'] = "Day cannot be empty ";

                        return response()->json($response, 400);
                    }
                    if ($value1["starttime"] == NULL) {
                        $response['message'] = "Start time cannot be empty ";

                        return response()->json($response, 400);
                    }
                    if ($value1["endtime"] == NULL) {
                        $response['message'] = "End time cannot be empty ";

                        return response()->json($response, 400);
                    }
                    if (($value1["starttime"]) > ($value1["endtime"])) {
                        $response['message'] = "Start time is always less than End time ";

                        return response()->json($response, 400);
                    }

                    if (($value1["starttime"]) > ($value1["endtime"])) {
                        $response['message'] = "Start time is always less than End time ";

                        return response()->json($response, 400);
                    }
                    if (($value1["starttime"]) == ($value1["endtime"])) {
                        $response['message'] = "Start time and  End time  cannot be same";

                        return response()->json($response, 400);
                    }
                    if (($value1["starttime"]) == "") {
                        $response['message'] = "Start time  cannot be empty";

                        return response()->json($response, 400);
                    }
                    if (($value1["endtime"]) == "") {
                        $response['message'] = "End time  cannot be empty";

                        return response()->json($response, 400);
                    }
                            //if break start empty or null

                            if($value1["breakstart"] != null || $value1["breakstart"] != ""){

                                if(($value1["breakstart"])!=="0")
                                {
                                    if (($value1["starttime"]) > ($value1["breakstart"])) {
                                        $response['message'] = "BreakStart time is always Greater than Start time ";
                                        return response()->json($response, 400);
                                    }
                                }
                            }
                            else{
                                $value1["breakstart"] = null;
                            }


                            //if breakend is empty
                            if($value1["breakend"] != null || $value1["breakend"] != ""){
                                if(($value1["breakend"])!=="0")
                                {
                                    if (($value1["breakstart"]) > ($value1["breakend"])) {
                                        $response['message'] = "Break end is always Greater than Start break ";
                                        return response()->json($response, 400);
                                    }
                                }
                            }
                            else{
                                $value1["breakend"] = null;
                            }

                    //$checkMerchantsWorkingHour =  MerchantsWorkingHour::where('week_day', '=', $value1["day"])->where('merchant_id', '=',  $id)->get();
                    $checkMerchantsWorkingHour =  MerchantsWorkingHour::where('week_day', '=', $value1["day"])->where('merchant_id', '=',  $id)->count();

                    $breakstart = null;
                    $breakend = null;
                      //if breakstart and breakend time is empty or null
                    if($value1["breakstart"] == "0" || $value1["breakstart"] == null){
                        // $breakstart  = null;
                    }
                    else{
                        $breakstart = date("Y-m-d h:i:s ", ($value1["breakstart"])/1000);
                    }

                    if($value1["breakend"] == "0" || $value1["breakstart"] == null){
                        // $breakend  = null;

                    }
                    else{
                        $breakend = date("Y-m-d  h:i:s ", ($value1["breakend"])/1000);
                    }

                    if ($checkMerchantsWorkingHour == Null) {

                        $service_working = new MerchantsWorkingHour();

                        $start_time = date("Y-m-d h:i:s ", ($value1["starttime"])/1000);
                        $endtime = date("Y-m-d h:i:s ", ($value1["endtime"])/1000);

                        // $breakstart = date("Y-m-d  h:i:s ", ($value1["breakstart"])/1000);
                        // $breakend = date("Y-m-d  h:i:s ", ($value1["breakend"])/1000);

                        $service_working->merchant_id = $id;
                        $service_working->week_day = $value1["day"];
                        $service_working->start_time = $start_time;
                        $service_working->end_time = $endtime;
                        $service_working->break_start_time = $breakstart;
                        $service_working->break_end_time = $breakend;
                        $service_working->status = "1";

                        if (($value1["starttime"]) < ($value1["endtime"])) {

                            $service_working->save();
                            $update = DB::table('merchants')->where('id', $id)->update(['verification_level' => '5']);

                        }
                    }

                    if (!empty($checkMerchantsWorkingHour)) {

                        $dayresult = MerchantsWorkingHour::select('week_day')->where('merchant_id', $id)->get();

                        foreach($dayresult as $dayresults){
                            $resday = $dayresults->week_day;
                            MerchantsWorkingHour::where('merchant_id', '=', $id)->where('week_day', '=', $resday)->delete();

                        }

                    //if breakstart and breakend time is empty or null

                    if($value1["breakstart"] == "0" || $value1["breakstart"] == null){
                        // $breakstart  = null;
                    }
                    else{
                        $breakstart = date("Y-m-d h:i:s ", ($value1["breakstart"])/1000);
                    }

                    if($value1["breakend"] == "0" || $value1["breakstart"] == null){
                        // $breakend  = null;

                    }
                    else{
                        $breakend = date("Y-m-d  h:i:s ", ($value1["breakend"])/1000);
                    }


                        $start_time = date("Y-m-d  h:i:s ", ($value1["starttime"])/1000);
                        $endtime = date("Y-m-d  h:i:s ", ($value1["endtime"])/1000);


                        // $breakstart = date("Y-m-d  h:i:s ", ($value1["breakstart"])/1000);
                        // $breakend = date("Y-m-d  h:i:s ", ($value1["breakend"])/1000);

                        $service_working = new MerchantsWorkingHour();
                        $service_working->merchant_id = $id;
                        $service_working->week_day = $value1["day"];
                        $service_working->start_time = $start_time;
                        $service_working->end_time = $endtime;
                        $service_working->break_start_time = $breakstart;
                        $service_working->break_end_time = $breakend;
                        $service_working->status = "1";

                        if ($value1["starttime"] < $value1["endtime"]) {

                            $service_working->save();
                            $update = DB::table('merchants')->where('id', $id)->update(['verification_level' => '5']);
                        }
                    }
                }
                return response()->json(['message' => " Service working hours added successfully"], $this->successStatus);

            }else{

                return response()->json(['message' => " Service working hours not added, Please login!"], 401);

            }
        }else{

            return response()->json(['message' => " Please Login!"], 401);

        }
        */
    }


    /*
     *
     * list of services given by merchant
     *
     *
    */

    public function selectMerchantService(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            if (!empty($user)) {

                $id =  Auth::id();
                
                // dd($id);
                //$category = MerchantService::select('id','service_category_id')->where('merchant_staff_id', '=', $id)->where('user_type', '=', 1)->groupBy('service_category_id')->get();
                $category = MerchantService::join('services', 'services.id', '=', 'merchant_services.service_id' )
                            ->join('service_categories', 'service_categories.id', '=', 'merchant_services.service_category_id')
                            ->select('service_category_id as id','category_name')
                            ->where('merchant_staff_id', '=', $id)
                            ->where('user_type',1)
                            ->get();

                /* $categ = array();

                foreach ($category as $key => $cat) {


                    $categor = ServiceCategory::select('category_name')->where('service_categories.id', '=', $cat['service_category_id'])->get()->first();
                    $name = $categor->category_name;
                    $cat['category_name'] = $name;
                    $categ[] = $cat;
                }
 */

                $all_service = array();
                foreach ($category as $cat1) {

                    $services = DB::table('merchant_services')
                                ->select('merchant_services.service_id','merchant_services.service_category_id as category_id', 'services.service_name','services.service_image')
                                ->join('services', 'merchant_services.service_id', '=', 'services.id')
                                ->where('merchant_services.service_category_id', '=', $cat1['id'])
                                ->where('merchant_services.merchant_staff_id', '=', $id)
                                ->where('user_type','=',1)
                                ->get()->toArray();


                    $service_list = array();
                    foreach($services as $service){
                        if($service->service_image == null || empty($service->service_image)){
                            $service->service_image = $this->default_cover_image;
                        }
                        array_push($service_list,$service);
                    }


                    $cat1['service'] = $service_list;

                    $all_service[] = $cat1;

                    
                }
                return response()->json(['message' => true,'data' => array_values(array_unique($all_service)) ]);
            }else{
                return response()->json(['message' => 'User not found! Please login']);
            }
        }
    }

    public function getStaffService(Request $request){

        $id = Auth::id();
        
        // dd($id);
        $validator = Validator::make($request->all(),
            [
                'staff_id' => 'required',
            ],
            [
                'staff_id.required' => 'Staff Id is required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $staff_id = $request->staff_id;

        //get service by merchant
        $merchant = Merchant::select('id')->where('id',$id)
    				->whereHas('merchantServices.serviceDetail')
    				->with('merchantServices.serviceDetail')
                    ->first();
                    
             
            
        if(!empty($merchant)){
            $list = [];
            $service_status = array();
            $merchant = $merchant->toArray();
              
            //get service by staff
                $staff = MerchantStaff::where('staff_id',$staff_id)
                ->where('merchant_id',$id)
                ->with('staffServices')
                ->first();
                
                if($staff == null){
                    return response()->json(['message' => "You are not authorized"], $this->error_code);
                }
                else{
                    if(!empty($staff)){
                        $staff_service_id = array();
                        $service_match = array();
                        $lists = [];
                        foreach($staff['staffServices'] as $value){
                            $staff_service_id[] = $value->service_id;
                            $staff_id =  $value->merchant_staff_id;
                        }
    
                      
                        // match service with staff service 
    
                        foreach($merchant['merchant_services'] as $staff_service_data){
                            
                            if(in_array($staff_service_data['service_id'],$staff_service_id)){
                                $staff_service_data['isChecked'] = true;
                                $staff_service_data['merchant_staff_id'] = $staff_id;
                            }
                            else{
                                $staff_service_data['isChecked'] = false;
                                
                            }
                          
                            $service_status[] = $staff_service_data;
                            
                        }
                       
                        
                        $cat_list = [];
                        foreach ($service_status as $service) {
                            $cat_id = $service['service_detail']['service_categories_id'];
                            if(!in_array($cat_id,$cat_list)){
                                array_push($cat_list,$cat_id);
                            }
                            
                        }
                        
                        $categories = ServiceCategory::whereIn('id',$cat_list)->get()->toArray();
                       
                        foreach($categories as $category){
                            $data = [
                                // 'id'            => $category['id'],
                                'category_name' =>$category['category_name'],
                                'services'       =>[]
                            ];
                            
                            foreach($service_status as $service){
                                if($service['service_detail']['service_categories_id'] == $category['id']){
                                    // print_r($service);
                                    
                                    
                                    $service['category_id'] =  $category['id'];
                                    $service['service_name'] =  $service['service_detail']['service_name'];
                                    unset($service['service_detail']);
                                    unset($service['created_at']);
                                    unset($service['updated_at']);
                                    unset($service['service_category_id']);
                                    unset($service['status']);
                                    unset($service['id']);
                                    unset($service['merchant_staff_id']);
                                    unset($service['user_type']);
                                    unset($service['service_image']);
                                    // $service['service_image'] = ($service['service_image'] == null) ? $this->default_cover_image : $service['service_image'];
                                    
                                    array_push($data['services'],$service);
                                }
                               
                            }   
                                array_push($lists,$data);
                        }
                        return response()->json(['message' => "Merchant staff service list","data"=>$lists],$this->success_code);
                    }
                    else{
                        foreach($merchant['merchant_services'] as $staff_service_data){
                                $staff_service_data['isChecked'] = false;
                                
                            }
                          
                            $service_status[] = $staff_service_data;

                            $cat_list = [];
                            foreach ($service_status as $service) {
                                $cat_id = $service['service_detail']['service_categories_id'];
                                if(!in_array($cat_id,$cat_list)){
                                    array_push($cat_list,$cat_id);
                                }
                                
                            }
                            
                            $categories = ServiceCategory::whereIn('id',$cat_list)->get()->toArray();
                           
                            foreach($categories as $category){
                                $data = [
                                    // 'id'=> $category['id'],
                                    'category_name'=>$category['category_name'],
                                    'services'  =>[]
                                ];
                                
                                foreach($service_status as $service){
                                    if($service['service_detail']['service_categories_id'] == $category['id']){
                                        $service['category_id'] =  $category['id'];
                                        $service['service_name'] =  $service['service_detail']['service_name'];

                                        unset($service['service_detail']);
                                        unset($service['created_at']);
                                        unset($service['updated_at']);
                                        unset($service['service_category_id']);
                                        unset($service['status']);
                                        unset($service['id']);
                                        unset($service['merchant_staff_id']);
                                        unset($service['user_type']);
                                        unset($service['service_image']);
                                        // $service['service_image'] = ($service['service_image'] == null) ? $this->default_cover_image : $service['service_image'];
                                        array_push($data['services'],$service);
                                    }
                                    array_push($lists,$data);
                                }
                            }    
                                    
                        return response()->json(['message' => "Merchant staff service list","data"=>$lists],$this->success_code);
                    }
                
                }    

            }
            else{
                return response()->json(['message' => "No service Available"],$this->unauthenticate_code);
            }

    }
    /*
     *
     * list of services given by staff for a merchant while adding appointment screen select_service
     *
     *
    */


//     public function  getStaffService(Request $request)
            //     {
            //         if (Auth::guard('merchants')) {

            //             $validator = Validator::make($request->all(),
            //                     [
            //                         'staff_id' => 'required',
            //                     ],
            //                     [
            //                         'staff_id.required' => ' Staff Id  is Required',
            //                     ]
            //                 );

            //             if ($validator->fails()) {
            //                 return response()->json(['message' => $validator->errors()], 401);
            //             }

            //             $user = Auth::user();

            //             if (!empty($user)) {

            //                 $id =  Auth::id();

            //                 $staff = new MerchantService();
            //                 $staff->staff_id = $request->staff_id;

            //                 // getting list of services category provided by merchant

            //                 $merchantCategory = MerchantService::where('merchant_staff_id', Auth::id())->where('user_type', 1)->select('merchant_services.id','merchant_services.service_category_id')->get();

            //                 // return response()->json(['message' => true,'data' =>$merchantCategory]);
            //                 // die();

            //                 $merchantCategoryName = array();

            //                 foreach ($merchantCategory as $key => $cat) {
            //                    $category = ServiceCategory::select('category_name')->where('service_categories.id', $cat['service_category_id'])->first();
            //                     $categoryName = $category->category_name;

            //                     $cat['category_name'] = $categoryName;

            //                     $merchantCategoryName[] = $cat;
            //                 }
            //                 // return response()->json(['message' => true,'data' =>$merchantCategoryName]);
            //                 // die();
            //                 $all_service1 = array();

            //                 foreach ($merchantCategoryName as $key => $cat1) {
            //                     $service = DB::table('merchant_services')
            //                                 ->select('services.id', 'services.service_name', 'services.service_price', 'services.service_duration', 'services.service_buffer_time', 'services.service_image')
            //                                 ->join('services', 'merchant_services.service_id', '=', 'services.id')
            //                                 ->where('merchant_services.service_category_id',  $cat1['service_category_id'])
            //                                 ->where('merchant_services.merchant_staff_id', $id)
            //                                 ->where('merchant_services.id', $cat1['id'])
            //                                 ->get();


            //                     // foreach($service as  $value_s){


            //                     //     $cat1['service'] = $value_s;
            //                     //     $all_service1[] = $cat1;
            //                     // }

            //                     $cat1['service'] = $service;
            //                     $all_service1[] = $cat1;





            //                 }

            //                 // return response()->json(['message' => true,'data' =>$all_service1]);
            //                 // die();
            //                 //getting list of services category provided by Staff

            //                 $category = MerchantService::where('merchant_staff_id', $request->staff_id)->where('user_type', 2)->select('merchant_services.id','merchant_services.service_category_id')->get();


            //                 $categ = array();

            //                 foreach ($category as $key => $cat) {

            //                     $categor = ServiceCategory::select('category_name')->where('service_categories.id', '=', $cat['service_category_id'])->get()->->first();
            //                     $name = $categor->category_name;
            //                     $cat['category_name'] = $name;
            //                     $categ[] = $cat;
            //                 }

            //                 $all_service = array();
            //                 $cat_id = array();
            //                 foreach ($categ as $key => $cat1) {
            //                 //     dd($cat1);
            //                 //    if(!array_key_exists($cat1['service_category_id'])){
            //                 //         array_push($cat_id,$cat1['service_category_id']);
            //                 //    }
            //                 //    else{

            //                 //    }

            //                 //    dd($cat_id);
            //                     $service = DB::table('merchant_services')
            //                                 ->select('services.id', 'services.service_name', 'services.service_price', 'services.service_duration', 'services.service_buffer_time', 'services.service_image')
            //                                 ->join('services', 'merchant_services.service_id', '=', 'services.id')
            //                                 ->where('merchant_services.service_category_id', '=', $cat1['service_category_id'])
            //                                 ->where('merchant_services.merchant_staff_id', '=', $staff->staff_id)
            //                                 ->where('merchant_services.id','=',$cat1['id'])
            //                                 ->groupBy('services.id')
            //                                 ->get();

            //                     $cat1['service'] = $service;
            //                     $all_service[] = $cat1;
            //                 }

            //                 return response()->json(['message' => true,'data' =>$all_service1]);


                    // //                 $result = array();
                    // //                 $result[]= array_intersect_key($all_service1,$all_service);
                    // // // print_r($result);
                    // // return response()->json(['message' => true,'data' => $result]);
                    // /*
                    //                 $final = array();
                    //                 // $result = array();


                    //                 foreach ($all_service1 as $key => $merchant)
                    //                 {

                    //                     $temp1 = $merchant['service'];

                    //                     $combine = array();
                    //                     foreach ($temp1 as $key => $merchantservices)
                    //                     {
                    //                         $merchantservice_id = $merchantservices->id;

                    //                         foreach ($all_service as $key => $staff)
                    //                         {

                    //                             $temp3 = $staff['service'];

                    //                             foreach ($temp3 as $key => $staffservices)
                    //                             {

                    //                                 $staffservice_id = $staffservices->id;
                    //                                 if($staffservice_id == $merchantservice_id)
                    //                                 {
                    //                                   $merchantservices->status=true;
                    //                                   $combine = $merchant;
                    //                                   $final[] = $combine;
                    //                                   break;
                    //                                 }
                    //                             //   else
                    //                             //   {

                    //                             //         $combine = $merchant;
                    //                             //         $merchantservices->status="unselected";
                    //                             //         $allselected = $combine;
                    //                             //         print_r($allselected);
                    //                             //         break;
                    //                             //     // print_r($combine);
                    //                             //     // die();
                    //                             //   }
                    //                             }
                    //                         }

                    //                         // print_r($combine);

                    //                         // echo"after";
                    //                         // $resultservice = array();
                    //                         $result = array_diff($all_service1,$final);
                    //                         // $unselected = $result['service'];
                    //                         // print_r($final);
                    //                         //     die();
                    //                         foreach ($result as $key => $unselected)
                    //                         {

                    //                             print_r($result);
                    //                                 die();
                    //                             foreach ($unselected as $key => $unselectedservices)
                    //                             {

                    //                                 $unselectedservices->status=false;

                    //                                 // print_r($unselectedservices);
                    //                                 // print_r($unselectedservices);
                    //                                 // die();
                    //                             }
                    //                         }

                    //                         $final[]=$result;
                    //                         // print_r($result);
                    //                         // die();
                    //                         // $resultservice = $result['service'];
                    //                         // $final[] = array_merge($result,$combine);

                    //                         // print_r($result);

                    //                         // $combine[] = $result;
                    //                         // print_r($combine);
                    //                         // die();
                    //                         // foreach ($resultservice as $key => $unselectedservices)
                    //                         // {
                    //                         //     $unselectedservice_id = $unselectedservices->id;

                    //                         //     $combine = $merchant;
                    //                         //     $merchantservices->status="unselected";
                    //                         //     $allselected = $combine;
                    //                         //     print_r($allselected);
                    //                         //     die();
                    //                         // }
                    //                     }
                    //                 }




                    // //     //    die('dg');



                    //                 return response()->json([
                    //                     'message' => 'success',
                    //                     'Merchant Service data' => $finalfalse,
                    //                     // 'Staff Service data' => $all_service
                    //                 ]); */


                    //             } else {
                    //                 $response['message'] = "Merchant cannot be Exist ";

                    //                 return response()->json($response, 401);
                    //             }
        //         }
//     }



/*
        Function name : getWorkingHour
        Function Purpose : Get working hour of merchant
        Created by : Sourabh
        Created on : 20 Oct 2020
    */
  //get working hour
  public function getWorkingHour(Request $request){

    if (Auth::guard('merchants'))
    {

        $validator = Validator::make($request->all(),
            [
                'timezone' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }
        $user = Auth::user();

        if (!empty($user)){

            $id =  Auth::id();

            $working_hour = Merchant::select(['id'])->where('id',Auth::id())->with('workingHours')->first();

            $hours = $working_hour->workingHours->toArray();

            if(!empty($hours)){
                $working_hours = array_map(function($hours) use ($request){
                    $hours['start_time'] = $this->convertToTimestamp($hours['start_time'],$request->timezone);
                    $hours['end_time'] = $this->convertToTimestamp($hours['end_time'],$request->timezone);
                    $hours['break_start_time'] = $this->convertToTimestamp($hours['break_start_time'],$request->timezone);
                    $hours['break_end_time'] = $this->convertToTimestamp($hours['break_end_time'],$request->timezone);
                    unset($hours['merchant_id']);
                    return $hours;
                },$hours);
                $data = [
                    'merchant_id' => $working_hour->id,
                    'working_hours' => $working_hours
                ];
                return response()->json($data,200);
            }
            else{
                return response()->json(['message' => 'No working hours found'], 401);
            }
        }
        else{
            return response()->json(['message' => 'Please login'], 401);
       }

    }
    else {
        return response()->json(['message' => "Merchant doesn't exist"], 401);
    }

}

    /*
        Function 0name : updateWorkingHour
        Function Purpose : Update working hour of merchant
        Created by : Sourabh       
        Created on : 21 Oct 2020   

        Modified by : Gurpreet singh
        updated on 29-oct-2020     
    */

//update working hour

public function updateWorkingHour(Request $request) {
    $validator = Validator::make($request->all(),
                [
                    'timezone' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }
    $days = (gettype($request['day']) == 'string') ? json_decode($request['day'],true) : $request['day'];
        if($days != null){
            $timezone = $request->timezone;
            $message = '';
            $working_hours =  array_filter($days,function($day){            
                                    $start_time = $day['start_time'];
                                    $end_time = $day['end_time'];
                                    $break_start = $day['break_start_time'];
                                    $break_end = $day['break_end_time'];
                                    if($start_time < $end_time){
                                        // if($break_start > 0 && $break_end > 0){
                                        //     // if($break_start > $start_time && $break_end < $end_time && $break_end > $break_start){
                                        //     //     return true;
                                        //     // }
                                            
                                        // }                                    
                                        return true;
                                    }
                                });    
            if(count($working_hours) == count($days)){
                $working_hours_list = array_map(function($working_hours) use ($timezone){
                                            $working_hours['merchant_id'] = Auth::id();
                                            $working_hours['status'] = 1;
                                            $working_hours['start_time'] = $this->convertToLocalTime($working_hours['start_time'],$timezone);
                                            $working_hours['end_time'] = $this->convertToLocalTime($working_hours['end_time'],$timezone);
                                            $working_hours['break_start_time'] = (($working_hours['break_start_time'] == 0) || empty($working_hours['break_start_time'] )) ? null : $this->convertToLocalTime($working_hours['break_start_time'],$timezone);
                                            $working_hours['break_end_time'] = (($working_hours['break_end_time'] == 0) || empty($working_hours['break_end_time'] )) ? null : $this->convertToLocalTime($working_hours['break_end_time'],$timezone);
                                            return $working_hours;
                                        }, $working_hours);
                                        
                                        
                if(MerchantsWorkingHour::where('merchant_id',Auth::id())->delete()){
                    if(MerchantsWorkingHour::insert($working_hours_list)){
                        $response = response()->json(['message' => 'Merchant working hour update successfully'], $this->success_code);   
                    }
                    else{
                        $response = response()->json(['message' => 'Soemthing went wrong while inserting data'], $this->error_code);
                    }
                }
                else{
                    $response = response()->json(['message' => 'Soemthing went wrong while deleting data'], $this->error_code);
                }
            }
            else{
                $response = response()->json(['message' => 'Your start time should be less than your endtime'], $this->error_code);
            }
        }
        else{
            $response = response()->json(['message' => 'Somethign went wrong'], $this->error_code);
        }


        return $response;

    /*
        if (Auth::guard('merchants')) {

            $user = Auth::user();
            
            $validator = Validator::make($request->all(),
                [
                    'day' => 'required',
                ],
                [
                    'day.required' => ' Day is required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }

            if (!empty($user)){

                $id =  Auth::id();

                $working_hour = Merchant::select(['id'])->where('id',$id)->with('workingHours')->first()->toArray();

                if(!empty($working_hour['working_hours'])){
                        // $remove_working_hours = MerchantsWorkingHour::where('merchant_id','=',$id)->delete();
                    // if($remove_working_hours){
                        $dayJSON = $request->day;
                        $dayArray = json_decode($dayJSON, true);
                        // dd($dayArray);
                        foreach ($dayArray as $key1 => $value1) {


                            if ($value1["day"] == NULL) {
                                $response['message'] = "Day cannot be empty ";

                                return response()->json($response, 400);
                            }
                            if ($value1["starttime"] == NULL) {
                                $response['message'] = "Start time cannot be empty ";

                                return response()->json($response, 400);
                            }
                            if ($value1["endtime"] == NULL) {
                                $response['message'] = "End time cannot be empty ";

                                return response()->json($response, 400);
                            }
                            if (($value1["starttime"]) > ($value1["endtime"])) {
                                $response['message'] = "Start time is always less than End time ";

                                return response()->json($response, 400);
                            }

                            if (($value1["starttime"]) > ($value1["endtime"])) {
                                $response['message'] = "Start time is always less than End time ";

                                return response()->json($response, 400);
                            }
                            if (($value1["starttime"]) == ($value1["endtime"])) {
                                $response['message'] = "Start time and  End time  cannot be same";

                                return response()->json($response, 400);
                            }
                            if (($value1["starttime"]) == "") {
                                $response['message'] = "Start time  cannot be empty";

                                return response()->json($response, 400);
                            }
                            if (($value1["endtime"]) == "") {
                                $response['message'] = "End time  cannot be empty";

                                return response()->json($response, 400);
                            }
                                    //if break start empty or null

                                    if($value1["breakstart"] != null || $value1["breakstart"] != ""){

                                        if(($value1["breakstart"]) !== "0")
                                        {
                                            if (($value1["starttime"]) > ($value1["breakstart"])) {
                                                $response['message'] = "Break start time is always Greater than Start time ";
                                                return response()->json($response, 400);
                                            }
                                        }
                                    }
                                    else{

                                        $value1["breakstart"] = null;
                                    }


                                    //if breakend is empty
                                    if($value1["breakend"] != null || $value1["breakend"] != ""){
                                        if(($value1["breakend"]) !== "0")
                                        {
                                            if (($value1["breakstart"]) > ($value1["breakend"])) {
                                                $response['message'] = "Break end is always Greater than Start break ";
                                                return response()->json($response, 400);
                                            }
                                        }
                                    }
                                    else{
                                        $value1["breakend"] = null;
                                    }

                            //$checkMerchantsWorkingHour =  MerchantsWorkingHour::where('week_day', '=', $value1["day"])->where('merchant_id', '=',  $id)->get();
                            $checkMerchantsWorkingHour =  MerchantsWorkingHour::where('week_day', '=', $value1["day"])
                            ->where('merchant_id', '=', $id)->count();

                            $breakstart = null;
                            $breakend  = null;

                              //if breakstart and breakend time is empty or null
                            if($value1["breakstart"] == "0" || $value1["breakstart"] == null || $value1["breakstart"] == ""){
                                // $breakstart  = null;
                            }
                            else{
                                $breakstart = date("Y-m-d h:i:s ", ($value1["breakstart"])/1000);
                            }

                            if($value1["breakend"] == "0" || $value1["breakend"] == null || $value1["breakend"] == ""){
                                // $breakend  = null;

                            }
                            else{
                                $breakend = date("Y-m-d  h:i:s ", ($value1["breakend"])/1000);
                            }

                            if ($checkMerchantsWorkingHour == Null) {

                                $service_working = new MerchantsWorkingHour();

                                $start_time = date("Y-m-d h:i:s ", ($value1["starttime"])/1000);
                                $endtime = date("Y-m-d h:i:s ", ($value1["endtime"])/1000);

                                // $breakstart = date("Y-m-d  h:i:s ", ($value1["breakstart"])/1000);
                                // $breakend = date("Y-m-d  h:i:s ", ($value1["breakend"])/1000);
                                 if($breakstart == Null || $breakend == NULL){
                                    $breakstart = NULL;
                                    $breakend = NULL;
                                 }

                                $service_working->merchant_id = $id;
                                $service_working->week_day = $value1["day"];
                                $service_working->start_time = $start_time;
                                $service_working->end_time = $endtime;
                                $service_working->break_start_time = $breakstart;
                                $service_working->break_end_time = $breakend;
                                $service_working->status = "1";

                                if (($value1["starttime"]) < ($value1["endtime"])) {

                                    $service_working->save();
                                    $update = DB::table('merchants')->where('id', $id)->update(['verification_level' => '5']);

                                }
                            }

                            if (!empty($checkMerchantsWorkingHour)) {

                                $dayresult = MerchantsWorkingHour::select('week_day')->where('merchant_id', $id)->get();

                                foreach($dayresult as $dayresults){
                                    $resday = $dayresults->week_day;
                                    MerchantsWorkingHour::where('merchant_id', '=', $id)->where('week_day', '=', $resday)->delete();

                                }

                            //if breakstart and breakend time is empty or null

                            if($value1["breakstart"] == "0" || $value1["breakstart"] == NULL || $value1["breakstart"] == ""){
                                // $breakstart  = null;
                            }
                            else{
                                $breakstart = date("Y-m-d h:i:s ", ($value1["breakstart"])/1000);
                            }

                            if($value1["breakend"] == "0" || $value1["breakend"] == NULL || $value1["breakend"] == ""){
                                // $breakend  = null;

                            }
                            else{
                                $breakend = date("Y-m-d  h:i:s ", ($value1["breakend"])/1000);
                            }


                                $start_time = date("Y-m-d  h:i:s ", ($value1["starttime"])/1000);
                                $endtime    = date("Y-m-d  h:i:s ", ($value1["endtime"])/1000);


                                // $breakstart = date("Y-m-d  h:i:s ", ($value1["breakstart"])/1000);
                                // $breakend = date("Y-m-d  h:i:s ", ($value1["breakend"])/1000);

                                $service_working = new MerchantsWorkingHour();
                                $service_working->merchant_id = $id;
                                $service_working->week_day = $value1["day"];
                                $service_working->start_time = $start_time;
                                $service_working->end_time = $endtime;
                                $service_working->break_start_time = $breakstart;
                                $service_working->break_end_time = $breakend;
                                $service_working->status = "1";

                                if ($value1["starttime"] < $value1["endtime"]) {

                                    $service_working->save();

                                    $update = DB::table('merchants_working_hours')
                                    ->where('merchant_id','=', $id)
                                    ->update(['week_day'        => $service_working->week_day,
                                            'start_time'       => $service_working->start_time,
                                            'end_time'         => $service_working->end_time,
                                            'break_start_time' => $service_working->break_start_time,
                                            'break_end_time'   => $service_working->break_end_time,
                                            'status'           => $service_working->status
                                            ]);

                                    // $update = DB::table('merchants')->where('id', $id)->update(['verification_level' => '5']);
                                }
                            }
                        }
                        return response()->json(['message' => " Service working hours update successfully"], $this->successStatus);

                    // }
                }
                else{
                    $set_working_hour = $this->ServiceWorkingTime($request);
                    if($set_working_hour){
                        return response()->json(['message' => " Service working hours saved successfully"], $this->successStatus);
                    }
                    else{

                        return response()->json(['message' => " Service working hours not added, Please login!"], 401);
                    }
                }
            }
            else{
                return response()->json(['message' => 'Please login'], 401);
           }

        }
        else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    */

}








    public function getBusinessProfile()
    {
        if (Auth::guard('merchants'))
        {
            if(Auth::user()){

                $merchantBussinessProfile    =   MerchantDetail::join('merchants', 'merchants.id','=','merchant_details.merchant_id')->where('merchants.id', Auth::user()->id)->select('merchants.id','merchant_details.merchant_service_type','merchant_details.merchant_business_name','merchant_details.merchant_company_name','merchant_details.landline','merchant_details.salon_type','merchant_details.merchant_name','merchants.mobile','merchant_details.merchant_image')->first();
                $response['message'] = "Merchant business details";
                $merchantBussinessProfile['merchant_image'] = ($merchantBussinessProfile['merchant_image'] != null ) ?  $merchantBussinessProfile['merchant_image'] : $this->default_profile_image;
                $response['data'] =  $merchantBussinessProfile;//(!empty($merchantBussinessProfile))? $merchantBussinessProfile : array();
                return response()->json($response,200);

            }
            else{
                return response()->json(['message' => 'Please login'], 401);
            }
        }
        else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    }

    public function editBusinessProfile(Request $request)
    {
        // if (Auth::guard('merchants'))
        // {
            if(Auth::user()){
                

                $validator = Validator::make($request->all(),
                [
                    'user_type' => "required",
                    
                ]
                
                );

                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()],$this->error_code);
                }
                
                if($request->user_type == 1){
                    $validator = Validator::make($request->all(),
                        [
                            'merchant_service_type' => "required|integer|between:1,3'",
                            'merchant_business_name' => "required",
                            'merchant_company_name' => "required",
                            'landline' => 'required|numeric',
                            'salon_type' => "required|integer|between:1,3'",
                            'merchant_name' => 'required',
                        ]
                    
                    );

                    if ($validator->fails()) {
                        return response()->json(['message' => $validator->errors()], $this->error_code);
                    }

                $merchantBussinessProfile    =   MerchantDetail::where('merchant_id', Auth::id())
                ->update([
                            'merchant_service_type'     =>  $request->merchant_service_type ,
                            'merchant_business_name'    =>  $request->merchant_business_name,
                            'merchant_company_name'     =>  $request->merchant_company_name ,
                            'landline'                  =>  (!empty($request->landline)) ? $request->landline : '',
                            'salon_type'                =>  $request->salon_type,
                            'merchant_name'             =>  $request->merchant_name
                        ]);
                    
                    if($merchantBussinessProfile == true){
                        $response['message'] = "Merchant business profile update Successfully";
                        return response()->json($response,200);
                    }
                    else{
                        $response['message'] = "Something went wrong! Please try again";
                        return response()->json($response,$this->error_code);
                    }

                }
                if($request->user_type == 2){
                    $validator = Validator::make($request->all(),
                        [
                        'merchant_name' => 'required',
                        ]
                    
                    );

                    if($validator->fails()) {
                        return response()->json(['message' => $validator->errors()], $this->error_code);
                    }
                    
                    $merchantBussinessProfile = MerchantDetail::where('merchant_id', Auth::id())
                                                            ->update([
                                                                    'merchant_name' => $request->merchant_name
                                                                    ]);
                        if($merchantBussinessProfile == true){
                            $response['message'] = "Staff profile update Successfully";
                            return response()->json($response,$this->success_code);
                        }
                        else{
                            $response['message'] = "Something went wrong! Please try again";
                            return response()->json($response,$this->error_code);
                        }


                }
                
                

                // $merchantBussinessProfile    =   MerchantDetail::where('merchant_id', Auth::user()->id)
                //                                 ->update([
                //                                             'merchant_service_type'     =>  $request->merchant_service_type ,
                //                                             'merchant_business_name'    =>  $request->merchant_business_name,
                //                                             'merchant_company_name'     =>  $request->merchant_company_name ,
                //                                             'landline'                  =>  (!empty($request->landline)) ? $request->landline : '',
                //                                             'salon_type'                =>  $request->salon_type,
                //                                             'merchant_name'             =>  $request->merchant_name
                //                                         ]);

                // if($merchantBussinessProfile == true){
                //     $response['message'] = "Merchant business profile update Successfully";
                //     return response()->json($response,200);
                // }else{
                //     $response['message'] = "Something went wrong! Please try again";
                //     return response()->json($response,$this->error_code);
                // }
            }
            else{
                return response()->json(['message' => 'Please login'], $this->error_code);
            }
        // }
        // else {
        //     return response()->json(['message' => "Merchant doesn't exist"], 401);
        // }
    }



    /*
        Function 0name : getMerchantServicesList
        Function Purpose : Get Merchant Services List
        Created by : Sourabh
        Created on : 23 Oct 2020
    */
     
    public function getMerchantServicesList(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            if (!empty($user)) {

                $id =  Auth::id();

                $category = MerchantService::select('id','service_category_id')->where('merchant_staff_id', '=', $id)->where('user_type', '=', 1)->get();
                dd($category);

                $categ = array();

                foreach ($category as $key => $cat) {


                    $categor = ServiceCategory::select('category_name')->where('service_categories.id', '=', $cat['service_category_id'])->get()->first();
                    $name = $categor->category_name;
                    $cat['category_name'] = $name;
                    $categ[] = $cat;
                }


                $all_service = array();
                foreach ($categ as $key => $cat1) {

                    $service = DB::table('merchant_services')
                        ->select('services.id', 'services.service_name', 'services.service_price', 'services.service_duration', 'services.service_buffer_time', 'services.service_image')
                        ->join('services', 'merchant_services.service_id', '=', 'services.id')
                        ->where('merchant_services.service_category_id', '=', $cat1['service_category_id'])->where('merchant_services.merchant_staff_id', '=', $id)->where('user_type','=',1)->where('merchant_services.id','=',$cat1['id'])
                        ->groupBy('merchant_services.id')
                        ->get();
                    $cat1['service'] = $service;
                    $all_service[] = $cat1;
                }
                return response()->json([
                    'message' => true,
                    'data' => $all_service
                ]);
            }
        }
    }

    /*
        Function 0name : getBussinessServiceList
        Function Purpose : Get Bussiness Service List
        Created by : Sourabh
        Created on : 23 Oct 2020
    */

    public function getBussinessServiceList(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            if (!empty($user)) {

                $id =  Auth::id();
                
 
                 
                $category = MerchantService::where('merchant_staff_id', '=', $id)
                                            ->where('user_type', '=', 1)
                                            ->leftJoin('services','services.id','=','merchant_services.service_id')
                                            ->select('merchant_services.id','merchant_services.service_id','merchant_services.merchant_staff_id','merchant_services.service_category_id','merchant_services.user_type','merchant_services.service_price','merchant_services.service_duration','merchant_services.service_buffer_time','merchant_services.service_image','merchant_services.status','services.service_name')
                                            ->get();
                

            
                $categ = array();

                foreach ($category as $key => $cat) {
                    
                    
                    $cat['service_image'] = ($cat['service_image'] == null ? $this->default_salon_image : $cat['service_image']);
                   
                    
                    unset($cat['created_at']);
                    unset($cat['updated_at']);
                    
                    $categ[] = $cat->toArray();
                }
                
                
                if(!empty($categ)){
                    $response['status'] = true;
                    $response['data'] = $categ;
                    return response()->json($response,200);
                    
                }
                else{
                    $response['message'] = "No service found";
                    return response()->json($response,401);
                }
                
            }
            else{
                return response()->json(['message' => 'Please login'], 401);
            }

        }
        else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    }
    /*
        Function 0name : addEditBussinessServiceList
        Function Purpose : Add/Edit  Bussiness Service List
        Created by : Sourabh
        Created on : 23 Oct 2020
    */

    public function addEditBussinessService(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            if (!empty($user)) {

                $id =  Auth::id();
                
                $validator = Validator::make(
                    $request->all(),
                    [
                        'service_id'          => 'required',
                        'service_category_id' => 'required',
                        'service_price'       => 'required|numeric',
                        'service_duration'    => 'required|integer',
                        'service_buffer_time' => 'required|integer',
    
                    ],
                    [
                        'service_id.required'          => ' Service Id is Required',
                        'service_category_id.required'  => ' Service Categoty Id is Required',
                        'service_price.required'       => ' Service Price is Required',
                        'service_duration.required'    => 'Service Duration is Required',
                        'service_buffer_time.required' => 'Service Buffer Time is Required',
                        'service_price.float    '        => ' Please enter a Valid  Price',
                        'service_duration.integer'     => ' Please enter a Valid  Duration',
                        'service_buffer_time.integer'  => ' Please enter a Valid Buffer Time',
                    ]
                );
    
    
                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], 400);
                }

                
                $category = MerchantService::select('id','service_id','merchant_staff_id as merchant_id','service_category_id as category_id','user_type')
                                            ->where('merchant_staff_id', '=', $id)
                                            ->where('user_type', '=', 1)
                                            ->where('service_id', '=', $request->service_id)
                                            ->where('service_category_id', '=', $request->service_category_id)
                                            ->get();
                
           
                
                $merchant_service = [
                    'service_price'    => $request->service_price,
                    'service_duration' => $request->service_duration,
                    'service_buffer_time' => $request->service_buffer_time
                ];
                
                if(!empty($category->toArray())){
                    // dd($merchant_service);
                    $update_service = DB::table('merchant_services')
                                        ->where('id','=',$category[0]->id)
                                        ->where('merchant_staff_id','=',$id)
                                        ->where('user_type','=',1)
                                        ->where('service_id', '=', $request->service_id)
                                        ->update($merchant_service);
                   
                        $response['message'] = "Merchant service update successfully";
                            return response()->json($response,200);
                     
                }
                else{
               
                    $merchant_service['merchant_staff_id'] = $id;    
                    $merchant_service['service_id'] = $request->service_id;
                    $merchant_service['service_category_id'] = $request->service_category_id;
                    $merchant_service['user_type'] = 1;
                    $merchant_service['status'] = $request->service_id;

                    $add_service = MerchantService::create($merchant_service);
                    
                    if($add_service){
                        $response['message'] = "Merchant service add successfully";
                            return response()->json($response,200);
                    }
                    else{
                        $response['message'] = "Merchant service not add ";
                            return response()->json($response,401);
                    }
                }
                
            }
            else{
                return response()->json(['message' => 'Please login'], 401);
            }

        }
        else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    }

       /*
        Function 0name : deleteBussinessService
        Function Purpose : Delete Bussiness Service List
        Created by : Sourabh
        Created on : 23 Oct 2020
    */

    public function deleteBussinessService(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            if (!empty($user)) {

                $id =  Auth::id();
                // dd($id);
                $validator = Validator::make(
                    $request->all(),
                    [
                        'service_id'          => 'required',
                    ],
                    [
                        'service_id.required'          => ' Service Id is Required',
                    ]
                );
    
    
                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], 400);
                }

                
                $category = MerchantService::select('id','service_id','merchant_staff_id as merchant_id','service_category_id as category_id','user_type')
                                            ->where('merchant_staff_id', '=', $id)
                                            ->where('user_type', '=', 1)
                                            ->where('service_id', '=', $request->service_id)
                                            ->get();
                

                                
                if(!empty($category->toArray())){
                    $delet_service = DB::table('merchant_services')
                                        ->where('merchant_staff_id','=',$id)
                                        ->where('user_type','=',1)
                                        ->where('service_id', '=', $request->service_id)
                                        ->delete();
                     if($delet_service){
                        $response['message'] = "Merchant service delete successfully";
                            return response()->json($response,200);
                     }
                     else{
                        $response['message'] = "Error in delete merchant service";
                        return response()->json($response,401);
                     }
                }
                else{
                    $response['message'] = "Merchant service not found ";
                            return response()->json($response,401);
                    }
                
                
            }
            else{
                return response()->json(['message' => 'Please login'], 401);
            }

        }
        else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    }
    
    



}
