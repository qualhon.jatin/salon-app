<?php

namespace App\Http\Controllers\v1\API;

use Auth;
use Validator;
use App\Requests;
use App\Responses;
use App\User;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PushNotificationTokens;
use App\ProblemReports;
use Mail;

class RequestsController extends Controller {



    /**
    *  @OA\Get(
    *     path="/api/clearNotificationsss",
    *     description="clear all notification  - Bearer xxxx",
    *     security={{"passport": {}}},
    *     @OA\Response(response="200", description="clear all notification"),
    *      @OA\Parameter(
    *            name="id",
    *            in="query",
    *            required=false,
    *            @OA\Schema(
    *                type="string"
    *            )
    *        )
    *    )
    */

    public function clearNotification(Request $request) {
        $userInfo = Auth::user();
        $input = $request->all();
        if(empty($input)) {
            Notification::where('user_id', '=', $userInfo->id)->delete();
            $success['message'] = 'Successfully cleared!';
            return response()->json($success, 200);
        } else {
            if(isset($input['id'])) {
                Notification::where('user_id', '=', $userInfo->id)->where('id', '=', $input['id'])->delete();
                $success['message'] = 'Successfully cleared!';
                return response()->json($success, 200);
            } else {
                $error['message']='somthing went wrong!';
                return response()->json($error, 401);
            }
        }
    }

    /**
    *  @OA\Get(
    *     path="/api/notification",
    *     description="Get all notification  - Bearer xxxx",
    *     security={{"passport": {}}},
    *     @OA\Response(response="200", description="Get all notification"),
    *    )
    */

    public function notification(Request $request) {
        $userInfo = Auth::user();
        $input = $request->all();

        $notification = Notification::where('user_id', '=', $userInfo->id)->get()->sortByDesc('created_at')->toArray();
        $unread_notification = Notification::where('user_id', '=', $userInfo->id)->where('is_read', '=', '0')->get()->sortByDesc('created_at')->toArray();
        $success['data'] = array();
        foreach($notification as $noti) {
            $noti['timestamp'] = strtotime($noti['created_at']);
            $success['data'][] = $noti;
        }
        $success['count'] = count($notification);
        $success['unreadCount'] = count($unread_notification);
        return response()->json($success, 200);
    }

    /**
    *  @OA\Post(
    *     path="/api/readNotification",
    *     description="Read notification - Bearer xxxxx",
    *     security={{"passport": {}}},
    *     @OA\Response(response="200", description="Read notification. ids is one or more, with comma seprated."),
    *      @OA\Parameter(
    *            name="ids",
    *            in="query",
    *            required=true,
    *            @OA\Schema(
    *                type="string"
    *            )
    *        )
    *    )
    */

    public function readNotification(Request $request) {
        $userInfo = Auth::user();
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'ids' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $ids = array();
        if(strpos($input['ids'],",")) {
            $ids = explode(',',$input['ids']);
        } else {
            $ids[] = $input['ids'];
        }
        /* Notification::where('user_id', '=', $userInfo->id)->whereIn('id', $ids)->update([
            'is_read' => '1'
        ]); */
        Notification::where('user_id', '=', $userInfo->id)->update([
            'is_read' => '1'
        ]);
        $success['data'] = $input;
        return response()->json($success, 200);
    }










}
