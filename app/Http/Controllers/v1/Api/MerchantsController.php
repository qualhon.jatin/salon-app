<?php

namespace App\Http\Controllers\v1\Api;

use App\Merchant;
use App\MerchantDetail;
use App\UsersDetail;
use App\Favourity;
use App\MerchantStaff;
use App\ServiceCategory;
use App\MerchantPortfolio;
use App\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\v1\Api\Merchant\OffersPackagesController;
use App\Http\Controllers\v1\Api\Merchant\UserController;


class MerchantsController extends Controller {
    
    private $OffersPackages;
    private $usersController;

    public function __construct(OffersPackagesController $OffersPackagesController /*, UserController $user_controller*/){
        $this->OffersPackages = $OffersPackagesController;        
        // $this->usersController = $user_controller;
    }	
    public function getMerchantDetail(Request $req){      

    	if(Auth::user()){            
            $validator = Validator::make(
                $req->all(),
                [
                    'timezone' => 'required',             
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }

    		$action = $req->action;
    		$merchant_id = $req->merchant_id;
            $staff_id = (isset($req->staff_id) && $req->staff_id != null) ? $req->staff_id : 0;
    		$page = (isset($req->page)) ? $req->page : 1;
    		$limit = (isset($req->limit)) ? $req->limit : 20;
            
            $current_lat = $req->current_lat;
            $current_lng = $req->current_lng;

            switch ($action) {
            	case 'reviews':
                    $reviews = $this->getReviews($merchant_id,$page,$limit,$req->timezone);   
                    $data = (empty($reviews)) ? ['message'=>'No reviews found'] : ['message'=>'Merchant reviews list','favourt_status'=>$reviews['favourt_status'],'data'=>$reviews['list'],'total_reviews' => $reviews['total_review'],'total'=>$reviews['total'],'star_count' => $reviews['star_count'],'average_rating'=>$reviews['average_rating']];
                    // $data = (empty($reviews)) ? ['message'=>'No reviews found'] : ['message'=>'Merchant reviews list','data'=>$reviews['data']];
             		$response = response()->json($data,$this->success_code);
            		break;

            	case 'portfolio':
            		$portfolio = $this->getPortfolio($merchant_id,$page,$limit);            		
            		$data = (empty($portfolio)) ? ['message'=>'No portfolio list found'] : ['message'=>'Merchant portfolio list','data'=>$portfolio['list'],'favourt_status'=>$portfolio['favourt_status']];
             		$response = response()->json($data,$this->success_code);
            		break;

            	case 'services':                    
            		$services = $this->getServices($merchant_id,$page,$limit,$req->timezone);                    
            		$data = (empty($services)) ? ['message'=>'No service list found'] : ['message'=>'Merchant service list','favourt_status'=>$services['fav_status'],'data'=>$services['list']['data']];
             		$response = response()->json($data,$this->success_code);
            		break;

            	case 'staff':
            		$data = $this->getStaff($merchant_id,$staff_id,$req->timezone);
            		// $data = (empty($staff)) ? ['message'=>'No staff list found'] : ['message'=>'Merchant staff list along with working hours','favourt_status'=>$staff['fav_status'],'data'=>$staff['list']];
             		$response = $data;
            		break;
                case 'dev-services':
                    $staff = $this->devServices($merchant_id,$page,$limit);
                    // $data = (empty($staff)) ? ['message'=>'No staff list found'] : ['message'=>'Merchant staff list along with working hours','favourt_status'=>$staff['fav_status'],'data'=>$staff['list']];
                    $response = response()->json($staff,$this->success_code);
                    break;            	
                case 'profile':
                    $staff = $this->vendorProfile($merchant_id,$current_lat,$current_lng);
                    // $data = (empty($staff)) ? ['message'=>'No staff list found'] : ['message'=>'Merchant staff list along with working hours','favourt_status'=>$staff['fav_status'],'data'=>$staff['list']];
                    $response = response()->json(['message'=>'Merchant profile','data'=>$staff],$this->success_code);
                    break;            	
            	default:
            		$data =  ['message'=>'Pass Valid Action'];
            		$response =  response()->json($data,$this->error_code);
            		break;
            }

            return $response;
    	}
    	else{
    		return response()->json(['message' => 'Merchant not found! Please login'], $this->unauthenticate_code);
    	}
    }

    private function getReviews($merchant_id,$page = 1,$limit =20,$timezone){
    	$merchant = Merchant::select('id')->where('id',$merchant_id)->with('rating.appointment.detail'
        )->first();
        
        $merchant_rating = $merchant->rating->toArray();        
        $r_list = array_filter($merchant_rating,function($mr) use ($merchant_id){
             if(!empty($mr['appointment']['detail'])){
                 return true;
             }
        });
        
        $rating_list = array_map(function($r_list) use ($merchant_id,$timezone){
                           
                            $user = UsersDetail::where('user_id',$r_list['user_id'])->first();        
                            $r_list['user_name'] = $user->name;
                            $r_list['created_at'] = $this->convertToTimestamp($r_list['created_at'],$timezone);
                            $r_list['user_image'] = ($user->user_image != null) ? $user->user_image : $this->OffersPackages->default_profile_image;
                            $appointment_services = $r_list['appointment']['detail'];
                            $service_list = array_map(function($appointment_services){
                                        return $appointment_services['name'];
                            },$appointment_services);
                            
                            unset($r_list['appointment']['detail']);
                            $r_list['service_name'] = implode(',',$service_list);
                            // end getting service name
                            $r_list['staffer'] = '';
                            
                            
                            if($r_list['appointment']['staff_id'] != $merchant_id){
                                $mr_detail = MerchantDetail::where('merchant_id',$r_list['appointment']['staff_id'])->first();                                
                                $r_list['staffer'] = $mr_detail->merchant_name;
                            }
                            unset($r_list['appointment']);
                            return $r_list;
                        },$r_list);                        
        $merchant['rating'] = $rating_list;
        
    

    	$fav_merchant =  Favourity::where(['user_id'=>Auth::id(),'merchant_id'=>$merchant_id])->first();    	
    	$favourt_status = ($fav_merchant != null) ? true : false;
        $ratings = $merchant['rating'];
        $rating_count = count($merchant['rating']);
        
        $star_count = [];
        $avg_rating = 0;   
        for($i=1;$i<=5;$i++){
            $star_count['star_'.$i] = 0;
            foreach ($merchant['rating'] as $rating) {                
                if($i == explode('.',$rating['rating'])[0] ){
                    $star_count['star_'.$i] = $star_count['star_'.$i]+$rating['rating'];
                }
            }
        }

        foreach($ratings as $rate){
            $avg_rating += $rate['rating'];
        }
        if($rating_count != 0){
            $avg_rating = $avg_rating/$rating_count;
            $avg_rating = number_format($avg_rating,2);
            $avg_rating =(float)$avg_rating;
        }
        // dd($avg_rating);
        
        $ratings = $this->paginate($ratings,$limit,$page);
        $rating = $this->removePaginatorParam($ratings->toArray());
        $merchant->rating = $rating;
        // dump(['average_rating'=>$avg_rating]);die();
        $data = [
            'list'=>$rating['data'],'favourt_status'=>$favourt_status,
            'total'=>$rating['last_page'],'total_review'=>$rating_count,
            'star_count' => $star_count,'average_rating'=> $avg_rating
        ];
          
        
        // dd($data);
    	return $data;
    }

    private function getPortfolio($merchant_id){
    	// $merchant = Merchant::with('portfolio')->where('id',$merchant_id)->select('id')->first()->toArray();    	
    	$fav_merchant =  Favourity::where(['user_id'=>Auth::id(),'merchant_id'=>$merchant_id])->first();    	
    	$favourt_status = ($fav_merchant != null) ? true : false;


        // $data = $this->usersController->getPortfolio($merchant_id);
        $data['favourt_status'] = $favourt_status;



        // if($merchant_id != null) { 
            // $m_id = ($merchant_id ==  null) ? Auth::id() : $merchant_id;
            $portfolio_list = MerchantPortfolio::select('id','image')->where('merchant_id',$merchant_id)->get();
            $merchant_detail = MerchantDetail::where('merchant_id',$merchant_id)->first();
            $list = [                 
                        'favourt_status' => $favourt_status,
                        'salon_name' => $merchant_detail['merchant_business_name'],
                        'cover_image' => $merchant_detail['merchant_business_name'],
                        'detail_image' => $merchant_detail['detail_image'],
                        'gallery' => $portfolio_list
                    ];
            // $response = response()->json(['message' => "Portfolio list",'data'=>$data], $this->success_code);        
    	$data['list'] = $list;
        return $data;	
    }

    private function getServices($merchant_id,$page,$limit,$timezone){
        $offers = $this->OffersPackages->getOffers($timezone, $merchant_id);

    	$merchant = Merchant::select('id')->where('id',$merchant_id)->where('merchant_user_type',1)
    				->whereHas('services.serviceDetail')
                    ->orWhereHas('services.offer')
    				->with('services.serviceDetail','services.offer')                    
    				->first();        

    	$list = [];            
        $service_final_list = [];
    	if($merchant != null){
    		$merchant = $merchant->toArray();            
	        $list = $this->devServices($merchant_id);
            foreach($list as $l){
                $service_list = [];
                if(isset($l['service'])){
                    foreach ($l['service'] as $key=> $service) {                    
                        foreach ($offers as $offer) {                                       
                            if($service['service_id'] ==  $offer['service_id']){
                                unset($offer['category_name']);
                                unset($offer['category_id']);
                                $offer['package_service'] = null;
                                $l['service'][$key] = $offer;                                                            
                        
                            }
                        }
                       
                        array_push($service_list, $service);                    
                    }
                }
                else{
                    array_push($service_list, $l);
                }
                array_push($service_final_list,$l);
            }            
	    }	    
        
        $special_packages = $this->OffersPackages->getPackages($timezone,$merchant_id);
        $packages = [];
        foreach ($special_packages as $package) {
            $package_service = [];
            foreach ($package['services'] as $service) {
                array_push($package_service,$service['service_name']);
            }

            unset($package['services']);
            $package['package_service'] = implode(',', $package_service);
            
            $package['service_category_id'] = $package['service_buffer_time'] = $package['after_discount_price'] = $package['offer_id'] = $package['max_discount_price'] = $package['discount']= null;

            $package['service_id'] = $package['package_id'];
            $package['service_price'] = $package['package_price'];            
            $package['service_name'] = $package['package_name'];   
            $package['service_image'] = $this->OffersPackages->default_image;         
            $package['service_duration'] = ($package['end_time']/1000 - $package['start_time']/1000) / 60;

            $unset_vars = ['package_id','package_name','package_price'];
            foreach($unset_vars as $key){
                unset($package[$key]);
            }

            array_push($packages,$package);
        }
        $special_packages_arr = ['id'=>0,'name'=>'Special packages','service'=>$packages];
        if(!empty($service_final_list)) {            
            array_push($service_final_list,$special_packages_arr);
        }
                
        if (($key = array_search('6', $service_final_list)) !== false) {
            unset($service_final_list[$key]);
        }
        
        $fav_merchant =  Favourity::where(['user_id'=>Auth::id(),'merchant_id'=>$merchant_id])->first();    	
    	$favourt_status = ($fav_merchant != null) ? true : false;
        
	    $final_list = $this->paginate($service_final_list,$limit,$page)->toArray();
        $final_list = $this->removePaginatorParam($final_list); 
        $final_list['data'] = array_values($final_list['data']);    

        $data = ['list'=>$final_list,'fav_status'=>$favourt_status];        
    	return $data;
    }

/*
    private function getStaff($merchant_id,$staff_id){
        $fav_merchant =  Favourity::where(['user_id'=>Auth::id(),'merchant_id'=>$merchant_id])->first();
        $favourt_status = ($fav_merchant != null) ? true : false;
        $data = ['fav_status'=>$favourt_status];

        if($staff_id != 0){
            $merchant = MerchantStaff::whereHas('staffServices')->with('workingHours','staffDetail')->where(['merchant_id'=>$merchant_id,'staff_id'=>$staff_id])->select('id','merchant_id','staff_id')->first();                        
        }

        else{
            // $merchant = Merchant::whereHas('services')->with('staff','workingHours')->where('id',$merchant_id)->select('id')->first();       
            $merchant = Merchant::whereHas('services')->with('staff.staffDetail','workingHours')->where('id',$merchant_id)->select('id')->first();
        }        
        // dd() 
        if($merchant != null){
            $merchant = $merchant->toArray();                    
            if($staff_id != 0){
                $data['data'] = $merchant;
                $data['message'] = "Staff working hours";
            }
            else{
                $data['message'] = "Merchant working hours";
                $merchant_staff_list = $merchant['staff'];
                $s_list = array_filter($merchant_staff_list,function($list){
                    if($list['staff_detail'] != null){
                        return true;
                    }
                });
                $staff_list =  array_map(function($s_list){
                                $s_list['staff_name'] = $s_list['staff_detail']['merchant_name'];
                                $s_list['staff_image'] = ($s_list['staff_detail']['merchant_image'] != null ) ? $s_list['staff_detail']['merchant_image'] : $this->OffersPackages->default_profile_image;
                                unset($s_list['staff_detail']);
                                return $s_list;
                },$s_list);
                $merchant['staff'] = array_values($staff_list) ;
                $data['data'] = $merchant;
            }
        }
        else{            
            $data = ['data'=>[],'fav_status'=>false,'message'=>'Merchant working hours'];
        }
        // dd($data);
        // $working_hours = $data['data']['working_hours'];
        // $hours_list = array_map(function($working_hours){
        //                 $working_hours['start_time'] = $this->convertToTimestamp($working_hours['start_time']);
        //                 $working_hours['end_time'] = $this->convertToTimestamp($working_hours['end_time']);
        //                 $working_hours['break_start_time'] = $this->convertToTimestamp($working_hours['break_start_time']);
        //                 $working_hours['break_end_time'] = $this->convertToTimestamp($working_hours['break_end_time']);
        //                 return $working_hours;
        // }, $working_hours);


        $data['data']['working_hours'] = $hours_list;
        $working_hours = $data['data']['working_hours'];
            $hours_list = array_map(function($working_hours){
                            $working_hours['start_time'] = $this->convertToTimestamp($working_hours['start_time']);
                            $working_hours['end_time'] = $this->convertToTimestamp($working_hours['end_time']);
                            $working_hours['break_start_time'] = $this->convertToTimestamp($working_hours['break_start_time']);
                            $working_hours['break_end_time'] = $this->convertToTimestamp($working_hours['break_end_time']);
                            return $working_hours;
            }, $working_hours);


        if($staff_id != 0){
            $data['data']['staff_image'] = ($data['data']['staff_detail']['merchant_image'] != null) ? $data['data']['staff_detail']['merchant_image'] : $this->OffersPackages->default_profile_image;
            $data['data']['staff_name'] = $data['data']['staff_detail']['merchant_name'];
            $data['data']['staff_gender'] = $data['data']['staff_detail']['gender'];

            unset($data['data']['staff_detail']);
        }
    	return $data;
    }
*/
    private function getStaff($merchant_id,$staff_id,$timezone){
        $fav_merchant =  Favourity::where(['user_id'=>Auth::id(),'merchant_id'=>$merchant_id])->first();
        $favourt_status = ($fav_merchant != null) ? true : false;
        $data = ['favourt_status'=>$favourt_status];

        if($staff_id != 0){
            $merchant = MerchantStaff::whereHas('staffServices')->with('workingHours','staffDetail')->where(['merchant_id'=>$merchant_id,'staff_id'=>$staff_id])->select('id','merchant_id','staff_id')->first();                        
        }

        else{
            $merchant = Merchant::whereHas('services')->with('staff.staffDetail','workingHours')->where('id',$merchant_id)->select('id')->first();       
        }        


        if($merchant != null){
            $merchant = $merchant->toArray();                    
            if($staff_id != 0){
                $data['data'] = $merchant;
                $data['message'] = "Staff working hours";
            }
            else{
                $data['message'] = "Merchant working hours";
                $merchant_staff_list = $merchant['staff'];
                $s_list = array_filter($merchant_staff_list,function($list){
                    if($list['staff_detail'] != null){
                        return true;
                    }
                });
                $staff_list =  array_map(function($s_list){
                                $s_list['staff_name'] = $s_list['staff_detail']['merchant_name'];
                                $s_list['staff_image'] = ($s_list['staff_detail']['merchant_image'] != null ) ? $s_list['staff_detail']['merchant_image'] : $this->OffersPackages->default_profile_image;
                                unset($s_list['staff_detail']);
                                return $s_list;
                },$s_list);
                $merchant['staff'] = array_values($staff_list) ;
                $data['data'] = $merchant;
            }


            $working_hours = $data['data']['working_hours'];
            $hours_list = array_map(function($working_hours) use ($timezone){
                            $working_hours['start_time'] = $this->convertToTimestamp($working_hours['start_time'],$timezone);
                            $working_hours['end_time'] = $this->convertToTimestamp($working_hours['end_time'],$timezone);
                            $working_hours['break_start_time'] = $this->convertToTimestamp($working_hours['break_start_time'],$timezone);
                            $working_hours['break_end_time'] = $this->convertToTimestamp($working_hours['break_end_time'],$timezone);
                            return $working_hours;
            }, $working_hours);


            $data['data']['working_hours'] = $hours_list;

            if($staff_id != 0){
                $data['data']['staff_image'] = ($data['data']['staff_detail']['merchant_image'] != null) ? $data['data']['staff_detail']['merchant_image'] : $this->OffersPackages->default_profile_image;
                $data['data']['staff_name'] = $data['data']['staff_detail']['merchant_name'];
                $data['data']['staff_gender'] = $data['data']['staff_detail']['gender'];

                unset($data['data']['staff_detail']);
            }
        }
        else{            
            $data =  response()->json(['message' => "User not found"],$this->error_code);
        return $data;
        } 
        $data = response()->json(['message' => "Staff detail",'data'=> $data['data']],$this->success_code);   
        
        return $data;
    }

    public function vendorProfile($merchant_id,$current_lat,$current_lng){
        
        $merchant = Merchant::select('id','mobile','is_verified')
                            ->where('id',$merchant_id)
                            ->where('merchant_user_type',1)
                            ->with('detail','address','rating')
                            ->first();
                    
            if($merchant != null){
                $total_review = 0;  
              
                foreach($merchant->rating as $rating){
                    $total_review += $rating['rating'];
                }
                $total =  count($merchant->rating);
                $avg_rating = 0;
                if($total > 0){
                    $avg_rating = $total_review/$total;
                $avg_rating = number_format($avg_rating,2);
                $avg_rating = (float)$avg_rating;
                }
                
                $lat = $merchant->address['lat'];
                $lng = $merchant->address['lng'];
               
            $vendor['id'] = $merchant['id'];
             $vendor['is_verified'] = $merchant['is_verified'];
             $vendor['distance'] = $this->getDistanceBetweenPointsNew($current_lat,$current_lng,$lat,$lng);
             $vendor['merchantRating'] = $avg_rating;
             $vendor['merchantReview'] = $total;
             $vendor['discount'] = null;
             $vendor['merchantBusinessAddress'] = $merchant->address['address1'];
             $vendor['merchantBusinessAddress2'] = $merchant->address['address2'];
             $vendor['merchatLat'] = $lat;
             $vendor['merchatLng'] = $lng;
             $vendor['mercahntAddressId'] = $merchant->address['id'];
             $vendor['merchantBusinessCity'] = $merchant->address['city'];
             $vendor['merchantBusinessPostcode'] = $merchant->address['postcode'];
             $vendor['merchantDetailId'] = $merchant->detail['id'];
             $vendor['merchantBusinessName'] = $merchant->detail['merchant_business_name'];
             $vendor['merchantBusinessLandline'] = $merchant->detail['landline'];
             $vendor['salonType'] = $merchant->detail['salon_type'];
             $vendor['businessCoverImage'] = ($merchant->detail['business_cover_image'] != null) ? $merchant->detail['business_cover_image'] : $this->OffersPackages->default_cover_image;
             $vendor['deatilImage'] = ($merchant->detail['detail_image'] != null) ? $merchant->detail['detail_image'] : $this->OffersPackages->default_salon_image;
             $vendor['merchantId'] = $merchant->detail['merchant_id'];
         
            
             return $vendor;
            //  return  response()->json($vendor,$this->success_code);
             exit;
            }        
            // echo json_encode($list);
            // die;
        // dd($list);
        
        // return $list;
        // return  response()->json($list,$this->success_code);
        // exit;
    }

    public function devServices($merchant_id){        
        $merchant = Merchant::select('id')->where('id',$merchant_id)->where('merchant_user_type',1)                    
                    ->with('services.serviceDetail','services.offer','services.category')
                    ->first();        
        if($merchant != null){
            $cat_list = [];  
            foreach ($merchant['services'] as $service) {
                $cat_id = $service->serviceDetail->service_categories_id;
                if(!in_array($cat_id,$cat_list)){
                    array_push($cat_list,$cat_id);
                }
            }            
    
            $categories = ServiceCategory::whereIn('id',$cat_list)->get()->toArray();
            $list = [];            
            foreach ($categories as $category) {
                $data = [
                    'id' => $category['id'],
                    'name' => $category['category_name'],
                    'service' => []
                ];
                foreach ($merchant['services'] as $service) {                    
                    $service['service_image'] = ($service['service_image'] == null) ? $this->OffersPackages->default_image: $service['service_image'];
                    $service['discount'] = $service['max_discount_price'] = $service['start_time'] = $service['end_time'] = $service['offer_id'] = $service['after_discount_price'] = $service['package_service'] = null;                
                    $service['service_name'] = $service->serviceDetail['service_name'];
                    $service['merchant_id'] = (int) $merchant_id;
                    if($service->serviceDetail->service_categories_id == $category['id']){
                        array_push($data['service'],$service);
                    }
                    $unset_vars = ['category','offer','id','merchant_staff_id'];
                    foreach ($unset_vars as $key) {
                        unset($service[$key]);
                    }
                    unset($service->serviceDetail);
                }    
                            
                array_push($list,$data);
            }                    
        }
        return $list;        
    }
}
