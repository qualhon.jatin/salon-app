<?php

namespace App\Http\Controllers\v2\Api\Merchant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use App\Merchant;
use App\MerchantStaff;
use App\MerchantService;
use App\MerchantDetail;
use App\Appointment;
use App\MerchantReport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Arr;
use DateTime;
use DatePeriod;
use DateInterval;

class MerchantReports extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     echo "merchant_report";
    // }
    
    public function totalReport(Request $request)
    {
            $user = Auth::user();
            $id = Auth::id();
            
            $validator = Validator::make($request->all(),
                [
                    'timezone' => 'required',
                ]
                
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }
            $timezone = $request->timezone;
            
            $merchantRevenue = MerchantReport::select('total_revenue','current_month_revenue','last_month_revenue')->where('merchant_id',$id)->first();
            if($merchantRevenue == null){
                
                $revenu_data = [
                    'total_revenue' => 0,
                    'current_month_revenue' => 0,
                    'last_month_revenue' => 0
                ];
                
                $response = response()->json(['message' => 'Merchant reports','data'=>$revenu_data], $this->success_code);            
                return $response;
            }
            else{
                $response = response()->json(['message' => 'Merchant reports','data'=>$merchantRevenue], $this->success_code);            
                return $response;
            }
             $response = response()->json(['message' => 'Merchant reports','data'=>$merchantRevenue], $this->success_code);            
            return $response;    
    }
    
    public function totalReportCron(Request $request)
    {
            $user = Auth::user();
            $id = Auth::id();
            
            $validator = Validator::make($request->all(),
                [
                    'timezone' => 'required',
                ]
                
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }
            $timezone = $request->timezone;
            
            $merchant_ids = Appointment::select('merchant_id')
                                    ->where('appointment_status',2)->pluck('merchant_id')->toArray();
            $merchant_ids = array_values(array_unique($merchant_ids));
           
            $revenue_reports = [];
            foreach($merchant_ids as $merchant_id){
            
           
            $merchant_appointment = Appointment::where('merchant_id',$merchant_id)
                                                 ->where('appointment_status',2)->get()->toArray();
            
                                                        
            $total_revenue = 0;
            $current_month_revenue = 0;
            $last_month_revenue = 0;
            
            foreach($merchant_appointment as $appointment){
                $total_revenue += $appointment['final_price'];
            }
            
            
            // current month revenue
            $current_month_appointment = Appointment::where('merchant_id',$merchant_id)
                                                ->where('appointment_status',2)
                                                ->whereMonth('appointment_date_time', date('m'))
                                                ->get()
                                                ->toArray();
            foreach($current_month_appointment as $appointment){
                $current_month_revenue += $appointment['final_price'];
            }
            
            
            // last month revenue
            $month = date('m')-1;
            $last_month_appointment = Appointment::where('merchant_id',$merchant_id)
                                                ->where('appointment_status',2)
                                                ->whereMonth('appointment_date_time', $month)
                                                ->get()
                                                ->toArray();

            foreach($last_month_appointment as $appointment){
                $last_month_revenue += $appointment['final_price'];
            }
            // $revenue = [
            //     'total_revenue'          => $total_revenue,
            //     'current_month_revenue'  => $current_month_revenue,
            //     'last_month_revenue' => $last_month_revenue,
            // ];
            
            $revenue_date = [
                'merchant_id'          => $merchant_id,
                'total_revenue'          => $total_revenue,
                'current_month_revenue'  => $current_month_revenue,
                'last_month_revenue' => $last_month_revenue,
                'created_at' => $this->getCurrentTime($timezone),
                'updated_at' => $this->getCurrentTime($timezone)
            ];

            array_push($revenue_reports,$revenue_date);
        }

    
  
        $merchant_revenue_rerport = MerchantReport::insert($revenue_reports);

        // $response = response()->json(['message' => 'Merchant reports','data'=>$revenue], $this->success_code);            
        //   return $response;                                      


    }

    public function graphReview(Request $request)
    {
            $user = Auth::user();
            $id = Auth::id();
                        
            $validator = Validator::make($request->all(),
                [
                    'timezone' => 'required',
                ]
                
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }
            $timezone = $request->timezone;
            $merchant_appointment = Appointment::where('merchant_id',$id)
                                                 ->where('appointment_status',2)
                                                 ->whereMonth('appointment_date_time', date('m'))
                                                 ->get()
                                                 ->toArray();
            
            $current_month = [];
            $revenue_day_wise = [];
            $revenue_date_wise = [];
            $revenue_month_wise = [];
            
            foreach($merchant_appointment as $appointment){
                $total_revenue['date']  = $this->convertToTimestamp($appointment['appointment_date_time'],$timezone);
                $total_revenue['final_price']  = $appointment['final_price'];
            
                array_push($current_month,$total_revenue);
            }
       
            $current = array_column($current_month, 'date');                     
            array_multisort($current, SORT_ASC, $current_month);             
            $tmp_arr = [];
            foreach ($current_month as $month) {
                $date_arr = explode("-", date("Y-M-d",$month['date']));
                $year = $date_arr[0];
                $mnth = $date_arr[1];
                $day = $date_arr[2];
                $month['date'] = $day." ".$mnth;
                array_push($tmp_arr, $month);
            }
            $current_month =  $tmp_arr;
            
            $revenue =  $current_month;
      
            if(isset($request->from) && isset($request->to)){
                if($request->from < $request->to ){
                    $start = $this->convertToDate($request->from);
                    $end =  $this->convertToDate($request->to);
                    $days = strtotime($end) - strtotime($start);
                    $days = $days/86400;

                    $start = $start . " 00:00:00";
                    $end =  $end ." 23:59:59";
                    // echo $start."<br>";
                    // echo $end;
                    // die();
                    $merchant_appointment = Appointment::where('merchant_id',$id)
                                            ->where('appointment_status',2)
                                            ->whereBetween('appointment_date_time', [$start,$end])
                                            ->get()
                                            ->toArray();
                    
                    
                    if($days <= 7){
                       
                        foreach($merchant_appointment as $revenue){
                            
                            $day_wise['day'] = $this->convertToTimestamp($revenue['appointment_date_time'],$timezone);
                            $day_wise['final_price'] = $revenue['final_price'];

                            array_push($revenue_day_wise,$day_wise);
                        }
                        $day_reports = array_column($revenue_day_wise, 'day');                     
                        array_multisort($day_reports, SORT_ASC, $revenue_day_wise); 
                        
                        $day_filter = [];

                   
                        
                        $week_day = [
                            'Mon' => 0,
                            'Tue' => 0,
                            'Wed' => 0,
                            'Thu' => 0,
                            'Fri' => 0,
                            'Sat' => 0,
                            'Sun' => 0,
                            
                           ];


                        foreach($revenue_day_wise as $day){
                            $day_exist = $this->getDayShort($day['day'],$timezone);

                            if(array_key_exists($day_exist,$week_day)) {                                                            
                                $week_day[$day_exist] = $day['final_price'];
                            }  
                        }
                        
                        foreach($week_day as $key => $value){
                            $filter['date'] = $key;
                            $filter['amount'] = $value;
                        
                            array_push($day_filter,$filter);
                        }
                       
                        $revenue =  $day_filter;
                        
                    }
                    if($days > 7 && $days < 31){ 
                    
                        function getDatesFromRange($start, $end, $format = 'Y-m-d') { 
      
                            // Declare an empty array 
                            $array = array(); 
                              
                            // Variable that store the date interval 
                            // of period 1 day 
                            $interval = new DateInterval('P1D'); 
                          
                            $realEnd = new DateTime($end); 
                            $realEnd->add($interval); 
                          
                            $period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
                          
                            // Use loop to store date into array 
                            foreach($period as $date) {                  
                                $array[] = $date->format($format);  
                            } 
                          
                            // Return the array elements 
                            return $array; 
                        } 
                          
                        // Function call with passing the start date and end date 
                        
                        $all_date = getDatesFromRange($start, $end);                       
                    
                        
                        foreach($merchant_appointment as $appointment){
                            
                                    $data['date'] = date('Y-m-d',strtotime($appointment['appointment_date_time']));
                                    $data['amount'] = $appointment['final_price'];
                               
                                    array_push($revenue_date_wise,$data);
                                    
                                 }
                                
                                 $date_reports = array_column($revenue_date_wise, 'date');                     
                                 array_multisort($date_reports, SORT_ASC, $revenue_date_wise);           
                        
                        
                        foreach($all_date as $dates){
                            $data = [];
                            foreach($revenue_date_wise as $revenues){
                                   if($revenues['date'] == $dates){
                                        $data['date'] = date("d M",strtotime($dates));       
                                        $data['amount'] = $revenues['amount'];
                                        break;
                                 
                                    }else{
                                        $data['date'] = date("d M",strtotime($dates));
                                        $data['amount'] = 0;
                                       
                                    }
                            }
                            array_push($revenue,$data);
                        }
                                                                        
                    }
                    if($days > 31){
                              
                       $final_mnth_arr = [
                        'Jan' => 0,
                        'Feb' => 0,
                        'Mar' => 0,
                        'Apr' => 0,
                        'May' => 0,
                        'Jun' => 0,
                        'Jul' => 0,
                        'Aug' => 0,
                        'Sep' => 0,
                        'Oct' => 0,
                        'Nov' => 0,
                        'Dec' => 0
                       ];                      
                        foreach($merchant_appointment as $appointment){
                            $appointment_month = date('M',strtotime($appointment['appointment_date_time']));
                                                                                    
                            if(array_key_exists($appointment_month,$final_mnth_arr)) {                                                            
                                $final_mnth_arr[$appointment_month] += $appointment['final_price'];
                            }                                                       
                        }

                        foreach($final_mnth_arr as $key => $value){
                            $month_wise['date'] = $key;
                            $month_wise['amount'] = $value;
                            
                            array_push($revenue_month_wise,$month_wise);
                        }
                        $revenue = $revenue_month_wise;

                    }   
                
                }
                else{
                 $response = response()->json(['message' => 'From time is always greater than start time'], $this->error_code);            
                return $response;  
             }

        }
         
        $response = response()->json(['message' => 'Merchant reports','data'=>$revenue], $this->success_code);            
        return $response;                                      


    }



    // public function graphReview(Request $request)
    // {
    //         $user = Auth::user();
    //         $id = Auth::id();            
    //         $validator = Validator::make($request->all(),
    //             [
    //                 'timezone' => 'required',
    //             ]
                
    //         );

    //         if ($validator->fails()) {
    //             return response()->json(['message' => $validator->errors()], $this->error_code);
    //         }
    //         $timezone = $request->timezone;
    //         $merchant_appointment = Appointment::where('merchant_id',$id)
    //                                              ->where('appointment_status',2)
    //                                              ->whereMonth('appointment_date_time', date('m'))
    //                                              ->get()
    //                                              ->toArray();
            
    //         $current_month = [];
    //         $revenue_day_wise = [];
    //         $revenue_date_wise = [];
    //         $revenue_month_wise = [];
            
    //         foreach($merchant_appointment as $appointment){
    //             $total_revenue['date']  = $this->convertToTimestamp($appointment['appointment_date_time'],$timezone);
    //             $total_revenue['final_price']  = $appointment['final_price'];
            
    //             array_push($current_month,$total_revenue);
    //         }

    //         $current = array_column($current_month, 'date');                     
    //         array_multisort($current, SORT_ASC, $current_month);             
    //         $tmp_arr = [];
    //         foreach ($current_month as $month) {
    //             $date_arr = explode("-", date("Y-M-d",$month['date']));
    //             $year = $date_arr[0];
    //             $mnth = $date_arr[1];
    //             $day = $date_arr[2];
    //             $month['date'] = $day." ".$mnth;
    //             array_push($tmp_arr, $month);
    //         }
    //         $current_month =  $tmp_arr;
            
    //         $revenue =  $current_month;
    //         if(isset($request->from) && isset($request->to)){
    //             if($request->from < $request->to ){
    //                 $start = $this->convertToDate($request->from);
    //                 $end =  $this->convertToDate($request->to);
    //                 $days = strtotime($end) - strtotime($start);
    //                 $days = $days/86400;

                    
    //                 $end =  $end ." 23:59:59";
                    
    //                 $merchant_appointment = Appointment::where('merchant_id',$id)
    //                                         ->where('appointment_status',2)
    //                                         ->whereBetween('appointment_date_time', [$start,$end])
    //                                         ->get()
    //                                         ->toArray();

                    
    //                 if($days <= 7){
                       
    //                     foreach($merchant_appointment as $revenue){
    //                         $day_wise['day'] = $this->convertToTimestamp($revenue['appointment_date_time'],$timezone);
    //                         $day_wise['final_price'] = $revenue['final_price'];

    //                         array_push($revenue_day_wise,$day_wise);
    //                     }
    //                     $day_reports = array_column($revenue_day_wise, 'day');                     
    //                     array_multisort($day_reports, SORT_ASC, $revenue_day_wise); 
    //                     $day_filter = [];

                   
                        
    //                     $week_day = [
    //                         'Monday' => 0,
    //                         'Tuesday' => 0,
    //                         'Wednesday' => 0,
    //                         'Thursday' => 0,
    //                         'May' => 0,
    //                         'Saturday' => 0,
    //                         'Sunday' => 0,
                            
    //                        ];


    //                     foreach($revenue_day_wise as $day){
    //                         $day_exist = $this->getDay($day['day'],$timezone);

    //                         if(array_key_exists($day_exist,$week_day)) {                                                            
    //                             $week_day[$day_exist] = $appointment['final_price'];
    //                         }  
    //                     }
                        
    //                     foreach($week_day as $key => $value){
    //                         $filter['day'] = $key;
    //                         $filter['amount'] = $value;
                        
    //                         array_push($day_filter,$filter);
    //                     }
                       
    //                     $revenue =  $day_filter;
                        
    //                 }
    //                 if($days > 7 && $days < 31){                       
    //                     foreach($merchant_appointment as $appointment){
    //                                 $data['date'] = $this->convertToTimestamp($appointment['appointment_date_time'],$timezone);
    //                                 $data['amount'] = $appointment['final_price'];
                                                
    //                                 array_push($revenue_date_wise,$data);
    //                              }
                                 
    //                              $date_reports = array_column($revenue_date_wise, 'date');                     
    //                              array_multisort($date_reports, SORT_ASC, $revenue_date_wise);           
                        
    //                     $revenue =  $revenue_date_wise;                                                
    //                 }
    //                 if($days > 31){
                              
    //                    $final_mnth_arr = [
    //                     'Jan' => 0,
    //                     'Feb' => 0,
    //                     'Mar' => 0,
    //                     'Apr' => 0,
    //                     'May' => 0,
    //                     'Jun' => 0,
    //                     'Jul' => 0,
    //                     'Aug' => 0,
    //                     'Sep' => 0,
    //                     'Oct' => 0,
    //                     'Nov' => 0,
    //                     'Dec' => 0
    //                    ];                      
    //                     foreach($merchant_appointment as $appointment){
    //                         $appointment_month = date('M',strtotime($appointment['appointment_date_time']));
                                                                                    
    //                         if(array_key_exists($appointment_month,$final_mnth_arr)) {                                                            
    //                             $final_mnth_arr[$appointment_month] += $appointment['final_price'];
    //                         }                                                       
    //                     }

    //                     foreach($final_mnth_arr as $key => $value){
    //                         $month_wise['date'] = $key;
    //                         $month_wise['amount'] = $value;
                            
    //                         array_push($revenue_month_wise,$month_wise);
    //                     }
    //                     $revenue = $revenue_month_wise;

    //                 }   
                
    //             }
    //             else{
    //              $response = response()->json(['message' => 'From time is always greater than start time'], $this->error_code);            
    //             return $response;  
    //          }

    //     }
         
    //     $response = response()->json(['message' => 'Merchant reports','data'=>$revenue], $this->success_code);            
    //     return $response;                                      


    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
