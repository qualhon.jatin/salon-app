<?php
namespace App\Http\Controllers\v2\Api;

use App\User;
use App\Merchant;
use App\Appointment;
use App\ReviewRating;
use App\MerchantUser;
use App\AppointmentDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Traits\PushNotificationTrait;
use Illuminate\Support\Facades\Validator;

class AppoientmentController extends Controller
{
	use PushNotificationTrait;
    
    public function index(Request $request){
    	if(Auth::user()){    	    		
				
			$validator = Validator::make(
	            $request->all(),
	            [	            	
	                'timezone' => 'required'
	            ]
	        );

	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
			}
			
			// if((empty($request->timestamp)) && (empty($request->date)) ){
			// 	return $response = response()->json(['message' => 'Please provide  atleast one param time or datetime'], $this->error_code);	
			// }

			$timezone = $request->timezone;
			$current_date_time = $this->getCurrentTime($timezone);
			$merchant_user_type_with_id = (Auth::user()->merchant_user_type == 1) ? ['merchant_id'=>Auth::id()] :  ['staff_id'=>Auth::id()];

    		$options = (strpos($request->route()->uri, 'merchant') !== false) ? $merchant_user_type_with_id : ['user_id'=>Auth::id()];        		

    		$columns =['id','merchant_id','staff_id','user_id','appointment_date_time','actual_price','final_price','client_message','appointment_status'];
    		
    		$appointments = Appointment::where($options)->select($columns)->whereHas('merchantDetail')->with('detail','merchantDetail','merchantAddress','staffDetail','userDetail')->get()->toArray();  	
			
    		$list = $appointment_list = array_map(function($appointments) use($timezone){
    								$appointments['client_message'] = ($appointments['client_message'] == null) ? "" : $appointments['client_message'];
    								$address = $appointments['merchant_address'];    								    								
    								$appointments['appointment_date_time'] = $this->convertToTimestamp($appointments['appointment_date_time'],$timezone);
    								$appointments['merchant_business_name'] = $appointments['merchant_detail']['merchant_business_name'];    								
    								$appointments['address'] = $address['address1']." ".$address['address2']." ".$address['city']." ".$address['postcode'];
    								$appointments['lat'] = (double)$address['lat'];
    								$appointments['lng'] = (double)$address['lng'];
    								$appointments['staff_name'] =  $appointments['staff_detail']['merchant_name'];
    								$appointments['user_name'] =  $appointments['user_detail']['name'];
    								// $appointments['status'] =  $appointments['appointment_status'];
    								$appointments['appointment_end_time'] = $appointments['appointment_date_time'];

    								$services = $appointments['detail'];
    								$service_list = array_map(function($services){        										
    									return $services['name'];
									},$services);

    								$end_time = 0;
									foreach ($services as $service) {
										$end_time += $service['service_duration'];
									}
									$appointments['appointment_end_time'] = $appointments['appointment_end_time'] + $this->minsToMiliseconds($end_time);
									$appointments['services'] =  implode(", " ,$service_list);

    								$unset_vars = ['merchant_detail','merchant_address','detail','staff_detail','user_detail'];
    								foreach ($unset_vars as $key) {
    									unset($appointments[$key]);
    								}    								
    								return $appointments;
								},$appointments);
								
    		if(isset($request->timestamp) && $request->timestamp != null){
    			$list = array_filter($appointment_list,function($appointment) use ($request){    				
    					if($this->convertToTime($appointment['appointment_date_time']) == $this->convertToTime($request->timestamp)){
    						return true;
    					}					
    			});
			}
			// filter based on date 
    		if(isset($request->date) && $request->date != null ){
    			$list = array_filter($appointment_list,function($appointment) use ($request){
						 if($this->convertToDate($appointment['appointment_date_time']) == $this->convertToDate($request->date,$request->timezone)){
							return true;
						}
				});

					// // filter based on client id
					if(isset($request->client_id) && $request->client_id != null ){
						$list = array_filter($list,function($appointment) use ($request){
								if($appointment['user_id'] == $request->client_id && $appointment['merchant_id'] == Auth::id() ){
									return true;
									 
								}
						});
						
					}    			
			}
			else{
					// // filter based on client id
					if(isset($request->client_id) && $request->client_id != null ){
						$list = array_filter($appointment_list,function($appointment) use ($request){
								if($appointment['user_id'] == $request->client_id && $appointment['merchant_id'] == Auth::id() ){
									return true;
									 
								}
						});
						
					}
			}
		
		
			// // // filter base on staff id
			$staff_ids = [];
			$staff_id = $request->staff_id;
			$staff_ids = explode(",",$staff_id);
			// filter base on client id
			if(isset($request->staff_id) && $request->staff_id != null){
				$list = array_filter($list,function($appointment) use ($request,$staff_ids){
						foreach($staff_ids as $value){
							if($appointment['staff_id'] == $value && $appointment['merchant_id'] == Auth::id() ){
								return true;
								 
							}
			
						}
						
					});
			}
			// filter based on appointment stats 
			$appointment_status = [];
			$appintment_st = $request->status;
			$appointment_status = explode(",",$appintment_st);				
			
			if(isset($request->status) && $request->status != null){
				$list = array_filter($list,function($appointment) use ($request,$appointment_status){
						foreach($appointment_status as $values){
							if($appointment['appointment_status'] == $values && $appointment['merchant_id'] == Auth::id() ){
								return true;
								 
							}
							
						}		
					});
			}				

    	   		
    		$data = ['upcoming'=>[],'finished'=>[]];
			
			foreach ($list as $value) {
				$current_time = $this->convertToTimestamp($current_date_time,$timezone);
			
				
				($value['appointment_end_time']) > $current_time  && $value['appointment_status'] == 1 ? array_push($data['upcoming'], $value) : array_push($data['finished'], $value);
		  	}
			
			
			$upcoming_columns = array_column($data['upcoming'], 'appointment_date_time');                     
			array_multisort($upcoming_columns, SORT_ASC, $data['upcoming']);
			
			$finished_columns = array_column($data['finished'], 'appointment_date_time');                     
			array_multisort($finished_columns, SORT_DESC, $data['finished']);
			
			
			$finished =  $data['finished'];
			
			$upcoming =  $data['upcoming'];
			
			$finished_status = array();
			$upcoming_status = array();
		
			foreach($finished as $finish){
				if($finish['appointment_status'] == 1){
					$finish['appointment_status'] = 3;
				}
				array_push($finished_status,$finish);
			} 
			$upcoming_status = array_filter($upcoming,function($appointment) use ($request){    				
					if($appointment['appointment_status'] == 1){
						return true;
					}					
			});
	
			
			$data['finished'] = array_values($finished_status);
			$data['upcoming'] = array_values($upcoming_status);
			$response = response()->json(['message' => 'Appointment list','data'=>$data], $this->success_code);
    	}
    	else{
    		$response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
    	}
    	return $response;
    }

    public function show(Request $request){
    	if(Auth::user()){			    		
    		$validator = Validator::make(
            $request->all(),
				[
					'appointment_id' => 'required',
					'timezone' => 'required'
				
				]
	        );
	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
	        }
			
			$timezone = $request->timezone;

			
			
			$merchant_options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id'=>Auth::id(),'id'=>$request->appointment_id] : ['user_id'=>Auth::id(),'id'=>$request->appointment_id];
			
	        $review_options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id'=>Auth::id(),'id'=>$request->appointment_id] : ['user_id'=>Auth::id(),'id'=>$request->appointment_id];
			
	        $columns =['id','merchant_id','staff_id','user_id','appointment_date_time','appointment_status','actual_price','final_price','client_message','cancel_message'];
	        $appointment = Appointment::select($columns)->where($merchant_options)->with('detail','merchantDetail','staffDetail','userDetail.user')->first();
	        $rating = ReviewRating::where($review_options)->first();
		
	        if($appointment != null){

	        	$appointment['rating_status'] = ($rating != null) ? true : false;

		        $appointment['name']= $appointment->staffDetail['merchant_name'];
		        $appointment['cancel_message']= ($appointment['cancel_message'] ==  null) ? "" : $appointment['cancel_message'];
		        $appointment['client_message']= ($appointment['client_message'] ==  null) ? "" : $appointment['client_message'];
		        $appointment['user_name'] = $appointment->userDetail['name'];
		        $appointment['user_email'] = $appointment->userDetail['email'];
		        $appointment['user_mobile'] = $appointment->userDetail['user']['mobile'];
		        $appointment['total_price']= $appointment['final_price'];
		        $appointment['image']= ($appointment->staffDetail['merchant_image'] == null) ? $this->default_profile_image : $appointment->staffDetail['merchant_image'];
		        $appointment['appointment_date_time'] = $this->convertToTimestamp($appointment['appointment_date_time'],$timezone);
		        $appointment['appointment_end_time'] = $appointment['appointment_date_time'];

		        $services = $appointment->detail->toArray();
		        $service_list = array_map(function($services){    								   				
		        						unset($services['id']);
		        						unset($services['appointment_id']);
	    								return $services;
						    		},$services);
		        $appointment['services'] = $service_list;	        
		        $end_time = 0;
				foreach ($services as $service) {
					$end_time += $service['service_duration'];
				}
				$appointment['appointment_end_time'] = $appointment['appointment_end_time'] + $this->minsToMiliseconds($end_time);
		        unset($appointment['detail']);
		        unset($appointment->merchantDetail);
		        unset($appointment->userDetail);		        
		        unset($appointment['final_price']);	        
		        
		        $response = response()->json(['message' => 'Appointment Detail','data'=>$appointment], $this->success_code);
		    }
		    else{
		    	$response = response()->json(['message' => 'Invalid appointment ID'], $this->error_code);	
		    }
    	}
    	else{
    		$response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
    	}
    	return $response;
    }

    public function create(Request $request){    	    	
    		$validator = Validator::make(
            $request->all(),
	            [	            	
	            	'appointment_date_time' => 'required',	            	
	            	'actual_price' => 'required',	            	
	            	'total_price' => 'required',	            	
	            	'services' => 'required',	
	            	'timezone' => 'required',            	
	            ]
	        );
	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
	        }
	    	
	    	$data = $request->all();
	    	if(gettype($data['services']) == 'string'){
	    		$services = $data['services'] = json_decode($data['services'],true);
	    	}
	    	else{
	    		$services = $data['services'];
	    	}	   

	    	unset($data['services']);
	 
	    	$appointment = $data;
			$appointment['merchant_id'] = $merchant_id =(strpos($request->route()->uri, 'merchant') !== false) ? Auth::id() : $request->merchant_id;
			$appointment['user_id'] = $user_id =(strpos($request->route()->uri, 'merchant') !== false) ? $request->user_id : Auth::id();
	    	$appointment['final_price'] = $data['total_price'];	    	
			$appointment['appointment_date_time'] = $this->convertToLocalTime($data['appointment_date_time'],$request->timezone);			
		    
			$appointment['appointment_id'] = $this->appointmentId();	    	

			$merchant_user = MerchantUser::where('merchant_id',$merchant_id)->where('user_id',$user_id)->first();
			$user = User::where('id',$user_id)->with('detail')->first();
			$merchant = Merchant::where('id',$merchant_id)->with('detail','address')->first();
			
			$username = $user['detail']['name'];
			$dateTime = $this->DateTimeFormat($appointment['appointment_date_time']);
			$merchant_business_name = $merchant['detail']['merchant_business_name'];
			

			$appointment['merchant_address'] = $merchant['address1'];
			$appointment['merchant_lat'] = $merchant['lat'];
			$appointment['merchant_lng'] = $merchant['lng'];

			if(strpos($request->route()->uri, 'merchant')){
				if($merchant_user == null){
					$data = [
						'merchant_id' => $merchant_id,
						'user_id'     => $user_id
					];
					MerchantUser::create($data)->id;
				}
				$merchant_notification = [
		    								'title'     => "Appointment booked Successfully",
					                        'body'      => "An appointment for $dateTime has been booked successfully with $username",
					                        'user_id'  	=>  $merchant_id,
					                        'user_type'	=> 1
		    							];
		    	$user_notification = 	[
		    								'title'     => "Appointment Booked received",
					                        'body'      => "Your appointment has been booked successfully with $merchant_business_name at $dateTime",
					                        'user_id'  	=>  $user_id,
					                        'user_type'	=> 0
		    							];
											  
			}
			else{
				
				$merchant_notification = [
		    								'title'     => "Appointment Booked received",
					                        'body'      => "$username has booked a new appointment for $dateTime",
					                        'user_id'  	=>  $merchant_id,
					                        'user_type'	=> 1
		    							];
		    	$user_notification = 	[
		    								'title'     => "Appointment Booked Successfully",
					                        'body'      => "Your appointment has been booked successfully with $merchant_business_name for $dateTime",
					                        'user_id'  	=>  $user_id,
					                        'user_type'	=> 0
		    							];
			}			
			$user_data = User::where('id',$user_id)->with('detail')->first();
			$merchant_data = Merchant::where('id',$merchant_id)->with('detail')->first();			
			$user_push_status = $user_data['detail']['push_status'];
			$merchant_push_status = $merchant_data['detail']['push_status'];
			
	    	if(Appointment::where(['staff_id'=>$request->staff_id,'appointment_date_time'=>$this->convertToLocalTime($request->appointment_date_time,$request->timezone)])->where('appointment_status',1)->first() == null){
		    	if($this->addInterval($request->appointment_date_time,180000) >= $this->addInterval($this->convertToTimestamp($this->getCurrentTime($request->timezone),$request->timezone),18000)) {
			    	if($appoientment_data = Appointment::create($appointment)) {
			    		$appoientment_id = $appoientment_data['id'];
			    		$appointment_services = array_map(function($services) use ($appoientment_id){
				    		$services['appointment_id'] = $appoientment_id;			    		
				    		return $services;
				    	},$services);				    	
			    		if(AppointmentDetail::insert($appointment_services)){
			    			
						    if($user_push_status == 1){
						    	$this->pushNotification($user_notification,1,$appoientment_id);
						    }	

						    if($merchant_push_status == 1){
						    	$this->pushNotification($merchant_notification,1,$appoientment_id);
						    }					    
			    			
			    			return response()->json(['message' => 'Appointment added successfully'], $this->success_code);		
			    		}
			    		else{
			    			return response()->json(['message' => 'Unable to add data into appointment detail table'], $this->error_code);
			    		}
			    	}
			    	else{
			    		return response()->json(['message' => 'Unable to add data into appointment table'], $this->error_code);
			    	}
			    }
			    else{
			    	$response =  response()->json(['message' => 'Appointment date time should be equal or greater than current date time'], $this->error_code);
			    }			
	    	}
	    	else{
				$response =  response()->json(['message' => 'Time slot is not available'], $this->error_code);
			}		
		return $response;
    }    

    public function cancel(Request $request){    	
		$validator = Validator::make(
        $request->all(),
            [
            	'appointment_id' => 'required',            	
            	'timezone' => 'required'
            ]
        );	        
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }        
        $message = (isset($request->msg) && $request->msg != null) ? $request->msg : null;	        
        $timezone = $request->timezone;
		$user_id = "";
		$merchant_id = "";
		$current_date_time = $this->getCurrentTime($timezone);
				
		if(strpos($request->route()->uri, 'merchant') !== false){
			$cancel_by = $merchant_id = Auth::id();		
			$merchant_data = Merchant::where('id',$merchant_id)->with('detail')->first();		
			// echo json_encode($merchant_data);
			// die();
			$appointment = Appointment::select('user_id','appointment_date_time')->where(['id'=>$request->appointment_id,'merchant_id'=>$merchant_id])->first();
			
			$appointment_date_time = $appointment['appointment_date_time'];
			$appointment_date_time = date("d-M-Y h:i A", strtotime($appointment_date_time));
			
			$business_name = $merchant_data['detail']['merchant_business_name'];
			// echo json_encode($business_name);
			// die();

			if($appointment != null){
				$user_id = $appointment->user_id;
				$user_data = User::where('id',$user_id)->with('detail')->first();
				$username = $user_data['detail']['name'];


				$merchant_notification = [
		    								'title'     => "Appointment Cancelled Successfully",
					                        'body'      => "Appointment for $appointment_date_time has been cancelled successfully with $username",
					                        'user_id'  	=>  $merchant_id,
					                        'user_type'	=> 1
		    							];
		    	$user_notification = 	[
		    								'title'     => "Appointment Cancelled",
					                        'body'      => "Your appointment for $appointment_date_time has been cancelled by the $business_name",
					                        'user_id'  	=>  $user_id,
					                        'user_type'	=> 0
		    							];

				// $notification_list =[
				// 			[
				// 				'title'     => "Appointment Cancelled",
				  //                       'body'      => "Your appointment for $appointment_date_time has been cancelled by the $business_name ",
				  //                       'user_id'  	=>  $user_id,
				  //                       'user_type'	=> 0
						// 			],
						// 			[
						// 				'title'     => "Appointment Cancelled Successfully",
				  //                       'body'      => "Appointment for $appointment_date_time has been cancelled successfully with $username",
				  //                       'user_id'  	=>  $merchant_id,
				  //                       'user_type'	=> 1
				// 			]
				// 		];

			// 	echo json_encode($merchant_notification);
			// 	echo "<br>";
			// 	echo json_encode($user_notification);
			// die();
			}
			else{
				return $response = response()->json(['message' => 'You are not authorized to cancel this appointment'], $this->error_code);
			}							
		}
		else{
			$cancel_by = $user_id = Auth::id();
		
			$user_data = User::where('id',$user_id)->with('detail')->first();
			
			$appointment = Appointment::select('merchant_id','appointment_date_time')->where('id',$request->appointment_id)->where('user_id',$user_id)->first();
		
			$appointment_date_time = $appointment['appointment_date_time'];
			$appointment_date_time = date("d-M-Y h:i A", strtotime($appointment_date_time));
			$username = $user_data['detail']['name'];			
						
			if($appointment != null){
				$merchant_id = $appointment->merchant_id;

				$merchant_notification = [
											'title'     => "Cancel Appointment",
					                        'body'      => "$username has cancelled its Appointment due at $appointment_date_time",
					                        'user_id'  	=>  $merchant_id,
					                        'user_type'	=> 1
										];
		    	$user_notification = 	[
											'title'     => "Appointment Cancelled Successfully",
					                        'body'      => "Your appointment for $appointment_date_time has been cancelled successfully",
					                        'user_id'  	=>  $user_id,
					                        'user_type'	=> 0
										];

				// $notification_list =[
				// 			[
				// 				'title'     => "Appointment Cancelled Successfully",
				  //                       'body'      => "Your appointment for $appointment_date_time has been cancelled successfully",
				  //                       'user_id'  	=>  $user_id,
				  //                       'user_type'	=> 0
						// 			],
						// 			[
						// 				'title'     => "Cancel Appointment",
				  //                       'body'      => "$username has cancelled its Appointment due at $appointment_date_time",
				  //                       'user_id'  	=>  $merchant_id,
				  //                       'user_type'	=> 1
				// 			]
				// 		];

		
			}
			else{
				return $response = response()->json(['message' => 'You are not authorized to cancel this appointment'], $this->error_code);
			}
		}
		
		$user_data = User::where('id',$user_id)->with('detail')->first();
		
		$merchant_data = Merchant::where('id',$merchant_id)->with('detail')->first();			
		$user_push_status = $user_data['detail']['push_status'];
		$merchant_push_status = $merchant_data['detail']['push_status'];
		
        $response = $options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id'=>$merchant_id,'id'=>$request->appointment_id] : ['user_id'=>$user_id,'id'=>$request->appointment_id];			
		$appointment = Appointment::where($options)->first();
			
		if($appointment != null){				
			$appointment_date_time = $appointment->appointment_date_time;		
			if($appointment_date_time == null){
				return response()->json(['message' => 'Soemthing went wrong while comparing datetime'], $this->error_code);
			}
			if($this->getDateTimeDiff($appointment_date_time,$current_date_time) > 30) {
				if(Appointment::where($options)->update(['appointment_status'=>0,'cancel_message'=>$message,'cancel_by'=>$cancel_by])){	        	                    

	                if($user_push_status == 1){
				    	$this->pushNotification($user_notification,1,$request->appointment_id);
				    }	

				    if($merchant_push_status == 1){
				    	$this->pushNotification($merchant_notification,1,$request->appointment_id);
				    }

		        	$response = response()->json(['message' => 'Appointment cancelled successfully'], $this->success_code);
		        }
		        else{
		        	$response = response()->json(['message' => 'Unable to cancel appointment'], $this->error_code);	
		        }
		    }
		    else{
		    	$response = response()->json(['message' => 'You are only able to cancel appointment, before 30 mintue of appointment time.'], $this->error_code);	
		    }
		}
		else{
			$response = response()->json(['message' => 'You are no authenticate to cancel this appointment'], $this->error_code);
		}    	
    	return $response;
    }

    public function update(Request $request){
    	if(Auth::user()){
    		$validator = Validator::make(
            $request->all(),
	            [	            		            	
	            	'appointment_date_time' => 'required',
	            	'actual_price' => 'required',	            	
	            	'total_price' => 'required',	            	
	            	'services' => 'required',
	            	'timezone' => 'required',
	            ]
	        );
	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
	        }

	        $appointment = $request->all();	     

	        $appointment['merchant_id'] = $merchant_id =(strpos($request->route()->uri, 'merchant') !== false) ? Auth::id() : $request->merchant_id;
	    	$appointment['user_id'] = $user_id =(strpos($request->route()->uri, 'merchant') !== false) ? $request->user_id : Auth::id();   

	        $appointment['final_price'] = $appointment['total_price'];
	        $appointment['appointment_date_time'] =$this->convertToLocalTime($appointment['appointment_date_time'],$request->timezone);
	        $services = $appointment['services'];	        
	        unset($appointment['services']);
	        unset($appointment['appointment_id']);
	        unset($appointment['total_price']);
	        $service_list = array_map(function($services) use ($request){
	        		$services['appointment_id'] = $request['appointment_id'];
	        		return $services;
	        },$services);
	        if(Appointment::where(['id'=>$request->appointment_id,'user_id'=>$user_id])->first() != null){
		        if(Appointment::where(['id'=>$request->appointment_id,'user_id'=>$user_id])->update($appointment)){
		        	AppointmentDetail::where('appointment_id', $request->appointment_id)->delete();
		        	if(AppointmentDetail::insert($service_list)){
		        		$response = response()->json(['message' => 'Appointment updated successfully'], $this->success_code);
		        	}
		        }
		        else{
		        	$response = response()->json(['message' => 'Something went wrong'], $this->error_code);
		        }	        
		    }
		    else{
		    	$response = response()->json(['message' => 'Appointment not found'], $this->error_code);
		    }
    	}
    	else{
    		$response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
    	}
    	return $response;
    }    

    public function delete(Request $request) {    	
    	if(Auth::user()){
			$id = Auth::id();
			$appointment_ids = explode(",",$request->appointment_id);
			
			if(Appointment::where('merchant_id',$id)
				->whereIN('id',$appointment_ids)
				->delete()){
    			$response = response()->json(['message' => 'Appointment deleted successfully'], $this->success_code);
    		}
    		else{
    			$response = response()->json(['message' => 'You are not authenticate to delete this appointment'], $this->error_code);
    		}
    	}
    	else{
    		$response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
    	}
    	return $response;
	}
	

    public function completeAppointment(Request $request) {    	
    	if(Auth::user()){
			$id = Auth::id();
			
			$validator = Validator::make(
				$request->all(),
					[	            		            	
						'appointment_id' => 'required',
						'timezone' => 'required',
					]
				);
				if ($validator->fails()) {
					return response()->json(['message' => $validator->errors()], $this->error_code);
				}

			$appointment_id = $request->appointment_id;
			if(Appointment::where('id',$appointment_id)
							->where('merchant_id',$id)
							->update(['appointment_status'=> 2])
				){
    			$response = response()->json(['message' => 'Appointment complete successfully'], $this->success_code);
    		}
    		else{
    			$response = response()->json(['message' => 'You are not authenticate to delete this appointment'], $this->error_code);
    		}
    	}
    	else{
    		$response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
    	}
    	return $response;
    }

    private function appointmentId(){
    	$randomNo = 'opayn'.$this->randomNo(6);
    	    	
		if(Appointment::where('appointment_id',$randomNo)->first() != null){
			$this->appointmentId();
		}
		else{
			return $randomNo;
		}    
	}
	

	public function userAppointmentList(Request $request){
    	if(Auth::user()){    	    		
				
			$validator = Validator::make(
	            $request->all(),
	            [	            	
	                'timezone' => 'required'
	            ]
	        );

	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
			}
			
		

			$timezone = $request->timezone;
			$current_date_time = $this->getCurrentTime($timezone);
			
    		$options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id'=>Auth::id()] : ['user_id'=>Auth::id()];    	

    		$columns =['id','merchant_id','staff_id','user_id','appointment_date_time','actual_price','final_price','client_message','appointment_status'];
    		
    		$appointments = Appointment::where($options)->select($columns)->whereHas('merchantDetail')->with('detail','merchantDetail','merchantAddress','staffDetail','userDetail')->get()->toArray();  	
	
    		$list = $appointment_list = array_map(function($appointments) use($timezone){
    								$appointments['client_message'] = ($appointments['client_message'] == null) ? "" : $appointments['client_message'];
    								$address = $appointments['merchant_address'];    								    								
    								$appointments['appointment_date_time'] = $this->convertToTimestamp($appointments['appointment_date_time'],$timezone);
    								$appointments['merchant_business_name'] = $appointments['merchant_detail']['merchant_business_name'];    								
    								$appointments['address'] = $address['address1']." ".$address['address2']." ".$address['city']." ".$address['postcode'];
    								$appointments['lat'] = (double)$address['lat'];
    								$appointments['lng'] = (double)$address['lng'];
    								$appointments['staff_name'] =  $appointments['staff_detail']['merchant_name'];
    								$appointments['user_name'] =  $appointments['user_detail']['name'];
    								// $appointments['status'] =  $appointments['appointment_status'];
    								$appointments['appointment_end_time'] = $appointments['appointment_date_time'];

    								$services = $appointments['detail'];
    								$service_list = array_map(function($services){        										
    									return $services['name'];
									},$services);

    								$end_time = 0;
									foreach ($services as $service) {
										$end_time += $service['service_duration'];
									}
								
									$appointments['appointment_end_time'] = $appointments['appointment_end_time'] + $this->minsToMiliseconds($end_time);
									$appointments['services'] =  implode(", " ,$service_list);

    								$unset_vars = ['merchant_detail','merchant_address','staff_detail','detail','user_detail'];
    								foreach ($unset_vars as $key) {
    									unset($appointments[$key]);
    								}    								
    								return $appointments;
								},$appointments);

							
								
    		if(isset($request->timestamp) && $request->timestamp != null){
    			$list = array_filter($appointment_list,function($appointment) use ($request){    				
    					if($this->convertToTime($appointment['appointment_date_time']) == $this->convertToTime($request->timestamp)){
    						return true;
    					}					
    			});
			}
		
    		$data = ['upcoming'=>[],'finished'=>[]];
			foreach($appointment_list as $appointments){
				foreach ($list as $value) {
					$current_time = $this->convertToTimestamp($current_date_time,$timezone);
				($value['appointment_end_time']) > $current_time  && $value['appointment_status'] == 1 ? array_push($data['upcoming'], $value) : array_push($data['finished'], $value);
				  }
			}

	

			$finished =  array_values(array_unique($data['finished'],SORT_REGULAR));
			$upcoming =  array_values(array_unique($data['upcoming'],SORT_REGULAR));
			

		

			$upcoming_columns = array_column($upcoming, 'appointment_date_time');                     
			array_multisort($upcoming_columns, SORT_ASC, $upcoming);
			
			$finished_columns = array_column($finished, 'appointment_date_time');                     
			array_multisort($finished_columns, SORT_DESC, $finished);
			
			
			$finished =  $finished;
			$upcoming =  $upcoming;
			
			$finished_status = array();
			$upcoming_status = array();
			foreach($finished as $finish){
				if($finish['appointment_status'] == 1){
					$finish['appointment_status'] = 3;
				}
				array_push($finished_status,$finish);
			} 
			$upcoming_status = array_filter($upcoming,function($appointment) use ($request){    				
					if($appointment['appointment_status'] == 1){
						return true;
					}					
			});
			
			
			$data['finished'] = array_values($finished_status);
			$data['upcoming'] = array_values($upcoming_status);
			$response = response()->json(['message' => 'Appointment list','data'=>$data], $this->success_code);
    	}
    	else{
    		$response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
    	}
    	return $response;
    }

	public function merchantAppointment(Request $request){
    	if(Auth::user()){    	    		
				
			$validator = Validator::make(
	            $request->all(),
	            [	            	
	                'timezone' => 'required'
	            ]
	        );

	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
			}
			
			// if((empty($request->timestamp)) && (empty($request->date)) ){
			// 	return $response = response()->json(['message' => 'Please provide  atleast one param time or datetime'], $this->error_code);	
			// }

			$timezone = $request->timezone;
			$current_date_time = $this->getCurrentTime($timezone);

			$current_date_time = date("Y-m-d",strtotime($current_date_time));
			
			$start_date = $current_date_time. " 00:00:00";
		
			if(isset($request->start_date) || $request->start_date != null ){
					$start_date = $this->convertToLocalTime($request->start_date,$timezone); //." 00:00:00" ;
					$start_date = date("Y-m-d",strtotime($start_date));
					$start_date = $start_date." 00:00:00";
			}
			
			
			$end_date = $start_date;
			$end_date = strtotime($end_date);
			$end_date = strtotime("+7 day", $end_date);
			$end_date =  date("Y-m-d", $end_date)." 23:59:59";
			
		
    		$options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id'=>Auth::id()] : ['user_id'=>Auth::id()];    	
		
			
    		$columns =['id','merchant_id','staff_id','user_id','appointment_date_time','actual_price','final_price','client_message','appointment_status'];
    		
			$appointments = Appointment::where($options)
										->select($columns)
										->whereHas('merchantDetail')->with('detail','merchantDetail','merchantAddress','userDetail')
										->whereBetween('appointment_date_time',[$start_date,$end_date])
										->get()->toArray();  
			
			$list = $appointment_list = array_map(function($appointments) use($timezone){
    								$appointments['client_message'] = ($appointments['client_message'] == null) ? "" : $appointments['client_message'];
    								$address = $appointments['merchant_address'];    								    								
    								$appointments['appointment_date_time'] = $this->convertToTimestamp($appointments['appointment_date_time'],$timezone);
    								$appointments['merchant_business_name'] = $appointments['merchant_detail']['merchant_business_name'];    								
    								$appointments['address'] = $address['address1']." ".$address['address2']." ".$address['city']." ".$address['postcode'];
    								$appointments['lat'] = (double)$address['lat'];
    								$appointments['lng'] = (double)$address['lng'];
    								$appointments['staff_name'] =  $appointments['merchant_detail']['merchant_name'];
    								$appointments['user_name'] =  $appointments['user_detail']['name'];
    								$appointments['status'] =  $appointments['appointment_status'];
    								$appointments['appointment_end_time'] = $appointments['appointment_date_time'];

    								$services = $appointments['detail'];
    								$service_list = array_map(function($services){        										
    									return $services['name'];
									},$services);

    								$end_time = 0;
									foreach ($services as $service) {
										$end_time += $service['service_duration'];
									}
									$appointments['appointment_end_time'] = $appointments['appointment_end_time'] + $this->minsToMiliseconds($end_time);
									$appointments['services'] =  implode(", " ,$service_list);

    								$unset_vars = ['merchant_detail','merchant_address','detail','user_detail'];
    								foreach ($unset_vars as $key) {
    									unset($appointments[$key]);
    								}    								
    								return $appointments;
								},$appointments);
								
								$final_list = [];
			
								$current_date_time = $this->getCurrentTime($timezone);
								$current_time = $this->convertToTimestamp($current_date_time,$timezone);
								
								foreach($list as $lists){
									if($lists['appointment_end_time'] < $current_time && $lists['status'] == 1){
											$lists['status'] = 3;		
									}
									array_push($final_list,$lists);	
									
								}
			
			
			if(isset($request->client_id) && $request->client_id != null ){
				$final_list = array_filter($final_list,function($appointment) use ($request){
						if($appointment['user_id'] == $request->client_id && $appointment['merchant_id'] == Auth::id() ){
							return true;
							 
						}
				});
				
			}   

		
			//  filter base on staff id
			
			$staff_id = $request->staff_id;
			
			if(isset($request->staff_id) && $request->staff_id != null){
				$final_list = array_filter($final_list,function($appointment) use ($request,$staff_id){
					if($appointment['staff_id'] == $staff_id && $appointment['merchant_id'] == Auth::id() ){
						return true;
						 
					}
						
					});
			}
		

			$data = array_column($final_list, 'appointment_date_time');                     
			
			 array_multisort($data, SORT_ASC, $final_list);

			
			
			
			
				// filter based on appointment stats 
			
			$appointment_status = $request->status;
			if(isset($request->status) && $request->status != null){
				$final_list = array_filter($final_list,function($appointment) use ($request,$appointment_status){
					if($appointment['status'] == $appointment_status && $appointment['merchant_id'] == Auth::id() ){
						return true;
						 
					}		
					});
			}		

			$data = $final_list;

			$result  = [];
			$result1  = [];
			foreach($final_list as $key => $value){
				$date = $this->convertToDate($value['appointment_date_time'],$timezone);
				// $date = $value['appointment_date_time'];
				
				
				$data = [
					'date'  => $date,
					'timestamp' => $value['appointment_date_time'],
					'appointments' => [$value]
				];
				if (empty($result)) {
						
					array_push($result1,$data);
					array_push($result,$date);
				}else {
					if(in_array($date,$result)){
						$index = array_search($date, $result);
						array_push($result1[$index]['appointments'],$value);
						
					} else {
						array_push($result1,$data);
						array_push($result,$date);
					}
				}
				

				// echo $date;
				// $result[$date][] = $value;
				// $result['date'] = $date;
				// $result['aapointments'][] = $value;
				// $result[$date][] = $result;
				
				// array_push($result1,$result);
			}
			// die;

				// echo json_encode($result);
				// die();
				$data = $result1;
			$response = response()->json(['message' => 'Appointment list','data'=>$data], $this->success_code);
    	}
    	else{
    		$response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
    	}
    	return $response;
    }


	// public function merchantAppointment(Request $request){
    // 	if(Auth::user()){    	    		
				
	// 		$validator = Validator::make(
	//             $request->all(),
	//             [	            	
	//                 'timezone' => 'required'
	//             ]
	//         );

	//         if ($validator->fails()) {
	//             return response()->json(['message' => $validator->errors()], $this->error_code);
	// 		}
			
	// 		// if((empty($request->timestamp)) && (empty($request->date)) ){
	// 		// 	return $response = response()->json(['message' => 'Please provide  atleast one param time or datetime'], $this->error_code);	
	// 		// }

	// 		$timezone = $request->timezone;
	// 		$current_date_time = $this->getCurrentTime($timezone);

	// 		$current_date_time = date("Y-m-d",strtotime($current_date_time));
			
	// 		$start_date = $current_date_time. " 00:00:00";
			
	// 		if(isset($request->start_date) || $request->start_date != null ){
	// 				$start_date = $this->convertToDate($request->start_date,$timezone)." 00:00:00" ;
				
	// 		}
		
	// 		$end_date = $start_date;
	// 		$end_date = strtotime($end_date);
	// 		$end_date = strtotime("+7 day", $end_date);
	// 		$end_date =  date("Y-m-d", $end_date)." 23:59:59";
		
		
    // 		$options = (strpos($request->route()->uri, 'merchant') !== false) ? ['staff_id'=>Auth::id()] : ['user_id'=>Auth::id()];    	
				
    // 		$columns =['id','merchant_id','staff_id','user_id','appointment_date_time','actual_price','final_price','client_message','appointment_status'];
    // 		// dd($columns);
	// 		$appointments = Appointment::where($options)
	// 									->select($columns)
	// 									->whereHas('merchantDetail')->with('detail','merchantDetail','merchantAddress','userDetail')
	// 									->whereBetween('appointment_date_time',[$start_date,$end_date])
	// 									->get()->toArray();  	
			
    // 		$list = $appointment_list = array_map(function($appointments) use($timezone){
    // 								$appointments['client_message'] = ($appointments['client_message'] == null) ? "" : $appointments['client_message'];
    // 								$address = $appointments['merchant_address'];    								    								
    // 								$appointments['appointment_date_time'] = $this->convertToTimestamp($appointments['appointment_date_time'],$timezone);
    // 								$appointments['merchant_business_name'] = $appointments['merchant_detail']['merchant_business_name'];    								
    // 								$appointments['address'] = $address['address1']." ".$address['address2']." ".$address['city']." ".$address['postcode'];
    // 								$appointments['lat'] = (double)$address['lat'];
    // 								$appointments['lng'] = (double)$address['lng'];
    // 								$appointments['staff_name'] =  $appointments['merchant_detail']['merchant_name'];
    // 								$appointments['user_name'] =  $appointments['user_detail']['name'];
    // 								$appointments['status'] =  $appointments['appointment_status'];
    // 								$appointments['appointment_end_time'] = $appointments['appointment_date_time'];

    // 								$services = $appointments['detail'];
    // 								$service_list = array_map(function($services){        										
    // 									return $services['name'];
	// 								},$services);

    // 								$end_time = 0;
	// 								foreach ($services as $service) {
	// 									$end_time += $service['service_duration'];
	// 								}
	// 								$appointments['appointment_end_time'] = $appointments['appointment_end_time'] + $this->minsToMiliseconds($end_time);
	// 								$appointments['services'] =  implode(", " ,$service_list);

    // 								$unset_vars = ['merchant_detail','merchant_address','detail','user_detail'];
    // 								foreach ($unset_vars as $key) {
    // 									unset($appointments[$key]);
    // 								}    								
    // 								return $appointments;
	// 							},$appointments);
								
	// 							$final_list = [];
			
	// 							$current_date_time = $this->getCurrentTime($timezone);
	// 							$current_time = $this->convertToTimestamp($current_date_time,$timezone);
								
	// 							foreach($list as $lists){
	// 								if($lists['appointment_end_time'] < $current_time && $lists['status'] == 1){
	// 										$lists['status'] = 3;		
	// 								}
	// 								array_push($final_list,$lists);	
									
	// 							}
			
			
	// 		if(isset($request->client_id) && $request->client_id != null ){
	// 			$final_list = array_filter($final_list,function($appointment) use ($request){
	// 					if($appointment['user_id'] == $request->client_id && $appointment['merchant_id'] == Auth::id() ){
	// 						return true;
							 
	// 					}
	// 			});
				
	// 		}   

		
	// 		//  filter base on staff id
			
	// 		$staff_id = $request->staff_id;
			
	// 		if(isset($request->staff_id) && $request->staff_id != null){
	// 			$final_list = array_filter($final_list,function($appointment) use ($request,$staff_id){
	// 				if($appointment['staff_id'] == $staff_id && $appointment['merchant_id'] == Auth::id() ){
	// 					return true;
						 
	// 				}
						
	// 				});
	// 		}
		

	// 		$data = array_column($final_list, 'appointment_date_time');                     
			
	// 		 array_multisort($data, SORT_ASC, $final_list);

			
			
			
			
	// 			// filter based on appointment stats 
			
	// 		$appointment_status = $request->status;
	// 		if(isset($request->status) && $request->status != null){
	// 			$final_list = array_filter($final_list,function($appointment) use ($request,$appointment_status){
	// 				if($appointment['status'] == $appointment_status && $appointment['merchant_id'] == Auth::id() ){
	// 					return true;
						 
	// 				}		
	// 				});
	// 		}		

	// 		$data = $final_list;
	
	// 		$response = response()->json(['message' => 'Appointment list','data'=>array_values($data)], $this->success_code);
    // 	}
    // 	else{
    // 		$response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
    // 	}
    // 	return $response;
    // }

}