<?php

namespace App\Http\Controllers\v3;

use Auth;
use App\Report;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'action' => 'required',
                'email' => 'required',
                'title' => 'required',
                'message' => 'required'                
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }
        $response = 'okay';
        $type = $request->action;
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['from'] = $from = $request->email;
        $send_to = 'gurpreetsingh.qualhon@gmail.com';
        
        switch ($type) {
            case 'issue':
                $data['status'] = 0;
                $data['type'] = 1;                                              
                if(Report::create($data)){
                    $this->sendMail($send_to,$from, $request->title, $request->message);
                    $response = response()->json(['message'=>'Report sent successfully'], $this->success_code);   
                }
                else{
                    $response = response()->json(['message'=>'Something went wrong'], $this->error_code);   
                }
                break;
            case 'abuse':
                $validator = Validator::make(
                $request->all(),
                    [ 'merchant_id' => 'required']
                );

                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], $this->error_code);
                }
                $data['status'] = 0;
                $data['type'] = 2;                
                $data['merchant_id'] = $request->merchant_id;
                if(Report::create($data)){
                    $this->sendMail($send_to,$from, $request->title, $request->message);
                    $response = response()->json(['message'=>'Report sent successfully'], $this->success_code);   
                }
                else{
                    $response = response()->json(['message'=>'Something went wrong'], $this->error_code);   
                }
                break;
            default:
                $response = response()->json(['message'=>'Please enter valid input'], $this->error_code);   
                break;
        }

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
