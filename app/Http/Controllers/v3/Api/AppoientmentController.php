<?php
namespace App\Http\Controllers\v3\Api;

use App\Appointment;
use App\AppointmentCustomer;
use App\AppointmentDetail;
use App\Http\Controllers\Controller;
use App\Merchant;
use App\MerchantDetail;
use App\MerchantUser;
use App\ReviewRating;
use App\Traits\PushNotificationTrait;
use App\User;
use App\UsersDetail;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AppoientmentController extends Controller
{
    use PushNotificationTrait;

    public function index(Request $request)
    {
        if (Auth::user()) {

            $validator = Validator::make(
                $request->all(),
                [
                    'timezone' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            // if((empty($request->timestamp)) && (empty($request->date)) ){
            //     return $response = response()->json(['message' => 'Please provide  atleast one param time or datetime'], $this->error_code);
            // }

            $timezone = $request->timezone;
            $current_date_time = $this->getCurrentTime($timezone);
            $merchant_user_type_with_id = (Auth::user()->merchant_user_type == 1) ? ['merchant_id' => Auth::id()] : ['staff_id' => Auth::id()];

            $options = (strpos($request->route()->uri, 'merchant') !== false) ? $merchant_user_type_with_id : ['user_id' => Auth::id()];

            $columns = ['id', 'merchant_id', 'staff_id', 'user_id', 'appointment_date_time', 'actual_price', 'final_price', 'client_message', 'appointment_status', 'appointment_service_type'];

            $appointments = Appointment::where($options)->select($columns)->whereHas('merchantDetail')->with('detail', 'merchantDetail', 'merchantAddress', 'staffDetail', 'userDetail')->get()->toArray();
            // echo json_encode($appointments);
            // die();
            $list = $appointment_list = array_map(function ($appointments) use ($timezone) {
                $appointments['client_message'] = ($appointments['client_message'] == null) ? "" : $appointments['client_message'];
                $address = $appointments['merchant_address'];
                $appointments['appointment_date_time'] = $this->convertToTimestamp($appointments['appointment_date_time'], $timezone);
                $appointments['merchant_business_name'] = $appointments['merchant_detail']['merchant_business_name'];
                $appointments['address'] = $address['address1'] . " " . $address['address2'] . " " . $address['city'] . " " . $address['postcode'];
                $appointments['lat'] = (double) $address['lat'];
                $appointments['lng'] = (double) $address['lng'];
                $appointments['staff_name'] = $appointments['staff_detail']['merchant_name'];
                $appointments['user_name'] = $appointments['user_detail']['name'];
                // $appointments['status'] =  $appointments['appointment_status'];
                $appointments['appointment_end_time'] = $appointments['appointment_date_time'];
                $appointments['appointment_service_type'] = $appointments['appointment_service_type'];

                $services = $appointments['detail'];
                $service_list = array_map(function ($services) {
                    return $services['name'];
                }, $services);

                $end_time = 0;
                foreach ($services as $service) {
                    $end_time += $service['service_duration'];
                }
                $appointments['appointment_end_time'] = $appointments['appointment_end_time'] + $this->minsToMiliseconds($end_time);
                $appointments['services'] = implode(", ", $service_list);

                $unset_vars = ['merchant_detail', 'merchant_address', 'detail', 'staff_detail', 'user_detail'];
                foreach ($unset_vars as $key) {
                    unset($appointments[$key]);
                }
                return $appointments;
            }, $appointments);

            if (isset($request->timestamp) && $request->timestamp != null) {
                $list = array_filter($appointment_list, function ($appointment) use ($request) {
                    if ($this->convertToTime($appointment['appointment_date_time']) == $this->convertToTime($request->timestamp)) {
                        return true;
                    }
                });
            }
            // filter based on date
            if (isset($request->date) && $request->date != null) {
                $list = array_filter($appointment_list, function ($appointment) use ($request) {
                    if ($this->convertToDate($appointment['appointment_date_time']) == $this->convertToDate($request->date, $request->timezone)) {
                        return true;
                    }
                });

                // // filter based on client id
                if (isset($request->client_id) && $request->client_id != null) {
                    $list = array_filter($list, function ($appointment) use ($request) {
                        if ($appointment['user_id'] == $request->client_id && $appointment['merchant_id'] == Auth::id()) {
                            return true;

                        }
                    });

                }
            } else {
                // // filter based on client id
                if (isset($request->client_id) && $request->client_id != null) {
                    $list = array_filter($appointment_list, function ($appointment) use ($request) {
                        if ($appointment['user_id'] == $request->client_id && $appointment['merchant_id'] == Auth::id()) {
                            return true;

                        }
                    });

                }
            }

            // // // filter base on staff id
            $staff_ids = [];
            $staff_id = $request->staff_id;
            $staff_ids = explode(",", $staff_id);
            // filter base on client id
            if (isset($request->staff_id) && $request->staff_id != null) {
                $list = array_filter($list, function ($appointment) use ($request, $staff_ids) {
                    foreach ($staff_ids as $value) {
                        if ($appointment['staff_id'] == $value && $appointment['merchant_id'] == Auth::id()) {
                            return true;

                        }

                    }

                });
            }
            // filter based on appointment stats
            $appointment_status = [];
            $appintment_st = $request->status;
            $appointment_status = explode(",", $appintment_st);

            if (isset($request->status) && $request->status != null) {
                $list = array_filter($list, function ($appointment) use ($request, $appointment_status) {
                    foreach ($appointment_status as $values) {
                        if ($appointment['appointment_status'] == $values && $appointment['merchant_id'] == Auth::id()) {
                            return true;
                        }
                    }
                });
            }

            $data = ['upcoming' => [], 'finished' => []];

            foreach ($list as $value) {
                $current_time = $this->convertToTimestamp($current_date_time, $timezone);

                ($value['appointment_end_time']) > $current_time && $value['appointment_status'] == 1 ? array_push($data['upcoming'], $value) : array_push($data['finished'], $value);
            }

            $upcoming_columns = array_column($data['upcoming'], 'appointment_date_time');
            array_multisort($upcoming_columns, SORT_ASC, $data['upcoming']);

            $finished_columns = array_column($data['finished'], 'appointment_date_time');
            array_multisort($finished_columns, SORT_DESC, $data['finished']);

            $finished = $data['finished'];

            $upcoming = $data['upcoming'];

            $finished_status = array();
            $upcoming_status = array();

            foreach ($finished as $finish) {
                if ($finish['appointment_status'] == 1) {
                    $finish['appointment_status'] = 3;
                }
                array_push($finished_status, $finish);
            }
            $upcoming_status = array_filter($upcoming, function ($appointment) use ($request) {
                if ($appointment['appointment_status'] == 1) {
                    return true;
                }
            });

            $data['finished'] = array_values($finished_status);
            $data['upcoming'] = array_values($upcoming_status);
            $response = response()->json(['message' => 'Appointment list', 'data' => $data], $this->success_code);
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function show(Request $request)
    {
        if (Auth::user()) {
            $validator = Validator::make(
                $request->all(),
                [
                    'appointment_id' => 'required',
                    'timezone' => 'required',

                ]
            );
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            $timezone = $request->timezone;

            $merchant_options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id' => Auth::id(), 'id' => $request->appointment_id] : ['user_id' => Auth::id(), 'id' => $request->appointment_id];

            $review_options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id' => Auth::id(), 'id' => $request->appointment_id] : ['user_id' => Auth::id(), 'appointment_id' => $request->appointment_id];
          
            $columns = ['id', 'merchant_id', 'staff_id', 'user_id', 'appointment_date_time', 'appointment_status', 'actual_price', 'final_price', 'client_message', 'cancel_message', 'cancel_by', 'cancel_by_id', 'appointment_service_type'];
            $appointment = Appointment::select($columns)->where($merchant_options)->with('detail', 'merchantDetail', 'staffDetail', 'userDetail.user', 'customerDetail')->first();
            $rating = ReviewRating::where($review_options)->first();
         
            $cancel_user_type = null;
            $cancel_user_name = null;

        
            if ($appointment != null) {

                if ($appointment['appointment_status'] == 0) {
                    // print_r($appointment['cancel_by_id']);
                    // die;
                    if ($appointment['cancel_by_id'] != " " && $appointment['cancel_by_id'] != null) {

                        switch ($appointment['cancel_by']) {
                            case 0:
                                $cancel_user_type = 'user';
                                $cancel_user_name = UsersDetail::select('name')->where('user_id', $appointment['cancel_by_id'])->first();
                                $cancel_user_name = $cancel_user_name->name;
                                break;
                            case 1:
                                $cancel_user_type = 'merchant';
                                $cancel_user_name = MerchantDetail::select('merchant_name', 'merchant_company_name')->where('merchant_id', $appointment['cancel_by_id'])->first();
                                $cancel_user_name = $cancel_user_name->merchant_company_name;
                                break;
                            default:
                                $cancel_user_type = null;
                                $cancel_user_name = null;
                                break;
                        }
                    } else {

                        $cancel_user_type = null;
                        $cancel_user_name = null;

                    }

                }

                // echo json_encode($appointment->userDetail->user_image);
                // die();

                $appointment['rating_status'] = ($rating != null) ? true : false;

                $appointment['name'] = $appointment->staffDetail['merchant_name'];
                $appointment['cancel_message'] = ($appointment['cancel_message'] == null) ? "" : $appointment['cancel_message'];
                $appointment['client_message'] = ($appointment['client_message'] == null) ? "" : $appointment['client_message'];
                // $appointment['user_name'] = $appointment->userDetail['name'];
                $appointment['user_email'] = $appointment->userDetail['email'];
                $appointment['users_allergies'] = $appointment->userDetail['allergies'];
                // $appointment['user_mobile'] = $appointment->userDetail['user']['mobile'];
                $appointment['total_price'] = $appointment['final_price'];
                $appointment['image'] = ($appointment->userDetail['user_image'] == null) ? $this->default_profile_image : $appointment->userDetail['user_image'];
                $appointment['appointment_date_time'] = $this->convertToTimestamp($appointment['appointment_date_time'], $timezone);
                $appointment['appointment_end_time'] = $appointment['appointment_date_time'];
                $appointment['cancel_by'] = $cancel_user_type;
                $appointment['cancel_by_user_name'] = $cancel_user_name;

                $appointment['customer_name'] = (!empty($appointment->customerDetail['customer_name'])) ? $appointment->customerDetail['customer_name'] : null;
                $appointment['customer_mobile'] = (!empty($appointment->customerDetail['customer_mobile'])) ? $appointment->customerDetail['customer_mobile'] : null;
                $appointment['customer_address'] = (!empty($appointment->customerDetail['customer_address'])) ? $appointment->customerDetail['customer_address'] : null;
                $appointment['customer_landmark'] = (!empty($appointment->customerDetail['customer_landmark'])) ? $appointment->customerDetail['customer_landmark'] : null;
                $appointment['customer_postcode'] = (!empty($appointment->customerDetail['customer_postcode'])) ? (int) $appointment->customerDetail['customer_postcode'] : null;
                $appointment['customer_lat'] = (!empty($appointment->customerDetail['customer_lat'])) ? (int) $appointment->customerDetail['customer_lat'] : 0;
                $appointment['customer_lng'] = (!empty($appointment->customerDetail['customer_lng'])) ? (int) $appointment->customerDetail['customer_lng'] : 0;

                $services = $appointment->detail->toArray();
                $service_list = array_map(function ($services) {
                    unset($services['id']);
                    unset($services['appointment_id']);
                    return $services;
                }, $services);
                $appointment['services'] = $service_list;
                $end_time = 0;
                foreach ($services as $service) {
                    $end_time += $service['service_duration'];
                }
                $appointment['appointment_end_time'] = $appointment['appointment_end_time'] + $this->minsToMiliseconds($end_time);
                unset($appointment['detail']);
                unset($appointment->merchantDetail);
                unset($appointment->userDetail);
                unset($appointment->customerDetail);
                unset($appointment['cancel_by_id']);

                $response = response()->json(['message' => 'Appointment Detail', 'data' => $appointment], $this->success_code);
            } else {
                $response = response()->json(['message' => 'Invalid appointment ID'], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function create(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'appointment_date_time' => 'required',
                'actual_price' => 'required',
                'total_price' => 'required',
                'services' => 'required',
                'timezone' => 'required',
                'appointment_service_type' => 'required|integer|between:1,3',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

        $data = $request->all();
        if (gettype($data['services']) == 'string') {
            $services = $data['services'] = json_decode($data['services'], true);
        } else {
            $services = $data['services'];
        }

        unset($data['services']);        
        $appointment = $data;
        $appointment['merchant_id'] = $merchant_id = (strpos($request->route()->uri, 'merchant') !== false) ? Auth::id() : $request->merchant_id;
        $appointment['user_id'] = $user_id = (strpos($request->route()->uri, 'merchant') !== false) ? $request->user_id : Auth::id();
        $appointment['final_price'] = $data['total_price'];
        $appointment['appointment_date_time'] = $this->convertToLocalTime($data['appointment_date_time'], $request->timezone);

        $appointment['appointment_id'] = $this->appointmentId();

        $merchant_user = MerchantUser::where('merchant_id', $merchant_id)->where('user_id', $user_id)->first();
        $user = User::where('id', $user_id)->with('detail')->first();
        $merchant = Merchant::where('id', $merchant_id)->with('detail', 'address')->first();
        // echo json_encode($merchant['address']);
        // die();
        $username = $user['detail']['name'];
        $dateTime = $this->DateTimeFormat($appointment['appointment_date_time']);
        $merchant_business_name = $merchant['detail']['merchant_business_name'];

        $appointment['merchant_address'] = $merchant['address']['address1'];
        $appointment['merchant_lat'] = $merchant['address']['lat'];
        $appointment['merchant_lng'] = $merchant['address']['lng'];
        $appointment['appointment_service_type'] = $request->appointment_service_type;

        if (strpos($request->route()->uri, 'merchant')) {
            if ($merchant_user == null) {
                $data = [
                    'merchant_id' => $merchant_id,
                    'user_id' => $user_id,
                ];
                MerchantUser::create($data)->id;
            }
            $merchant_notification = [
                'title' => "Appointment Booked Successfully",
                'body' => "An appointment for $dateTime has been booked successfully with $username",
                'user_id' => $merchant_id,
                'user_type' => 1,
            ];
            $user_notification = [
                'title' => "Appointment Booked received",
                'body' => "Your appointment has been booked successfully with $merchant_business_name at $dateTime",
                'user_id' => $user_id,
                'user_type' => 0,
            ];

        } else {

            $merchant_notification = [
                'title' => "Appointment Booked received",
                'body' => "$username has booked a new appointment for $dateTime",
                'user_id' => $merchant_id,
                'user_type' => 1,
            ];
            $user_notification = [
                'title' => "Appointment Booked Successfully",
                'body' => "Your appointment has been booked successfully with $merchant_business_name for $dateTime",
                'user_id' => $user_id,
                'user_type' => 0,
            ];
        }

        $user_data = User::where('id', $user_id)->with('detail')->first();
        $merchant_data = Merchant::where('id', $merchant_id)->with('detail')->first();

        $user_push_status = $user_data['detail']['push_status'];
        $merchant_push_status = $merchant_data['detail']['push_status'];

        //if allergies is provide by user
        $allergies = null;
        if (!empty($request->allergies)) {
            $allergies = $request->allergies;
        }
        if (Appointment::where(['staff_id' => $request->staff_id, 'appointment_date_time' => $this->convertToLocalTime($request->appointment_date_time, $request->timezone)])->where('appointment_status', 1)->first() == null) {
            if ($this->addInterval($request->appointment_date_time, 180000) >= $this->addInterval($this->convertToTimestamp($this->getCurrentTime($request->timezone), $request->timezone), 18000)) {
                if ($appoientment_data = Appointment::create($appointment)) {
                    $appoientment_id = $appoientment_data['id'];
                    $curtomer_detail = [
                        'appointment_id' => $appoientment_id,
                        'customer_name' => $request->customer_name,
                        'customer_mobile' => $request->customer_mobile,
                        'customer_address' => $request->customer_address,
                        'customer_landmark' => $request->customer_landmark,
                        'customer_postcode' => $request->customer_postcode,
                        'customer_lat' => $request->customer_lat,
                        'customer_lng' => $request->customer_lng,
                    ];
                    //update user allergies in user detail table
                    $update_allergies = UsersDetail::where('user_id', $user_id)->update(['allergies' => $allergies]);
                    $customer_appoientment_data = AppointmentCustomer::create($curtomer_detail);
                    $appointment_services = array_map(function ($services) use ($appoientment_id) {
                        $services['appointment_id'] = $appoientment_id;
                        return $services;
                    }, $services);
                    if (AppointmentDetail::insert($appointment_services)) {

                        if ($user_push_status == 1) {
                            $this->pushNotification($user_notification, 1, $appoientment_id);
                        }

                        if ($merchant_push_status == 1) {
                            $this->pushNotification($merchant_notification, 1, $appoientment_id);
                        }

                        return response()->json(['message' => 'Appointment booked successfully'], $this->success_code);
                    } else {
                        return response()->json(['message' => 'Unable to add data into appointment detail table'], $this->error_code);
                    }
                } else {
                    return response()->json(['message' => 'Unable to add data into appointment table'], $this->error_code);
                }
            } else {
                $response = response()->json(['message' => 'Appointment date time should be equal or greater than current date time'], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'Time slot is not available'], $this->error_code);
        }
        return $response;
    }

    public function cancel(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'appointment_id' => 'required',
                'timezone' => 'required',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }
        $message = (isset($request->msg) && $request->msg != null) ? $request->msg : null;
        $timezone = $request->timezone;
        $user_id = "";
        $merchant_id = "";
        $current_date_time = $this->getCurrentTime($timezone);
        $user_type = ((strpos($request->route()->uri, 'merchant') !== false) ? 1 : 0);

        if (strpos($request->route()->uri, 'merchant') !== false) {
            $cancel_by = $merchant_id = Auth::id();
            $merchant_data = Merchant::where('id', $merchant_id)->with('detail')->first();
            // echo json_encode($merchant_data);
            // die();
            $appointment = Appointment::select('user_id', 'appointment_date_time')->where(['id' => $request->appointment_id, 'merchant_id' => $merchant_id])->first();

            $appointment_date_time = $appointment['appointment_date_time'];
            $appointment_date_time = date("d-M-Y h:i A", strtotime($appointment_date_time));

            $business_name = $merchant_data['detail']['merchant_business_name'];
            // echo json_encode($business_name);
            // die();

            if ($appointment != null) {
                $user_id = $appointment->user_id;
                $user_data = User::where('id', $user_id)->with('detail')->first();
                $username = $user_data['detail']['name'];

                $merchant_notification = [
                    'title' => "Appointment Cancelled Successfully",
                    'body' => "Appointment for $appointment_date_time has been cancelled successfully with $username",
                    'user_id' => $merchant_id,
                    'user_type' => 1,
                ];
                $user_notification = [
                    'title' => "Appointment Cancelled",
                    'body' => "Your appointment for $appointment_date_time has been cancelled by the $business_name",
                    'user_id' => $user_id,
                    'user_type' => 0,
                ];

                // $notification_list =[
                //             [
                //                 'title'     => "Appointment Cancelled",
                //                       'body'      => "Your appointment for $appointment_date_time has been cancelled by the $business_name ",
                //                       'user_id'      =>  $user_id,
                //                       'user_type'    => 0
                //             ],
                //             [
                //                 'title'     => "Appointment Cancelled Successfully",
                //                       'body'      => "Appointment for $appointment_date_time has been cancelled successfully with $username",
                //                       'user_id'      =>  $merchant_id,
                //                       'user_type'    => 1
                //             ]
                //         ];

                //     echo json_encode($merchant_notification);
                //     echo "<br>";
                //     echo json_encode($user_notification);
                // die();
            } else {
                return $response = response()->json(['message' => 'You are not authorized to cancel this appointment'], $this->error_code);
            }
        } else {
            $cancel_by = $user_id = Auth::id();

            $user_data = User::where('id', $user_id)->with('detail')->first();

            $appointment = Appointment::select('merchant_id', 'appointment_date_time')->where('id', $request->appointment_id)->where('user_id', $user_id)->first();

            $appointment_date_time = $appointment['appointment_date_time'];
            $appointment_date_time = date("d-M-Y h:i A", strtotime($appointment_date_time));
            $username = $user_data['detail']['name'];

            if ($appointment != null) {
                $merchant_id = $appointment->merchant_id;

                $merchant_notification = [
                    'title' => "Cancel Appointment",
                    'body' => "$username has cancelled its Appointment due at $appointment_date_time",
                    'user_id' => $merchant_id,
                    'user_type' => 1,
                ];
                $user_notification = [
                    'title' => "Appointment Cancelled Successfully",
                    'body' => "Your appointment for $appointment_date_time has been cancelled successfully",
                    'user_id' => $user_id,
                    'user_type' => 0,
                ];

                // $notification_list =[
                //             [
                //                 'title'     => "Appointment Cancelled Successfully",
                //                       'body'      => "Your appointment for $appointment_date_time has been cancelled successfully",
                //                       'user_id'      =>  $user_id,
                //                       'user_type'    => 0
                //             ],
                //             [
                //                 'title'     => "Cancel Appointment",
                //                       'body'      => "$username has cancelled its Appointment due at $appointment_date_time",
                //                       'user_id'      =>  $merchant_id,
                //                       'user_type'    => 1
                //             ]
                //         ];

            } else {
                return $response = response()->json(['message' => 'You are not authorized to cancel this appointment'], $this->error_code);
            }
        }

        $user_data = User::where('id', $user_id)->with('detail')->first();

        $merchant_data = Merchant::where('id', $merchant_id)->with('detail')->first();
        $user_push_status = $user_data['detail']['push_status'];
        $merchant_push_status = $merchant_data['detail']['push_status'];

        $response = $options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id' => $merchant_id, 'id' => $request->appointment_id] : ['user_id' => $user_id, 'id' => $request->appointment_id];
        $appointment_user = Appointment::where($options)->first();

        if ($appointment != null) {
            $appointment_date_time = $appointment->appointment_date_time;

            if ($appointment_date_time == null) {
                return response()->json(['message' => 'Soemthing went wrong while comparing datetime'], $this->error_code);
            }
            if ($this->getDateTimeDiff($appointment_date_time, $current_date_time) > 30) {
                if (Appointment::where($options)->update([
                    'appointment_status' => 0,
                    'cancel_message' => $message,
                    'cancel_by' => $user_type,
                    'cancel_by_id' => Auth::id(),
                ])) {

                    if ($user_push_status == 1) {
                        $this->pushNotification($user_notification, 1, $request->appointment_id);
                    }

                    if ($merchant_push_status == 1) {
                        $this->pushNotification($merchant_notification, 1, $request->appointment_id);
                    }

                    $response = response()->json(['message' => 'Appointment cancelled successfully'], $this->success_code);
                } else {
                    $response = response()->json(['message' => 'Unable to cancel appointment'], $this->error_code);
                }
            } else {
                $response = response()->json(['message' => 'You are only able to cancel appointment, before 30 mintue of appointment time.'], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'You are no authenticate to cancel this appointment'], $this->error_code);
        }
        return $response;
    }

    public function update(Request $request)
    {
        if (Auth::user()) {
            $validator = Validator::make(
                $request->all(),
                [
                    'appointment_date_time' => 'required',
                    'actual_price' => 'required',
                    'total_price' => 'required',
                    'services' => 'required',
                    'timezone' => 'required',
                ]
            );
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            $appointment = $request->all();

            $appointment['merchant_id'] = $merchant_id = (strpos($request->route()->uri, 'merchant') !== false) ? Auth::id() : $request->merchant_id;
            $appointment['user_id'] = $user_id = (strpos($request->route()->uri, 'merchant') !== false) ? $request->user_id : Auth::id();

            $appointment['final_price'] = $appointment['total_price'];
            $appointment['appointment_date_time'] = $this->convertToLocalTime($appointment['appointment_date_time'], $request->timezone);
            $services = $appointment['services'];
            unset($appointment['services']);
            unset($appointment['appointment_id']);
            unset($appointment['total_price']);
            $service_list = array_map(function ($services) use ($request) {
                $services['appointment_id'] = $request['appointment_id'];
                return $services;
            }, $services);
            if (Appointment::where(['id' => $request->appointment_id, 'user_id' => $user_id])->first() != null) {
                if (Appointment::where(['id' => $request->appointment_id, 'user_id' => $user_id])->update($appointment)) {
                    AppointmentDetail::where('appointment_id', $request->appointment_id)->delete();
                    if (AppointmentDetail::insert($service_list)) {
                        $response = response()->json(['message' => 'Appointment updated successfully'], $this->success_code);
                    }
                } else {
                    $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
                }
            } else {
                $response = response()->json(['message' => 'Appointment not found'], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function delete(Request $request)
    {
        if (Auth::user()) {
            $id = Auth::id();
            $appointment_ids = explode(",", $request->appointment_id);

            if (Appointment::where('merchant_id', $id)
                ->whereIN('id', $appointment_ids)
                ->delete()) {
                $response = response()->json(['message' => 'Appointment deleted successfully'], $this->success_code);
            } else {
                $response = response()->json(['message' => 'You are not authenticate to delete this appointment'], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function completeAppointment(Request $request)
    {
        if (Auth::user()) {
            $id = Auth::id();

            $validator = Validator::make(
                $request->all(),
                [
                    'appointment_id' => 'required',
                    'timezone' => 'required',
                ]
            );
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            $appointment_id = $request->appointment_id;
            if (Appointment::where('id', $appointment_id)
                ->where('merchant_id', $id)
                ->update(['appointment_status' => 2])
            ) {
                $response = response()->json(['message' => 'Appointment completed successfully'], $this->success_code);
            } else {
                $response = response()->json(['message' => 'You are not authenticate to delete this appointment'], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    private function appointmentId()
    {
        $randomNo = 'opayn' . $this->randomNo(6);

        if (Appointment::where('appointment_id', $randomNo)->first() != null) {
            $this->appointmentId();
        } else {
            return $randomNo;
        }
    }

    public function userAppointmentList(Request $request)
    {
        if (Auth::user()) {

            $validator = Validator::make(
                $request->all(),
                [
                    'timezone' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            $timezone = $request->timezone;
            $current_date_time = $this->getCurrentTime($timezone);

            $options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id' => Auth::id()] : ['user_id' => Auth::id()];

            $columns = ['id', 'merchant_id', 'staff_id', 'user_id', 'appointment_date_time', 'actual_price', 'final_price', 'client_message', 'appointment_status', 'appointment_service_type'];

            $appointments = Appointment::where($options)->select($columns)->whereHas('merchantDetail')->with('detail', 'merchantDetail', 'merchantAddress', 'staffDetail', 'userDetail')->get()->toArray();

            $list = $appointment_list = array_map(function ($appointments) use ($timezone) {
                $appointments['client_message'] = ($appointments['client_message'] == null) ? "" : $appointments['client_message'];
                $address = $appointments['merchant_address'];
                $appointments['appointment_date_time'] = $this->convertToTimestamp($appointments['appointment_date_time'], $timezone);
                $appointments['merchant_business_name'] = $appointments['merchant_detail']['merchant_business_name'];
                $appointments['address'] = $address['address1'] . " " . $address['address2'] . " " . $address['city'] . " " . $address['postcode'];
                $appointments['lat'] = (double) $address['lat'];
                $appointments['lng'] = (double) $address['lng'];
                $appointments['staff_name'] = $appointments['staff_detail']['merchant_name'];
                $appointments['user_name'] = $appointments['user_detail']['name'];
                // $appointments['status'] =  $appointments['appointment_status'];
                $appointments['appointment_end_time'] = $appointments['appointment_date_time'];
                $appointments['appointment_service_type'] = $appointments['appointment_service_type'];

                $services = $appointments['detail'];
                $service_list = array_map(function ($services) {
                    return $services['name'];
                }, $services);

                $end_time = 0;
                foreach ($services as $service) {
                    $end_time += $service['service_duration'];
                }

                $appointments['appointment_end_time'] = $appointments['appointment_end_time'] + $this->minsToMiliseconds($end_time);
                $appointments['services'] = implode(", ", $service_list);

                $unset_vars = ['merchant_detail', 'merchant_address', 'staff_detail', 'detail', 'user_detail'];
                foreach ($unset_vars as $key) {
                    unset($appointments[$key]);
                }
                return $appointments;
            }, $appointments);

            if (isset($request->timestamp) && $request->timestamp != null) {
                $list = array_filter($appointment_list, function ($appointment) use ($request) {
                    if ($this->convertToTime($appointment['appointment_date_time']) == $this->convertToTime($request->timestamp)) {
                        return true;
                    }
                });
            }

            $data = ['upcoming' => [], 'finished' => []];
            foreach ($appointment_list as $appointments) {
                foreach ($list as $value) {
                    $current_time = $this->convertToTimestamp($current_date_time, $timezone);
                    ($value['appointment_end_time']) > $current_time && $value['appointment_status'] == 1 ? array_push($data['upcoming'], $value) : array_push($data['finished'], $value);
                }
            }

            $finished = array_values(array_unique($data['finished'], SORT_REGULAR));
            $upcoming = array_values(array_unique($data['upcoming'], SORT_REGULAR));

            $upcoming_columns = array_column($upcoming, 'appointment_date_time');
            array_multisort($upcoming_columns, SORT_ASC, $upcoming);

            $finished_columns = array_column($finished, 'appointment_date_time');
            array_multisort($finished_columns, SORT_DESC, $finished);

            $finished = $finished;
            $upcoming = $upcoming;

            $finished_status = array();
            $upcoming_status = array();
            foreach ($finished as $finish) {
                if ($finish['appointment_status'] == 1) {
                    $finish['appointment_status'] = 3;
                }
                array_push($finished_status, $finish);
            }
            $upcoming_status = array_filter($upcoming, function ($appointment) use ($request) {
                if ($appointment['appointment_status'] == 1) {
                    return true;
                }
            });

            $data['finished'] = array_values($finished_status);
            $data['upcoming'] = array_values($upcoming_status);
            $response = response()->json(['message' => 'Appointment list', 'data' => $data], $this->success_code);
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function merchantAppointment(Request $request)
    {
        
        if (Auth::user()) {

            $validator = Validator::make(
                $request->all(),
                [
                    'timezone' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            // if((empty($request->timestamp)) && (empty($request->date)) ){
            //     return $response = response()->json(['message' => 'Please provide  atleast one param time or datetime'], $this->error_code);
            // }

            $timezone = $request->timezone;
            $current_date_time = $this->getCurrentTime($timezone);

            $current_date_time = date("Y-m-d", strtotime($current_date_time));

            $start_date = $current_date_time . " 00:00:00";

            if (isset($request->start_date) || $request->start_date != null) {
                $start_date = $this->convertToLocalTime($request->start_date, $timezone); //." 00:00:00" ;
                $start_date = date("Y-m-d", strtotime($start_date));
                $start_date = $start_date . " 00:00:00";
            }

            $end_date = $start_date;
            $end_date = strtotime($end_date);
            $end_date = strtotime("+6 day", $end_date);
            $end_date = date("Y-m-d", $end_date) . " 23:59:59";

            $options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id' => Auth::id()] : ['user_id' => Auth::id()];

            $columns = ['id', 'merchant_id', 'staff_id', 'user_id', 'appointment_date_time', 'actual_price', 'final_price', 'client_message', 'appointment_status'];

            $appointments = Appointment::where($options)
                ->select($columns)
                ->whereHas('merchantDetail')->with('detail', 'merchantDetail', 'merchantAddress', 'userDetail')
                ->whereBetween('appointment_date_time', [$start_date, $end_date])
                ->get()->toArray();

            $list = $appointment_list = array_map(function ($appointments) use ($timezone) {
                $appointments['client_message'] = ($appointments['client_message'] == null) ? "" : $appointments['client_message'];
                $address = $appointments['merchant_address'];
                $appointments['appointment_date_time'] = $this->convertToTimestamp($appointments['appointment_date_time'], $timezone);
                $appointments['merchant_business_name'] = $appointments['merchant_detail']['merchant_business_name'];
                $appointments['address'] = $address['address1'] . " " . $address['address2'] . " " . $address['city'] . " " . $address['postcode'];
                $appointments['lat'] = (double) $address['lat'];
                $appointments['lng'] = (double) $address['lng'];
                $appointments['staff_name'] = $appointments['merchant_detail']['merchant_name'];
                $appointments['user_name'] = $appointments['user_detail']['name'];
                $appointments['status'] = $appointments['appointment_status'];
                $appointments['appointment_end_time'] = $appointments['appointment_date_time'];

                $services = $appointments['detail'];
                $service_list = array_map(function ($services) {
                    return $services['name'];
                }, $services);

                $end_time = 0;
                foreach ($services as $service) {
                    $end_time += $service['service_duration'];
                }
                $appointments['appointment_end_time'] = $appointments['appointment_end_time'] + $this->minsToMiliseconds($end_time);
                $appointments['services'] = implode(", ", $service_list);

                $unset_vars = ['merchant_detail', 'merchant_address', 'detail', 'user_detail'];
                foreach ($unset_vars as $key) {
                    unset($appointments[$key]);
                }
                return $appointments;
            }, $appointments);

            $final_list = [];

            $current_date_time = $this->getCurrentTime($timezone);
            $current_time = $this->convertToTimestamp($current_date_time, $timezone);

            foreach ($list as $lists) {
                if ($lists['appointment_end_time'] < $current_time && $lists['status'] == 1) {
                    $lists['status'] = 3;
                }
                array_push($final_list, $lists);

            }

            if (isset($request->client_id) && $request->client_id != null) {
                $final_list = array_filter($final_list, function ($appointment) use ($request) {
                    if ($appointment['user_id'] == $request->client_id && $appointment['merchant_id'] == Auth::id()) {
                        return true;

                    }
                });

            }

            //  filter base on staff id

            $staff_id = $request->staff_id;

            if (isset($request->staff_id) && $request->staff_id != null) {
                $final_list = array_filter($final_list, function ($appointment) use ($request, $staff_id) {
                    if ($appointment['staff_id'] == $staff_id && $appointment['merchant_id'] == Auth::id()) {
                        return true;

                    }

                });
            }

            $data = array_column($final_list, 'appointment_date_time');

            array_multisort($data, SORT_ASC, $final_list);

            // filter based on appointment stats

            $appointment_status = $request->status;
            if (isset($request->status) && $request->status != null) {
                $final_list = array_filter($final_list, function ($appointment) use ($request, $appointment_status) {
                    if ($appointment['status'] == $appointment_status && $appointment['merchant_id'] == Auth::id()) {
                        return true;

                    }
                });
            }

            $data = $final_list;

            $result = [];
            $result1 = [];
            foreach ($final_list as $key => $value) {
                $date = $this->convertToDate($value['appointment_date_time'], $timezone);
                // $date = $value['appointment_date_time'];

                $data = [
                    'date' => $date,
                    'timestamp' => $value['appointment_date_time'],
                    'appointments' => [$value],
                ];
                if (empty($result)) {

                    array_push($result1, $data);
                    array_push($result, $date);
                } else {
                    if (in_array($date, $result)) {
                        $index = array_search($date, $result);
                        array_push($result1[$index]['appointments'], $value);

                    } else {
                        array_push($result1, $data);
                        array_push($result, $date);
                    }
                }

                // $result[$date][] = $value;
                // $result['date'] = $date;
                // $result['aapointments'][] = $value;
                // $result[$date][] = $result;

                // array_push($result1,$result);
            }

            $data = $result1;
            $response = response()->json(['message' => 'Appointment list', 'data' => $data], $this->success_code);
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function merchantAppCalender(Request $request)
    {
        if (Auth::user()) {

            $validator = Validator::make(
                $request->all(),
                [
                    'timezone' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            // if((empty($request->timestamp)) && (empty($request->date)) ){
            //     return $response = response()->json(['message' => 'Please provide  atleast one param time or datetime'], $this->error_code);
            // }

            $timezone = $request->timezone;
            $current_date_time = $this->getCurrentTime($timezone);

            $current_date_time = date("Y-m-d", strtotime($current_date_time));

            $start_date = $current_date_time . " 00:00:00";

            if (isset($request->start_date) || $request->start_date != null) {
                $start_date = $this->convertToLocalTime($request->start_date, $timezone); //." 00:00:00" ;
                $start_date = date("Y-m-d", strtotime($start_date));
                $start_date = $start_date . " 00:00:00";
            }

            $end_date = $start_date;
            $end_date = strtotime($end_date);
            $end_date = strtotime("+6 day", $end_date);
            $end_date = date("Y-m-d", $end_date) . " 23:59:59";

            $options = (strpos($request->route()->uri, 'merchant') !== false) ? ['merchant_id' => Auth::id()] : ['user_id' => Auth::id()];

            $columns = ['id', 'merchant_id', 'staff_id', 'user_id', 'appointment_date_time', 'actual_price', 'final_price', 'client_message', 'appointment_status'];

            $appointments = Appointment::where($options)
                ->select($columns)
                ->whereHas('merchantDetail')->with('detail', 'merchantDetail', 'merchantAddress', 'userDetail')
                ->whereBetween('appointment_date_time', [$start_date, $end_date])
                ->get()->toArray();

            $list = $appointment_list = array_map(function ($appointments) use ($timezone) {
                $appointments['client_message'] = ($appointments['client_message'] == null) ? "" : $appointments['client_message'];
                $address = $appointments['merchant_address'];
                $appointments['appointment_date_time'] = $this->convertToTimestamp($appointments['appointment_date_time'], $timezone);
                $appointments['merchant_business_name'] = $appointments['merchant_detail']['merchant_business_name'];
                $appointments['address'] = $address['address1'] . " " . $address['address2'] . " " . $address['city'] . " " . $address['postcode'];
                $appointments['lat'] = (double) $address['lat'];
                $appointments['lng'] = (double) $address['lng'];
                $appointments['staff_name'] = $appointments['merchant_detail']['merchant_name'];
                $appointments['user_name'] = $appointments['user_detail']['name'];
                $appointments['status'] = $appointments['appointment_status'];
                $appointments['appointment_end_time'] = $appointments['appointment_date_time'];

                $services = $appointments['detail'];
                $service_list = array_map(function ($services) {
                    return $services['name'];
                }, $services);

                $end_time = 0;
                foreach ($services as $service) {
                    $end_time += $service['service_duration'];
                }
                $appointments['appointment_end_time'] = $appointments['appointment_end_time'] + $this->minsToMiliseconds($end_time);
                $appointments['services'] = implode(", ", $service_list);

                $unset_vars = ['merchant_detail', 'merchant_address', 'detail', 'user_detail'];
                foreach ($unset_vars as $key) {
                    unset($appointments[$key]);
                }
                return $appointments;
            }, $appointments);

            $final_list = [];

            $current_date_time = $this->getCurrentTime($timezone);
            $current_time = $this->convertToTimestamp($current_date_time, $timezone);

            foreach ($list as $lists) {
                if ($lists['appointment_end_time'] < $current_time && $lists['status'] == 1) {
                    $lists['status'] = 3;
                }
                array_push($final_list, $lists);

            }

            if (isset($request->client_id) && $request->client_id != null) {
                $final_list = array_filter($final_list, function ($appointment) use ($request) {
                    if ($appointment['user_id'] == $request->client_id && $appointment['merchant_id'] == Auth::id()) {
                        return true;

                    }
                });

            }

            //  filter base on staff id

            $staff_id = $request->staff_id;

            if (isset($request->staff_id) && $request->staff_id != null) {
                $final_list = array_filter($final_list, function ($appointment) use ($request, $staff_id) {
                    if ($appointment['staff_id'] == $staff_id && $appointment['merchant_id'] == Auth::id()) {
                        return true;

                    }

                });
            }

            $data = array_column($final_list, 'appointment_date_time');

            array_multisort($data, SORT_ASC, $final_list);

            // filter based on appointment stats

            $appointment_status = $request->status;
            if (isset($request->status) && $request->status != null) {
                $final_list = array_filter($final_list, function ($appointment) use ($request, $appointment_status) {
                    if ($appointment['status'] == $appointment_status && $appointment['merchant_id'] == Auth::id()) {
                        return true;

                    }
                });
            }

            $data = $final_list;

            $result = [];
            $result1 = [];

            // set appoinment date wise ,same date appoinment in same objects
            foreach ($final_list as $key => $value) {
                $date = $this->convertToDate($value['appointment_date_time'], $timezone);
                // $date = $value['appointment_date_time'];

                $data = [
                    'date' => $date,
                    'timestamp' => $value['appointment_date_time'],
                    'appointments' => [$value],
                ];
                if (empty($result)) {

                    array_push($result1, $data);
                    array_push($result, $date);
                } else {
                    if (in_array($date, $result)) {
                        $index = array_search($date, $result);
                        array_push($result1[$index]['appointments'], $value);

                    } else {
                        array_push($result1, $data);
                        array_push($result, $date);
                    }
                }

            }

            // get all dates from 2 dates
            function getDatesFromRange($start, $end, $format = 'Y-m-d')
            {

                // Declare an empty array
                $array = array();

                // Variable that store the date interval
                // of period 1 day
                $interval = new DateInterval('P1D');

                $realEnd = new DateTime($end);

                // $realEnd->add($interval);

                $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

                // Use loop to store date into array
                foreach ($period as $date) {
                    $array[] = $date->format($format);
                }

                // Return the array elements
                return $array;
            }

            // Function call with passing the start date and end date

            $all_date = getDatesFromRange($start_date, $end_date);

            $calender = [];
            foreach ($all_date as $dates) {
                // $cal = [];
                if ($result1 != null) {
                    foreach ($result1 as $result) {
                        if ($dates == $result['date']) {
                            break;
                        } else {
                            $result['date'] = $dates;
                            $result['timestamp'] = $result['timestamp'];
                            $result['appointments'] = [];
                        }
                    }
                } else {
                    $result['date'] = $dates;
                    $result['timestamp'] = $this->convertToTimestamp($result['date'], $timezone);
                    $result['appointments'] = [];
                }
                array_push($calender, $result);
            }

            $time_slot_arr = [];

            // populate timeslot array with desired timeslote
            for ($i = 6; $i <= 24; $i++) {
                if ($i == 24) {
                    array_push($time_slot_arr, ['timeslot' => '00', 'appointments' => []]);
                } else {
                    array_push($time_slot_arr, ['timeslot' => "$i", 'appointments' => []]);
                }
            }

            $slot = array();

            // echo json_encode($calender);
            // die();

            foreach ($calender as $cal) {
                // if appoinment array is null then push empty timeslot
                if ($cal['appointments'] == null) {
                    $slot_data['date'] = $cal['date'];
                    // $slot_data['timestamp'] = $cal['timestamp'];
                    $slot_data['timestamp'] = $this->convertToTimestamp($slot_data['date'], $timezone);
                    $slot_data['slots'] = $time_slot_arr;

                    array_push($slot, $slot_data);

                } else {
                    $final_time_slot_arr = [];
                    foreach ($time_slot_arr as $key => $timeslot) {
                        foreach ($cal['appointments'] as $data) {
                            // match timeslot hour with appoiment date time hour
                            if (date('H', strtotime($this->convertToDateTime($data['appointment_date_time'], $timezone))) == $timeslot['timeslot']) {
                                // if match hour then push appoinment to same time slot
                                array_push($timeslot['appointments'], $data);

                            }

                        }
                        //push time slot into another new array
                        array_push($final_time_slot_arr, $timeslot);
                    }

                    $slot_data['date'] = $cal['date'];
                    // $slot_data['timestamp'] = $cal['timestamp'];
                    $slot_data['timestamp'] = $this->convertToTimestamp($slot_data['date'], $timezone);
                    $slot_data['slots'] = $final_time_slot_arr;
                    array_push($slot, $slot_data);
                }
            }

            //final appointment data

            $data = $slot;
            $response = response()->json(['message' => 'Appointment list', 'data' => $data], $this->success_code);
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }


    public function userAppointmentDetailUpdate(Request $request)
    {
       $appointment_id = Appointment::select('id')->get()->toArray();
       $appointment_customer_id = AppointmentCustomer::select('appointment_id')->get()->toArray();
       $user_ids = [];
        foreach($appointment_id as $app_id){
            foreach($appointment_customer_id as $cus_id){
                // if(in_array($app_id,$cus_id)){
                // // break; 
                // }   
                // else{
                //     array_push($user_ids,$app_id);
                // break;
                // }
                
            }
        }

    //    var_dump($appointment_id);
    //    print_r($appointment_id);
    //    print_r($appointment_customer_id);
       
    }

    // public function merchantAppointment(Request $request){
    //     if(Auth::user()){

    //         $validator = Validator::make(
    //             $request->all(),
    //             [
    //                 'timezone' => 'required'
    //             ]
    //         );

    //         if ($validator->fails()) {
    //             return response()->json(['message' => $validator->errors()], $this->error_code);
    //         }

    //         // if((empty($request->timestamp)) && (empty($request->date)) ){
    //         //     return $response = response()->json(['message' => 'Please provide  atleast one param time or datetime'], $this->error_code);
    //         // }

    //         $timezone = $request->timezone;
    //         $current_date_time = $this->getCurrentTime($timezone);

    //         $current_date_time = date("Y-m-d",strtotime($current_date_time));

    //         $start_date = $current_date_time. " 00:00:00";

    //         if(isset($request->start_date) || $request->start_date != null ){
    //                 $start_date = $this->convertToDate($request->start_date,$timezone)." 00:00:00" ;

    //         }

    //         $end_date = $start_date;
    //         $end_date = strtotime($end_date);
    //         $end_date = strtotime("+7 day", $end_date);
    //         $end_date =  date("Y-m-d", $end_date)." 23:59:59";

    //         $options = (strpos($request->route()->uri, 'merchant') !== false) ? ['staff_id'=>Auth::id()] : ['user_id'=>Auth::id()];

    //         $columns =['id','merchant_id','staff_id','user_id','appointment_date_time','actual_price','final_price','client_message','appointment_status'];
    //         // dd($columns);
    //         $appointments = Appointment::where($options)
    //                                     ->select($columns)
    //                                     ->whereHas('merchantDetail')->with('detail','merchantDetail','merchantAddress','userDetail')
    //                                     ->whereBetween('appointment_date_time',[$start_date,$end_date])
    //                                     ->get()->toArray();

    //         $list = $appointment_list = array_map(function($appointments) use($timezone){
    //                                 $appointments['client_message'] = ($appointments['client_message'] == null) ? "" : $appointments['client_message'];
    //                                 $address = $appointments['merchant_address'];
    //                                 $appointments['appointment_date_time'] = $this->convertToTimestamp($appointments['appointment_date_time'],$timezone);
    //                                 $appointments['merchant_business_name'] = $appointments['merchant_detail']['merchant_business_name'];
    //                                 $appointments['address'] = $address['address1']." ".$address['address2']." ".$address['city']." ".$address['postcode'];
    //                                 $appointments['lat'] = (double)$address['lat'];
    //                                 $appointments['lng'] = (double)$address['lng'];
    //                                 $appointments['staff_name'] =  $appointments['merchant_detail']['merchant_name'];
    //                                 $appointments['user_name'] =  $appointments['user_detail']['name'];
    //                                 $appointments['status'] =  $appointments['appointment_status'];
    //                                 $appointments['appointment_end_time'] = $appointments['appointment_date_time'];

    //                                 $services = $appointments['detail'];
    //                                 $service_list = array_map(function($services){
    //                                     return $services['name'];
    //                                 },$services);

    //                                 $end_time = 0;
    //                                 foreach ($services as $service) {
    //                                     $end_time += $service['service_duration'];
    //                                 }
    //                                 $appointments['appointment_end_time'] = $appointments['appointment_end_time'] + $this->minsToMiliseconds($end_time);
    //                                 $appointments['services'] =  implode(", " ,$service_list);

    //                                 $unset_vars = ['merchant_detail','merchant_address','detail','user_detail'];
    //                                 foreach ($unset_vars as $key) {
    //                                     unset($appointments[$key]);
    //                                 }
    //                                 return $appointments;
    //                             },$appointments);

    //                             $final_list = [];

    //                             $current_date_time = $this->getCurrentTime($timezone);
    //                             $current_time = $this->convertToTimestamp($current_date_time,$timezone);

    //                             foreach($list as $lists){
    //                                 if($lists['appointment_end_time'] < $current_time && $lists['status'] == 1){
    //                                         $lists['status'] = 3;
    //                                 }
    //                                 array_push($final_list,$lists);

    //                             }

    //         if(isset($request->client_id) && $request->client_id != null ){
    //             $final_list = array_filter($final_list,function($appointment) use ($request){
    //                     if($appointment['user_id'] == $request->client_id && $appointment['merchant_id'] == Auth::id() ){
    //                         return true;

    //                     }
    //             });

    //         }

    //         //  filter base on staff id

    //         $staff_id = $request->staff_id;

    //         if(isset($request->staff_id) && $request->staff_id != null){
    //             $final_list = array_filter($final_list,function($appointment) use ($request,$staff_id){
    //                 if($appointment['staff_id'] == $staff_id && $appointment['merchant_id'] == Auth::id() ){
    //                     return true;

    //                 }

    //                 });
    //         }

    //         $data = array_column($final_list, 'appointment_date_time');

    //          array_multisort($data, SORT_ASC, $final_list);

    //             // filter based on appointment stats

    //         $appointment_status = $request->status;
    //         if(isset($request->status) && $request->status != null){
    //             $final_list = array_filter($final_list,function($appointment) use ($request,$appointment_status){
    //                 if($appointment['status'] == $appointment_status && $appointment['merchant_id'] == Auth::id() ){
    //                     return true;

    //                 }
    //                 });
    //         }

    //         $data = $final_list;

    //         $response = response()->json(['message' => 'Appointment list','data'=>array_values($data)], $this->success_code);
    //     }
    //     else{
    //         $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
    //     }
    //     return $response;
    // }

}
