<?php

namespace App\Http\Controllers\v3\Api\Merchant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Merchant;
use App\MerchantDetail;
use App\Staff;
use App\Service;
use App\ServiceCategory;
use App\MerchantStaff;
use App\MerchantService;
use App\MerchantsWorkingHour;
use App\UsersDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class StaffController extends Controller
{

    /*
     *
     * Add Staff by Merchant screen add_staff
     *
    */
    public $dummy_banner = '';
    public $dummy_profile_pic = '';
    
     /*
        Function name : addStaffByMerchant
        Function Purpose : Add staff by merchant
        Created by : Gurpreet Singh
        ReCreated on : 24 Nov 2020
        Reason : Method created in too complex way
    */
    public function addStaffByMerchant(Request $request) {
        /*
            $validator = Validator::make($request->all(),
                    [
                        'mobile_number' => 'required',
                        'name'          => 'required',
                        'doj'           => 'required',
                        'services'      => 'required',
                        'day'           => 'required',
                        'staff_id'      => 'required'
                    ]
                );        
            $service_ids = $request->services;
            $days = $request->day;
            
            $categoryArray = (gettype($service_ids) == 'string') ? json_decode($service_ids, true) : $service_ids;
            $dayArray = (gettype($days) == 'string') ? json_decode($days, true) : $days;

            $response = ['message'=>'everything okay','data'=>''];
            foreach ($dayArray as $day) {
                if($day['start_time'] == null || $day['end_time'] == null || $day['week_day'] == null){
                    return response()->json(['message'=>'start_time, end_time, week_day should not be empty or null'], $this->error_code);
                }
            }
            if(empty($categoryArray)){
                return response()->json(['message'=>'Please choose at least one service'], $this->error_code);
            }

            $mobile_number = $request->mobile_number;
            $request->merge([
                'mobile_number' => substr($request->mobile_number, -10)
            ]);        
            // dd(Merchant::where('mobile',$request->mobile_number)->first());
            if(Merchant::where('mobile',$request->mobile_number)->first() == null){
                $login_merchant = Merchant::where('id',Auth::id())->with('detail')->first();            
                // $staff = Merchant::create(['mobile'=>$request->mobile_number,'merchant_user_type'=>2,'date_of_joining'=>$this->convertToLocalTime($request->doj,$request->timezone)]); // add staff into merchant table
                // MerchantDetail::create(['merchant_id'=>$staff->id,'merchant_name'=>$request->name,'salon_type'=>$login_merchant->detail->salon_type,'gender'=>$request->gender]); // add staff detailinot merchant detail table
                // MerchantStaff::create(['merchant_id'=>$login_merchant->id,'staff_id'=>$staff->id]); // add created satff into merchatnn_staff table
                $services = MerchantService::where('merchant_staff_id',$login_merchant->id)->get()->toArray();
                $response = response()->json(['message'=>$services], $this->success_code);
                $staff_services = array_map(function($services) {
                                            // dump($services);
                                            $services['user_type'] = 2;
                                            $services['merchant_staff_id'] = $staff->id;
                                            unset($services['id']);
                                            unset($services['created_at']);
                                            unset($services['updated_at']);
                                             // $data = [
                                            //             'service_id'=>'',
                                            //             'merchant_staff_id'=>'',
                                            //             'service_category_id'=>'',
                                            //             'user_type'=>2,
                                            //             'service_price'=>'',
                                            //             'service_duration'=>'',
                                            //             'service_buffer_time'=>'',

                                            //         ]
                                            return $services;
                                        }
                                        ,$services);

                // MerchantService::insert($staff_working_hours)
                $response = response()->json(['message'=>$services], $this->success_code);
            }
            else{
                $response = response()->json(['message'=>'This mobile number is already in use'], $this->error_code);
            }
            // send message to added client
            
            $merchant = Merchant::where('id',$id)->with('detail')->first();
            $business_name = $merchant->detail->merchant_business_name;
            $message = "Hi $request->name, $business_name has added you as one of their Staff Member. Please login to your account using your Reg. Number. link"; 
            $this->sendMessage($message, $request->mobile_number);
        

            // $response = ['message'=>$request->all(),'data'=>''];
            return $response;
        */

        if (Auth::guard('merchants')) {
            $user = Auth::user();
            $break_start_time = null;
            $break_end_time   = null;
            $validator = Validator::make($request->all(),
                [
                    'name'          => 'required',
                    'country_code'  => 'required',
                    'mobile_number' => 'required',
                    'doj'           => 'required',
                    'services'      => 'required',
                    'day'           => 'required',
                    'timezone'      => 'required'
                ],
                [
                    'name.required' => ' Name is required',
                    'mobile_number.required' => ' Mobile Number is required',
                    'mobile_number.digits' => ' Please enter a Valid Mobile Number',
                    'doj.required' => ' Date of Joining is required',
                    'services.required' => 'Services is required',
                    'day.required' => ' Day is required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }
            $request->merge([
                'mobile_number' => str_replace(' ', '', $request->mobile_number)
            ]);

            // return ej($request->mobile_number);
            if (!empty($user)) {

                $id = Auth::id();

                $staff = new Merchant();

                $staff->country_code = $request->country_code;
                $staff->mobile = $request->mobile_number;
                $staff->date_of_joining = $this->convertToLocalTime($request->doj,$request->timezone);
              
                $staff->merchant_user_type = '2';

                $staff1 = new MerchantDetail();
                $staff1->merchant_id = $id;
                $staff1->merchant_name = $request->name;
                $staff1->gender = $request->gender;
                $categoryJSON = $request->services;
                $dayJSON = $request->day;

                $categoryArray = $categoryJSON;
                $dayArray = $dayJSON;
                $categoryArray = (gettype($categoryJSON) == 'string') ? json_decode($categoryJSON, true) : $categoryJSON;
                $dayArray = (gettype($dayJSON) == 'string') ? json_decode($dayJSON, true) : $dayJSON;
                $timezone = $request->timezone;                

                foreach ($categoryArray as $key => $value) {

                    $category_id =  Service::select('service_categories_id')->where('id', '=', $value)->first();
                    $category = $category_id['service_categories_id'];

                    if ($category == NULL) {
                        $response['message'] = "Category cannot be empty ";
                        return response()->json($response);
                    }

                    if ($value == NULL) {
                        $response['message'] = "Service cannot be empty ";
                        return response()->json($response);
                    }
                }

                foreach ($dayArray as $key1 => $value1) {
                    // dump($value1);
                    $service_working = new MerchantsWorkingHour();

                    $service_working->week_day = $value1["week_day"];

                    if ($value1["week_day"] == NULL) {
                        $response['message'] = "Day cannot be empty ";
                        return response()->json($response, 400);
                    }
                    if ($value1["start_time"] == NULL) {
                        $response['message'] = "Start time cannot be empty ";
                        return response()->json($response, 400);
                    }

                    if ($value1["end_time"] == NULL) {
                        $response['message'] = "End time cannot be empty ";
                        return response()->json($response, 400);
                    }
                    if (($value1["start_time"]) == "") {
                        $response['message'] = "Start time  cannot be empty";
                        return response()->json($response, 400);
                    }
                    if (($value1["end_time"]) == "") {
                        $response['message'] = "End time  cannot be empty";
                        return response()->json($response, 400);
                    }

                    if (($value1["start_time"]) > ($value1["end_time"])) {
                        $response['message'] = "Start time is always less than End time ";
                        return response()->json($response, 400);
                    }


                    if (($value1["start_time"]) > ($value1["end_time"])) {
                        $response['message'] = "Start time is always less than End time ";
                        return response()->json($response, 400);
                    }
                    if (($value1["start_time"]) == ($value1["end_time"])) {
                        $response['message'] = "Start time and  End time  cannot be same";
                        return response()->json($response, 400);
                    }

                    //if break start empty or null

                    // if($value1["break_start_time"] == 0 || $value1["break_start_time"] != ""){
                    //     $value1["break_start_time"] = null;

                    // }
                    // else{
                    //     if(($value1["start_time"]) > ($value1["break_start_time"])) {
                    //         $response['message'] = "Break start time is always Greater than Start time ";
                    //         return response()->json($response, 400);

                    //     }
                    // }


                    // //if break_end_time is empty
                    // if($value1["break_end_time"] == "" || $value1["break_end_time"] == "0"){
                    //     $value1["break_end_time"] = null;

                    // }
                    // else{
                    //     if (($value1["break_start_time"]) > ($value1["break_end_time"])) {
                    //         $response['message'] = "Break end is always Greater than Start break ";
                    //         return response()->json($response, 400);
                    //     }
                    // }

                }

            } else {

                return response()->json(['message' => 'Staff Not Added'], 400);

            }


            // //if break_start_time and break_end_time time is 0 or null

            // if($value1["break_start_time"] == "0" || $value1["break_start_time"] == null){
            //     // $break_start_time  = null;

            // }
            // else{
            //     $break_start_time = date("Y-m-d h:i:s ", ($value1["break_start_time"])/1000);
            // }

            // if($value1["break_end_time"] == "0" || $value1["break_start_time"] == null){
            //     // $break_end_time  = null;

            // }
            // else{
            //     $break_end_time = date("Y-m-d  h:i:s ", ($value1["break_end_time"])/1000);
            // }

            if (!empty($user)) {

                $staff = new Merchant();

                // $staff->mobile = substr($request->mobile_number, -10);
                $staff->mobile = $request->mobile_number;
                $staff->country_code = $request->country_code;
                $staff->date_of_joining = $this->convertToLocalTime($request->doj,$request->timezone);;
                $staff->merchant_user_type = '2';

                $data = Merchant::where('mobile', '=', $staff->mobile)->first();

                if (!empty($data)) {
                    return response()->json(['message' => 'Staff Already Exist'], 400);

                }else{

                    $staff->save();
                    $merchant = Merchant::where('id',$id)->with('detail')->first();
                    $business_name = $merchant->detail->merchant_business_name;
                    $message = "Hi $request->name, $business_name has added you as one of their Staff Member. Please login to your account using your Reg. Number. https://opayn.com/merchant.html"; 
                    $this->sendMessage($message, $request->country_code.$request->mobile_number);
                    $staff_id = $staff->id;

                    $staff1 = new MerchantDetail();
                    $staff1->merchant_id = $staff_id;
                    $staff1->merchant_name = $request->name;
                    $staff1->gender = $request->gender;
                    $staff1->save();
                    $categoryJSON = $request->services;

                    /*
                    //$dayJSON = $request->day;

                    //$dayArray = json_decode($dayJSON, true);
                     */


                    foreach ($dayArray as $key1 => $value1) {

                        $service_working = new MerchantsWorkingHour();
                        $service_working->merchant_id = $staff_id;
                        $service_working->week_day = $value1["week_day"];

                        if ($value1["week_day"] == NULL) {
                            $response['message'] = "Day cannot be empty ";

                            return response()->json($response, 400);
                        }
                        if ($value1["start_time"] == NULL) {
                            $response['message'] = "Start time cannot be empty ";

                            return response()->json($response, 400);
                        }

                        if ($value1["end_time"] == NULL) {
                            $response['message'] = "End time cannot be empty ";

                            return response()->json($response, 400);
                        }
                        if (($value1["start_time"]) == "") {
                            $response['message'] = "Start time  cannot be empty";

                            return response()->json($response, 400);
                        }
                        if (($value1["end_time"]) == "") {
                            $response['message'] = "End time  cannot be empty";

                            return response()->json($response, 400);
                        }

                        if (($value1["start_time"]) > ($value1["end_time"])) {
                            $response['message'] = "Start time is always less than End time ";

                            return response()->json($response, 400);
                        }
                        if (($value1["start_time"]) == ($value1["end_time"])) {
                            $response['message'] = "Start time and  End time  cannot be same";

                            return response()->json($response, 400);
                        }



                        //if break start empty or null

                        // if($value1["break_start_time"] == 0 || $value1["break_start_time"] == ""){
                        //     $value1["break_start_time"] = null;

                        // }
                        // else{
                        //     if(($value1["start_time"]) > ($value1["break_start_time"])) {
                        //         $response['message'] = "Break start time is always Greater than Start time ";
                        //         return response()->json($response, 400);

                        //     }
                        // }


                        // //if break_end_time is empty
                        // if($value1["break_end_time"] == 0 || $value1["break_end_time"] == ""){
                        //     $value1["break_end_time"] = null;

                        // }
                        // else{
                        //     if (($value1["break_start_time"]) > ($value1["break_end_time"])) {
                        //         $response['message'] = "Break end is always Greater than Start break ";
                        //         return response()->json($response, 400);
                        //     }
                        // }


                    if($value1["break_start_time"] == null){
                        $break_start_time  = null;

                    }
                    else{
                        // $break_start_time = date("Y-m-d h:i:s ", ($value1["break_start_time"])/1000);
                        $break_start_time = $this->convertToLocalTime($value1["break_start_time"],$timezone);

                    }

                    if($value1["break_end_time"] == null){
                        $break_end_time  = null;

                    }
                    else{
                        // $break_end_time = date("Y-m-d  h:i:s ",($value1["break_end_time"])/1000);
                        $break_end_time = $this->convertToLocalTime($value1["break_end_time"],$timezone);

                    }

                        $check =  MerchantsWorkingHour::where('week_day', '=', $value1["week_day"])->where('merchant_id', '=',  $staff_id)->count();

                        if ($check == Null) {

                            $service_working = new MerchantsWorkingHour();

                            // $start_time = date("Y-m-d h:i:s ", ($value1["start_time"])/1000);
                            // $end_time = date("Y-m-d h:i:s ", ($value1["end_time"])/1000);


                            $start_time = $this->convertToLocalTime($value1["start_time"],$timezone);
                            $end_time = $this->convertToLocalTime($value1["end_time"],$timezone);



                            // $break_start_time = date("Y-m-d h:i:s ", ($value1["break_start_time"])/1000);
                            // $break_end_time = date("Y-m-d  h:i:s ", ($value1["break_end_time"])/1000);

                            $service_working->merchant_id = $staff_id;
                            $service_working->week_day = $value1["week_day"];
                            $service_working->start_time = $start_time;
                            $service_working->end_time = $end_time;
                            $service_working->break_start_time = $break_start_time;
                            $service_working->break_end_time = $break_end_time;
                            $service_working->status = "1";


                            if (($value1["start_time"]) < ($value1["end_time"])) {

                                $service_working->save();

                            }
                        }

                        if (!empty($check)){

                            $dayresult = MerchantsWorkingHour::select('week_day')->where('merchant_id', $staff_id)->get();

                            foreach($dayresult as $dayresults){
                                $resday = $dayresults->week_day;
                                MerchantsWorkingHour::where('merchant_id', '=', $staff_id)->where('week_day', '=', $resday)->delete();
                            }

                            // $start_time = date("Y-m-d  h:i:s ", ($value1["start_time"])/1000);
                            // $end_time = date("Y-m-d  h:i:s ", ($value1["end_time"])/1000);
                             $start_time = $this->convertToLocalTime($value1["start_time"],$timezone);
                            $end_time = $this->convertToLocalTime($value1["end_time"],$timezone);
                            // $break_start_time = date("Y-m-d  h:i:s ", ($value1["break_start_time"])/1000);
                            // $break_end_time = date("Y-m-d  h:i:s ", ($value1["break_end_time"])/1000);

                            $service_working = new MerchantsWorkingHour();
                            $service_working->merchant_id = $staff_id;
                            $service_working->week_day = $value1["week_day"];
                            $service_working->start_time = $start_time;
                            $service_working->end_time = $end_time;
                            $service_working->break_start_time = $break_start_time;
                            $service_working->break_end_time = $break_end_time;
                            $service_working->status = "1";

                            if ($value1["start_time"] < $value1["end_time"]) {

                                $service_working->save();
                            }
                        }
                    }

                    /* $categoryArray = json_decode($categoryJSON, true); */

                    foreach ($categoryArray as $key => $value) {

                        $category_id =  Service::select('service_categories_id')->where('id', '=', $value)->first();
                        $category = $category_id['service_categories_id'];
                        $services_values =  Service::select('service_price', 'service_duration', 'service_buffer_time', 'service_image', 'status')->where('id', '=', $value)->first();

                        $user_type =  Merchant::select('merchant_user_type')->where('id', '=', $staff_id)->first();

                        $merchantservices = new MerchantService();

                        $merchantservices->merchant_staff_id = $staff_id;
                        $merchantservices->service_category_id = $category;
                        $merchantservices->service_id = $value;
                        $merchantservices->service_image = $services_values['service_image'];
                        $merchantservices->service_price = $services_values['service_price'];
                        $merchantservices->service_duration = $services_values['service_duration'];
                        $merchantservices->service_buffer_time = $services_values['service_buffer_time'];
                        $merchantservices->user_type = $user_type['merchant_user_type'];

                        $merchantservices->status = "1";

                        if ($category == NULL) {
                           return response()->json(['message' => 'Service cannot be empty'],$this->error_code);
                        }

                        $merchantservices->service_id = $value;

                        if ($value == NULL) {
                            return response()->json(['message' => 'Service cannot be empty'],$this->error_code);
                        }

                        $merchantservices->save();

                    }

                    $staff2 = new MerchantStaff();

                    $staff2->status = "1";
                    $staff2->staff_id = $staff_id;
                    $staff2->merchant_id = $id;
                    $staff2->save();

                return response()->json(['message' => 'Staff added successfully'],$this->success_code);

                }
            }else {
                return response()->json(['message' => 'Staff not added'],$this->error_code);
            }
        }else {
            return response()->json(['message' => 'Merchant Not Exist'],$this->unauthenticate_code);
        }
    }






    /*
     *
     * Show list of services given by Merchant while adding staff screen select_staff_service
     *
    */


    public function selectMerchantServiceForStaff(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();
            if (!empty($user)) {

                $id =  Auth::id();

                $category = MerchantService::join('services', 'services.id', '=', 'merchant_services.service_id' )
                            ->join('service_categories', 'service_categories.id', '=', 'merchant_services.service_category_id')
                            ->select('service_category_id as id','category_name')
                            ->where('merchant_staff_id', '=', $id)
                            ->where('user_type',1)
                            ->get();

                $all_service = array();



                // get service offer
                $merchant_offer = Merchant::where('id',$id)->with('offers')->first();


                foreach ($category as $cat1) {
                    $offer_service = array();
                    $services = DB::table('merchant_services')
                                ->join('services', 'merchant_services.service_id', '=', 'services.id')
                                ->select('merchant_services.service_id','merchant_services.service_category_id as category_id', 'services.service_name', 'merchant_services.service_price','merchant_services.service_duration','merchant_services.service_buffer_time','services.service_image')
                                ->where('merchant_services.service_category_id', '=', $cat1['id'])
                                ->where('merchant_services.merchant_staff_id', '=', $id)
                                ->get()->toArray();                    
                        foreach($services as $merchant_services){
                            foreach($merchant_offer->offers->toArray() as $offer){
                                $merchant_services->discount = null;
                                $merchant_services->max_discount_price = null;
                                $merchant_services->start_time = null;
                                $merchant_services->end_time = null;
                                $merchant_services->after_discount_price = null;
                                $merchant_services->package_service = null;
                                $merchant_services->after_discount_price  = null;

                                    if(in_array($merchant_services->service_id,$offer)){
                                        $merchant_services->discount = $offer['discount'];
                                        $merchant_services->max_discount_price = $offer['max_discount_price'];
                                        $merchant_services->start_time = $offer['start_time'];
                                        $merchant_services->end_time = $offer['end_time'];
                                        $selling_price = $this->discountAmount($merchant_services->service_price,$offer['discount']);
                                        $after_discount_price =  (($selling_price) < ($merchant_services->service_price - $offer['max_discount_price'])) ? ($merchant_services->service_price - $offer['max_discount_price']) : $selling_price;
                                        $merchant_services->after_discount_price = (float)number_format($after_discount_price , 2);
                                    }
                            }
                            array_push($offer_service,$merchant_services);
                        }

                   $service_list = array();
                    foreach($offer_service as $service){
                        if($service->service_image == null || empty($service->service_image)){
                            $service->service_image = $this->dummy_banner;
                        }
                        array_push($service_list,$service);
                    }


                    $cat1['services'] = $service_list;

                    $all_service[] = $cat1;


                }                

                return response()->json(['message' => true,'data' => array_values(array_unique($all_service))],$this->success_code);
            }
        }
    }


    /*
        *
        * Merchant's staff profile with name and appointment screen staff_profile
        * Updated by : Gurpreet Singh
        * date : 30-Oct-2020
    */


    public function getStaffProfile(Request $request)
    {

            $validator = Validator::make(
                $request->all(),
                ['staff_id' => 'required','timezone'=>'required']
            );


            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            $id = Auth::id();

            $staff_id = $request->staff_id;
            $timezone = $request->timezone;

            if(MerchantStaff::where('staff_id',$staff_id)->where('merchant_id', $id)->first() != null){
                $merchant_staff = MerchantStaff::where('staff_id',$staff_id)->with('staffDetail','workingHours','staffAppointment')->first();
                $merchant = Merchant::where('id',$staff_id)->with('address')->first();
                $working_hours = $merchant_staff->workingHours->toArray();

                // dump($this->getDay($this->getCurrentTime($timezone),$timezone));
                $current_timestamp = $this->convertToTimestamp($this->getCurrentTime($timezone),$timezone);
                $day = $this->getDay($current_timestamp,$timezone);
                $day_key = array_search($day,$this->days);                
                // $day = $request->datetime;

                // $date = $this->convertToDate($day);
                // $day = $this->getDay($day,$timezone);
                // $day_key = array_search($day,$this->days);

                $working_hours_list = array_map(function($working_hours) use ($timezone){
                                            unset($working_hours['id']);
                                            unset($working_hours['merchant_id']);
                                            $working_hours['start_time'] = $this->convertToTimestamp($working_hours['start_time'],$timezone);
                                            $working_hours['end_time'] = $this->convertToTimestamp($working_hours['end_time'],$timezone);
                                            $working_hours['break_start_time'] = $this->convertToTimestamp($working_hours['break_start_time'],$timezone);
                                            $working_hours['break_end_time'] = $this->convertToTimestamp($working_hours['break_end_time'],$timezone);
                                            return $working_hours;
                                        }, $working_hours);

                $data = [
                            "id" => $working_hours[0]['merchant_id'],
                            "mobile" => $merchant->mobile,
                            "country_code" => $merchant->country_code,
                            "name" => $merchant_staff->staffDetail->merchant_name,
                            "image" => ($merchant_staff->staffDetail->merchant_image != null) ? $merchant_staff->staffDetail->merchant_image  : $this->default_profile_image,
                            "email" => (!empty($merchant_staff->staffDetail->email) ?$merchant_staff->staffDetail->email : "N/A" ),
                            "gender" => $merchant_staff->staffDetail->gender,
                            "date_of_joining" => $this->convertToTimestamp($merchant->date_of_joining,$timezone),
                            // "status" => $merchant_staff->status,
                            "status" => $this->valueExsist($working_hours_list,$day_key,'week_day') ? 1 : 0,
                            'working_hours' => $working_hours_list
                        ];


                $response = response()->json(['message' => 'Staff detail along with working hours','data' => $data],$this->success_code);
            }
            else{
                $response = response()->json(['message' => 'Your are not authorized to access this staff member data'],$this->error_code);
            }

            return $response;
            // if (!empty($user)){
                // $staff = new MerchantStaff();

                // $data = MerchantStaff::where('staff_id',$staff_id)->where('merchant_id', '=', $id)->first();


                // if (!empty($data)){


                //     $list = array();
                //     $user_data = array();

                //     // $user_data = Merchant::where('merchants.id', '=', $staff->id)->where('merchants.merchant_user_type', '=', 2)->get();
                //     // dd(Auth::id());
                //     Merchant::where('id',Auth::id())->first();

                //     $user_data = Merchant::where('merchants.id', '=', $staff->id)->where('merchants.merchant_user_type', '=', 2)
                //     ->join('merchant_details','merchant_details.merchant_id','=','merchants.id')
                //     ->join('merchant_addresses','merchant_addresses.merchant_id','=','merchants.id')
                //     ->select('merchants.id','merchants.mobile','merchant_details.merchant_name','merchant_details.merchant_image','merchant_details.email','merchant_details.gender','merchant_addresses.address1','merchant_addresses.address2','merchant_addresses.city','merchant_addresses.postcode')->first();

                //     $staff_working_hour = Merchant::where('merchants.id', '=', $staff->id)->where('merchants.merchant_user_type', '=', 2)
                //      ->join('merchants_working_hours','merchants_working_hours.merchant_id','=','merchants.id')
                //     ->select('merchants_working_hours.week_day','merchants_working_hours.start_time','merchants_working_hours.end_time','merchants_working_hours.break_start_time','merchants_working_hours.break_end_time')->get()->toArray();


                // //    dd($user_data);
                //     $user_data->merchant_image = ($user_data->merchant_image != null) ? $user_data->merchant_image : $this->dummy_profile_pic;


                //     $user_data['working_hour'] = array_map(function($staff_working_hour){
                //         $staff_working_hour['start_time'] = $this->convertToTimestamp($staff_working_hour['start_time']);
                //         $staff_working_hour['end_time'] = $this->convertToTimestamp($staff_working_hour['end_time']);
                //         unset($staff_working_hour['break_start_time']);
                //         unset($staff_working_hour['break_end_time']);
                //         return $staff_working_hour;
                //     },$staff_working_hour);

                //     $merchant_info = Merchant::select('id')
                //                                 ->where('id',$staff->id)
                //                                 ->whereHas('staffAppointment')
                //                                 ->with('staffAppointment.detail','staffAppointment.userDetail')
                //                                 ->first();

                //     $service = [];
                //     $appointment = [];

                //     foreach($merchant_info->staffAppointment as $staff){
                //             $list['appointment_date'] = $this->convertToTimestamp($staff['appointment_date_time']);
                //             $list['service_start_time'] = $this->convertToTimestamp($staff['appointment_date_time']);
                //             $list['service_end_time'] = $this->addInterval( $this->convertToTimestamp($staff['appointment_date_time']));

                //             $price = 0;
                //             $name = [];
                //             $user_name = "";
                //             foreach($staff['detail'] as $detail){
                //                  $price = $detail->price + $price;
                //                  $name[] = $detail->name;
                //                  $user_name = $staff->userDetail->name;

                //             }

                //             $list['price'] = $price;
                //             $list['service_name'] = implode(",",$name);
                //             $list['user_detail'] = $staff->userDetail;
                //             $list['customer_name'] = $user_name;

                //         array_push($service,$list);

                //     }

                //         foreach($service as $service_value){
                //             unset($service_value['user_detail']);
                //             array_push($appointment,$service_value);
                //         }

                //         $user_data['appointments'] = $appointment;
                //         // $appointment_pagination = $this->paginate($appointment,$limit = 20,$page_no = 1);
                //         // $appointment_list = $this->removePaginatorParam($appointment_pagination->toArray());
                //         // $user_data['appointments'] = $appointment_list['data'];
                //         // $user_data['total_page'] = $appointment_list['last_page'];

                        // $response['message'] = "Staff profile";
                        // $response['data'] = $user_data;
                        // return response()->json($response,$this->success_code);
                // }
                // else{
                //     $response['message'] = "You are not authorize to access this staff member";
                //     return response()->json($response,$this->error_code);
                // }
        // }
        // else {
        //     $response['message'] = "You are not authorized";
        //     return response()->json($response,$this->unauthenticate_code);
        // }
        // }
        // if (empty($user)) {
        //         $response['message'] = "Merchant Not Exist";
        //         return response()->json($response, 401);
        //     }
    }
    /*
     *
     * Get Staff listing while adding appointment screen select_staff
     *
    */

    // public function getstaffListing(Request $request)
    // {

    //     try {
    //         if (Auth::guard('merchants')) {
    //             $staffId   =   Merchant::Join('merchant_staff', 'merchant_staff.merchant_id', '=', 'merchants.id')
    //                                    ->where('merchants.id', Auth::user()->id)
    //                                    ->where('merchant_staff.status',1)
    //                                    ->select('staff_id')
    //                                     ->get();

    //             $staffListings   =   array();
    //             $staffListings[]=   array("merchantId"=> 0, "merchantStaffId"=> 0, "merchantDetailId"=> 0, "staffName"=> "Anyone", "staffImage"=>null, "staffAvailabilityStatus"=> 1, "merchantUserType"=> 2);

    //             if(!empty($staffId)){

    //                 foreach($staffId as $staffIds){
    //                     $staffListing   =    Merchant::Join('merchant_details', 'merchant_details.merchant_id', '=', 'merchants.id')
    //                     ->Join('merchant_staff', 'merchant_staff.staff_id', '=', 'merchants.id')
    //                     ->where('merchants.id', $staffIds->staff_id)
    //                     ->select('merchants.id as merchantId', 'merchant_staff.id as merchantStaffId', 'merchant_details.id as merchantDetailId','merchant_details.merchant_name as staffName', 'merchant_details.merchant_image as staffImage', 'merchant_staff.availability_status as staffAvailabilityStatus','merchants.merchant_user_type as merchantUserType')
    //                     ->first();
    //                     if(!empty($staffListing)){
    //                         $staffListings[]   =   $staffListing;
    //                     }

    //                 // //   dd($staffListings);
    //                 //     foreach($staffListings as $list){
    //                 //         // dd($list['staffImage']);
    //                 //         ($list['staffImage'] == null) ? $this->dummy_banner : $list['staffImage'];
    //                 //         dd($list);
    //                 //     }
    //                 //     // $staffListings[]   =   $list;


    //                 }

    //                 return response()->json(['message' => 'Staff listing','data' => $staffListings]);
    //             }else{

    //                 return response()->json(['message' => 'Staff listing','data' => $staffListings]);
    //             }

    //         } else {
    //             return response()->json(['message' => 'Merchant not found, Please login'], 401);
    //         }
    //     }catch(\Exception $e){
    //         return response()->json(['message' => $e->getMessage()], 401);
    //     }
    // }


    public function getstaffListing(Request $request){
        if (Auth::guard('merchants')) {
             $validator = Validator::make(
                $request->all(),
                ['timezone'=>'required']
            );


            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }
            $id =  Auth::id();

            $day_key = 0;
            $timezone = $request->timezone;            
            $date  = 0; 
            
            
            if($request->datetime){
                $day = $request->datetime;
                $date = $this->convertToDate($day);
                $day = $this->getDay($day,$timezone);
                $day_key = array_search($day,$this->days);
                
            }            

            $staff_avail = [];
            $hours = [];

            $merchant_staff = MerchantStaff::where('merchant_id',$id)
                                            ->select('merchant_id','staff_id','status')
                                            ->with('staffDetail','workingHours','staffAppointment')                                            
                                            ->get()->toArray();
          

            $staff_list = array_map(function($merchant_staff) use ($timezone, $day_key){
                
                $merchant_staff['week_day'] = $merchant_staff['start_time'] = $merchant_staff['end_time'] = null; 
                $merchant_staff['status'] = $this->valueExsist($merchant_staff['working_hours'],$day_key,'week_day') ? 1 : 0;

                if($merchant_staff['status'] == true){
                    $working_hours = $merchant_staff['working_hours'];
                    $working_day = array_filter($working_hours, function($working_hour) use ($day_key){
                                        if($working_hour['week_day'] == $day_key){
                                            return true;
                                        }
                                    });
                    $merchant_staff['week_day'] = $day_key;
                    $working_day = array_values($working_day);
                    $merchant_staff['start_time'] = $this->convertToTimestamp($working_day[0]['start_time'],$timezone);
                    $merchant_staff['end_time'] = $this->convertToTimestamp($working_day[0]['end_time'],$timezone);                        
                }
                
                // dump($merchant_staff);

                $merchant_staff['name'] = $merchant_staff['staff_detail']['merchant_name'];
                $merchant_staff['gender'] = $merchant_staff['staff_detail']['gender'];
                $merchant_staff['image'] = ($merchant_staff['staff_detail']['merchant_image'] == null ? $this->default_profile_image  : $merchant_staff['staff_detail']['merchant_image']);
                
                $merchant_staff['total_appointment'] = count($merchant_staff['staff_appointment']);
                unset($merchant_staff['staff_detail']);
                unset($merchant_staff['working_hours']);
                unset($merchant_staff['staff_appointment']);
                return $merchant_staff;
            },$merchant_staff);                
            $staff_list = array_values($staff_list);
            $finished_columns = array_column($staff_list, 'status');
            array_multisort($finished_columns, SORT_DESC, $staff_list);

            return response()->json(['message' => 'Staff listing','data' =>$staff_list],$this->success_code);
        } else {
            return response()->json(['message' => "Please login"], $this->unauthenticate_code);
        }
    }

    /*
        Function name : deleteStaffDetails
        Function Purpose : Delete  Staff Add By Merchant
        Created by : Sourabhs
        Created on : 20 Oct 2020
    */


    public function deleteStaffDetails(Request $request)
    {
    
        $user = Auth::user();            
        $validator = Validator::make(
            $request->all(),
            ['staff_id' => 'required']
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        if (!empty($user))
        {
            $id =  Auth::id();
            $merchantStaff = MerchantStaff::where('merchant_id',$id)->where('staff_id',$request->staff_id)->first();
            if(!empty($merchantStaff)){
                    $staffdetails = Merchant::where('id',$request->staff_id)->where('merchant_user_type',2)->first();

                    if(!empty($staffdetails)){
                        //delete staff detail from merchant tabel
                        $delete_staff = DB::table('merchants')->where('id', $request->staff_id)->where('merchant_user_type', 2)->delete();
                        $response['message'] = "Staff deleted successfully";
                        return response()->json($response,$this->success_code);
                    }
                    else{
                        $response['message'] = "Staff not found";
                        return response()->json($response,$this->error_code);
                    }
            }
        }
    }





    /*
        Function name : editStaffDetailsByMerchant
        Function Purpose : Edit Staff Details
        Created by : Sourabh
        Created on : 22 Oct 2020
        Updated by : Gurpreet Singh
        Updated on : 30 oct 2020
    */

    public function editStaffDetailsByMerchant(Request $request) {
        /*
            $validator = Validator::make($request->all(),
                    [
                        'name'          => 'required',
                        'doj'           => 'required',
                        'services'      => 'required',
                        'day'           => 'required',
                        'staff_id'      => 'required'
                    ]
                );        
            $categoryJSON = $request->services;
            $dayJSON = $request->day;
            $categoryArray = (gettype($categoryJSON) == 'string') ? json_decode($categoryJSON, true) : $categoryJSON;
            $dayArray = (gettype($dayJSON) == 'string') ? json_decode($dayJSON, true) : $dayJSON;

            foreach ($dayArray as $day) {
                if($day['start_time'] == null || $day['end_time'] == null || $day['week_day'] == null){
                    return response()->json('start_time, end_time, week_day should not be empty or null', $this->error_code);
                }
            }
            if(empty($categoryArray)){
                return response()->json('Please choose at least one service', $this->error_code);
            }
            dump($dayArray);
            dump($categoryArray);
            dd($request->all());
            return $response;
            




            exit;
        */
        if (Auth::guard('merchants')){
            $user = Auth::user();
           
            $validator = Validator::make($request->all(),
                [
                    'name'          => 'required',
                    'doj'           => 'required',
                    'services'      => 'required',
                    'day'           => 'required',
                    'gender'           => 'required|in:1,2',
                    'staff_id'      => 'required',
                    'country_code' => 'required',
                    'mobile_number' => 'required|digits:10',
                    'timezone'      => 'required'
                ],
                [
                    'name.required' => ' Name is required',
                    'doj.required' => ' Date of Joining is required',
                    'services.required' => 'Services is required',
                    'day.required' => ' Day is required',
                    'staff_id.required' => 'Staff Id is required',
                    'mobile_number.required' => ' Mobile Number is Required',
                    'mobile_number.digits' => ' Please enter a Valid Mobile Number',
                ]
            );

            $timezone = $request->timezone;
            
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }            
            if (!empty($user)) {
                // dump('if statement');
                $id = Auth::id();
                $staff = new Merchant();

                $staff->mobile = $request->mobile_number;
                $staff->country_code = $request->country_code;
                // $staff->date_of_joining = $this->convertToLocalTime($request->doj,$timezone);
                $staff->merchant_user_type = '2';
                $staff->date_of_joining = $this->convertToLocalTime($request->doj,$request->timezone);

               

                $staff1 = new MerchantDetail();
                $staff1->merchant_id = $id;
                $staff1->merchant_name = $request->name;
                $categoryJSON = $request->services;
                $dayJSON = $request->day;

                $categoryArray = $categoryJSON;
                $dayArray = $dayJSON;


                $categoryArray = (gettype($categoryJSON) == 'string') ? json_decode($categoryJSON, true) : $categoryJSON;
                $dayArray = (gettype($dayJSON) == 'string') ? json_decode($dayJSON, true) : $dayJSON;



                $break_start_time = null;
                $break_end_time   = null;

                foreach ($categoryArray as $key => $value) {

                    $category_id =  Service::select('service_categories_id')->where('id', '=', $value)->first();
                    $category = $category_id['service_categories_id'];

                    if ($category == NULL) {
                        $response['message'] = "Category cannot be empty ";
                        return response()->json($response);
                    }

                    if ($value == NULL) {
                        $response['message'] = "Service cannot be empty ";
                        return response()->json($response);
                    }
                }

                foreach ($dayArray as $key1 => $value1) {

                    $service_working = new MerchantsWorkingHour();

                    $service_working->week_day = $value1["week_day"];

                    if ($value1["week_day"] == NULL) {
                        $response['message'] = "Day cannot be empty ";
                        return response()->json($response, $this->error_code);
                    }
                    if ($value1["start_time"] == NULL) {
                        $response['message'] = "Start time cannot be empty ";
                        return response()->json($response, $this->error_code);
                    }

                    if ($value1["end_time"] == NULL) {
                        $response['message'] = "End time cannot be empty ";
                        return response()->json($response, $this->error_code);
                    }
                    if (($value1["start_time"]) == "") {
                        $response['message'] = "Start time  cannot be empty";
                        return response()->json($response, $this->error_code);
                    }
                    if (($value1["end_time"]) == "") {
                        $response['message'] = "End time  cannot be empty";
                        return response()->json($response, $this->error_code);
                    }

                    if (($value1["start_time"]) > ($value1["end_time"])) {
                        $response['message'] = "Start time is always less than End time ";
                        return response()->json($response, $this->error_code);
                    }


                    if (($value1["start_time"]) > ($value1["end_time"])) {
                        $response['message'] = "Start time is always less than End time ";
                        return response()->json($response, $this->error_code);
                    }
                    if (($value1["start_time"]) == ($value1["end_time"])) {
                        $response['message'] = "Start time and  End time  cannot be same";
                        return response()->json($response, $this->error_code);
                    }

                    //if break start empty or null

                    // if($value1["break_start_time"] == "0" || $value1["break_start_time"] == ""){
                    //     $value1["break_start_time"] = null;

                    // }
                    // else{
                    //     if(($value1["start_time"]) > ($value1["break_start_time"])) {
                    //         $response['message'] = "Break start time is always Greater than Start time ";
                    //         return response()->json($response,$this->error_code);

                    //     }
                    // }



                      //if break_end_time is empty
                    // if($value1["break_end_time"] == "0" || $value1["break_end_time"] == ""){
                    //     $value1["break_end_time"] = null;

                    // }
                    // else{
                    //     if (($value1["break_start_time"]) > ($value1["break_end_time"])) {
                    //         $response['message'] = "Break end is always Greater than Start break ";
                    //         return response()->json($response, $this->error_code);
                    //     }
                    // }

                }



            } else{

                return response()->json(['message' => 'Please login'], $this->unauthenticate_code);

            }


            if (!empty($user)) {

                $staff = new Merchant();
              
                $staff->mobile = $request->mobile_number;
                $staff->country_code = $request->country_code;
                $staff->date_of_joining = $this->convertToLocalTime($request->doj,$request->timezone);
                $staff->merchant_user_type = '2';
                $staff->staff_id = $request->staff_id;
                $staff->gender = $request->gender;
           
             
                $merchant_data = Merchant::where('id','=',$staff->staff_id)
                                ->where('merchant_user_type','=',$staff->merchant_user_type )
                                ->first();

                if (empty($merchant_data)) {

                    // add  new staff
                    // $response['message'] = "Staff Already Exist";
                    // return response()->json($response);

                }
                else{
                    $merchants_data = MerchantStaff::where('merchant_id', '=', $id)
                                    ->where('staff_id', '=', $staff->staff_id)
                                    ->first();

                    if(!empty($merchant_data)){

                        $merchant_number = Merchant::where('id','=',$staff->staff_id)
                                                    ->where('mobile','=',$staff->mobile)
                                                    ->first();
                        
                        // if($merchant_number == null){
                        //     $number_exist = Merchant::where('mobile','=',$staff->mobile)->first();
                            
                        //     if($number_exist != null){
                        //         return response()->json(['message' => 'This no is already in use'],$this->error_code);
                        //     }
                        //     else{
                                
                        //     }
                        // }
                        if($merchant_number != null){
                            $staff->mobile = $request->mobile_number;
                            $staff->country_code = $request->country_code;
                        }
                        else{
                            $number_exist = Merchant::where('mobile','=',$staff->mobile)->first();
                            
                            if($number_exist != null){
                                return response()->json(['message' => 'This number is already in use'],$this->error_code);
                            }
                            else{
                                $staff->mobile = $request->mobile_number;
                                $staff->country_code = $request->country_code;
                            }
                        }

                        
                        $update_doj = DB::table('merchants')->where('id','=',$staff->staff_id)
                                                            ->update([
                                                                'date_of_joining' => $staff->date_of_joining,
                                                                'country_code' => $staff->country_code,
                                                                'mobile' =>  $staff->mobile
                                                                ]);

                        $update_merchant_details = DB::table('merchant_details')
                                                    ->where('merchant_id','=',$staff->staff_id)
                                                    ->update([
                                                        'merchant_name' => $request->name,
                                                        'gender' => $request->gender,
                                                         ]);


                                     /*
                                    //$dayJSON = $request->day;

                                    //$dayArray = json_decode($dayJSON, true);
                                     */
                                    $working_hours_data = [];

                                foreach ($dayArray as $key1 => $value1) {

                                    $service_working = new MerchantsWorkingHour();

                                    $service_working->merchant_id = $staff->staff_id;
                                    $service_working->week_day = $value1["week_day"];
                                        if ($value1["week_day"] == NULL) {
                                            $response['message'] = "Day cannot be empty ";

                                            return response()->json($response, $this->error_code);
                                        }
                                        if ($value1["start_time"] == NULL) {
                                            $response['message'] = "Start time cannot be empty ";

                                            return response()->json($response,$this->error_code);
                                        }

                                        if ($value1["end_time"] == NULL) {
                                            $response['message'] = "End time cannot be empty ";

                                            return response()->json($response, $this->error_code);
                                        }
                                        if (($value1["start_time"]) == "") {
                                            $response['message'] = "Start time  cannot be empty";

                                            return response()->json($response,$this->error_code);
                                        }
                                        if (($value1["end_time"]) == "") {
                                            $response['message'] = "End time  cannot be empty";

                                            return response()->json($response, $this->error_code);
                                        }

                                        if (($value1["start_time"]) > ($value1["end_time"])) {
                                            $response['message'] = "Start time is always less than End time ";

                                            return response()->json($response, $this->error_code);
                                        }
                                        if (($value1["start_time"]) == ($value1["end_time"])) {
                                            $response['message'] = "Start time and  End time  cannot be same";

                                            return response()->json($response, $this->error_code);
                                        }

                                      //if break start empty or null

                                        // if($value1["break_start_time"] == "0" || $value1["break_start_time"] == ""){
                                        //     $value1["break_start_time"] = null;

                                        // }
                                        // else{
                                        //     if(($value1["start_time"]) > ($value1["break_start_time"])) {
                                        //         $response['message'] = "Break start time is always Greater than Start time ";
                                        //         return response()->json($response, $this->error_code);

                                        //     }
                                        // }



                                        //if break_end_time is empty
                                        // if($value1["break_end_time"] == "0" || $value1["break_end_time"] == ""){
                                        //     $value1["break_end_time"] = null;

                                        // }
                                        // else{
                                        //     if (($value1["break_start_time"]) > ($value1["break_end_time"])) {
                                        //         $response['message'] = "Break end is always Greater than Start break ";
                                        //         return response()->json($response, $this->error_code);
                                        //     }
                                        // }

                                        if($value1["break_start_time"] == null){
                                            $break_start_time  = null;

                                        }
                                        else{
                                            // $break_start_time = date("Y-m-d h:i:s ", ($value1["break_start_time"])/1000);
                                            $break_start_time = $this->convertToLocalTime($value1["break_start_time"],$timezone);

                                        }

                                        if($value1["break_end_time"] == null){
                                            $break_end_time  = null;

                                        }
                                        else{
                                            // $break_end_time = date("Y-m-d  h:i:s ",($value1["break_end_time"])/1000);
                                            $break_end_time = $this->convertToLocalTime($value1["break_end_time"],$timezone);

                                        }

                                        $check =  MerchantsWorkingHour::where('week_day', '=', $value1["week_day"])->where('merchant_id', '=',  $staff->staff_id)->count();
                                        $service_working = new MerchantsWorkingHour();
                                        if ($check == Null) {

                                            $start_time = $this->convertToLocalTime($value1["start_time"],$timezone);
                                            $end_time = $this->convertToLocalTime($value1["end_time"],$timezone);

                                            // $break_start_time = date("Y-m-d h:i:s ", ($value1["break_start_time"])/1000);
                                            // $break_end_time = date("Y-m-d  h:i:s ", ($value1["break_end_time"])/1000);

                                            $service_working->merchant_id = $staff->staff_id;
                                            $service_working->week_day = $value1["week_day"];
                                            $service_working->start_time = $start_time;
                                            $service_working->end_time = $end_time;
                                            $service_working->break_start_time = $break_start_time;
                                            $service_working->break_end_time = $break_end_time;
                                            $service_working->status = "1";



                                            if (($value1["start_time"]) < ($value1["end_time"])) {

                                                $remove_old_working_hour = DB::table('merchants_working_hours')->where('merchant_id','=',$staff->staff_id)->delete();

                                            }
                                        }

                                        if (!empty($check)) {

                                            $dayresult = MerchantsWorkingHour::select('week_day')->where('merchant_id', $staff->staff_id)->get();

                                            foreach($dayresult as $dayresults){
                                                $resday = $dayresults->week_day;
                                                MerchantsWorkingHour::where('merchant_id', '=', $staff->staff_id)
                                                ->delete();
                                            }



                                            $start_time = $this->convertToLocalTime($value1["start_time"],$timezone);
                                            $end_time = $this->convertToLocalTime($value1["end_time"],$timezone);

                                            if ($value1["start_time"] < $value1["end_time"]) {


                                                $remove_old_working_hour = DB::table('merchants_working_hours')->where('merchant_id','=',$staff->staff_id)->delete();

                                                // $service_working = new MerchantsWorkingHour();
                                                $service_working->merchant_id = $staff->staff_id;
                                                $service_working->week_day = $value1["week_day"];
                                                $service_working->start_time = $start_time;
                                                $service_working->end_time = $end_time;
                                                $service_working->break_start_time = $break_start_time;
                                                $service_working->break_end_time = $break_end_time;
                                                $service_working->status = "1";


                                            }
                                        }
                                        array_push($working_hours_data,$service_working->toArray());
                                }


                                $merchantservices = new MerchantService();
                                $sevice_array = [];

                                foreach ($categoryArray as $key => $value) {
                                    // dump($value);
                                    $category_id =  Service::select('service_categories_id')->where('id', '=', $value)->first();
                                    $category = $category_id['service_categories_id'];

                                    $services_values =  Service::select('service_price', 'service_duration', 'service_buffer_time', 'service_image', 'status')->where('id', '=', $value)->first();

                                    $user_type =  Merchant::select('merchant_user_type')->where('id', '=', $staff->staff_id)->first();

                                    $remove_old_service = DB::table('merchant_services')->where('merchant_staff_id','=',$staff->staff_id)->delete();

                                    // $merchantservices = new MerchantService();

                                    $merchantservices->merchant_staff_id = $staff->staff_id;
                                    $merchantservices->service_category_id = $category;
                                    $merchantservices->service_id = $value;
                                    $merchantservices->service_image = $services_values['service_image'];
                                    $merchantservices->service_price = $services_values['service_price'];
                                    $merchantservices->service_duration = $services_values['service_duration'];
                                    $merchantservices->service_buffer_time = $services_values['service_buffer_time'];
                                    $merchantservices->user_type = $user_type['merchant_user_type'];
                                    // dump($merchantservices);
                                    $merchantservices->status = "1";

                                    if ($category == NULL) {

                                        return response()->json(['message' => 'Category cannot be empty'], $this->error_code);
                                    }

                                    $merchantservices->service_id = $value;


                                    if ($value == NULL) {
                                        $response['message'] = "Service cannot be empty ";

                                        return response()->json($response);
                                    }

                                    array_push($sevice_array,$merchantservices->toArray());

                                }



                                 $working_day = MerchantsWorkingHour::insert($working_hours_data);
                                $service_cat = MerchantService::insert($sevice_array);
                                return response()->json(['message' => 'Staff updated successfully'],$this->success_code);

                    }
                    else{
                        return response()->json(['message' => 'You are not authorized'],$this->error_code);
                    }
                }

            }else{
                    return response()->json(['message' => 'Staff not added'], $this->error_code);
                }
        }else{
                return response()->json(['message' => 'Merchant Not Exist'],$this->unauthenticate_code);
        }
    }

    private function discountAmount($actualPrice,$discount){
		return  $actualPrice - ($actualPrice * ($discount / 100));

    }
}