<?php

namespace App\Http\Controllers\v3\Api\Merchant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Service;
use App\Merchant;
use App\MerchantService;
use App\MerchantCareer;
use App\MerchantsWorkingHour;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Arr;

class TrainingAndVacancy extends Controller
{
    public function __construct(){
    
     }

      //-----//
      public function addTraining_vacancy(Request $request)
      {
        
        if (Auth::guard('merchants'))
        {
           
            $user = Auth::user();

            if (!empty($user)){

                $id =  Auth::id();
                $message = "";
                // dd($id);

                $validator = Validator::make($request->all(),
                [
                    'name' => 'required',
                    'job_description' => 'required',
                    'career_type' => 'required'
                ],
                [
                    'name.required' => 'name is required',
                    'job_description.required' => 'Job description is required',
                    'career_type.required' => 'Career type is required',
                ]
                );
        
                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], 401);
                }
                
                $merchant_career = MerchantCareer::where('merchant_id',"=",$id)
                                                ->where('name',"=",$request->name)
                                                ->where('career_type',"=",$request->career_type)
                                                    ->first();
                                
                $data = array(
                    'merchant_id' => $id,
                    'name' => $request->name,
                    'job_description' => $request->job_description,
                    'career_type' => $request->career_type
                );
                
                if(empty($merchant_career)){
                    
                    if($request->career_type == "0"){
                            $data['education'] =  $request->education;
                    }
    
                    //career type vacancy
                    if($request->career_type == "1"){
                        $data['speciality'] =  $request->speciality;
                        $data['experience'] =  $request->experience;
                    }
                   
                    if($id = MerchantCareer::create($data)->id){
                       $msg =  ($request->career_type == "0") ? "Training course added successfully" : "Vacancy added successfully" ;
                       $ids =  ($request->career_type == "0") ? "training_id" : "vacancy_id" ;
                        // $message = array(                            
                        //     'message' => $msg ." added successfully"
                        // );
                        $data = [$ids => $id];
                        return response()->json(['message' => $msg,'data'=>$data], 200);
                    }
                    else{
                        return response()->json(['message' => 'Something went wrong'],401);
                    }
                }

                //if merchant career already saved then update

                else{
                    
                    if($request->career_type == "0"){
                        
                        $data['education'] =  $request->education;
                       
                    }
                    
                    //career type vacancy
                    if($request->career_type == "1"){
                        $data['speciality'] =  $request->speciality;
                        $data['experience'] =  $request->experience;
                    }
                
                    
                    if( MerchantCareer::where('id',$merchant_career->id)
                                       ->where('merchant_id',$id)
                                       ->where('career_type',"=",$request->career_type)
                                       ->update($data)){
                    $msg =  ($request->career_type == "0") ? "Training course" : "Vacancy" ;
                        $message = array(
                            'message' => $msg ." updated successfully"
                        );
                        return response()->json($message, 200);
                    }
                    else{
                        return response()->json(['message' => 'Something went wrong'],401);
                    }  
           
                }
            }
            else{
                return response()->json(['message' => 'Service not added. Please login'], 401);
            }      
            
        }
        else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    }


    /**
     * 
     * get vacancy and training list
     * 
     */

    public function getTraining_vacancy(Request $request){
        
        if (Auth::guard('merchants'))
        {
           
            $user = Auth::user();

            if (!empty($user)){

                $id =  Auth::id();
                // dd($id);
                
                $validator = Validator::make($request->all(),
                [
                   
                    'career_type' => 'required'
                ],
                [
                    
                    'career_type.required' => 'Career type is required',
                ]
                );
        
                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], 401);
                }
                
                
                $data = MerchantCareer::where('merchant_id',"=",$id)
                                        ->where('career_type',"=",$request->career_type)
                                        ->select('id as career_id','merchant_id','image','name','experience','speciality','education','job_description','career_type')
                                        ->get()->toArray();
                
                    $response['message'] = "Merchant career list";
                    $response['data'] =  $data;
                    return response()->json($response,200);
                 
               
            }   
            else{
                return response()->json(['message' => 'Please login'], 401);
           }
                
        }
        else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    
    }
    
     /**
     * 
     * delete vacancy and training list
     * 
     */

    public function deleteTraining_vacancy(Request $request){
        
        if (Auth::guard('merchants'))
        {
           
            $user = Auth::user();

            if (!empty($user)){

                $id =  Auth::id();
                
                $validator = Validator::make($request->all(),
                [
                   
                    'career_id' => 'required',
                    'career_type' => 'required'
                ],
                [
                    'career_type' => 'Career type is required',
                    'career_id.required' => 'Merchant career id is required'
                ]
                );
        
                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], 401);
                }
                
                //get merchant career 
                $msg = ($request->career_type == 0 ? "Training course deleted successfully" : "Vacancy deleted successfully" );

               
                $merchant_career = MerchantCareer::where('merchant_id',"=",$id)
                                        ->where('id',"=",$request->career_id)                
                                        ->where('career_type',"=",$request->career_type)
                                        ->get()->toArray();
                
                //merchant career is available then delete 
                if(!empty($merchant_career)){
                    $delete_career = MerchantCareer::where('merchant_id',"=",$id)
                    ->where('id',"=",$request->career_id)                
                    ->where('career_type',"=",$request->career_type)->delete();
                    if(!empty($delete_career)){
                        $response['message'] = $msg;
                        return response()->json($response,200);
                    }
                    else{
                        return response()->json(['message' => 'Merchant carrer not delete'],401);    
                    }
                    
                }
                else{
                    return response()->json(['message' => 'Merchant carrer not available'],401);
                }
           
            }   
            else{
                return response()->json(['message' => 'Please login'], 401);
           }
                
        }
        else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    
    }


  
    
    public function test(Request $request){
        $working_hour = Merchant::select(['id'])->where('id',Auth::id())->with('workingHours')->first();            
        
        $hours = $working_hour->workingHours->toArray();
        
        $working_hours = array_map(function($hours){
            unset($hours['merchant_id']);
            return $hours;
        },$hours);
        $data = [
            'merchant_id' => $working_hour->id,
            'working_hours' => $working_hours
        ];
        return response()->json($data,200);
    }

}