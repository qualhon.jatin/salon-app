<?php

namespace App\Http\Controllers\v5\Api;

use App\Trader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
class TraderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {   

        $limit = (isset($request->limit)) ? $request->limit : 10;      
        $traders =  Trader::where('actual_types','!=','art_gallery')
                        ->where('actual_types','!=','beauty_salon')
                        ->where('actual_types','!=','hair_care')
                        ->where('actual_types','!=','cafe')
                        ->where('actual_types','!=','campground')
                        ->where('actual_types','!=','city_hall')
                        ->where('actual_types','!=','clothing_store')
                        ->where('actual_types','!=','dentist')
                        ->where('actual_types','!=','department_store')
                        ->where('actual_types','!=','drugstore')
                        ->where('actual_types','!=','electronics_store')
                        ->where('actual_types','!=','florist')
                        ->where('actual_types','!=','grocery_or_supermarket')
                        ->where('actual_types','!=','gym')
                        ->where('actual_types','!=','jewelry_store')
                        ->where('actual_types','!=','night_club')
                        ->where('actual_types','!=','physiotherapist')
                        ->where('actual_types','!=','real_estate_agency')
                        ->where('actual_types','!=','restaurant')
                        ->where('actual_types','!=','shoe_store')
                        ->where('actual_types','!=','shopping_mall')
                        ->where('actual_types','!=','spa')
                        ->where('actual_types','!=','store')
                        ->where('actual_types','!=','university')                                                
                        ->get()->toArray();           
        
        // CHANGE LAT LNG DATA TYPE FROM STRING TO FLOAT

        $traders_list =  array_map(function($traders){
            $traders['lat'] = (float)$traders['lat'];
            $traders['lng'] = (float)$traders['lng'];
            $traders['rating'] = (float)$traders['rating'];
            $traders['total_rating'] = (int)$traders['total_rating'];            
            $traders['miles'] = null;
            return $traders;
       },$traders);

            $filter_list = [
                1 => "plumber",
                2 => "electrician",
                3 => "hardware_store",
                4 => "painter",
                5 => "general_contractor",
                6 => "roofing_contractor",
                7 => "locksmith",
                8 => "home_goods_store",
                9 => "car_repair"
            ];

            $actual_type = explode(',',$request->actual_type);
            $list = [];
            if(!$request->actual_type || empty($request->actual_type))
            {
                $traders = $traders;
            }
            if($request->actual_type) {
                    foreach($actual_type as $value)
                    {
                        if($value > 9)
                        {
                            $data['message']='No Trader Found';
                            $data['data'] = [];
                            return $data;
                        }

                        $match  = $filter_list[$value];
                        $traders = array_values(array_filter($traders_list,function($trader) use ($match){            
                                    if($trader['actual_types'] == $match)
                                        return true;
                        }));  
                        foreach($traders as $traders)
                        {
                            array_push($list,$traders);
                        }
                    }   
                $traders = $list;
            } 
            


        //  GET TRADERS LIST BASED ON LAT LONG
        if($request->lat != null && $request->lng != null){
            $traders_list =  array_map(function($traders) use ($request){            
                            $miles = $this->getDistanceBetweenPointsNew($traders['lat'],$traders['lng'],$request->lat,$request->lng);
                            $distance = ($request->distance == null) ? 20 : $request->distance;
                            if($miles <= $distance) {                                
                                $traders['miles'] = $miles;
                                return $traders;
                            }                                                      
                        },$traders);
            $traders = array_values(array_filter($traders_list));
            $columns = array_column($traders, 'miles');
            array_multisort($columns, SORT_ASC, $traders);            
        }

        // GET TRADERS LIST BASED ON SEARCH KEYWORD
        if($request->keyword != null){
            $traders_list = array_filter($traders,function($trader) use ($request){
                                $search_keyword = strtolower($request->keyword);
                                if((strpos(strtolower($trader['actual_types']), $search_keyword) !== false) || (strpos(strtolower($trader['name']), $search_keyword) !== false)){
                                    return true;
                                }
                            });
            $traders = array_values(array_filter($traders_list));
        }

        $traders__ = array_map(function($traders){
            $traders['actual_types'] = ucfirst(str_replace('_', ' ', $traders['actual_types'])); 
            return $traders;                       
        },$traders);

        $traders = $this->paginate($traders__,$request->limit,$request->page)->toArray();                
        $traders['data'] = array_values($traders['data']);
        $traders = $this->removePaginatorParam($traders);

        return response()->json($traders, $this->success_code);               
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Trader  $trader
     * @return \Illuminate\Http\Response
     */
    public function show(Trader $trader)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trader  $trader
     * @return \Illuminate\Http\Response
     */
    public function edit(Trader $trader)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trader  $trader
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trader $trader)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trader  $trader
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trader $trader)
    {
        //
    }
}
