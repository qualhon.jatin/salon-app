<?php

namespace App\Http\Controllers\v5\Api\Merchant;

use App\Amenitie;
use App\Http\Controllers\Controller;
use App\Merchant;
use App\MerchantAddress;
use App\MerchantAmenitie;
use App\MerchantCareer;
use App\MerchantDetail;
use App\MerchantsWorkingHour;
use App\MerchantPortfolio;
use App\MerchantStaff;
use App\MerchantUser;
use App\PushToken;
use App\ReviewRating;
use App\Traits\PushNotificationTrait;
use App\User;
use App\UsersDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    // merchants
    use PushNotificationTrait;
    public $successStatus = 200;
    /*
     *
     * Merchant login
     *
     */

    public function login(Request $request)
    {
        if (Auth::guard('merchants')) {
            $validator = Validator::make(
                $request->all(),
                [
                    'country_code' => 'required',
                    'mobile_number' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            $mobile_number = $request->country_code . $request->mobile_number;
            $country_code = $request->country_code;
          
            $user = Merchant::with('detail')->select('*')->where('mobile', '=', $request->mobile_number)->first();

            $user_name = (empty($user->detail) ? null : $user->detail['merchant_name']);

            $otp_status = false;
            $pin_status = false;

            if (!empty($user)) {                
                if($user->status == 1){
                    $otp_status = ($user['is_verified'] == 1) ? true : false;
                    $pin_status = ($user['pin'] != "") ? true : false;                    
                    if (!$otp_status) {
                        $otp = ($request->mobile_number == '7876083232' || $request->mobile_number == '7854858377') ? '0000' :

                        $this->sendOTP($mobile_number);

                        if (isset($otp['status']) == "0000" && $otp['status'] == 400) {
                            return response()->json(['message' => "Please enter a valid Phone number"], $this->error_code);
                        } else {
                            Merchant::where('id', $user->id)->update(['otp' => $otp]);
                        }

                    }

                    $response = response()->json(
                        [
                            'otp_status' => $otp_status,
                            'pin_status' => $pin_status,
                            'user_type' => $user->merchant_user_type,
                            'user_name' => $user_name,
                        ],
                        $this->success_code);
                }    
                else{
                    $response = response()->json(['title'=>'Account De-activated', 'message' => "Your Account has been de-activated. Please contact the support team at support@opayn.com"], $this->error_code);
                }
                return $response;
                // $userinfo = DB::table('merchants')->where('mobile', $mobile_number)->update(array('otp' => $otp, 'country_code' => $request->country_code));
                // return response()->json(['message' => "OTP send successfully"], $this->successStatus);
            } else {

                $otp = ($request->mobile_number == '7876083232' || $request->mobile_number == '7854858377') ? '0000' :

                $this->sendOTP($mobile_number);

                if (isset($otp['status']) == "0000" && $otp['status'] == 400) {
                    return response()->json(['message' => "Please enter a valid Phone number"], $this->error_code);
                }
                $input = array(
                    'otp' => $otp,
                    'mobile' => $request->mobile_number,
                    'country_code' => $country_code,
                );

                $user = Merchant::create($input);
                return $response = response()->json(
                    [
                        'otp_status' => $otp_status,
                        'pin_status' => $pin_status,
                        'user_type' => 1,
                        'user_name' => $user_name,
                    ], $this->success_code);
            }
        }
    }

    /*
     *
     * Merchant OTP verify
     *
     */

    public function verify(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'otp' => 'required|digits:4',
                'country_code' => 'required',
                'mobile_number' => 'required',

            ],
            [
                'otp.required' => ' OTP is Required',
                'otp.digits' => ' Please enter a Valid OTP',
                'mobile_number.required' => ' Mobile number is Required',
                'mobile_number.digits' => ' mobile number is not valid',
            ]
        );

        // if (!isset($request->push_token) || $request->push_token == null) {

        //     $request->merge([
        //         'push_token' => $this->randomNo(15),
        //     ]);
        // }

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

        $otp = $request->otp;
        $mobile = $request->mobile_number;

        // $mobile = substr($mobile, -10);
        $checkmobile = Merchant::where('mobile', '=', $mobile)->first();
        if (!empty($checkmobile)) {

            $user = Merchant::where('mobile', '=', $mobile)->where('otp', '=', $otp)->first();
            if (!empty($user)) {

                $checksteps = Merchant::select('verification_level', 'merchant_user_type')->where('mobile', '=', $mobile)->first();

                $userinfo = DB::table('merchants')->where('mobile', $mobile)->update(array('otp' => '', 'is_verified' => 1));
                $success['token'] = $user->createToken('MyApp')->accessToken;
                $success['verification_level'] = $checksteps->verification_level;

                $response['message'] = "User verified succesfully";
                $response['data'] = $user->createToken('MyApp')->accessToken;
                $data['token'] = $user->createToken('MyApp')->accessToken;
                $data['verification_level'] = $checksteps->verification_level;
                $data['user_type'] = $checksteps->merchant_user_type;
                $data['merchant_id'] = $user->id;
                $response['data'] = $data;
                // $user_type = ($data['user_type'] == 1) ? 2 : 3;
                $user_type = $data['user_type'];

                // if (empty(PushToken::where('user_id', $user->id)->where('token', $request->push_token)->first())) {

                //     PushToken::create([
                //         'user_id' => $data['merchant_id'],
                //         'user_type' => $user_type,
                //         'token' => $request->push_token,
                //     ]);
                // }

                return response()->json($response, $this->success_code);

            } else {

                return response()->json(['message' => 'Please enter a Valid OTP'], $this->error_code);

            }
        } else {
            return response()->json(['message' => 'Mobile Number does not Exist.'], $this->error_code);
        }
    }

    /*
     *
     * Merchant logout
     *
     */

    public function logout(Request $request)
    {
         $validator = Validator::make(
            $request->all(),
            [
                'push_token' => 'required',
                'user_type' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

        if (Auth::guard('merchants')) {

            $user = auth()->guard('merchants')->user()->toArray();
            if (PushToken::where(['user_id' => Auth::id(), 'token' => $request->push_token, 'user_type' => $request->user_type])->delete()) {
                $token = $request->user()->token();
                $token->revoke();
                $response = ['message' => 'User logged out successfully!'];
                return response($response, 200);
            }
            else{                
                $response = ['message' => 'Unable to logout user check usertype and push token'];
                return response($response, 400);
            }            
        }
    }

    /*
     *
     * Merchant Information
     *
     */

    public function merchantInfo()
    {

        $user = Auth::user();

        if ($user) {

            $merchantInfo = Merchant::where('merchants.id', $user->id)
                ->Join('merchant_details', 'merchant_details.merchant_id', '=', 'merchants.id')
                ->select('merchants.id', 'merchants.mobile', 'merchant_details.merchant_business_name', 'merchant_details.merchant_company_name', 'merchant_details.business_cover_image', 'merchant_details.merchant_name', 'merchant_details.salon_type', 'merchant_details.merchant_service_type', 'merchant_details.landline')->first();

            return response()->json(['message' => 'Merchant Infomation', 'data' => $merchantInfo], $this->success_code);
        } else {
            return response()->json(['message' => "Merchant doesn't exist! Please login"], $this->unauthenticate_code);
        }
    }

    /*
     *
     * Get merchant location screen
     *
     */

    public function getLocation()
    {
        if (Auth::guard('merchants')) {
            if (Auth::user()) {

                $merchantAddressData = MerchantAddress::where('merchant_id', Auth::user()->id)->first();
                unset($merchantAddressData->created_at);
                unset($merchantAddressData->updated_at);

                $response['message'] = "Merchant location details";
                $response['data'] = $merchantAddressData;
                return response()->json($response, $this->success_code);

            } else {
                return response()->json(['message' => 'Please login'], $this->unauthenticate_code);
            }
        } else {
            return response()->json(['message' => "Merchant doesn't exist"], $this->error_code);
        }
    }

    /*
     *
     * Edit merchant location screen
     *
     */

    public function editLocation(Request $request)
    {
        if (Auth::guard('merchants')) {
            if (Auth::user()) {

                $validator = Validator::make($request->all(),
                    [
                        'address1' => 'required',
                        'city' => 'required',
                        'postcode' => 'required',
                        'lat' => 'required',
                        'lng' => 'required',
                    ],
                    [
                        'address1.required' => 'Address1 is Required',
                        'city.required' => 'City Name is Required',
                        'postcode.required' => 'Postcode  is Required',
                        'lat.required' => 'Latitude is Required',
                        'lng.required' => 'Longitude  is Required',
                    ]
                );

                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], $this->error_code);
                }

                /* $merchantAddressData   =    MerchantAddress::where('merchant_id',Auth::user()->id)->first();

                if(!empty($merchantAddressData))
                { */
                $MerchantAddressUpdate = MerchantAddress::where('merchant_id', Auth::user()->id)
                    ->update([
                        'address1' => $request->address1,
                        'address2' => (!empty($request->address2)) ? $request->address2 : '',
                        'city' => $request->city,
                        'postcode' => $request->postcode,
                        'lat' => $request->lat,
                        'lng' => $request->lng,
                    ]);

                if ($MerchantAddressUpdate == true) {
                    $response['message'] = "Merchant address updated successfully";
                    return response()->json($response, $this->success_code);
                } else {
                    $response['message'] = "Something went wrong! Please try again";
                    return response()->json($response, $this->error_code);
                }
            } else {
                return response()->json(['message' => 'Please login'], $this->unauthenticate_code);
            }
        } else {
            return response()->json(['message' => "Merchant doesn't exist"], $this->error_code);
        }
    }

    /*
     *
     * Merchant Company Image while register company screen company_detail1
     *
     */

    public function uploadProfileImage(Request $req)
    {

        $user = Auth::user();
        if (!empty($user)) {

            $validator = Validator::make($req->all(),
                [
                    'image' => 'required|image|mimes:jpeg,png,jpg',
                    'type' => 'required',
                ]
            );
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            $type = $req->type;
            $merchant_id = Auth::id();
            switch ($type) {
                /*
                 *   Upload Business cover image
                 */
                case '1':
                    if ($image = $this->uploadFile($req->image, 'business_pictures')) {
                        if (MerchantDetail::where('merchant_id', $merchant_id)->update(['business_cover_image' => $image])) {
                            $response = response()->json(['message' => 'Business cover image uploaded successfully'], $this->success_code);
                        } else {
                            $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
                        }
                    } else {
                        $response = response()->json(['message' => 'Unable to upload image to server'], $this->error_code);
                    }
                    break;
                /*
                 *   Upload merchant profile picture
                 */
                case '2':
                    if ($image = $this->uploadFile($req->image, 'merchant_profile_pictures')) {
                        if (MerchantDetail::where('merchant_id', $merchant_id)->update(['merchant_image' => $image])) {
                            $response = response()->json(['message' => 'Profile picture uploaded successfully'], $this->success_code);
                        } else {
                            $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
                        }
                    } else {
                        $response = response()->json(['message' => 'Unable to upload image to server'], $this->error_code);
                    }
                    break;
                /*
                 *   Upload vacancy image
                 */
                case '3':
                    $validator = Validator::make($req->all(),
                        [
                            'vacancy_id' => 'required',
                        ]
                    );
                    if ($validator->fails()) {
                        return response()->json(['message' => $validator->errors()], $this->error_code);
                    }

                    $v_id = $req->vacancy_id;
                    if (MerchantCareer::where(['id' => $v_id, 'merchant_id' => $merchant_id])->first()) {
                        if ($image = $this->uploadFile($req->image, 'vacancy_pictures')) {
                            if (MerchantCareer::where(['id' => $v_id, 'merchant_id' => $merchant_id])->update(['image' => $image])) {
                                $response = response()->json(['message' => 'Vacancy image uploaded successfully'], $this->success_code);
                            } else {
                                $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
                            }
                        } else {
                            $response = response()->json(['message' => 'Unable to upload image to server'], $this->error_code);
                        }
                    } else {
                        $response = response()->json(['message' => 'You are not authorized to upload vacancy image'], $this->error_code);
                    }
                    break;
                /*
                 *   Upload training image
                 */
                case '4':
                    $validator = Validator::make($req->all(),
                        [
                            'traning_id' => 'required',
                        ]
                    );
                    if ($validator->fails()) {
                        return response()->json(['message' => $validator->errors()], $this->error_code);
                    }

                    $t_id = $req->traning_id;
                    if (MerchantCareer::where(['id' => $t_id, 'merchant_id' => $merchant_id])->first()) {
                        if ($image = $this->uploadFile($req->image, 'traning_pictures')) {
                            if (MerchantCareer::where('id', $t_id)->update(['image' => $image])) {
                                $response = response()->json(['message' => 'Training image uploaded successfully'], $this->success_code);
                            } else {
                                $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
                            }
                        } else {
                            $response = response()->json(['message' => 'Unable to upload image to server'], $this->error_code);
                        }
                    } else {
                        $response = response()->json(['message' => 'You are not authorized to upload vacancy image'], $this->error_code);
                    }
                    break;
                /*
                 *   Upload portfolio image
                 */
                case '5':
                    if ($image = $this->uploadFile($req->image, 'portfolio_pictures')) {
                        if (MerchantPortfolio::create(['merchant_id' => $merchant_id, 'image' => $image])) {
                            $response = response()->json(['message' => 'Portfolio image uploaded successfully'], $this->success_code);
                        } else {
                            $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
                        }
                    } else {
                        $response = response()->json(['message' => 'Unable to upload image to server'], $this->error_code);
                    }
                    break;
                case '6':
                    if ($image = $this->uploadFile($req->image, 'Salon_detail_pictures')) {
                        if (MerchantDetail::where('merchant_id', $merchant_id)->update(['detail_image' => $image])) {
                            $response = response()->json(['message' => 'Salon detail image uploaded successfully'], $this->success_code);
                        } else {
                            $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
                        }
                    } else {
                        $response = response()->json(['message' => 'Unable to upload image to server'], $this->error_code);
                    }
                    break;
                default:
                    $response = response()->json(['message' => 'Invalid input'], $this->error_code);
                    break;
            }

            /*
        if ($type == "1") {

        $company = new MerchantDetail();

        $imageName = 'business_cover_image/' . time() . '.' . $request->profilepic->getClientOriginalExtension();
        $image = $request->file('profilepic');
        $t = Storage::disk('s3')->put($imageName, file_get_contents($image), 'public');
        $imageName = Storage::disk('s3')->url($imageName);

        try {
        MerchantDetail::where('merchant_id', $user->id)->update(['business_cover_image' => $imageName]);
        $response['message'] = "Image Upload Successfully";
        }catch(\Exception $e){
        return response()->json(['message' => 'Image Not upload! Something Went Wrong'], 401);
        }

        return response()->json($response);

        }else {

        return response()->json(['message' => 'Please Select Type'], 400);
        }
         */

        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }

        return $response;
    }

    /*
     *
     * user details edit by merchant apart from profile picture screen Edit_client
     *
     */

    public function editUserDetails(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            $validator = Validator::make(
                $request->all(),
                [
                    'name' => 'required',
                    'country_code' => 'required',
                    'mobile_number' => 'required',
                    // 'address1' => 'required',
                    // 'city' => 'required',
                    // 'postalcode' => 'required',
                    'user_id' => 'required',
                ]
            );
            // print_r($user);die();

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            if (!empty($user)) {
                $id = Auth::id();

                $user1 = new User();

                $user1->mobile = $request->mobile_number;

                $user = new UsersDetail();
                $user->user_id = $request->user_id;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->user_address1 = $request->address1;
                $user->user_address2 = $request->address2;
                $user->gender = $request->gender;
                $user->user_postcode = $request->postalcode;
                $user->user_city = $request->city;
                $user->nickname = $request->nickname;

                $userdata = User::where('id', '=', $user->user_id)
                    ->where('is_user_verified', '=', 0)
                    ->first();

                if (!empty($userdata)) {
                    $usercheck = DB::table('merchant_users')
                        ->where('merchant_id', $id)
                        ->where('user_id', $user->user_id)
                        ->first();

                    if (!empty($usercheck)) {
                        $merchantcheck = DB::table('users_details')
                            ->where('added_user_type', 2)
                            ->where('added_by', $id)
                            ->where('user_id', $user->user_id)
                            ->first();

                        if (!empty($merchantcheck)) {
                            $update = DB::table('users_details')
                                ->where('user_id', $user->user_id)
                                ->update([
                                    'name' => $user->name,
                                    'email' => $user->email,
                                    'user_address1' => $user->user_address1,
                                    'user_address2' => $user->user_address2,
                                    'gender' => $user->gender,
                                    'user_postcode' => $user->user_postcode,
                                    'user_city' => $user->user_city,
                                    'birthday' => $request->birthday,
                                    'anniversary' => $request->anniversary,
                                    'allergies' => $request->allergies,
                                    'phone_preference' => $request->phone_preference,
                                    'text_preference' => $request->text_preference,
                                    'nickname' => $request->nickname,
                                ]);

                            $response['message'] = "Client details updated successfully";
                            return response()->json($response, $this->success_code);

                        } else {
                            $response['message'] = "This is an Opayn user. You are not Authorized to update this client's information";
                            return response()->json($response, $this->error_code);
                        }
                    } else {
                        $response['message'] = "You are not authorized to edit this client";
                        return response()->json($response, $this->error_code);
                    }

                } else {
                    $response['message'] = "You are not authorized to edit this client";
                    return response()->json($response, $this->error_code);
                }

            } else {
                $response['message'] = "Please login";
                return response()->json($response, $this->unauthenticate_code);
            }
        } else {
            $response['message'] = "Unauthorized";
            return response()->json($response, $this->unauthenticate_code);
        }

    }

    /*
     *
     * user list added by merchant screen client_list
     *
     */

    public function userList(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['timezone' => 'required']
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->unauthenticate_code);
        }

        $timezone = $request->timezone;
        $user_list = MerchantUser::select('id', 'merchant_id', 'user_id')->where('merchant_id', Auth::id())->with('user', 'detail', 'appointments')->get()->toArray();

        // dd($user_list);
        if ($user_list == null) {
            $response['message'] = "Client Not Exist";
            $response['data'] = [];
            return response()->json($response, $this->success_code);
        } else {
            $list = array_map(function ($user_list) use ($timezone) {
                $appointments = $user_list['appointments'];
                $user_list['id'] = $user_id = $user_list['user_id'];
                $user_list['name'] = $user_list['detail']['name'];
                $user_list['nickname'] = $user_list['detail']['nickname'];
                $user_list['user_image'] = ($user_list['detail']['user_image'] != null) ? $user_list['detail']['user_image'] : $this->default_profile_image;
                $user_list['mobile'] = $user_list['user']['mobile'];
                $user_list['country_code'] = $user_list['user']['country_code'];
                $user_list['email'] = $user_list['detail']['email'];
                $user_list['allergies'] = $user_list['detail']['allergies'];

                $appointment_list = [];
                $user_list['total_appointments'] = 0;
                $user_list['latest_appointment'] = null;
                $user_list['latest_appointment_id'] = null;
                $user_list['latest_appointment_date_time'] = null;

                foreach ($appointments as $appointment) {
                    if ($appointment['user_id'] == $user_id) {
                        $data['id'] = $appointment['id'];
                        $data['appointment_date_time'] = $this->convertToTimestamp($appointment['appointment_date_time'], $timezone);
                        $user_list['total_appointments']++;
                        array_push($appointment_list, $data);
                    }
                }
                if (!empty($appointment_list)) {
                    $appointment_id = max(array_column($appointment_list, 'id'));
                    $latest_appoientment_index = array_search($appointment_id, array_column($appointment_list, 'id'));
                    $user_list['latest_appointment'] = $appointment_list[$latest_appoientment_index];

                    $user_list['latest_appointment_id'] = $user_list['latest_appointment']['id'];
                    $user_list['latest_appointment_date_time'] = $user_list['latest_appointment']['appointment_date_time'];
                }

                unset($user_list['appointments']);
                unset($user_list['user']);
                unset($user_list['detail']);
                unset($user_list['user_id']);
                unset($user_list['merchant_id']);
                unset($user_list['latest_appointment']);
                return $user_list;
            }, $user_list);
            $columns = array_column($list, 'latest_appointment_date_time');
            array_multisort($columns, SORT_ASC, $list);
            $response['message'] = "Client Details";
            $response['data'] = $list;
            return response()->json($response, $this->success_code);
        }
    }

    /*
     *
     * get those client profile details and appointments which are added by Merchant
     *
     */
    public function getClientProfile_dev(Request $req)
    {
        $m_id = Auth::id();
        $u_id = $req->user_id;
        $user_profile = MerchantUser::where(['user_id' => $u_id, 'merchant_id' => $m_id])->with('detail')->first();
        $response = ['user_profile' => $user_profile];
        return response()->json($response, 200);

    }
    public function getClientProfile(Request $request)
    {

        if (Auth::guard('merchants')) {
            $user = Auth::user();
            $id = Auth::id();

            $validator = Validator::make(
                $request->all(),
                [
                    'user_id' => 'required',
                ],
                [
                    'user_id.required' => ' User_id is Required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            if (!empty($user)) {
                $user = new MerchantUser();
                $user->user_id = $request->user_id;

                $data = MerchantUser::where('user_id', '=', $user->user_id)->first();

                if (!empty($data)) {

                    $client_data = MerchantUser::where('merchant_users.merchant_id', $id)
                        ->where('merchant_users.user_id', $user->user_id)
                    // ->select('merchant_users.merchant_id')
                        ->join('users', 'merchant_users.user_id', '=', 'users.id')
                        ->join('users_details', 'users_details.user_id', '=', 'users.id')
                        ->select('users.id', 'users_details.name', 'users_details.gender', 'users_details.user_image as image', 'users.mobile', 'users.country_code', 'users_details.user_address1 as address1', 'users_details.user_address2 as address2', 'users_details.user_city as city', 'users_details.email', 'users_details.birthday', 'users_details.anniversary', 'users_details.allergies', 'users_details.phone_preference', 'users_details.text_preference', 'users_details.nickname', 'users_details.user_postcode as postcode')
                        ->first();

                    if (!empty($client_data)) {
                        if ($client_data['image'] == null || empty($client_data['image'])) {
                            $client_data['image'] = $this->default_profile_image;
                        }

                        $response['message'] = "Client Details";
                        $response['data'] = $client_data;
                        return response()->json($response, $this->success_code);

                    } else {
                        $response['message'] = "Client Details";
                        $response['data'] = "";
                        return response()->json($response, 200);
                    }
                } else {
                    $response['message'] = "User cannot added by this merchant";
                    return response()->json($response, $this->error_code);
                }

            }
            if (empty($user)) {
                $response['message'] = "Merchant Not Exist";
                return response()->json($response, $this->error_code);
            }
        }

    }

    // public function addUserProfilePicByMerchant(Request $request)
    // {
    //     // if (Auth::guard('merchants'))
    //     // {
    //         $user = Auth::user();
    //         // print_r($user->toArray());
    //         // die();
    //         $mobile_number = $user->mobile;

    //         // die();
    //         $validator = Validator::make(
    //             $request->all(),
    //             [
    //                 'profilepic' => 'required|image|mimes:jpeg,png,jpg',

    //             ],
    //             [
    //                 'profilepic.required' => '  Image is Required',

    //             ]
    //         );

    //         if ($validator->fails()) {
    //             return response()->json(['message' => $validator->errors()], 401);
    //         }

    //         $check = DB::table('users')
    //             ->where('mobile', '=', $mobile_number)->get('mobile')->first();
    //         // print_r($check);
    //         // die();
    //         if (empty($check)) {
    //             return response()->json(['message' => 'Mobile Number doesnot same'], 401);
    //         }

    //         if (!empty($user)) {

    //             $user = new User();
    //             $user->pic = !empty($request->pic) ? $request->pic : "";;

    //             $imageName = 'UserProfilePic/' . time() . '.' . $request->profilepic->getClientOriginalExtension();
    //             $image = $request->file('profilepic');
    //             $t = Storage::disk('s3')->put($imageName, file_get_contents($image), 'public');
    //             $imageName = Storage::disk('s3')->url($imageName);
    //             $user->profilepic = $imageName;
    //             //   print_r($mobile_number);
    //             // print_r($user->profilepic);
    //             // die();

    //             $update = DB::table('users')
    //                 ->where('mobile_number', '=', $mobile_number)
    //                 ->update(['profilepic' => $user->profilepic]);

    //             $response['message'] = "Image Upload Successfully";

    //             return response()->json($response);
    //         }
    //      else {
    //         return response()->json(['message' => 'Image Not upload Successfully.'], 401);
    //     }
    // }

    /*
     *
     * Add user/client details by merchant apart from profile picture screen add_client
     *
     */

    public function addUserByMerchant(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'mobile_number' => 'required',
                "country_code" => 'required',
                // 'address1' => 'required', // 'city' => 'required', // 'postalcode' => 'required', // 'gender' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

        $user_data = User::where('mobile', $request->mobile_number)->first();
        $user_id = null;
        // echo json_encode($user_data->id);
        // die();
        //    SAVING USER DATA IF USER NOT EXSIT IN DATABASE CODE START
        if ($user_data == null) {
            $user = $request->all();
            $user['otp'] = "0000";
            $user['mobile'] = $request->mobile_number;
            $user['country_code'] = $request->country_code;
            $new_user = User::create($user);
            $user_id = $new_user['id'];

            // SAVING DATA INTO USER_DETAIL TABLE
            $user['user_id'] = $user_id;
            $user['added_by'] = Auth::id();
            $user['user_address1'] = $request->address1;
            $user['user_address2'] = $request->address2;
            $user['user_city'] = $request->city;
            $user['user_postcode'] = $request->postalcode;
            $user['gender'] = $request->gender;
            $user['nickname'] = $request->nickname;
            $user['birthday'] = $request->birthday;
            $user['anniversary'] = $request->anniversary;
            $user['allergies'] = $request->allergies;
            $user['phone_preference'] = $request->phone_preference;
            $user['text_preference'] = $request->text_preference;
            $user['added_user_type'] = 2;
            UsersDetail::create($user);
        } else {
            $user_id = $user_data['id'];
            $update_user = [
                'nickname' => $request->nickname,
                'birthday' => $request->birthday,
                'anniversary' => $request->anniversary,
                'allergies' => $request->allergies,
                'phone_preference' => $request->phone_preference,
                'text_preference' => $request->text_preference,
            ];

            $update_user_detail = UsersDetail::where('user_id', $user_id)->update($update_user);

        }

        //    SAVING USER DATA IF USER NOT EXSIT IN DATABASE CODE END

        //    ADDING CLIENT INTO MERCHANT_USER TABLE CODE START

        $merchant_client = MerchantUser::where(['user_id' => $user_id, 'merchant_id' => Auth::id()])->first();
        // echo json_encode($merchant_client);
        // die();
        if ($merchant_client == null) {
            $client = [
                'user_id' => $user_id,
                'merchant_id' => Auth::id(),
            ];
            if (MerchantUser::create($client)) {
                $merchant = Merchant::where('id', Auth::id())->with('detail')->first();
                $salon_name = $merchant['detail']['merchant_business_name'];
                $data['message'] = "Client added successfully";
                $data['user_id'] = $user_id;
                $message = "Hi $request->name, $salon_name has added you as its client. You can also book your Appointments with $salon_name  in future from OPAYN user's app https://opayn.com/user.html";
                $this->sendMessage($message, $request->country_code . $request->mobile_number);
                $response = response()->json($data, $this->success_code);
            } else {
                $data['message'] = "Unable to add client";
                $response = response()->json($data, $this->error_code);
            }
        } else {
            $data['message'] = "This Client is already exist";
            $response = response()->json($data, $this->error_code);
        }

        //    ADDING CLIENT INTO MERCHANT_USER TABLE CODE END

        return $response;
    }

    /*
     *
     * Merchant Company Registration details screen company_detail1
     *
     */

    public function merchantsDetails(Request $request)
    {

        $user = Auth::user();

        if (!empty($user)) {

            $id = $user['id'];

            $validator = Validator::make($request->all(),
                [
                    'merchant_business_name' => "required",
                    'email' => 'required',
                    'landline' => 'required|numeric',
                    'merchant_name' => 'required',
                    'salon_type' => 'required|integer|between:1,3',
                ]
            );
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            $company = new MerchantDetail();

            $company->merchant_business_name = $request->merchant_business_name;
            $company->merchant_company_name = $request->merchant_company_name;
            $company->landline = $request->landline;
            $company->merchant_name = $request->merchant_name;
            $company->merchant_id = $id;
            $company->salon_type = $request->salon_type;
            $company->email = $request->email;

            $data = MerchantDetail::where('merchant_id', '=', $id)->first();

            if (empty($data)) {
                $company->save();
                $update = Merchant::where('id', $id)
                    ->update(['verification_level' => 1]);
            }
            if (!empty($data)) {
                $update = MerchantDetail::where('merchant_id', $id)
                    ->update(['merchant_business_name' => $company->merchant_business_name, 'merchant_company_name' => $company->merchant_company_name, 'landline' => $company->landline, 'merchant_name' => $company->merchant_name, 'email' => $company->email, 'salon_type' => $company->salon_type, 'merchant_id' => $id]);
                $update = Merchant::where('id', $id)
                    ->update(['verification_level' => 1]);

            }

            $response['message'] = "Company details added successfully";

            return response()->json($response, $this->success_code);
        } else {
            return response()->json([

                'message' => 'User Doesnot  Exist',
            ], $this->unauthenticate_code);
        }

    }

    /*
     *
     * Merchant Company address details screen company_address_detail
     *
     */

    public function merchantsAddress(Request $request)
    {

        $user = Auth::user();

        if (!empty($user)) {

            $id = $user['id'];

            $validator = Validator::make($request->all(),
                [
                    'address1' => 'required',
                    'city' => 'required',
                    'postcode' => 'required',
                    'lat' => 'required',
                    'lng' => 'required',

                ],
                [
                    'address1.required' => 'Address1 is Required',
                    'city.required' => 'City Name is Required',
                    'postcode.required' => 'Postcode  is Required',
                    'lat.required' => 'Latitude is Required',
                    'lng.required' => 'Longitude  is Required',

                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            $company = new MerchantAddress();
            $company->address1 = $request->address1;
            $company->address2 = $request->address2;
            $company->city = $request->city;
            $company->postcode = $request->postcode;
            $company->lat = $request->lat;
            $company->lng = $request->lng;
            $company->merchant_id = $id;

            $merchantAddress = MerchantAddress::where('merchant_id', '=', $id)->first();

            if (empty($merchantAddress)) {
                $company->save();
                $MerchantUpdate = Merchant::where('id', $id)->update(['verification_level' => 2]);
            }
            if (!empty($merchantAddress)) {
                $MerchantAddressUpdate = MerchantAddress::where('merchant_id', $id)
                    ->update([
                        'address1' => $company->address1,
                        'address2' => $company->address2,
                        'city' => $company->city,
                        'postcode' => $company->postcode,
                        'lat' => $company->lat,
                        'lng' => $company->lng,
                    ]);

                $MerchantUpdate = Merchant::where('id', $id)->update(['verification_level' => 2]);

            }

            $response['message'] = "Company address added successfully";
            return response()->json($response, $this->success_code);

        } else {

            return response()->json(['message' => "Merchant doesn't  exist, Please login!"], $this->unauthenticate_code);

        }

    }

    /*
     *
     * Merchant select service details like inhouse, Mobile, Both screen select services
     *
     */

    public function merchantServiceType(Request $request)
    {

        $user = Auth::user();

        if (!empty($user)) {

            $id = $user['id'];

            $validator = Validator::make($request->all(),
                [
                    'merchant_service_type' => "required|integer|between:1,3'",
                ],
                [
                    'merchant_service_type.required' => 'Service Type is Required',
                    'merchant_service_type.integer' => 'Please Enter Valid Service Type ',
                    'merchant_service_type.between' => 'Please Enter Valid Service Type ',
                ]
            );
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }

            $company = new MerchantDetail();
            $company->merchant_service_type = $request->merchant_service_type;
            $company->merchant_id = $id;

            $merchantDetailData = MerchantDetail::where('merchant_id', '=', $id)->first();

            if (empty($merchantDetailData)) {
                $company->save();

                $update = Merchant::where('id', $id)->update(['verification_level' => 3]);

            }
            if (!empty($merchantDetailData)) {

                $update = MerchantDetail::where('merchant_id', $id)->update
                    ([
                    'merchant_service_type' => $company->merchant_service_type,
                    'merchant_id' => $company->merchant_id,
                ]);

                $update = Merchant::where('id', $id)->update(['verification_level' => 3]);

            }

            $response['message'] = "Service type added successfully";
            return response()->json($response);

        } else {

            return response()->json(['message' => "Merchant doesn't exist, Please login!"], 401);
        }

    }

    /**
     *
     * find user/client list using ajax while adding appointment  screen add_appointment
     *
     */

    public function findUser(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
            ],
            [
                'name.required' => 'name is required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        try {

            if (Auth::guard('merchants')) //Merchant User
            {

                $userNameList = UsersDetail::Join('users', 'users.id', '=', 'users_details.user_id')
                    ->where('users_details.name', 'like', '%' . $request->name . '%')
                    ->select('users.id as userId', 'users_details.id as userDetailId', 'users_details.name as userName', 'users_details.email as userEmail', 'users.mobile as userMobile')
                    ->get();

                if (!empty($userNameList)) {
                    return response()->json(['message' => 'User infomation', 'data' => $userNameList]);
                }

            } else {

                return response()->json(['message' => "Merchant doesn't exist! Please login"], 401);

            }

        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 401);

        }

    }

/*
Function name : deleteUserDetails
Function Purpose : Delete User Add By Merchant
Created by : Sourabh
Created on : 9 Oct 2020
 */

    public function deleteUserDetails(Request $request)
    {

        if (Auth::guard('merchants')) {
            $user = Auth::user();
            $validator = Validator::make(
                $request->all(),
                [
                    'user_id' => 'required',

                ],
                [
                    'user_id.required' => ' User Id is Required',

                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }

            if (!empty($user)) {
                $id = Auth::id();
                $user1 = new User();

                $merchant_user = new MerchantUser();
                // $user->user_id = $request->user_id;
                $userdata = MerchantUser::where('merchant_id', '=', $id)
                    ->where('user_id', '=', $request->user_id)
                    ->first();

                if (!empty($userdata)) {
                    // delete user details from merchant user table
                    $delete_user = DB::table('merchant_users')
                        ->where('user_id', '=', $request->user_id)
                        ->where('merchant_id', '=', $id)
                        ->delete();

                    $response['message'] = "Client deleted successfully";
                    return response()->json($response, 200);
                } else {
                    $response['message'] = "You are not authorized";
                    return response()->json($response, 400);
                }

            }
        }

    }

    /*
    Function name : deleteStaffDetails
    Function Purpose : Delete Staff  By Merchant
    Created by : Sourabh
    Created on : 20 Oct 2020
     */

    public function deleteStaffDetails(Request $request)
    {

        if (Auth::guard('merchants')) {
            $user = Auth::user();
            // dd($user);
            $validator = Validator::make(
                $request->all(),
                [
                    'staff_id' => 'required',

                ],
                [
                    'staff_id.required' => ' Staff Id is Required',

                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }

            if (!empty($user)) {
                $id = Auth::id();
                $user1 = new User();

                $merchant_user = new MerchantStaff();
                // $user->user_id = $request->staff_id;
                $clientdata = MerchantStaff::where('merchant_id', '=', $id)
                    ->where('staff_id', '=', $request->staff_id)
                    ->first();

                if (!empty($clientdata)) {
                    $clientdetails = Merchant::where('id', '=', $request->staff_id)
                        ->where('merchant_user_type', '=', 2)
                        ->first();

                    if (!empty($clientdetails)) {
                        //delete staff detail from merchant tabel

                        $delete_client_details = DB::table('merchants')
                            ->where('id', $request->staff_id)
                            ->where('merchant_user_type', '=', 2)
                            ->delete();

                        $response['message'] = "Staff deleted successfully";
                        return response()->json($response, 200);

                    } else {
                        $response['message'] = "Staff not found";
                        return response()->json($response, 400);
                    }
                } else {
                    $response['message'] = "You are not authorized";
                    return response()->json($response, 400);
                }

            }
        }

    }

    public function getPortfolio($merchant_id = null)
    {
        if (Auth::user() || $merchant_id != null) {
            $m_id = ($merchant_id == null) ? Auth::id() : $merchant_id;
            $portfolio_list = MerchantPortfolio::select('id', 'image')->where('merchant_id', $m_id)->get();
            $merchant_detail = MerchantDetail::where('merchant_id', $m_id)->first();
            // echo json_encode($merchant_detail);
            // die();
            $data = [
                'merchant_id' => $m_id,
                'salon_name' => $merchant_detail['merchant_business_name'],
                'cover_image' => ($merchant_detail['business_cover_image'] != null) ? $merchant_detail['business_cover_image'] : $this->default_cover_image,
                'detail_image' => ($merchant_detail['detail_image'] != null) ? $merchant_detail['detail_image'] : $this->default_detail_image,
                'gallery' => $portfolio_list,
            ];
            $response = response()->json(['message' => "Portfolio list", 'data' => $data], $this->success_code);
        } else {
            $response = response()->json(['message' => "Please login"], $this->unauthenticate_code);
        }

        return ($merchant_id == null) ? $response : $data;
    }

    public function deletePortfolio(Request $req)
    {
        $validator = Validator::make(
            $req->all(),
            ['id' => 'required'],
            ['id.required' => ' Portfolio id is required']
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->unauthenticate_code);
        }

        $ids = (strpos($req->id, ',') !== false) ? explode(',', $req->id) : [$req->id];

        $response = (MerchantPortfolio::where('merchant_id', Auth::id())->whereIn('id', $ids)->delete()) ? response()->json(['message' => "Portfolio deleted successfully"], $this->success_code) : $response = response()->json(['message' => "You are not authorized to delete portfolio image"], $this->unauthenticate_code);

        return $response;
    }

    /*
    Function name : getAmenitiesListing
    Function Purpose : get merchant amenties
    Created by : Sourabh
    Created on : 21 Oct 2020
     */

    public function getAmenitiesListing(Request $request)
    {
        if (Auth::guard('merchants')) {
            $id = Auth::id();

            $amenities_listing = Amenitie::selectRaw('id as AmenityId, name as AmenityName')->get();
            $merchant_amenties = MerchantAmenitie::where('merchant_id', '=', $id)->selectRaw('id, amenitie_id, merchant_id')->get();

            $merchant_amenity = array();
            $value = "";

            foreach ($amenities_listing as $value) {
                $value['status'] = false;
                foreach ($merchant_amenties->toArray() as $amenity) {
                    if ($value->AmenityId == $amenity['amenitie_id']) {
                        $value['status'] = true;
                        break;
                    }

                }
                $merchant_amenity[] = $value;
            }

            return response()->json([
                'message' => 'Merchant Amenity Listing',
                'data' => $merchant_amenity,
            ], 200);
        } else {
            return response()->json(['message' => "Please login"], 401);
        }
    }

    /*
    Function name : setMerchantAmenitiesListing
    Function Purpose : set merchant amenties
    Created by : Sourabh
    Created on : 21 Oct 2020
     */

    public function setMerchantAmenitiesListing(Request $request)
    {
        if (Auth::guard('merchants')) {

            $user = Auth::user();

            $validator = Validator::make($request->all(),
                [
                    'amenity_id' => 'required',
                ],
                [
                    'amenity_id.required' => 'Amenity Id is required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }

            if (!empty($user)) {

                $id = Auth::id();

                $amenityJSON = $request->amenity_id;

                $amenityJSON = json_decode($amenityJSON, true);
                // dd($amenityJSON);
                $amenity_list = array();

                foreach ($amenityJSON as $key1 => $value) {
                    $data = array(
                        'amenitie_id' => $value,
                        'merchant_id' => $id,
                        'created_at' => date("y-m-d 00:00:00"),
                        'updated_at' => date("y-m-d 00:00:00"),

                    );
                    array_push($amenity_list, $data);
                }
                // dd($amenity_list);
                $remove_amenities = MerchantAmenitie::where('merchant_id', $id)->delete();
                $set = MerchantAmenitie::insert($amenity_list);
                if ($set) {
                    $message = (empty($amenityJSON)) ? "Merchant amenities removed successfully" : "Merchant amenities added successfully";
                    return response()->json(['message' => $message], $this->successStatus);
                } else {
                    return response()->json(['message' => "Merchant amenities not added, Please login!"], 401);
                }

            } else {

                return response()->json(['message' => "You are not authorized, Please login!"], 401);

            }
        } else {

            return response()->json(['message' => "Unauthorized"], 401);

        }
    }

    /*
    Function name : getMerchantReviewList
    Function Purpose : Get  merchant review
    Created by : Sourabh
    Created on : 24 Oct 2020
     */

    public function getMerchantReviewList(Request $request)
    {
        if (Auth::guard('merchants')) {
            $id = Auth::id();

            $validator = Validator::make($request->all(),
                [
                    'timezone' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }

            $user1 = Auth::User();

            $timezone = $request->timezone;

            $merchant_review = ReviewRating::where('merchant_id', $id)
                ->select('id as review_id', 'merchant_id', 'user_id', 'appointment_id', 'rating', 'review', 'created_at')
                ->with('userDetail')->get()->toArray();

            $review_list = array_map(function ($merchant_review) use ($timezone) {

                $merchant_review['name'] = $merchant_review['user_detail']['name'];
                $merchant_review['image'] = ($merchant_review['user_detail']['user_image'] == null) ? $this->default_profile_image : $merchant_review['user_detail']['user_image'];
                $merchant_review['created_at'] = $this->convertToTimestamp($merchant_review['created_at'], $timezone);
                unset($merchant_review['user_detail']);
                return $merchant_review;
            }, $merchant_review);

            if ($review_list == null) {
                return response()->json([
                    'message' => 'No reviews found',
                    'data' => $review_list,

                ], $this->success_code);
            }

            $toatl_review = count($review_list);

            $star_count = [];
            $avg_rating = 0;
            for ($i = 1; $i <= 5; $i++) {
                $star_count['star_' . $i] = 0;
                foreach ($review_list as $rating) {
                    if ($i == explode('.', $rating['rating'])[0]) {
                        $star_count['star_' . $i] = intval(round($star_count['star_' . $i] + 1));
                    }

                }
            }
            foreach ($review_list as $rating) {
                $avg_rating += $rating['rating'];
            }

            if ($toatl_review != 0) {
                $avg_rating = $avg_rating / $toatl_review;
                $avg_rating = number_format($avg_rating, 2);
                $avg_rating = (float) $avg_rating;
            }

            // $avg_rating = $avg_rating/$toatl_review;
            // $avg_rating = number_format($avg_rating,2);
            // $avg_rating =(float)$avg_rating;

            // $review_list['star_count'] = $star_count;
            // $review_list['average_rating'] = $avg_rating;

            return response()->json([
                'message' => 'Merchant review',
                'data' => array_values($review_list),
                'total_review' => $toatl_review,
                'star_count' => $star_count,
                'average_rating' => $avg_rating,
            ], 200);

        } else {
            return response()->json(['message' => "Please login"], 401);
        }
    }

    /*
    Function name : getSetting
    Function Purpose : Get  merchant setting
    Created by : Sourabh
    Created on : 11 Nov 2020
     */

    public function getSetting()
    {
        if (Auth::user()) {
            $user = Merchant::with('detail')->where('id', Auth::id())->first();
            
            $merchant_detail = $user->detail;
            $push_status = false;
            $email_push_status = false;
            $email = false;
            $pin = false;
            
            if(!empty($merchant_detail)){
                if($merchant_detail['push_status'] == 1){
                    $push_status = true;
                }
                if($merchant_detail['email_push_status'] == 1){
                    $email_push_status = true;
                }
                if($merchant_detail['email'] !=  ""){
                    $email = true;
                }
                if($user->pin !=  ""){
                    $pin = true;
                }
            }
            
             
            $setting = [
                'push_status' => $push_status,
                'email_push_status' => $email_push_status,
                'email_exist' => $email,
                'pin_status' => $pin,
            ];
            $response = response()->json(['message' => 'Merchant setting', 'data' => $setting], $this->success_code);
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    /*
    Function name : saveSetting
    Function Purpose : Save  merchant setting
    Created by : Sourabh
    Created on : 11 Nov 2020
     */

    public function saveSetting(Request $req)
    {
        if (Auth::user()) {

            $data = [
                'push_status' => $req->push_status,
                'email_push_status' => $req->email_push_status,
            ];

            $response = (MerchantDetail::where('merchant_id', Auth::id())->update($data)) ? response()->json(['message' => 'Setting updated successsfully'], $this->success_code) : $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    /*
     *
     * Merchant OTP verify
     *
     */

    public function verifyPin(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'pin' => 'required|digits:4',
                'country_code' => 'required',
                'mobile_number' => 'required',
                'push_token' => 'required'

            ]
            // [
            //     'pin.required' => ' Pin is Required',
            //     'pin.digits' => ' Please enter a Valid Pin',
            //     'mobile_number.required' => ' Mobile number is Required',
            //     'mobile_number.digits' => ' mobile number is not valid',
            // ]
        );

        if (!isset($request->push_token) || $request->push_token == null) {

            $request->merge([
                'push_token' => $this->randomNo(15),
            ]);
        }

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

        $pin = $request->pin;
        $mobile = $request->mobile_number;

        // $mobile = substr($mobile, -10);
        $checkmobile = Merchant::where('mobile', '=', $mobile)->first();

        if (!empty($checkmobile)) {

            $user = Merchant::where('mobile', '=', $mobile)->where('pin', '=', $pin)->first();

            if (!empty($user)) {

                // $checksteps = Merchant::select('verification_level', 'merchant_user_type')->where('mobile', '=', $mobile)->first();

                $userinfo = DB::table('merchants')->where('mobile', $mobile)->update(array('otp' => '', 'is_verified' => 1));
                // $success['token'] = $user->createToken('MyApp')->accessToken;
                // $success['verification_level'] = $checksteps->verification_level;

                $response['message'] = "User verified succesfully";
                $response['data'] = $user->createToken('MyApp')->accessToken;
                $data['token'] = $user->createToken('MyApp')->accessToken;
                $data['verification_level'] = $user->verification_level;
                $data['user_type'] = $user->merchant_user_type;
                $data['merchant_id'] = $user->id;
                $response['data'] = $data;
                $user_type = ($data['user_type'] == 1) ? 2 : 3;
                $user_type = $data['user_type'];

                if (empty(PushToken::where('user_id', $user->id)->where('token', $request->push_token)->first())) {
                    PushToken::create([
                        'user_id' => $data['merchant_id'],
                        'user_type' => $user_type,
                        'token' => $request->push_token,
                    ]);
                }

                // $user_name = MerchantDetail::select('merchant_name')->where('merchant_id', $user->merchant_id)->first();

                // $data = array(
                //     'title' => "Welcome Onboard!",
                //     'body' => "Hi Explore and enjoy the elite services offered by Opayn",
                //     'user_id' => $user->id,
                //     'user_type' => $user_type,
                // );

                // $this->pushNotification($data);

                return response()->json($response, $this->success_code);

            } else {

                return response()->json(['message' => 'Please enter a Valid Pin'], $this->error_code);

            }
        } else {
            return response()->json(['message' => 'Mobile Number does not Exist.'], $this->error_code);
        }
    }

    public function authSetPin(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['pin' => 'required|digits:4']
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

        $response = (Merchant::where('id', Auth::user()->id)->update(['pin' => $request->pin])) ? response()->json(['message' => 'Pin set successfully'], $this->success_code) : response()->json(['message' => 'Soemthing went wrong'], $this->success_code);
        return $response;
    }

    public function setPin(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'country_code' => 'required',
                'mobile' => 'required',
                'pin' => 'required',
                'user_type' => 'required',
                'push_token' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }        
        if (!isset($request->push_token) || $request->push_token == null) {

            $request->merge([
                'push_token' => $this->randomNo(15),
            ]);
        }
        if ((strlen($request->pin) < 4) || (strlen($request->pin) > 4)) {
            return $response = response()->json(['message' => "Please enter 4 digits pin"], $this->error_code);
        }

        $user_detail = Merchant::with('detail')->where('mobile', $request->mobile)->first();
        
        if (!empty($user_detail)) {
            $update_pin = Merchant::where('mobile', $request->mobile)->update([
                'pin' => $request->pin,
            ]);
            // if($request->user_type == 2){
                if (empty(PushToken::where('user_id', $user_detail->id)->where('token', $request->push_token)->first())) {
                    PushToken::create([
                        'user_id' => $user_detail->id,
                        'user_type' => $request->user_type,
                        'token' => $request->push_token,
                    ]);
                }
                $data   =   array(
                    'title'     => "Welcome Onboard!",
                    'body'      => "Now you can manage your appointments.",
                    'user_id'  =>  $user_detail->id,
                    'user_type' => $request->user_type
                );

                // if($user_detail->detail['push_status'] == 1){
                //     $this->pushNotification($data);
                // }

                $this->saveNotification($user_detail->id,$request->user_type,$data['title'],2,$ref_id = null);
            // }
            $token['token'] = $user_detail->createToken('MyApp')->accessToken;

            $response['message'] = "Pin set successfully";
            $response['data'] = $token;

            $response['data']['verification_level'] = $user_detail->verification_level;
            $response['data']['user_type'] = $user_detail->merchant_user_type;
            $response['data']['merchant_id'] = $user_detail->id;

            return response()->json($response, $this->success_code);

        } else {
            return $response = response()->json(['message' => "This number is not save in database"], $this->error_code);
        }

    }

    public function updatePin(Request $request) {
        // if (Auth::user()) {
        $id = Auth::id();
        $validator = Validator::make(
            $request->all(),
            [
                'old_pin' => 'required|digits:4',
                'new_pin' => 'required|digits:4',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        $user_detail = Merchant::where('id', $id)->first();

        if (!empty($user_detail)) {
            if ($user_detail->pin == $request->old_pin) {
                if ($user_detail->pin != $request->new_pin) {
                    $update_pin = Merchant::where('id', $id)->update([
                        'pin' => $request->new_pin,
                    ]);
                    $response = response()->json(['message' => "Pin updated successfully"], $this->success_code);
                } else {
                    $response = response()->json(['message' => "New pin must be different from old pin"], $this->error_code);
                }

            } else {
                $response = response()->json(['message' => "Old pin is incorrect"], $this->error_code);
            }

        } else {
            $response = response()->json(['message' => "This number is not save in database"], $this->error_code);
        }

        // return $response;

        // } else {
        //     $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        // }
        return $response;
    }

    public function forgetPin(Request $request) {
        $validator = Validator::make(
            $request->all(),
            [
                'country_code' => 'required',
                'mobile' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        $user_detail = Merchant::with('detail')->where('mobile', $request->mobile)->first();

        $mobile_no = $request->country_code . $request->mobile;
        $merchant_name = (!empty($user_detail->detail) ? $user_detail->detail['merchant_name'] : "");

        if (!empty($user_detail)) {
            if (!(isset($request->pin) && ($request->status))) {

                // $pin = ($request->mobile == '7988512302' || $request->mobile == '9530681066' || $request->mobile == '8528809228' || $request->mobile == '7854858377') ? '0000' : $this->sendPin($mobile_no, $merchant_name);

                $pin = ($request->mobile == '7988512302' || $request->mobile == '9530681066' || $request->mobile == '8528809228' || $request->mobile == '7854858377') ? $this->sendPin($mobile_no, $merchant_name) : $this->sendPin($mobile_no, $merchant_name);

                if (isset($pin['status']) == "0000" && $pin['status'] == 400) {
                    return response()->json(['message' => "Please enter a valid Phone number"], $this->error_code);
                }

                $response = response()->json(['message' => "Temporary pin has been sent on your registered mobile number", 'temp_pin' => $pin, 'token' => null, 'verification_level' => $user_detail->verification_level, 'user_type' => $user_detail->merchant_user_type, 'merchant_id' => $user_detail->id], $this->success_code);

            } else {
                $update_pin = Merchant::where('mobile', $request->mobile)->update(['pin' => $request->pin]);
                $token = $user_detail->createToken('MyApp')->accessToken;
                if(empty(PushToken::where('user_id', $user_detail->id)->where('token', $request->push_token)->first())) {
                    PushToken::create([
                        'user_id' => $user_detail->id,
                        'user_type' => 0,
                        'token' => $request->push_token,
                    ]);
                } 
                $response = response()->json([
                    'message' => "Pin set successfully", 
                    'temp_pin' => null, 
                    'token' => $token, 
                    'verification_level' => $user_detail->verification_level, 
                    'user_type' => $user_detail->merchant_user_type, 
                    'merchant_id' => $user_detail->id
                ], $this->success_code);
            }

        } else {
            $response = response()->json(['message' => "This number is not save in database"], $this->error_code);
        }

        return $response;

    }

    public function checkMobileExist(Request $request)
    {
        // if (Auth::user()) {

        $validator = Validator::make(
            $request->all(),
            [
                'country_code' => 'required',
                'mobile' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        $user_detail = Merchant::where('mobile', $request->mobile)->first();

        if (!empty($user_detail)) {
            $response = response()->json(['status' => false], $this->success_code);
        } else {
            $response = response()->json(['status' => true], $this->success_code);
        }

        return $response;
    }

    // merchant working hour
    public function getWorkingHours(Request $req)
    {
        $id = Auth::id();
        $response = ['message' => 'merchant and staff working hours list', 'data' => []];
        $this->timezone = $req->timezone;
        $week_day = array_search($this->getDay($req->dateTime, $this->timezone), $this->days);

        $merchant = Merchant::where('id', $id)->with('detail', 'workingHours', 'staff.staffDetail', 'staff.workingHours')->first();

        // echo json_encode($merchant);
        // die();


        $data['id'] = $merchant->id;
        $data['name'] = "salon";
        $data['image'] = (!empty($merchant_detail->merchant_image) ? $merchant_detail->merchant_image : $this->default_profile_image);

        $merchant_db_working_hours = $merchant->workingHours->toArray();
        $merchant_staff_db_working_hours = $merchant->staff->toArray();

        $merchant_working_hours = array_map(function ($merchant_db_working_hours) use ($req) {

            $data1['id'] = $merchant_db_working_hours['id'];
            $data1['week_day'] = $merchant_db_working_hours['week_day'];
            $data1['start_time'] = (!empty($merchant_db_working_hours['start_time']) ? $this->convertToTimestamp($merchant_db_working_hours['start_time'], $req->timezone) : null);

            $data1['end_time'] = (!empty($merchant_db_working_hours['end_time']) ? $this->convertToTimestamp($merchant_db_working_hours['end_time'], $req->timezone) : null);
            $data1['break_start_time'] = (!empty($merchant_db_working_hours['break_start_time']) ? $this->convertToTimestamp($merchant_db_working_hours['break_start_time'], $req->timezone) : null);
            $data1['break_end_time'] = (!empty($merchant_db_working_hours['break_end_time']) ? $this->convertToTimestamp($merchant_db_working_hours['break_end_time'], $req->timezone) : null);

            return $data1;
        }, $merchant_db_working_hours);

        $data['working_hour'] = $merchant_working_hours;
        $response['data'][0] = $data;

        $staff_hours_data = [];
        foreach ($merchant_staff_db_working_hours as $staff_working_hour) {
            
            $staff_data['id'] = $staff_working_hour['staff_id'];
            $staff_data['name'] = $staff_working_hour['staff_detail']['merchant_name'];
            $staff_data['image'] = (!empty($staff_working_hour['staff_detail']['merchant_image']) ? $staff_working_hour['staff_detail']['merchant_image'] : $this->default_profile_image);

            $staff_working_data = [];
            foreach ($staff_working_hour['working_hours'] as $staff_working) {

                $data2['id'] = $staff_working['id'];
                $data2['week_day'] = $staff_working['week_day'];
                $data2['start_time'] = (!empty($staff_working['start_time']) ? $this->convertToTimestamp($staff_working['start_time'], $req->timezone) : null);

                $data2['end_time'] = (!empty($staff_working['end_time']) ? $this->convertToTimestamp($staff_working['end_time'], $req->timezone) : null);
                $data2['break_start_time'] = (!empty($staff_working['break_start_time']) ? $this->convertToTimestamp($staff_working['break_start_time'], $req->timezone) : null);
                $data2['break_end_time'] = (!empty($staff_working['break_end_time']) ? $this->convertToTimestamp($staff_working['break_end_time'], $req->timezone) : null);

                array_push($staff_working_data, $data2);

            }

            $staff_data['working_hour'] = $staff_working_data;

            // array_push($staff_hours_data, $staff_data);
            array_push($response['data'], $staff_data);

        }

        // echo json_encode($response);
        // die();

        // array_push($response['data'], $staff_hours_data);

        return $response = response()->json($response, $this->success_code);

    }

    /*
    Function name : updateWorkingHour
    Function Purpose : Update working hour of merchant
    Created by : Sourabh
    Created on : 21 Oct 2020

    Modified by : Sourabh Verma
    updated on 05-Jan-2021
     */

    //update working hour

    public function updateWorkingHours(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'timezone' => 'required',
            ]
        );

   
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }
        $days = (gettype($request['day']) == 'string') ? json_decode($request['day'], true) : $request['day'];
        
        $msg = "";
        $id = "";
        $response = "";
        if(!isset($request->staff_id)){
            $id = Auth::id();
            $msg = "Merchant working hour updated successfully" ;
        }
        else{
            $id = $request->staff_id;
            $msg = "Staff working hour updated successfully" ;
        }

        if(isset($request->staff_id)){
            $merchant_staff = MerchantStaff::where('merchant_id',Auth::id())->where('staff_id',$request->staff_id)->first();
            if($merchant_staff == null){
                return $response = response()->json(['message' => 'You are not authorized to update working hour'], $this->error_code);
            }
        }
        
      
        if ($days != null) {
            $timezone = $request->timezone;
            $message = '';
            $working_hours = array_filter($days, function ($day) {
                $start_time = $day['start_time'];
                $end_time = $day['end_time'];
                $break_start = $day['break_start_time'];
                $break_end = $day['break_end_time'];
                if ($start_time < $end_time) {
              
                    return true;
                }
            });
      
            if (count($working_hours) == count($days)) {
                $working_hours_list = array_map(function ($working_hours) use ($timezone,$id,$msg) {
                    $working_hours['merchant_id'] = $id;
                    $working_hours['status'] = 1;
                    $working_hours['start_time'] = $this->convertToLocalTime($working_hours['start_time'], $timezone);
                    $working_hours['end_time'] = $this->convertToLocalTime($working_hours['end_time'], $timezone);
                    $working_hours['break_start_time'] = (($working_hours['break_start_time'] == 0) || empty($working_hours['break_start_time'])) ? null : $this->convertToLocalTime($working_hours['break_start_time'], $timezone);
                    $working_hours['break_end_time'] = (($working_hours['break_end_time'] == 0) || empty($working_hours['break_end_time'])) ? null : $this->convertToLocalTime($working_hours['break_end_time'], $timezone);
                    $working_hours['created_at'] = date("Y-m-d h:i:s"); 
                    $working_hours['updated_at'] = date("Y-m-d h:i:s"); 
                    return $working_hours;
                }, $working_hours);

               

                if (MerchantsWorkingHour::where('merchant_id', $id)->delete()) {
                    if (MerchantsWorkingHour::insert($working_hours_list)) {
                        $response = response()->json(['message' => $msg], $this->success_code);
                    } else {
                        $response = response()->json(['message' => 'Soemthing went wrong while inserting data'], $this->error_code);
                    }
                } else {
                    $response = response()->json(['message' => 'Soemthing went wrong while deleting data'], $this->error_code);
                }
            } else {
                $response = response()->json(['message' => 'Your start time should be less than your endtime'], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'Somethign went wrong'], $this->error_code);
        }

        return $response;
    }
}
