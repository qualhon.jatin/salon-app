<?php

namespace App\Http\Controllers\v5\Api\Merchant;

use App\Http\Controllers\Controller;
use App\Merchant;
use App\Service;
// use App\Category;
use App\MerchantDetail;
use App\MerchantService;
use App\ServiceCategory;
use App\MerchantStaff;
use App\MerchantsWorkingHour;
use App\Merchant_secondary_working_hour;
use App\Appointment;
use Illuminate\Support\Facades\DB;
// use App\ServiceWorkingHours;
// use App\ServiceWorkingDays;
use App\Traits\PushNotificationTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;



class ServiceController extends Controller
{
    use PushNotificationTrait;
    public $successStatus = 200;

    /*
     *
     * select merchant service while register company screen select_service
     *
     *
     */

    public function getServiceInfo(Request $request)
    {
        if (Auth::guard('merchants')) {
            $category = ServiceCategory::get()->toArray();
            $categ = array();

            foreach ($category as $key => $cat) {
                unset($cat['created_at']);
                unset($cat['updated_at']);
                unset($cat['status']);
                $categ[] = $cat;
            }

            $all_service = array();

            foreach ($categ as $key => $cat1) {

                $service = Service::select('id as service_id', 'service_categories_id as category_id', 'service_name', 'service_price', 'service_duration', 'service_buffer_time')
                    ->where('service_categories_id', '=', $cat1['id'])
                    ->get();
                $cat1['services'] = $service;
                $all_service[] = $cat1;
            }

            $merchant_services = MerchantService::where('merchant_staff_id', Auth::id())->get()->toArray();

            $service_ids = array_map(function ($merchant_services) {
                return $merchant_services['service_id'];
            }, $merchant_services);

            $final_list = $all_service;

            if ($request->status) {
                $categories = $all_service;

                $final_list = [];
                $cat_ids = [];
                $temp_array = [];
                $tmp_service_arr = [];
                $tmp_services = [];

                foreach ($categories as $key => $value) {
                    $tmp_service_arr['id'] = $value['id'];
                    $tmp_service_arr['category_name'] = $value['category_name'];
                    foreach ($value['services'] as $key_val) {
                        if (!in_array($key_val['service_id'], $service_ids)) {

                            array_push($temp_array, $key_val);
                        }
                    }
                    $tmp_service_arr['services'] = $temp_array;

                    if (!empty($temp_array)) {
                        $tmp_services[] = $tmp_service_arr;
                    }
                    $temp_array = [];

                }

                $final_list = $tmp_services;

            }

            return response()->json(['message' => true, 'data' => array_values($final_list)]);

        } else {

            return response()->json(['message' => "Merchant doesn't exist, Please login!"], 401);

        }
    }

    public function addNewService(Request $request)
    {
        if (Auth::guard('merchants')) {

            $user = Auth::user();
            $id = Auth::id();
            // dd($id);

            $validator = Validator::make(
                $request->all(),
                [
                    'service_name' => 'required',
                    'service_price' => 'required|integer',
                    'service_duration' => 'required|integer',
                    'service_buffer_time' => 'required|integer',

                ],
                [
                    'service_name.required' => ' Service Name is Required',
                    'service_price.required' => ' Service Price is Required',
                    'service_duration.required' => 'Service Duration is Required',
                    'service_buffer_time.required' => 'Service Buffer Time is Required',
                    'service_price.integer' => ' Please enter a Valid  Price',
                    'service_duration.integer' => ' Please enter a Valid  Duration',
                    'service_buffer_time.integer' => ' Please enter a Valid Buffer Time',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }

            if (!empty($user)) {

                $service = new Service();
                $service->service_categories_id = "13";
                $service->service_name = $request->service_name;
                $service->service_price = $request->service_price;
                $service->service_duration = $request->service_duration;
                $service->service_buffer_time = $request->service_buffer_time;

                $check = Service::select('*')->where('service_name', '=', $request->service_name)->first();

                if (empty($check)) {

                    $service->save();
                    return response()->json(['message' => "New service added successfully"], $this->successStatus);

                } else {

                    return response()->json(['message' => 'Service already exist'], 400);

                }

            } else {

                return response()->json(['message' => 'Service not added. Please login'], 401);

            }
        } else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    }

    public function merchantServices(Request $request)
    {

        if (Auth::guard('merchants')) {

            $user = Auth::user();

            $validator = Validator::make($request->all(),
                [
                    'services' => 'required',
                ],
                [
                    'services.required' => ' Services is required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }

            if (!empty($user)) {

                $id = Auth::id();

                DB::table('merchant_services')->where('merchant_staff_id', '=', $id)->delete();

                $categoryJSON = $request->services;

                if ($categoryJSON != "[]") {
                    $categoryArray = json_decode($categoryJSON, true);

                    $name = array();
                    foreach ($categoryArray as $key => $value) {

                        $category_id = Service::select('service_categories_id')->where('id', '=', $value)->first();

                        $category = $category_id['service_categories_id'];

                        $services_values = Service::select('service_name', 'service_price', 'service_duration', 'service_buffer_time', 'service_image', 'status')->where('service_categories_id', '=', $category)
                            ->where('id', '=', $value)
                            ->first();

                        array_push($name, $services_values->service_name);

                        $user_type = Merchant::select('merchant_user_type')->where('id', '=', $id)->first();

                        $merchantservices = new MerchantService();
                        $merchantservices->merchant_staff_id = $id;
                        $merchantservices->service_category_id = $category;
                        $merchantservices->service_id = $value;
                        $merchantservices->service_image = $services_values['service_image'];
                        $merchantservices->service_price = $services_values['service_price'];
                        $merchantservices->service_duration = $services_values['service_duration'];
                        $merchantservices->service_buffer_time = $services_values['service_buffer_time'];

                        $merchantservices->user_type = $user_type['merchant_user_type'];

                        $merchantservices->status = "1";

                        if ($category == null) {
                            // $response['message'] = "Category cannot be empty";
                            return response()->json(['message' => 'Category cannot be empty'], 400);

                        }
                        $merchantservices->service_id = $value;

                        if ($value == null) {

                            $response['message'] = "Service cannot be empty ";
                            return response()->json($response);

                        }

                        $merchantservices->save();
                        $update = DB::table('merchants')->where('id', $id)->update(['verification_level' => '4']);

                    }
                    // $name = implode(",",$name);
                    $response['message'] = " Service(s) added successfully";
                    return response()->json($response);
                } else {
                    return response()->json(['message' => 'Merchant list can not be empty'], 401);
                }

            }

        } else {

            return response()->json(['message' => 'Merchant Not Exist'], 401);

        }
    }

    public function ServiceWorkingTime(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'timezone' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }
         
        
        $days = (gettype($request['day']) == 'string') ? json_decode($request['day'], true) : $request['day'];

        $timezone = $request->timezone;

        $merchant_data = Merchant::where('id', Auth::id())->with('detail')->first();
        $mobile_number = $merchant_data['country_code'] . $merchant_data['mobile'];
        $merchant_push_status = $merchant_data['detail']['push_status'];

        $working_hours = array_filter($days, function ($day) {
            $start_time = $day['start_time'];
            $end_time = $day['end_time'];
            $break_start = $day['break_start_time'];
            $break_end = $day['break_end_time'];
            if ($start_time < $end_time) {
                if ($break_start > 0 && $break_end > 0) {
                    return true;
                }
                return true;
            }
        });
        if (count($working_hours) == count($days)) {
            $working_hours_list = array_map(function ($working_hours) use ($timezone) {
                $working_hours['merchant_id'] = Auth::id();
                $working_hours['status'] = 1;
                $working_hours['start_time'] = $this->convertToLocalTime($working_hours['start_time'], $timezone);
                $working_hours['end_time'] = $this->convertToLocalTime($working_hours['end_time'], $timezone);
                $working_hours['break_start_time'] = (($working_hours['break_start_time'] == 0) || empty($working_hours['break_start_time'])) ? null : $this->convertToLocalTime($working_hours['break_start_time'], $timezone);
                $working_hours['break_end_time'] = (($working_hours['break_end_time'] == 0) || empty($working_hours['break_end_time'])) ? null : $this->convertToLocalTime($working_hours['break_end_time'], $timezone);
                $working_hours['created_at'] = date("Y-m-d h:i:s"); 
                $working_hours['updated_at'] = date("Y-m-d h:i:s"); 
                return $working_hours;
            }, $working_hours);
            $working_hous = MerchantsWorkingHour::where('merchant_id', Auth::id())
                ->where('status', 1)
                ->first();

         

            $merchant = Merchant::with('detail')->where('id', Auth::id())->first();
            $merchant_name = (!empty($merchant->detail) ? $merchant->detail['merchant_name'] : null );
            $userType = $merchant->merchant_user_type;
            $type = 2;
         
            if ($working_hous != null) {
                $remove_working_hour = MerchantsWorkingHour::where('merchant_id', Auth::id())->delete();
                if (MerchantsWorkingHour::insert($working_hours_list)) {
                    Merchant::where('id', Auth::id())->update(['verification_level' => '5']);
                    $message = "Let your Clients book the appointments online using Opayn user app. Share the following message to your clients. Download the Opayn user app now using following link to Book appointments online https://opayn.com/user.html";
                    $this->sendMessage($message, $mobile_number);

                    $response = response()->json(['message' => 'Working hour updated successfully'], $this->success_code);
                } else {
                    $response = response()->json(['message' => 'Some error occured'], $this->error_code);
                }
            } else {
                if (MerchantsWorkingHour::insert($working_hours_list)) {
                    Merchant::where('id', Auth::id())->update(['verification_level' => '5']);
                    $data = array(
                        'title' => "Welcome Onboard!",
                        'body' => "You’ve accomplished a lot today. Let us handle the rest. Start Managing your business more effectively now.",
                        'user_type' => $userType,
                        'user_id' => Auth::id(),
                    );  
                    
                
                    // if($merchant->push_status == 1){
                        $this->pushNotification($data);
                    // }

                    $this->saveNotification(Auth::id(),$userType,$data['title'],$type,null);
                   
                    $message = "Let your Clients book the appointments online using Opayn user app. Share the following message to your clients. Download the Opayn user app now using following link to Book appointments online https://opayn.com/user.html";
                    $this->sendMessage($message, $mobile_number);

                    $response = response()->json(['message' => 'Working hours updated successfully'], $this->success_code);
                } else {
                    $response = response()->json(['message' => 'Some error occured'], $this->error_code);
                }
            }
        } else {
            $response = response()->json(['message' => 'Your start time should be less than your endtime'], $this->error_code);
        }

        return $response;
    }

    public function selectMerchantService(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            if (!empty($user)) {

                $id = Auth::id();

                // dd($id);
                //$category = MerchantService::select('id','service_category_id')->where('merchant_staff_id', '=', $id)->where('user_type', '=', 1)->groupBy('service_category_id')->get();
                $category = MerchantService::join('services', 'services.id', '=', 'merchant_services.service_id')
                    ->join('service_categories', 'service_categories.id', '=', 'merchant_services.service_category_id')
                    ->select('service_category_id as id', 'category_name')
                    ->where('merchant_staff_id', '=', $id)
                    ->where('user_type', 1)
                    ->get();

                /* $categ = array();

                foreach ($category as $key => $cat) {

                $categor = ServiceCategory::select('category_name')->where('service_categories.id', '=', $cat['service_category_id'])->get()->first();
                $name = $categor->category_name;
                $cat['category_name'] = $name;
                $categ[] = $cat;
                }
                 */

                $all_service = array();
                foreach ($category as $cat1) {

                    $services = DB::table('merchant_services')
                        ->select('merchant_services.service_id', 'merchant_services.service_category_id as category_id', 'services.service_name', 'services.service_image')
                        ->join('services', 'merchant_services.service_id', '=', 'services.id')
                        ->where('merchant_services.service_category_id', '=', $cat1['id'])
                        ->where('merchant_services.merchant_staff_id', '=', $id)
                        ->where('user_type', '=', 1)
                        ->get()->toArray();

                    $service_list = array();
                    foreach ($services as $service) {
                        if ($service->service_image == null || empty($service->service_image)) {
                            $service->service_image = $this->default_cover_image;
                        }
                        array_push($service_list, $service);
                    }

                    $cat1['service'] = $service_list;

                    $all_service[] = $cat1;

                }
                return response()->json(['message' => true, 'data' => array_values(array_unique($all_service))]);
            } else {
                return response()->json(['message' => 'User not found! Please login']);
            }
        }
    }

    public function getStaffService(Request $request)
    {

        $id = Auth::id();

        // dd($id);
        $validator = Validator::make($request->all(),
            [
                'staff_id' => 'required',
            ],
            [
                'staff_id.required' => 'Staff Id is required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $staff_id = $request->staff_id;

        //get service by merchant
        $merchant = Merchant::select('id')->where('id', $id)
            ->whereHas('merchantServices.serviceDetail')
            ->with('merchantServices.serviceDetail')
            ->first();

        if (!empty($merchant)) {
            $list = [];
            $service_status = array();
            $merchant = $merchant->toArray();

            //get service by staff
            $staff = MerchantStaff::where('staff_id', $staff_id)
                ->where('merchant_id', $id)
                ->with('staffServices')
                ->first();

            if ($staff == null) {
                return response()->json(['message' => "You are not authorized"], $this->error_code);
            } else {
                if (!empty($staff)) {
                    $staff_service_id = array();
                    $service_match = array();
                    $lists = [];
                    foreach ($staff['staffServices'] as $value) {
                        $staff_service_id[] = $value->service_id;
                        $staff_id = $value->merchant_staff_id;
                    }

                    // match service with staff service

                    foreach ($merchant['merchant_services'] as $staff_service_data) {

                        if (in_array($staff_service_data['service_id'], $staff_service_id)) {
                            $staff_service_data['isChecked'] = true;
                            $staff_service_data['merchant_staff_id'] = $staff_id;
                        } else {
                            $staff_service_data['isChecked'] = false;

                        }

                        $service_status[] = $staff_service_data;

                    }

                    $cat_list = [];
                    foreach ($service_status as $service) {
                        $cat_id = $service['service_detail']['service_categories_id'];
                        if (!in_array($cat_id, $cat_list)) {
                            array_push($cat_list, $cat_id);
                        }

                    }

                    $categories = ServiceCategory::whereIn('id', $cat_list)->get()->toArray();

                    foreach ($categories as $category) {
                        $data = [
                            // 'id'            => $category['id'],
                            'category_name' => $category['category_name'],
                            'services' => [],
                        ];

                        foreach ($service_status as $service) {
                            if ($service['service_detail']['service_categories_id'] == $category['id']) {
                                // print_r($service);

                                $service['category_id'] = $category['id'];
                                $service['service_name'] = $service['service_detail']['service_name'];
                                unset($service['service_detail']);
                                unset($service['created_at']);
                                unset($service['updated_at']);
                                unset($service['service_category_id']);
                                unset($service['status']);
                                unset($service['id']);
                                unset($service['merchant_staff_id']);
                                unset($service['user_type']);
                                unset($service['service_image']);
                                // $service['service_image'] = ($service['service_image'] == null) ? $this->default_cover_image : $service['service_image'];

                                array_push($data['services'], $service);
                            }

                        }
                        array_push($lists, $data);
                    }
                    return response()->json(['message' => "Merchant staff service list", "data" => $lists], $this->success_code);
                } else {
                    foreach ($merchant['merchant_services'] as $staff_service_data) {
                        $staff_service_data['isChecked'] = false;

                    }

                    $service_status[] = $staff_service_data;

                    $cat_list = [];
                    foreach ($service_status as $service) {
                        $cat_id = $service['service_detail']['service_categories_id'];
                        if (!in_array($cat_id, $cat_list)) {
                            array_push($cat_list, $cat_id);
                        }

                    }

                    $categories = ServiceCategory::whereIn('id', $cat_list)->get()->toArray();

                    foreach ($categories as $category) {
                        $data = [
                            // 'id'=> $category['id'],
                            'category_name' => $category['category_name'],
                            'services' => [],
                        ];

                        foreach ($service_status as $service) {
                            if ($service['service_detail']['service_categories_id'] == $category['id']) {
                                $service['category_id'] = $category['id'];
                                $service['service_name'] = $service['service_detail']['service_name'];

                                unset($service['service_detail']);
                                unset($service['created_at']);
                                unset($service['updated_at']);
                                unset($service['service_category_id']);
                                unset($service['status']);
                                unset($service['id']);
                                unset($service['merchant_staff_id']);
                                unset($service['user_type']);
                                unset($service['service_image']);
                                // $service['service_image'] = ($service['service_image'] == null) ? $this->default_cover_image : $service['service_image'];
                                array_push($data['services'], $service);
                            }
                            array_push($lists, $data);
                        }
                    }

                    return response()->json(['message' => "Merchant staff service list", "data" => $lists], $this->success_code);
                }

            }

        } else {
            return response()->json(['message' => "No service Available"], $this->unauthenticate_code);
        }

    }


    public function getWorkingHour(Request $request)
    {

        if (Auth::guard('merchants')) {

            $validator = Validator::make($request->all(),
                [
                    'timezone' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }
            $user = Auth::user();

            if (!empty($user)) {

                $id = Auth::id();

                $working_hour = Merchant::select(['id'])->where('id', Auth::id())->with('workingHours')->first();

                $hours = $working_hour->workingHours->toArray();

                if (!empty($hours)) {
                    $working_hours = array_map(function ($hours) use ($request) {
                        $hours['start_time'] = $this->convertToTimestamp($hours['start_time'], $request->timezone);
                        $hours['end_time'] = $this->convertToTimestamp($hours['end_time'], $request->timezone);
                        $hours['break_start_time'] = $this->convertToTimestamp($hours['break_start_time'], $request->timezone);
                        $hours['break_end_time'] = $this->convertToTimestamp($hours['break_end_time'], $request->timezone);
                        unset($hours['merchant_id']);
                        return $hours;
                    }, $hours);
                    $data = [
                        'merchant_id' => $working_hour->id,
                        'working_hours' => $working_hours,
                    ];
                    return response()->json($data, 200);
                } else {
                    return response()->json(['message' => 'No working hours found'], 401);
                }
            } else {
                return response()->json(['message' => 'Please login'], 401);
            }

        } else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }

    }

    public function updateWorkingHour(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'timezone' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }
        
        $merchant_id = ($request->staff_id == null) ? Auth::id() : (int)$request->staff_id;
        $user_type = ($request->staff_id == null) ? "Merchant" : "Staff";
        
        $days = (gettype($request['day']) == 'string') ? json_decode($request['day'], true) : $request['day'];
        if ($days != null) {
            $timezone = $request->timezone;
            $message = '';
            $working_hours = array_filter($days, function ($day) {
                $start_time = $day['start_time'];
                $end_time = $day['end_time'];
                $break_start = $day['break_start_time'];
                $break_end = $day['break_end_time'];
                if ($start_time < $end_time) {                    
                    return true;
                }
            });
            if (count($working_hours) == count($days)) {
                $working_hours_list = array_map(function ($working_hours) use ($timezone, $merchant_id) {
                    $working_hours['merchant_id'] = $merchant_id;
                    $working_hours['status'] = 1;
                    $working_hours['start_time'] = $this->convertToLocalTime($working_hours['start_time'], $timezone);
                    $working_hours['end_time'] = $this->convertToLocalTime($working_hours['end_time'], $timezone);
                    $working_hours['break_start_time'] = (($working_hours['break_start_time'] == 0) || empty($working_hours['break_start_time'])) ? null : $this->convertToLocalTime($working_hours['break_start_time'], $timezone);
                    $working_hours['break_end_time'] = (($working_hours['break_end_time'] == 0) || empty($working_hours['break_end_time'])) ? null : $this->convertToLocalTime($working_hours['break_end_time'], $timezone);
                    $working_hours['updated_at'] = date("Y-m-d h:i:s"); 
                    return $working_hours;
                }, $working_hours);

                if (MerchantsWorkingHour::where('merchant_id', $merchant_id)->delete()) {
                    if (MerchantsWorkingHour::insert($working_hours_list)) {                        
                        $response = response()->json(['message' => $user_type . ' working hours updated successfully'], $this->success_code);
                    } else {
                        $response = response()->json(['message' => 'Soemthing went wrong while inserting data'], $this->error_code);
                    }
                } else {
                    $response = response()->json(['message' => 'Soemthing went wrong while deleting data'], $this->error_code);
                }
            } else {
                $response = response()->json(['message' => 'Your start time should be less than your endtime'], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'Somethign went wrong'], $this->error_code);
        }
        return $response;    
    }

    public function getBusinessProfile()
    {

        if (Auth::user()) {
            $id = Auth::id();
            $merchantBussinessProfile = Merchant::with('detail', 'address')->where('id', $id)->first();

            $data['id'] = $merchantBussinessProfile->id;
            $data['country_code'] = $merchantBussinessProfile->country_code;
            $data['mobile'] = $merchantBussinessProfile->mobile;

            $data['merchant_business_name'] = (!empty($merchantBussinessProfile->detail) ? $merchantBussinessProfile->detail['merchant_business_name'] : null);
            $data['email'] = (!empty($merchantBussinessProfile->detail) ? $merchantBussinessProfile->detail['email'] : null);
            $data['merchant_company_name'] = (!empty($merchantBussinessProfile->detail) ? $merchantBussinessProfile->detail['merchant_company_name'] : null);
            $data['salon_type'] = (!empty($merchantBussinessProfile->detail) ? $merchantBussinessProfile->detail['salon_type'] : null);
            $data['merchant_name'] = (!empty($merchantBussinessProfile->detail) ? $merchantBussinessProfile->detail['merchant_name'] : null);
            $data['merchant_image'] = (!empty($merchantBussinessProfile->detail) ? $merchantBussinessProfile->detail['merchant_image'] : $this->default_profile_image);
            $data['merchant_service_type'] = (!empty($merchantBussinessProfile->detail) ? $merchantBussinessProfile->detail['merchant_service_type'] : null);
            $data['landline'] = (!empty($merchantBussinessProfile->detail) ? $merchantBussinessProfile->detail['landline'] : null);

            $data['address1'] = (!empty($merchantBussinessProfile->address) ? $merchantBussinessProfile->detail['address1'] : null);
            $data['address2'] = (!empty($merchantBussinessProfile->address) ? $merchantBussinessProfile->detail['address2'] : null);
            $data['city'] = (!empty($merchantBussinessProfile->address) ? $merchantBussinessProfile->detail['city'] : null);
            $data['postcode'] = (!empty($merchantBussinessProfile->address) ? $merchantBussinessProfile->detail['postcode'] : null);

            $response['message'] = "Merchant business details";
            $response['data'] = $data;

            return response()->json($response, $this->success_code);

        } else {
            return response()->json(['message' => 'Please login'], $this->error_code);
        }

    }

    public function editBusinessProfile(Request $request)
    {
        // if (Auth::guard('merchants'))
        // {
        if (Auth::user()) {

            $validator = Validator::make($request->all(),
                [
                    'user_type' => "required",

                ]

            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }

            if ($request->user_type == 1) {
                $validator = Validator::make($request->all(),
                    [
                        'merchant_service_type' => "required|integer|between:1,3'",
                        'merchant_business_name' => "required",
                        'landline' => 'required|numeric',
                        'salon_type' => "required|integer|between:1,3'",
                        'merchant_name' => 'required',
                        'email' => 'required',
                    ]

                );

                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], $this->error_code);
                }

                $merchantBussinessProfile = MerchantDetail::where('merchant_id', Auth::id())
                    ->update([
                        'merchant_service_type' => $request->merchant_service_type,
                        'merchant_business_name' => $request->merchant_business_name,
                        'merchant_company_name' => $request->merchant_company_name,
                        'landline' => (!empty($request->landline)) ? $request->landline : '',
                        'salon_type' => $request->salon_type,
                        'merchant_name' => $request->merchant_name,
                        'email' => $request->email,
                    ]);

                if ($merchantBussinessProfile == true) {
                    $response['message'] = "Merchant business profile updated successfully";
                    return response()->json($response, 200);
                } else {
                    $response['message'] = "Something went wrong! Please try again";
                    return response()->json($response, $this->error_code);
                }

            }
            if ($request->user_type == 2) {
                $validator = Validator::make($request->all(),
                    [
                        'merchant_name' => 'required',
                    ]

                );

                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], $this->error_code);
                }

                $merchantBussinessProfile = MerchantDetail::where('merchant_id', Auth::id())
                    ->update([
                        'merchant_name' => $request->merchant_name,
                    ]);
                if ($merchantBussinessProfile == true) {
                    $response['message'] = "Staff profile update Successfully";
                    return response()->json($response, $this->success_code);
                } else {
                    $response['message'] = "Something went wrong! Please try again";
                    return response()->json($response, $this->error_code);
                }

            }

            // $merchantBussinessProfile    =   MerchantDetail::where('merchant_id', Auth::user()->id)
            //                                 ->update([
            //                                             'merchant_service_type'     =>  $request->merchant_service_type ,
            //                                             'merchant_business_name'    =>  $request->merchant_business_name,
            //                                             'merchant_company_name'     =>  $request->merchant_company_name ,
            //                                             'landline'                  =>  (!empty($request->landline)) ? $request->landline : '',
            //                                             'salon_type'                =>  $request->salon_type,
            //                                             'merchant_name'             =>  $request->merchant_name
            //                                         ]);

            // if($merchantBussinessProfile == true){
            //     $response['message'] = "Merchant business profile update Successfully";
            //     return response()->json($response,200);
            // }else{
            //     $response['message'] = "Something went wrong! Please try again";
            //     return response()->json($response,$this->error_code);
            // }
        } else {
            return response()->json(['message' => 'Please login'], $this->error_code);
        }
        // }
        // else {
        //     return response()->json(['message' => "Merchant doesn't exist"], 401);
        // }
    }

    /*
    Function 0name : getMerchantServicesList
    Function Purpose : Get Merchant Services List
    Created by : Sourabh
    Created on : 23 Oct 2020
     */

    public function getMerchantServicesList(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            if (!empty($user)) {

                $id = Auth::id();

                $category = MerchantService::select('id', 'service_category_id')->where('merchant_staff_id', '=', $id)->where('user_type', '=', 1)->get();
                // dd($category);

                $categ = array();

                foreach ($category as $key => $cat) {

                    $categor = ServiceCategory::select('category_name')->where('service_categories.id', '=', $cat['service_category_id'])->get()->first();
                    $name = $categor->category_name;
                    $cat['category_name'] = $name;
                    $categ[] = $cat;
                }

                $all_service = array();
                foreach ($categ as $key => $cat1) {

                    $service = DB::table('merchant_services')
                        ->select('services.id', 'services.service_name', 'services.service_price', 'services.service_duration', 'services.service_buffer_time', 'services.service_image')
                        ->join('services', 'merchant_services.service_id', '=', 'services.id')
                        ->where('merchant_services.service_category_id', '=', $cat1['service_category_id'])->where('merchant_services.merchant_staff_id', '=', $id)->where('user_type', '=', 1)->where('merchant_services.id', '=', $cat1['id'])
                        ->groupBy('merchant_services.id')
                        ->get();
                    $cat1['service'] = $service;
                    $all_service[] = $cat1;
                }
                return response()->json([
                    'message' => true,
                    'data' => $all_service,
                ]);
            }
        }
    }

    /*
    Function 0name : getBussinessServiceList
    Function Purpose : Get Bussiness Service List
    Created by : Sourabh
    Created on : 23 Oct 2020
     */

    public function getBussinessServiceList(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            if (!empty($user)) {

                $id = Auth::id();

                $category = MerchantService::where('merchant_staff_id', '=', $id)
                    ->where('user_type', '=', 1)
                    ->leftJoin('services', 'services.id', '=', 'merchant_services.service_id')
                    ->select('merchant_services.id', 'merchant_services.service_id', 'merchant_services.merchant_staff_id', 'merchant_services.service_category_id', 'merchant_services.user_type', 'merchant_services.service_price', 'merchant_services.service_duration', 'merchant_services.service_buffer_time', 'merchant_services.service_image', 'merchant_services.status', 'services.service_name')
                    ->get();

                $categ = array();

                foreach ($category as $key => $cat) {

                    $cat['service_image'] = ($cat['service_image'] == null ? $this->default_salon_image : $cat['service_image']);

                    unset($cat['created_at']);
                    unset($cat['updated_at']);

                    $categ[] = $cat->toArray();
                }

                if (!empty($categ)) {
                    $response['status'] = true;
                    $response['data'] = $categ;
                    return response()->json($response, 200);

                } else {
                    $response['message'] = "No service found";
                    return response()->json($response, 401);
                }

            } else {
                return response()->json(['message' => 'Please login'], 401);
            }

        } else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    }
    /*
    Function 0name : addEditBussinessServiceList
    Function Purpose : Add/Edit  Bussiness Service List
    Created by : Sourabh
    Created on : 23 Oct 2020
     */

    public function addEditBussinessService(Request $request)
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();

            if (!empty($user)) {

                $id = Auth::id();

                $validator = Validator::make(
                    $request->all(),
                    [
                        'service_id' => 'required',
                        'service_category_id' => 'required',
                        'service_price' => 'required|numeric',
                        'service_duration' => 'required|integer',
                        'service_buffer_time' => 'required|integer',

                    ],
                    [
                        'service_id.required' => ' Service Id is Required',
                        'service_category_id.required' => ' Service Categoty Id is Required',
                        'service_price.required' => ' Service Price is Required',
                        'service_duration.required' => 'Service Duration is Required',
                        'service_buffer_time.required' => 'Service Buffer Time is Required',
                        'service_price.float    ' => ' Please enter a Valid  Price',
                        'service_duration.integer' => ' Please enter a Valid  Duration',
                        'service_buffer_time.integer' => ' Please enter a Valid Buffer Time',
                    ]
                );

                if ($validator->fails()) {
                    return response()->json(['message' => $validator->errors()], 400);
                }

                $category = MerchantService::select('id', 'service_id', 'merchant_staff_id as merchant_id', 'service_category_id as category_id', 'user_type')
                    ->where('merchant_staff_id', '=', $id)
                    ->where('user_type', '=', 1)
                    ->where('service_id', '=', $request->service_id)
                    ->where('service_category_id', '=', $request->service_category_id)
                    ->get();

                $merchant_service = [
                    'service_price' => $request->service_price,
                    'service_duration' => $request->service_duration,
                    'service_buffer_time' => $request->service_buffer_time,
                ];

                if (!empty($category->toArray())) {
                    // dd($merchant_service);
                    $update_service = DB::table('merchant_services')
                        ->where('id', '=', $category[0]->id)
                        ->where('merchant_staff_id', '=', $id)
                        ->where('user_type', '=', 1)
                        ->where('service_id', '=', $request->service_id)
                        ->update($merchant_service);

                    $response['message'] = "Merchant service updated successfully";
                    return response()->json($response, 200);

                } else {

                    $merchant_service['merchant_staff_id'] = $id;
                    $merchant_service['service_id'] = $request->service_id;
                    $merchant_service['service_category_id'] = $request->service_category_id;
                    $merchant_service['user_type'] = 1;
                    $merchant_service['status'] = $request->service_id;

                    $add_service = MerchantService::create($merchant_service);

                    if ($add_service) {
                        $response['message'] = "Merchant service added successfully";
                        return response()->json($response, 200);
                    } else {
                        $response['message'] = "Merchant service not add ";
                        return response()->json($response, 401);
                    }
                }

            } else {
                return response()->json(['message' => 'Please login'], 401);
            }

        } else {
            return response()->json(['message' => "Merchant doesn't exist"], 401);
        }
    }

    /*
    Function 0name : deleteBussinessService
    Function Purpose : Delete Bussiness Service List
    Created by : Sourabh
    Created on : 23 Oct 2020
     */

    public function deleteBussinessService(Request $request)
    {
        
        $user = Auth::user();
        $id = Auth::id();                
        $validator = Validator::make(
            $request->all(),
            [
                'service_id' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

        $services = explode(",", $request->service_id);        

        $category = MerchantService::select('id', 'service_id', 'merchant_staff_id as merchant_id', 'service_category_id as category_id', 'user_type')
            ->where('merchant_staff_id', $id)
            ->where('user_type', 1)
            ->whereIn('service_id', $services)
            ->get();        
        if (!empty($category->toArray())) {
            $delet_service = DB::table('merchant_services')
                ->where('merchant_staff_id', $id)
                ->where('user_type', 1)
                ->whereIn('service_id', $services)
                ->delete();
            if ($delet_service) {
                $response['message'] = "Merchant service(s) deleted successfully";
                return response()->json($response, $this->success_code);
            } else {
                $response['message'] = "Error in delete merchant service";
                return response()->json($response, $this->error_code);
            }
        } else {
            $response['message'] = "Merchant service not found ";
            return response()->json($response, $this->error_code);
        }       
    }

    public function updateServices(Request $request)
    {
        $id = Auth::id();
        $services = array($request['services']);
        $services = (gettype($request['services']) == 'string') ? json_decode($request['services'], true) : $request['services'];

        if (empty($services)) {
            return response()->json(['message' => 'Services cannot be null'], $this->error_code);
        }

        foreach ($services as $service_data) {

            if ($service_data['service_price'] == "") {
                return response()->json(['message' => 'Service price cannot be null'], $this->error_code);
            }
            if ($service_data['service_duration'] == "") {
                return response()->json(['message' => 'Service duration cannot be null'], $this->error_code);
            }
            if ($service_data['service_buffer_time'] == "") {
                return response()->json(['message' => 'Service buffer time cannot be null'], $this->error_code);
            }

            $data = [
                'service_price' => $service_data['service_price'],
                'service_duration' => $service_data['service_duration'],
                'service_buffer_time' => $service_data['service_buffer_time'],
            ];

            $update_service = MerchantService::where('merchant_staff_id', $id)
                ->where('service_id', $service_data['service_id'])
                ->update($data);

        }
        return response()->json(['message' => "Service updated successfully"], $this->success_code);
    }

    public function updateWorkingHourSecondary(Request $request)
    {
        $id = "";
        $timezone = $request->header('timezone');
        $id = Auth::id();

        $days = (gettype($request['day']) == 'string') ? json_decode($request['day'], true) : $request['day'];
       
        $secondary_working = [];

        $merchant_data = Merchant::where('id', Auth::id())->with('detail')->first();
        $mobile_number = $merchant_data['country_code'] . $merchant_data['mobile'];
        $merchant_push_status = $merchant_data['detail']['push_status'];

        if (isset($request->staff_id) || $request->staff_id != "" || $request->staff_id != 0) {
            $merchant_staff = MerchantStaff::where('merchant_id', $id)->where('staff_id', $request->staff_id)->first();        
            $id = $merchant_staff->staff_id;
            if (empty($merchant_staff)) {
                return $response = response()->json(['message' => 'You are not authorized to set working hour of this staff'], $this->error_code);
            }
        }
        
        // $days = $days->toArray();
        $appoinment_list = array_filter($days, function($day) use ($timezone, $id){
                                $date = date("Y-m-d", strtotime($this->convertToLocalTime($day['datetime'], $timezone)));                                
                                if(!empty($this->checkAppointment($date,$id))){
                                    return true;
                                }
                            });
        if(empty($appoinment_list) || $request->status == true){            
            foreach ($days as $working_day) {
                if ($working_day['status'] == 1) {
                    if ($working_day['start_time'] == null || $working_day['end_time'] == null) {
                        return $response = response()->json(['message' => 'Please provide start time and end time'], $this->error_code);
                    }
                }
                if ($working_day['start_time'] > $working_day['end_time']) {
                    return $response = response()->json(['message' => 'End time should greater than start time'], $this->error_code);
                }

                //get date form given timestamp
                $date = date("Y-m-d", strtotime($this->convertToLocalTime($working_day['datetime'], $timezone)));

                $this->checkAppointment($date,$id,true);
                            
                //get day number  form given timestamp
                $day_number = $this->getDayNo($working_day['datetime'], $timezone);
               
                $merchant_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $id)
                    ->whereDate('date', $date)->get()->toArray();

                if (!empty($merchant_temp_working_hour)) {
                    $update_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $id)->whereDate('date', $date)
                        ->update([
                            'date' => $date,
                            'week_day' => $day_number,
                            'start_time' => ($working_day['start_time'] == 0 ? null : $this->convertToDateTime($working_day['start_time'])),
                            'end_time' => ($working_day['end_time'] == 0 ? null : $this->convertToDateTime($working_day['end_time'])),
                            'break_start_time' => ($working_day['break_start_time'] == 0 ? null : $this->convertToDateTime($working_day['break_start_time'])),
                            'break_end_time' => ($working_day['break_end_time'] == 0 ? null : $this->convertToDateTime($working_day['break_end_time'])),
                            'status' => $working_day['status'],

                        ]);
                } 
                else {
                    $data = [
                        'merchant_id' => $id,
                        'date' => $date,
                        'week_day' => $day_number,
                        'start_time' => ($working_day['start_time'] == 0 ? null : $this->convertToDateTime($working_day['start_time'])),
                        'end_time' => ($working_day['end_time'] == 0 ? null : $this->convertToDateTime($working_day['end_time'])),
                        'break_start_time' => ($working_day['break_start_time'] == 0 ? null : $this->convertToDateTime($working_day['break_start_time'])),
                        'break_end_time' => ($working_day['break_end_time'] == 0 ? null : $this->convertToDateTime($working_day['break_end_time'])),
                        'status' => $working_day['status'],
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                    ];

                    array_push($secondary_working, $data);
                }

            }

            $update_temp_hour = Merchant_secondary_working_hour::insert($secondary_working);

            $response = response()->json(['message' => 'Working hours updated successfully','status'=>true], $this->success_code);
        }
        else{
            $appoinment_dates = array_map(function($appoinment_list){
                                   return $this->convertToDate($appoinment_list['datetime'],"d M, Y");
                                },$appoinment_list); 
            $appoinment_dates = implode(", ", $appoinment_dates);

            $response = response()->json(['message' => "You have Appointments scheduled for following dates. If you make the changes these appointments will be cancelled.\n - $appoinment_dates",'status'=> false], $this->success_code);
        }
        return $response;
    }

    //check appointment when merchant change working hour
    private function checkAppointment($date,$id, $updateStatus = false){
        $start_date = $date ." 00:00:00";
        $end_date = $date ." 23:59:59";
        
        
        if($updateStatus){            
            $appointment_ids = Appointment::where('staff_id',$id)->whereBetween('appointment_date_time' , [$start_date,$end_date])->pluck('id');
            Appointment::whereIn('id',$appointment_ids)->update(['appointment_status'=>"0"]);
        }
        else{
            $merchant_appointment  = Appointment::where('staff_id',$id)
                                            ->where('appointment_status',1)
                                            ->whereBetween('appointment_date_time' , [$start_date,$end_date])->get()->toArray();
            return $merchant_appointment;
        }
    }    

    public function getWorkingHourWeekwise(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'datetime' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }
        $timezone = $request->header('timezone');
        $id = "";
        $id = Auth::id();
        if (isset($request->staff_id) || $request->staff_id != "" || $request->staff_id != 0) {

            $merchant_staff = MerchantStaff::where('merchant_id', $id)->where('staff_id', $request->staff_id)->first();
            $id = $merchant_staff->staff_id;

            if (empty($merchant_staff)) {
                return $response = response()->json(['message' => 'You are not authorized to set working hour of this staff'], $this->error_code);
            }
        }

        return $this->getWhList($request->datetime, $id, $timezone);

    }

    private function getWhList($datetime, $id, $timezone)
    {
        // dd($id);
        $date = $datetime;

        $date = date("Y-m-d", strtotime($this->convertToLocalTime($date, $timezone)));

        $start_date = date("Y-m-d", strtotime($date));
        $start = date("Y-m-d", strtotime($date));
        $start_date = $start_date . " 00:00:00";

        $end_date = $start_date;
        $end_date = strtotime($end_date);
        $end_date = strtotime("+6 day", $end_date);
        $end = date("Y-m-d", $end_date);
        $end_date = date("Y-m-d", $end_date) . " 23:59:59";

        // get 7 days date from request timestamp
        $week_dates = $this->getDatesFromRange($start, $end);
        // 7 days date with day number
        $weekly_dates = getWeekDayFromDate($week_dates);

        $default_list = [];
        $final_list = [];

        //get merchant and its staff detail with working hour
        $merchant = $this->getMerchantAndStaff($id);

        //merchant working hour
        $merchant_default_working_hour = $merchant->workingHours->toArray();        

        //staff working hour
        if (!empty($merchant->staff)) {
            $merchant_staff_working_hour = $merchant->staff->toArray();
        }

        //get merchant temporary working hour
        $merchant_temporay_working_hour = $this->getMerchantTemporaryWorkingHour($id, $start_date, $end_date);                    
      
        //merchant working hour 7 days wise
        $merchant_day_wise = $this->merchantDayWise($merchant_default_working_hour, $weekly_dates, $timezone);        
    
        //staff working hour 7 days wise
        $staff_day_wise = $this->staffDayWise($merchant_staff_working_hour, $weekly_dates, $timezone);

        //compare  merchant default working hour with exception working hour
        $check_merchant_exception_date = $this->checkExceptionDate($merchant_temporay_working_hour, $merchant_day_wise, $timezone);
        // echo json_encode($check_merchant_exception_date);        

        //compare staff default working hour with exception working hour
        $staff_day_wise_2 = []; 

        foreach ($staff_day_wise as $staff_day) {            
            //get staff temporary working hour
            $staff_temporay_working_hour = $this->getStaffTemporaryWorkingHour($staff_day['id'], $start_date, $end_date);            
            $wh = $this->checkExceptionDate($staff_temporay_working_hour, $staff_day['working_hour'], $timezone);            
            
            $working_hours = array_map(function($wh){
                                    unset($wh['id']);                                    
                                    return $wh;
                                },$wh);
            $staff_day['working_hour'] = $working_hours;     
            array_push($staff_day_wise_2, $staff_day);
            
        }    
        $response['message'] = "7 days working hours";
        $data['id'] = $merchant->id;
        $data['name'] = "salon";
        $data['image'] = (!empty($merchant->detail['merchant_image']) ? $merchant->detail['merchant_image'] : $this->default_profile_image);

        $data['working_hour'] = $check_merchant_exception_date;

        $response['data'][0] = $data;
        foreach ($staff_day_wise_2 as $value) {
            array_push($response['data'], $value);

        }        
        return $response = response()->json($response, $this->success_code);

    }

    private function getMerchantAndStaff($id)
    {
        $merchant = Merchant::where('id', $id)->with('detail', 'workingHours', 'staff.staffDetail', 'staff.workingHours')->first();
        return $merchant;
    }

    private function getMerchantDefaultWorkingHour($id)
    {
        $merchant_working_hour = MerchantsWorkingHour::where('merchant_id', $id)->get();
        return $merchant_working_hour;
    }

    private function getMerchantTemporaryWorkingHour($id, $start_date, $end_date) {
        $merchant_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $id)
            ->whereBetween('date', [$start_date, $end_date])
            ->get()
            ->toArray();
        return $merchant_temp_working_hour;
    }

    private function getStaffTemporaryWorkingHour($id, $start_date, $end_date) {
        // $merchant_staff_id = MerchantStaff::where('merchant_id', $id)->pluck('staff_id');        
        // foreach ($merchant_staff_id as $id) {
            $staff_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $id)
                ->whereBetween('date', [$start_date, $end_date])
                ->get()
                ->toArray();            
            return $staff_temp_working_hour;
        // }

    }

    private function merchantDayWise($merchant_default_working_hour, $weekly_dates, $timezone) {
        $default_list = [];

        foreach ($weekly_dates as $key => $values) {
            foreach ($merchant_default_working_hour as $default_working_hour) {
           
                if ($default_working_hour['week_day'] == $values) {
                    // $data_list['id'] = $default_working_hour['id'];
                    $data_list['merchant_id'] = $default_working_hour['merchant_id'];
                    $data_list['date'] = $this->convertToTimestamp($key,$timezone);
                    // $data_list['date'] = $key;
                    $data_list['week_day'] = $values;
                    $data_list['start_time'] = $this->convertToTimestamp($default_working_hour['start_time'],$timezone);
                    $data_list['end_time'] = $this->convertToTimestamp($default_working_hour['end_time'],$timezone);
                    $data_list['break_start_time'] = (!empty($default_working_hour['break_start_time']) ? $this->convertToTimestamp($default_working_hour['break_start_time'],$timezone) : null);
                    $data_list['break_end_time'] = (!empty($default_working_hour['break_end_time']) ? $this->convertToTimestamp($default_working_hour['break_end_time'],$timezone) : null);
                    $data_list['status'] = (int) $default_working_hour['status'];
                    break;

                } else {
                    // $data_list['id'] = $default_working_hour['id'];
                    $data_list['merchant_id'] = $default_working_hour['merchant_id'];
                    $data_list['date'] = $this->convertToTimestamp($key,$timezone);
                    // $data_list['date'] = $key;
                    $data_list['week_day'] = $values;
                    $data_list['start_time'] = null;
                    $data_list['end_time'] = null;
                    $data_list['break_start_time'] = null;
                    $data_list['break_end_time'] = null;
                    $data_list['status'] = 0;

                }

            }
            array_push($default_list, $data_list);
        }
        return $default_list;
    }
    
    private function staffDayWise($merchant_staff_db_working_hours, $weekly_dates, $timezone) {        
        $staff_hours_data = [];
        foreach ($merchant_staff_db_working_hours as $staff_working_hour) {

            $staff_data['id'] = $staff_working_hour['staff_id'];
            $staff_data['name'] = $staff_working_hour['staff_detail']['merchant_name'];
            $staff_data['image'] = (!empty($staff_working_hour['staff_detail']['merchant_image']) ? $staff_working_hour['staff_detail']['merchant_image'] : $this->default_profile_image);

            $staff_working_data = [];
            foreach ($weekly_dates as $key => $values) {
                foreach ($staff_working_hour['working_hours'] as $staff_working) {
                    if ($staff_working['week_day'] == $values) {
                        $data_list['id'] = $staff_working['id'];
                        $data_list['merchant_id'] = $staff_working['merchant_id'];
                        $data_list['date'] = $this->convertToTimestamp($key,$timezone);
                        $data_list['week_day'] = $values;
                        $data_list['start_time'] = $this->convertToTimestamp($staff_working['start_time'],$timezone);
                        $data_list['end_time'] = $this->convertToTimestamp($staff_working['end_time'],$timezone);
                        $data_list['break_start_time'] = (!empty($staff_working['break_start_time']) ? $this->convertToTimestamp($staff_working['break_start_time'],$timezone) : null);
                        $data_list['break_end_time'] = (!empty($staff_working['break_end_time']) ? $this->convertToTimestamp($staff_working['break_end_time'],$timezone) : null);
                        $data_list['status'] = (int) $staff_working['status'];
                        break;
                    } else {
                        // $data_list['merchant_id'] = $staff_working['id'];
                        $data_list['merchant_id'] = $staff_working['merchant_id'];
                        $data_list['date'] = $this->convertToTimestamp($key,$timezone);
                        $data_list['week_day'] = $values;
                        $data_list['start_time'] = null;
                        $data_list['end_time'] = null;
                        $data_list['break_start_time'] = null;
                        $data_list['break_end_time'] = null;
                        $data_list['status'] = 0;
                    }
                }
                array_push($staff_working_data, $data_list);
                $staff_data['working_hour'] = $staff_working_data;
            }
            array_push($staff_hours_data, $staff_data);
        }        
        return $staff_hours_data;
    }

    private function checkExceptionDate($merchant_temporay_working_hour, $default_list, $timezone) {        
        $final_list = [];   
        if (!empty($merchant_temporay_working_hour)) {            
            foreach ($default_list as $list_default) {                
                foreach ($merchant_temporay_working_hour as $temp_working_data) {                    
                    if ($this->convertToDate($list_default['date']) == date('Y-m-d', strtotime($temp_working_data['date']))) {
                        $data['merchant_id'] = $list_default['merchant_id'];
                        $data['date'] = $list_default['date'];
                        $data['week_day'] = $temp_working_data['week_day'];
                        $data['start_time'] = (!empty($temp_working_data['start_time']) ? $this->convertToTimestamp($temp_working_data['start_time'],$timezone) : null );
                        $data['end_time'] =  (!empty($temp_working_data['end_time']) ? $this->convertToTimestamp($temp_working_data['end_time'],$timezone) : null );
                        $data['break_start_time'] = (!empty($temp_working_data['break_start_time']) ? $this->convertToTimestamp($temp_working_data['break_start_time'],$timezone) : null ) ;
                        $data['break_end_time'] =  (!empty($temp_working_data['break_end_time']) ? $this->convertToTimestamp($temp_working_data['break_end_time'],$timezone) : null );
                        $data['status'] = (int) $temp_working_data['status'];
                        break;
                    } else {
                        $data['merchant_id'] = $list_default['merchant_id'];
                        $data['date'] = $list_default['date'];
                        $data['week_day'] = $list_default['week_day'];
                        $data['start_time'] = $list_default['start_time'];
                        $data['end_time'] = $list_default['end_time'];
                        $data['break_start_time'] = $list_default['break_start_time'];
                        $data['break_end_time'] = $list_default['break_end_time'];
                        $data['status'] = $list_default['status'];
                    }                    
                }
                array_push($final_list, $data);
            }
        }
       
        $list = (empty($final_list)) ? $default_list : $final_list;            
        return $list;
    }

}
