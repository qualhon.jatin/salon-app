<?php

namespace App\Http\Controllers\v5\Api\User;

use App\AppBannerOffer;
use App\MerchantOffer;
use App\MerchantCareer;
use App\JobApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\v3\Api\Merchant\OffersPackagesController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class OfferBannerController extends Controller {
	private $OffersPackages;
	public function __construct(OffersPackagesController $OffersPackagesController ){
        $this->OffersPackages = $OffersPackagesController;              
        // $this->usersController = $user_controller;
    }
	
	public function getBannerList(){
		if(Auth::user()){
			$columns = ['id','banner_image','banner_title','offer_text','banner_type','discount_percent'];
			$banners = AppBannerOffer::select($columns)->where('status',1)->get();
			$response = response()->json(['message' => 'Offer banner list','data'=>$banners], $this->success_code);
		}
		else{
    		$response = response()->json(['message' => 'Merchant not found! Please login'], $this->unauthenticate_code);
    	}

    	return $response;
	}

    public function getBannerDetail(Request $req){
    	if(Auth::user()){
    		$validator = Validator::make(
	            $req->all(),
	            [
					'banner_type' => 'required',
					// 'location' => 'required'
						                	               
	            ]
	        );
	        if ($validator->fails()) {
	            return response()->json(['message' => $validator->errors()], $this->error_code);
			}

    		switch ($req->banner_type) {
    			case '1':
    				return $this->getoffers($req);
    				break;
    			case '2':
    				return $this->getTraning($req);
    				break;
    			case '3':
    				return $this->getVacancies($req);    				
    				break;
				case '4':
					$req->merge(["all_packages"=>"true"]);
					return $this->OffersPackages->trendingPackages($req);    				
    				break;
    			default:
    				return response()->json(['message' => 'Invalid input'], $this->error_code);
    				break;
    		}	        
    	}
    	else{
    		return response()->json(['message' => 'Merchant not found! Please login'], 401);
    	}
    }

    private function getOffers($req){
    	$validator = Validator::make(
            $req->all(),
            [
                'discount_percent' => 'required',
                'location' => 'required'             	               
            ]
        );
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }
        $columns = ['id','merchant_id','discount'];
        $current_lat = explode(',', $req->location)[0];
        $current_lng = explode(',', $req->location)[1];
        $banner_offers = MerchantOffer::select($columns)
        				->where('discount','<=',$req->discount_percent)
        				->whereHas('merchant.address')
        				->distinct('merchant_id')
        				->with('merchant.review','merchant.address')
        				->with('merchants')
        				->get()->toArray();
        	// echo json_encode($banner_offers);die();	
        $offer_list = [];
     	foreach ($banner_offers as $offer) {
     		if(!empty($offer['merchant'])){
     			$merchant = $offer['merchant'][0];
     			if(isset($req['dev']) && $req['dev'] == true){
     				return ej($merchant);
     			}
     			$offer['merchantId'] 				= $offer['merchant_id'];     			
     			$offer['distance']					= ($merchant['address'] != null) ? $this->getDistanceBetweenPointsNew($current_lat,$current_lng,$merchant['address']['lat'],$merchant['address']['lng']) : null;
 	     		$offer['merchantRating'] 			= 	$this->avgRating($merchant['review']) !== null ? $this->avgRating($merchant['review']) : 0;
	     	   	$offer['merchantReview'] 			=  (($merchant['review'] != 0))  ? count($merchant['review']) : 0;
	     	   	$offer['merchantBusinessAddress'] 	= ($merchant['address'] != null) ? $merchant['address']['address1'] : null ;
	     	   	$offer['merchantBusinessAddress2'] 	= ($merchant['address'] != null) ? $merchant['address']['address2'] : null ; 
		     	$offer['merchatLat'] 				= ($merchant['address'] != null) ? (double) $merchant['address']['lat'] : null ; 
		     	$offer['merchatLng'] 				= ($merchant['address'] != null) ? (double) $merchant['address']['lng'] : null ; 	     	   	
	     	   	$offer['merchantBusinessCity'] 		= ($merchant['address'] != null) ? $merchant['address']['city'] : null; 
	     	   	$offer['merchantBusinessPostcode'] 	= ($merchant['address'] != null) ? $merchant['address']['postcode'] : null;
	     	   	
	     	   	$offer['merchantBusinessName'] 		= $merchant['merchant_business_name']; 
	     	   	$offer['merchantBusinessLandline'] 	= $merchant['landline'];
	     	   	$offer['businessCoverImage'] 		= ($merchant['business_cover_image'] != null ) ? $merchant['business_cover_image'] : $this->default_image;
	     	   	$offer['deatilImage'] 		= 		( isset($merchant['detail_image']) &&$merchant['detail_image'] != null ) ? $merchant['detail_image'] : $this->default_image;
	     	   	$offer['merchantDetailId'] 		= $merchant['id'];
	     	   	$offer['salonType'] 		= $merchant['salon_type'];	     	
	     	   	$offer['mercahntAddressId'] 		= ($merchant['address'] != null) ? $merchant['address']['id'] : null;	
				$offer['is_verified'] = $offer['merchants']['is_verified'];
	     	   	unset($offer['merchant_id']);   	
	     	   	unset($offer['merchant']);	     	   	   		
	     	   	unset($offer['merchants']);	     	   	   		
	     	   	array_push($offer_list,$offer);
	     	}
		 }
		 $columns = array_column($offer_list, 'is_verified');
		 array_multisort($columns, SORT_DESC, $offer_list);
			
     	$offers = collect($offer_list);
		$final_offer_list = $offers->unique('merchantId');
		$final_offer_list = $final_offer_list->toArray();
		$final_list = array_filter($final_offer_list,function($offer_list){
			if($offer_list['distance'] <= 20){
				return true;
			}	
		});
		
		return response()->json(['message' => 'Offer list','data'=>array_values($final_list)], $this->success_code);
    }

    private function getTraning($req){    
		$validator = Validator::make(
            $req->all(),
            [
                'location' => 'required'             	               
            ]
        );
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

		$current_lat = explode(',', $req->location)[0];
        $current_lng = explode(',', $req->location)[1];

        $columns = ['id','merchant_id','image','name','experience','speciality','education','job_description','career_type'];
		$traning_list = MerchantCareer::select($columns)->where('career_type',0)->with('merchant.detail','merchant.address')->get();
		$final_list = [];
		
        foreach ($traning_list as $traning) {
            $m_detail = $traning['merchant']['detail'];
            $m_address = $traning['merchant']['address'];
            
            $traning['salon_name'] = $m_detail['merchant_business_name'];
            $traning['description'] = $traning['job_description'];             
            $traning['title'] = $traning['name'];             
            $traning['salon_id'] = $traning['merchant_id'];                                     
            $traning['salon_lat'] = (double) $m_address['lat'];
            $traning['salon_lng'] = (double) $m_address['lng'];
            $traning['salon_type'] = $m_detail['salon_type'];
			$traning['salon_address'] = $m_address['address1'].' '.$m_address['address2'].', '.$m_address['city'].', '.$m_address['postcode'];
			$traning['distance']	= ($m_address != null) ? $this->getDistanceBetweenPointsNew($current_lat,$current_lng,$traning['salon_lat'],$traning['salon_lng']) : null;             
            $unset_vars = ['name','merchant','merchant_id','job_description'];
            foreach($unset_vars as $key){
                unset($traning[$key]);
			}
			
			array_push($final_list,$traning);
		 }
		 
		 $final_list = array_filter($final_list,function($list){
			if($list['distance'] <= 20){
				return true;
			}	
		});
		 
		 
        return response()->json(['message' => 'Traning list','data'=>array_values($final_list)], $this->success_code);
    }

    private function getVacancies($req){
		$validator = Validator::make(
            $req->all(),
            [
                'location' => 'required'             	               
            ]
        );
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

		$current_lat = explode(',', $req->location)[0];
        $current_lng = explode(',', $req->location)[1];
		
		$columns = ['id','merchant_id','image','name','experience','speciality','education','job_description','career_type'];
		$vacancies = MerchantCareer::select($columns)->where('career_type',1)->with('merchant.detail')->whereHas('merchant.address')->get();                   
		$final_list = [];
        foreach ($vacancies as $vacancy) {
            $m_detail = $vacancy['merchant']['detail'];
            $m_address = $vacancy['merchant']['address'];        	
			$vacancy['salon_name'] = $m_detail['merchant_business_name'];
            $vacancy['description'] = $vacancy['job_description'];             
            $vacancy['title'] = $vacancy['name'];             
            $vacancy['salon_id'] = $vacancy['merchant_id'];                                     
            $vacancy['salon_lat'] = (double) $m_address['lat'];
            $vacancy['salon_lng'] = (double) $m_address['lng'];
            $vacancy['salon_type'] = $m_detail['salon_type'];
			$vacancy['salon_address'] = $m_address['address1'].' '.$m_address['address2'].', '.$m_address['city'].', '.$m_address['postcode'];
			$vacancy['distance']	= ($m_address != null) ? $this->getDistanceBetweenPointsNew($current_lat,$current_lng,$vacancy['salon_lat'],$vacancy['salon_lng']) : null; 
		
			
            $unset_vars = ['name','merchant','merchant_id','job_description'];
            foreach($unset_vars as $key){
                unset($vacancy[$key]);
			}
			
			array_push($final_list,$vacancy);
		 }
		 
		
		 $vacancy = $vacancy->toArray();
	
		 $final_list = array_filter($final_list,function($list){
			if($list['distance'] <= 20){
				return true;
			}
				
		});
		
        return response()->json(['message' => 'Vacancy list','data'=>array_values($final_list)], $this->success_code);
	}
	
	private function avgRating($data){
		if(count($data) > 0){
			$total_rating = 0;
			foreach($data as $data_rating){
				$total_rating += $data_rating['rating'];
			}

			$avg_rating = $total_rating/count($data);
            $avg_rating = number_format($avg_rating,2);
            $avg_rating =(float)$avg_rating;
			// echo $avg_rating;
			// die();
			return  $avg_rating;
		}
		
	}
	  
    /*
        Function name : applyJob
        Function Purpose : Apply Job
        Created by : Sourabh
        Created on : 26 Oct 2020
	*/
	
	public function applyJob(Request $request){
		if(Auth::guard('merchants')) {
			
		$id =  Auth::id();
		
		$user1 = Auth::User();

		$validator = Validator::make(
			$request->all(),
			[
				'career_id' => 'required',
				'merchant_id' => 'required',
				'name' => 'required',
				'email_id' => 'required',
				'phone_no' => 'required',
				'career_type' => 'required',
				'career_id' => 'required',
			]
		);


		if ($validator->fails()) {
			return response()->json(['message' => $validator->errors()], 401);
		}

		$apply_job = new JobApplication();
		$resume = NULL;
		if($request->career_type == 0){
			$resume = NULL; 
		}
		else{
			$validator = Validator::make(
				$request->all(),
				[
					'resume' => 'required|mimes:pdf,doc,docx',
				]
			
			);
			if ($validator->fails()) {
				return response()->json(['message' => $validator->errors()], 401);
			}


			$imageName = 'business_cover_image/' . time() . '.' . $request->resume->getClientOriginalExtension();
			$image = $request->file('resume');
			$t = Storage::disk('s3')->put($imageName, file_get_contents($image), 'public');
			$resume = Storage::disk('s3')->url($imageName);
			

		}
		  
		$apply_job->user_id = $id;
		$apply_job->career_id = $request->career_id;	
		$apply_job->merchant_id = $request->merchant_id;	
		$apply_job->name = $request->name;	
		$apply_job->email = $request->email_id;	
		$apply_job->phone = $request->phone_no;	
		$apply_job->resume = $resume;	
		if($apply_job->save()){
			return response()->json([
				'message' => 'Apply job successfully',
				], $this->success_code);
		}
		else{
			return response()->json([
				'message' => 'Error in apply job',
				
			], $this->error_code);
		}	

	} else {
		return response()->json(['message' => "Please login"], 401);
	}
}



}
