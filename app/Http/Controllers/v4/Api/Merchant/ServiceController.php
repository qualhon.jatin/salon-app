<?php

namespace App\Http\Controllers\v4\Api\Merchant;

use App\Http\Controllers\Controller;
use App\Merchant;
// use App\Category;
use App\MerchantService;
use App\MerchantStaff;
use App\MerchantsWorkingHour;
use App\Merchant_secondary_working_hour;
use App\Appointment;
// use App\ServiceWorkingHours;
// use App\ServiceWorkingDays;
use App\Traits\PushNotificationTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    use PushNotificationTrait;
    public $successStatus = 200;

    /*
     *
     * select merchant service while register company screen select_service
     *
     *
     */

    public function updateServices(Request $request)
    {
        $id = Auth::id();
        $services = array($request['services']);
        $services = (gettype($request['services']) == 'string') ? json_decode($request['services'], true) : $request['services'];

        if (empty($services)) {
            return response()->json(['message' => 'Services cannot be null'], $this->error_code);
        }

        foreach ($services as $service_data) {

            if ($service_data['service_price'] == "") {
                return response()->json(['message' => 'Service price cannot be null'], $this->error_code);
            }
            if ($service_data['service_duration'] == "") {
                return response()->json(['message' => 'Service duration cannot be null'], $this->error_code);
            }
            if ($service_data['service_buffer_time'] == "") {
                return response()->json(['message' => 'Service buffer time cannot be null'], $this->error_code);
            }

            $data = [
                'service_price' => $service_data['service_price'],
                'service_duration' => $service_data['service_duration'],
                'service_buffer_time' => $service_data['service_buffer_time'],
            ];

            $update_service = MerchantService::where('merchant_staff_id', $id)
                ->where('service_id', $service_data['service_id'])
                ->update($data);

        }
        return response()->json(['message' => "Service updated successfully"], $this->success_code);
    }

  

    public function updateWorkingHourSecondary(Request $request)
    {
        $id = "";
        $timezone = $request->header('timezone');
        $id = Auth::id();

        $days = (gettype($request['day']) == 'string') ? json_decode($request['day'], true) : $request['day'];
       
        $secondary_working = [];

        $merchant_data = Merchant::where('id', Auth::id())->with('detail')->first();
        $mobile_number = $merchant_data['country_code'] . $merchant_data['mobile'];
        $merchant_push_status = $merchant_data['detail']['push_status'];

        if (isset($request->staff_id) || $request->staff_id != "" || $request->staff_id != 0) {
            $merchant_staff = MerchantStaff::where('merchant_id', $id)->where('staff_id', $request->staff_id)->first();        
            $id = $merchant_staff->staff_id;
            if (empty($merchant_staff)) {
                return $response = response()->json(['message' => 'You are not authorized to set working hour of this staff'], $this->error_code);
            }
        }
        
        // $days = $days->toArray();
        $appoinment_list = array_filter($days, function($day) use ($timezone, $id){
                                $date = date("Y-m-d", strtotime($this->convertToLocalTime($day['datetime'], $timezone)));                                
                                if(!empty($this->checkAppointment($date,$id))){
                                    return true;
                                }
                            });
        if(empty($appoinment_list) || $request->status == true){            
            foreach ($days as $working_day) {
                if ($working_day['status'] == 1) {
                    if ($working_day['start_time'] == null || $working_day['end_time'] == null) {
                        return $response = response()->json(['message' => 'Please provide start time and end time'], $this->error_code);
                    }
                }
                if ($working_day['start_time'] > $working_day['end_time']) {
                    return $response = response()->json(['message' => 'End time should greater than start time'], $this->error_code);
                }

                //get date form given timestamp
                $date = date("Y-m-d", strtotime($this->convertToLocalTime($working_day['datetime'], $timezone)));

                $this->checkAppointment($date,$id,true);
                            
                //get day number  form given timestamp
                $day_number = $this->getDayNo($working_day['datetime'], $timezone);
               
                $merchant_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $id)
                    ->whereDate('date', $date)->get()->toArray();

                if (!empty($merchant_temp_working_hour)) {
                    $update_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $id)->whereDate('date', $date)
                        ->update([
                            'date' => $date,
                            'week_day' => $day_number,
                            'start_time' => ($working_day['start_time'] == 0 ? null : $this->convertToDateTime($working_day['start_time'])),
                            'end_time' => ($working_day['end_time'] == 0 ? null : $this->convertToDateTime($working_day['end_time'])),
                            'break_start_time' => ($working_day['break_start_time'] == 0 ? null : $this->convertToDateTime($working_day['break_start_time'])),
                            'break_end_time' => ($working_day['break_end_time'] == 0 ? null : $this->convertToDateTime($working_day['break_end_time'])),
                            'status' => $working_day['status'],

                        ]);
                } 
                else {
                    $data = [
                        'merchant_id' => $id,
                        'date' => $date,
                        'week_day' => $day_number,
                        'start_time' => ($working_day['start_time'] == 0 ? null : $this->convertToDateTime($working_day['start_time'])),
                        'end_time' => ($working_day['end_time'] == 0 ? null : $this->convertToDateTime($working_day['end_time'])),
                        'break_start_time' => ($working_day['break_start_time'] == 0 ? null : $this->convertToDateTime($working_day['break_start_time'])),
                        'break_end_time' => ($working_day['break_end_time'] == 0 ? null : $this->convertToDateTime($working_day['break_end_time'])),
                        'status' => $working_day['status'],
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                    ];

                    array_push($secondary_working, $data);
                }

            }

            $update_temp_hour = Merchant_secondary_working_hour::insert($secondary_working);

            $response = response()->json(['message' => 'Working hours updated successfully','status'=>true], $this->success_code);
        }
        else{
            $appoinment_dates = array_map(function($appoinment_list){
                                   return $this->convertToDate($appoinment_list['datetime'],"d M, Y");
                                },$appoinment_list); 
            $appoinment_dates = implode(", ", $appoinment_dates);

            $response = response()->json(['message' => "You have Appointments scheduled for following dates. If you make the changes these appointments will be cancelled.\n - $appoinment_dates",'status'=> false], $this->success_code);
        }
        return $response;
    }


    //check appointment when merchant change working hour

    private function checkAppointment($date,$id, $updateStatus = false){
        $start_date = $date ." 00:00:00";
        $end_date = $date ." 23:59:59";
        
        
        if($updateStatus){            
            $appointment_ids = Appointment::where('staff_id',$id)->whereBetween('appointment_date_time' , [$start_date,$end_date])->pluck('id');
            Appointment::whereIn('id',$appointment_ids)->update(['appointment_status'=>"0"]);
        }
        else{
            $merchant_appointment  = Appointment::where('staff_id',$id)
                                            ->where('appointment_status',1)
                                            ->whereBetween('appointment_date_time' , [$start_date,$end_date])->get()->toArray();
            return $merchant_appointment;
        }
    }    

    public function getWorkingHourWeekwise(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'datetime' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }
        $timezone = $request->header('timezone');
        $id = "";
        $id = Auth::id();
        if (isset($request->staff_id) || $request->staff_id != "" || $request->staff_id != 0) {

            $merchant_staff = MerchantStaff::where('merchant_id', $id)->where('staff_id', $request->staff_id)->first();
            $id = $merchant_staff->staff_id;

            if (empty($merchant_staff)) {
                return $response = response()->json(['message' => 'You are not authorized to set working hour of this staff'], $this->error_code);
            }
        }

        return $this->getWhList($request->datetime, $id, $timezone);

    }

    private function getWhList($datetime, $id, $timezone)
    {
        // dd($id);
        $date = $datetime;

        $date = date("Y-m-d", strtotime($this->convertToLocalTime($date, $timezone)));

        $start_date = date("Y-m-d", strtotime($date));
        $start = date("Y-m-d", strtotime($date));
        $start_date = $start_date . " 00:00:00";

        $end_date = $start_date;
        $end_date = strtotime($end_date);
        $end_date = strtotime("+6 day", $end_date);
        $end = date("Y-m-d", $end_date);
        $end_date = date("Y-m-d", $end_date) . " 23:59:59";

        // get 7 days date from request timestamp
        $week_dates = $this->getDatesFromRange($start, $end);
        // 7 days date with day number
        $weekly_dates = getWeekDayFromDate($week_dates);

        $default_list = [];
        $final_list = [];

        //get merchant and its staff detail with working hour
        $merchant = $this->getMerchantAndStaff($id);

        //merchant working hour
        $merchant_default_working_hour = $merchant->workingHours->toArray();        

        //staff working hour
        if (!empty($merchant->staff)) {
            $merchant_staff_working_hour = $merchant->staff->toArray();
        }

        //get merchant temporary working hour
        $merchant_temporay_working_hour = $this->getMerchantTemporaryWorkingHour($id, $start_date, $end_date);                    
      
        //merchant working hour 7 days wise
        $merchant_day_wise = $this->merchantDayWise($merchant_default_working_hour, $weekly_dates, $timezone);        
    
        //staff working hour 7 days wise
        $staff_day_wise = $this->staffDayWise($merchant_staff_working_hour, $weekly_dates, $timezone);

        //compare  merchant default working hour with exception working hour
        $check_merchant_exception_date = $this->checkExceptionDate($merchant_temporay_working_hour, $merchant_day_wise, $timezone);
        // echo json_encode($check_merchant_exception_date);        

        //compare staff default working hour with exception working hour
        $staff_day_wise_2 = []; 

        foreach ($staff_day_wise as $staff_day) {            
            //get staff temporary working hour
            $staff_temporay_working_hour = $this->getStaffTemporaryWorkingHour($staff_day['id'], $start_date, $end_date);            
            $wh = $this->checkExceptionDate($staff_temporay_working_hour, $staff_day['working_hour'], $timezone);            
            
            $working_hours = array_map(function($wh){
                                    unset($wh['id']);                                    
                                    return $wh;
                                },$wh);
            $staff_day['working_hour'] = $working_hours;     
            array_push($staff_day_wise_2, $staff_day);
            
        }    
        $response['message'] = "7 days working hours";
        $data['id'] = $merchant->id;
        $data['name'] = "salon";
        $data['image'] = (!empty($merchant->detail['merchant_image']) ? $merchant->detail['merchant_image'] : $this->default_profile_image);

        $data['working_hour'] = $check_merchant_exception_date;

        $response['data'][0] = $data;
        foreach ($staff_day_wise_2 as $value) {
            array_push($response['data'], $value);

        }        
        return $response = response()->json($response, $this->success_code);

    }

    private function getMerchantAndStaff($id)
    {
        $merchant = Merchant::where('id', $id)->with('detail', 'workingHours', 'staff.staffDetail', 'staff.workingHours')->first();
        return $merchant;
    }

    private function getMerchantDefaultWorkingHour($id)
    {
        $merchant_working_hour = MerchantsWorkingHour::where('merchant_id', $id)->get();
        return $merchant_working_hour;
    }

    private function getMerchantTemporaryWorkingHour($id, $start_date, $end_date) {
        $merchant_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $id)
            ->whereBetween('date', [$start_date, $end_date])
            ->get()
            ->toArray();
        return $merchant_temp_working_hour;
    }

    private function getStaffTemporaryWorkingHour($id, $start_date, $end_date) {
        // $merchant_staff_id = MerchantStaff::where('merchant_id', $id)->pluck('staff_id');        
        // foreach ($merchant_staff_id as $id) {
            $staff_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $id)
                ->whereBetween('date', [$start_date, $end_date])
                ->get()
                ->toArray();            
            return $staff_temp_working_hour;
        // }

    }

    private function merchantDayWise($merchant_default_working_hour, $weekly_dates, $timezone) {
        $default_list = [];

        foreach ($weekly_dates as $key => $values) {
            foreach ($merchant_default_working_hour as $default_working_hour) {
           
                if ($default_working_hour['week_day'] == $values) {
                    // $data_list['id'] = $default_working_hour['id'];
                    $data_list['merchant_id'] = $default_working_hour['merchant_id'];
                    $data_list['date'] = $this->convertToTimestamp($key,$timezone);
                    // $data_list['date'] = $key;
                    $data_list['week_day'] = $values;
                    $data_list['start_time'] = $this->convertToTimestamp($default_working_hour['start_time'],$timezone);
                    $data_list['end_time'] = $this->convertToTimestamp($default_working_hour['end_time'],$timezone);
                    $data_list['break_start_time'] = (!empty($default_working_hour['break_start_time']) ? $this->convertToTimestamp($default_working_hour['break_start_time'],$timezone) : null);
                    $data_list['break_end_time'] = (!empty($default_working_hour['break_end_time']) ? $this->convertToTimestamp($default_working_hour['break_end_time'],$timezone) : null);
                    $data_list['status'] = (int) $default_working_hour['status'];
                    break;

                } else {
                    // $data_list['id'] = $default_working_hour['id'];
                    $data_list['merchant_id'] = $default_working_hour['merchant_id'];
                    $data_list['date'] = $this->convertToTimestamp($key,$timezone);
                    // $data_list['date'] = $key;
                    $data_list['week_day'] = $values;
                    $data_list['start_time'] = null;
                    $data_list['end_time'] = null;
                    $data_list['break_start_time'] = null;
                    $data_list['break_end_time'] = null;
                    $data_list['status'] = 0;

                }

            }
            array_push($default_list, $data_list);
        }
        return $default_list;
    }
    
    private function staffDayWise($merchant_staff_db_working_hours, $weekly_dates, $timezone) {        
        $staff_hours_data = [];
        foreach ($merchant_staff_db_working_hours as $staff_working_hour) {

            $staff_data['id'] = $staff_working_hour['staff_id'];
            $staff_data['name'] = $staff_working_hour['staff_detail']['merchant_name'];
            $staff_data['image'] = (!empty($staff_working_hour['staff_detail']['merchant_image']) ? $staff_working_hour['staff_detail']['merchant_image'] : $this->default_profile_image);

            $staff_working_data = [];
            foreach ($weekly_dates as $key => $values) {
                foreach ($staff_working_hour['working_hours'] as $staff_working) {
                    if ($staff_working['week_day'] == $values) {
                        $data_list['id'] = $staff_working['id'];
                        $data_list['merchant_id'] = $staff_working['merchant_id'];
                        $data_list['date'] = $this->convertToTimestamp($key,$timezone);
                        $data_list['week_day'] = $values;
                        $data_list['start_time'] = $this->convertToTimestamp($staff_working['start_time'],$timezone);
                        $data_list['end_time'] = $this->convertToTimestamp($staff_working['end_time'],$timezone);
                        $data_list['break_start_time'] = (!empty($staff_working['break_start_time']) ? $this->convertToTimestamp($staff_working['break_start_time'],$timezone) : null);
                        $data_list['break_end_time'] = (!empty($staff_working['break_end_time']) ? $this->convertToTimestamp($staff_working['break_end_time'],$timezone) : null);
                        $data_list['status'] = (int) $staff_working['status'];
                        break;
                    } else {
                        // $data_list['merchant_id'] = $staff_working['id'];
                        $data_list['merchant_id'] = $staff_working['merchant_id'];
                        $data_list['date'] = $this->convertToTimestamp($key,$timezone);
                        $data_list['week_day'] = $values;
                        $data_list['start_time'] = null;
                        $data_list['end_time'] = null;
                        $data_list['break_start_time'] = null;
                        $data_list['break_end_time'] = null;
                        $data_list['status'] = 0;
                    }
                }
                array_push($staff_working_data, $data_list);
                $staff_data['working_hour'] = $staff_working_data;
            }
            array_push($staff_hours_data, $staff_data);
        }        
        return $staff_hours_data;
    }

    private function checkExceptionDate($merchant_temporay_working_hour, $default_list, $timezone) {        
        $final_list = [];   
        if (!empty($merchant_temporay_working_hour)) {            
            foreach ($default_list as $list_default) {                
                foreach ($merchant_temporay_working_hour as $temp_working_data) {                    
                    if ($this->convertToDate($list_default['date']) == date('Y-m-d', strtotime($temp_working_data['date']))) {
                        $data['merchant_id'] = $list_default['merchant_id'];
                        $data['date'] = $list_default['date'];
                        $data['week_day'] = $temp_working_data['week_day'];
                        $data['start_time'] = (!empty($temp_working_data['start_time']) ? $this->convertToTimestamp($temp_working_data['start_time'],$timezone) : null );
                        $data['end_time'] =  (!empty($temp_working_data['end_time']) ? $this->convertToTimestamp($temp_working_data['end_time'],$timezone) : null );
                        $data['break_start_time'] = (!empty($temp_working_data['break_start_time']) ? $this->convertToTimestamp($temp_working_data['break_start_time'],$timezone) : null ) ;
                        $data['break_end_time'] =  (!empty($temp_working_data['break_end_time']) ? $this->convertToTimestamp($temp_working_data['break_end_time'],$timezone) : null );
                        $data['status'] = (int) $temp_working_data['status'];
                        break;
                    } else {
                        $data['merchant_id'] = $list_default['merchant_id'];
                        $data['date'] = $list_default['date'];
                        $data['week_day'] = $list_default['week_day'];
                        $data['start_time'] = $list_default['start_time'];
                        $data['end_time'] = $list_default['end_time'];
                        $data['break_start_time'] = $list_default['break_start_time'];
                        $data['break_end_time'] = $list_default['break_end_time'];
                        $data['status'] = $list_default['status'];
                    }                    
                }
                array_push($final_list, $data);
            }
        }
       
        $list = (empty($final_list)) ? $default_list : $final_list;            
        return $list;
    }

}
