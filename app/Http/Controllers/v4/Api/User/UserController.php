<?php

namespace App\Http\Controllers\v4\Api\User;

use App\Amenitie;
use App\Favourity;
use App\Http\Controllers\Controller;
use App\Merchant;
use App\MerchantAddress;
use App\MerchantService;
use App\PushToken;
use App\ReviewRating;
use App\ServiceCategory;
use App\Traits\PushNotificationTrait;
use App\User;
use App\UsersDetail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    use PushNotificationTrait;

    public $successStatus = 200;

    /*
     *
     *        Login user api
     *
     */

    // public function __construct(){
    //     dump(self::$timezone);
    // }

    public function register(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'country_code' => 'required',
                'mobile' => 'required',
                'email' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        if (!isset($request->push_token) || $request->push_token == null) {

            $request->merge([
                'push_token' => $this->randomNo(15),
            ]);
        }
        $request->merge([
            'mobile' => str_replace(' ', '', $request->mobile)
        ]);
        $mobile_no = $request->country_code . $request->mobile;
        $user = User::where('mobile', $request->mobile)->first();
        if (empty($user)) {
            $check_email = UsersDetail::where('email', $request->email)->first();
            if (!$check_email) {
                $otp = ($request->mobile == '7988512302' || $request->mobile == '9530681066' || $request->mobile == '8528809228' || $request->mobile == '7854858377') ? '0000' : $this->sendOTP($mobile_no);
                if (isset($otp['status']) == "0000" && $otp['status'] == 400) {
                    return response()->json(['message' => "Please enter a valid Phone number"], $this->error_code);
                }
                $request->merge([
                    'otp' => $otp,
                    'is_user_verified' => '0',
                    'country_code' => $request->country_code,
                ]);
                $otp_status = false;
                $pin_status = false;

                if ($user = User::create($request->all())) {
                    $data = $request->all();
                    $data['user_id'] = $user->id;
                    $data['added_user_type'] = 1;
                    $user_id = $user->id;
                    $user_name = $request->name;
                    // $token = $user->createToken('MyApp')->accessToken;
                    // $response = (UsersDetail::create($data)) ?  $response = response()->json([
                    //     'otp_status' => $otp_status,
                    //     'pin_status' => $pin_status,
                    //     'user_type' => 1,
                    //     'user_name' => $request->name,
                    // ], $this->success_code)
                    // : response()->json(['message' => "Something wrong while saving user detail"], $this->error_code);

                    $id = UsersDetail::create($data)->id;

                    if (!empty($id)) {
                        $response = response()->json([
                            'otp_status' => $otp_status,
                            'pin_status' => $pin_status,
                            'user_type' => 1,
                            'user_name' => $request->name,

                        ], $this->success_code);
                    } else {
                        $response = response()->json(['message' => "Something wrong while saving user detail"], $this->error_code);
                    }

                } else {
                    $response = response()->json(['message' => "Something wrong while creating user"], $this->error_code);
                }
            } else {
                $response = response()->json(['message' => "This email is already registered"], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => "This mobile number is already registered"], $this->error_code);

        }
        return $response;
    }

    public function login(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                // 'name' => 'required',
                'country_code' => 'required',
                'mobile' => 'required',
                // 'email' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }
        $request->merge([
            'mobile' => str_replace(' ', '', $request->mobile)
        ]);
        $mobile_no = $request->country_code . $request->mobile;
        $user = User::with('detail')->where('mobile', $request->mobile)->first();
        $user_name = (empty($user->detail) ? null : $user->detail['name']);

        if (empty($user)) {

            $response = response()->json(['message' => "This number is not registered, Please register first"], $this->error_code);

        } else {
            if ($user->is_user_verified == 0) {
                $otp = ($request->mobile == '7988512302' || $request->mobile == '9530681066' || $request->mobile == '8528809228' || $request->mobile == '7854858377') ? '0000' : $this->sendOTP($mobile_no);
                if (isset($otp['status']) == "0000" && $otp['status'] == 400) {
                    return response()->json(['message' => "Please enter a valid Phone number"], $this->error_code);
                }

                $update_otp = User::where('id', $user->id)->update(['otp' => $otp]);
            }
            $opt_status = false;
            $pin_status = false;
            $opt_status = ($user->is_user_verified == 1) ? true : false;
            $pin_status = ($user->pin != "") ? true : false;

            $response = response()->json(['otp_status' => $opt_status, 'pin_status' => $pin_status, 'user_type' => 1, 'user_name' => $user_name], $this->success_code);

        }

        return $response;
    }

    /*
     *
     *        Verify OTP api
     *
     */
    public function verify(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'otp' => 'required|digits:4',
                'country_code' => 'required',
                'mobile' => 'required',
                'user_type' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }
        $request->merge([
            'mobile' => str_replace(' ', '', $request->mobile)
        ]);
        $checkmobile = User::where('mobile', $request->mobile)->first();

        if (!empty($checkmobile)) {

            $user = User::where('mobile', $request->mobile)->where('otp', $request->otp)->first();
            if (!empty($user)) {

                // if (empty(PushToken::where('user_id', $user->id)->where('token', $request->push_token)->first())) {
                //     PushToken::create([
                //         'user_id' => $user->id,
                //         'user_type' => 0,
                //         'token' => $request->push_token,
                //     ]);
                // }

                // $user_name = UsersDetail::select('name')->where('user_id', $user->id)->first();
                // if ($user->is_user_verified == 0) {
                //     $data = array(
                //         'title' => "Welcome Onboard!",
                //         'body' => "Hi " . $user_name->name . ", Explore and enjoy the elite services offered by Opayn",
                //         'user_id' => $user->id,
                //         'user_type' => 0,
                //     );

                //     $this->pushNotification($data);
                // }
                // $userinfo = User::where('mobile', $request->mobile)->update(array('otp' => ''));
                $userinfo = User::where('mobile', $request->mobile)
                    ->update([
                        'otp' => '',
                        'is_user_verified' => 1,
                    ]);

                $success['token'] = $user->createToken('MyApp')->accessToken;
                return response()->json(['success' => $success], $this->successStatus);
            } else {

                return response()->json(['message' => 'Please enter a valid OTP'], $this->error_code);
            }
        } else {

            return response()->json(['message' => "Mobile number doesn't exist"], $this->error_code);
        }
    }

    /*
     * Search vendor by using location, rating, newest & oldest (Screen filter)
     */

    public function NearBySalonListing_dev(Request $req)
    {

        $validator = Validator::make(
            $req->all(),
            [
                'location' => 'Required',
                'page' => 'required',
            ],
            [
                'page.required' => 'page is Required',
                'location.required' => 'location is Required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        $sort_id = (isset($req->sort_by)) ? $req->sort_by : 1;
        $merchant_service_type = (isset($req->merchant_service_type)) ? $req->merchant_service_type : 0;
        $limit = (isset($req->limit)) ? $req->limit : 20;
        $page = (isset($req->page)) ? $req->page : 1;
        $distance = (isset($req->distance)) ? $req->distance : 5;
        $current_lat = explode(',', $req->location)[0];
        $current_lng = explode(',', $req->location)[1];

        if (!isset($req->search) && empty($req->search)) {

            $salon_type = (empty($req->salon_type)) ? 0 : $req->salon_type;
            $merchantlist = Merchant::with([
                'address' => function ($query) {
                    $query->select('merchant_id', 'id', 'address1', 'address2', 'lat', 'lng', 'city', 'postcode');
                },
                'detail' => function ($query) {
                    $query->select('merchant_id', 'id', 'merchant_business_name', 'merchant_company_name', 'landline', 'merchant_name', 'merchant_service_type', 'salon_type', 'business_cover_image', 'detail_image');
                },
                'rating' => function ($query) {
                    $query->select(['merchant_id', 'id', 'review', 'rating']);
                },
                'services' => function ($query) {
                    // $query->select(['merchant_staff_id','service_category_id']);
                },
                'amenities',
            ])->select('id', 'is_verified')->where('status', 1)->whereIn('id', array_values($this->exsistedIds()))->get()->toArray();
            // echo json_encode($merchantlist);
            // die();

            $list = [];
            foreach ($merchantlist as $merchant) {
                if ($merchant['address'] != null) {
                    $merchant['distance'] = $this->getDistanceBetweenPointsNew($current_lat, $current_lng, $merchant['address']['lat'], $merchant['address']['lng']);

                    $merchant['merchantRating'] = 0;
                    $merchant['merchantReview'] = null;
                    $merchant['discount'] = null;

                    $merchant['merchantBusinessAddress'] = $merchant['address']['address1'];
                    $merchant['merchantBusinessAddress2'] = $merchant['address']['address2'];
                    $merchant['merchatLat'] = doubleval($merchant['address']['lat']);
                    $merchant['merchatLng'] = doubleval($merchant['address']['lng']);
                    $merchant['mercahntAddressId'] = $merchant['address']['id'];
                    $merchant['merchantBusinessCity'] = $merchant['address']['city'];
                    $merchant['merchantBusinessPostcode'] = $merchant['address']['postcode'];
                }
                if ($merchant['detail'] != null) {
                    $merchant['merchantDetailId'] = $merchant['detail']['id'];
                    $merchant['merchantBusinessName'] = $merchant['detail']['merchant_business_name'];
                    $merchant['merchantServiceType'] = $merchant['detail']['merchant_service_type'];
                    $merchant['merchantBusinessLandline'] = $merchant['detail']['landline'];
                    $merchant['salonType'] = $merchant['detail']['salon_type'];
                    $merchant['businessCoverImage'] = ($merchant['detail']['business_cover_image'] != null) ? $merchant['detail']['business_cover_image'] : $this->default_image;
                    $merchant['deatilImage'] = ($merchant['detail']['detail_image'] != null) ? $merchant['detail']['detail_image'] : $this->default_image;
                }
                if ($merchant['rating'] != null) {
                    $r_list = $merchant['rating'];
                    $rating_list = array_map(function ($r_list) {
                        return $r_list['rating'];
                    }, $r_list);
                    $total = array_sum($rating_list);
                    $avg = $total / count($rating_list);
                    $merchant['merchantRating'] = $avg;
                    $merchant['merchantReview'] = count($merchant['rating']);
                }
                unset($merchant['detail']);
                unset($merchant['address']);
                unset($merchant['rating']);
                $merchant['merchantId'] = $merchant['id'];

                if ($merchant['distance'] <= $distance) {
                    array_push($list, $merchant);
                }
            }

            //    echo json_encode($list);
            // die();

            $vendor_list = [];
            $vendor_list_2 = [];
            $vendor_list_3 = [];

            foreach ($list as $data) {
                if ($data['salonType'] == $salon_type) {
                    array_push($vendor_list, $data);
                } elseif ($salon_type == 0) {
                    array_push($vendor_list, $data);
                }

            }

            $vendor_list_2 = $vendor_list;

            foreach ($vendor_list_2 as $data) {
                if ($data['merchantServiceType'] == $merchant_service_type) {
                    array_push($vendor_list_3, $data);
                } elseif ($merchant_service_type == 0) {
                    array_push($vendor_list_3, $data);
                }

            }

            switch ($sort_id) {
                case '1':
                    $columns = array_column($vendor_list_3, 'distance');
                    array_multisort($columns, SORT_ASC, $vendor_list_3);
                    break;
                case '2':
                    $columns = array_column($vendor_list_2, 'merchantRating');
                    array_multisort($columns, SORT_DESC, $vendor_list_3);
                    break;
                case '3':
                    $columns = array_column($vendor_list_3, 'merchantBusinessName');
                    array_multisort($columns, SORT_ASC, $vendor_list_3);

                    break;
                case '4':
                    $columns = array_column($vendor_list_3, 'merchantBusinessName');
                    array_multisort($columns, SORT_DESC, $vendor_list_3);
                    break;
                default:
                    # code...
                    break;
            }

            $final_list = [];
            if ($req->category != null || $req->amenities != null) {
                $filter = [
                    'categories' => ($req->category != null) ? explode(',', $req->category) : null,
                    'amenities' => ($req->amenities != null) ? explode(',', $req->amenities) : null,
                ];
                $final_list = $this->findbyFilter($filter, $vendor_list_3);

            } else {
                foreach ($vendor_list_3 as $list) {
                    unset($list['services']);
                    unset($list['amenities']);
                    array_push($final_list, $list);
                }
            }

            // $distance_final_list = array_filter($final_list,function($fl){
            //                             if($fl['distance'] <= $distance)
            //                         });

            $columns = array_column($final_list, 'is_verified');
            array_multisort($columns, SORT_DESC, $final_list);

            $final_list = $this->paginate($final_list, $limit, $page)->toArray();
            $final_list = $this->removePaginatorParam($final_list);
            $final_list['data'] = array_values($final_list['data']);

            $response = (empty($final_list)) ? ['message' => 'Empty Merchant Listing', 'data' => $final_list['data']] : ['message' => 'Merchant Listing', 'total' => $final_list['last_page'], 'data' => $final_list['data']];
            return response()->json($response, 200);
        } else {
            $type = (isset($req->salon_type)) ? $req->salon_type : 0;
            return $this->search($req->search, $type, $limit, $page, $current_lat, $current_lng);
        }
    }

    private function findbyFilter($filter, $vendor_list)
    {
        $arr = [];
        if ($filter['categories'] != null) {
            foreach ($filter['categories'] as $cat_id) {
                foreach ($vendor_list as $list) {
                    foreach ($list['services'] as $service) {
                        if ($cat_id == $service['service_category_id']) {
                            if ($filter['amenities'] == null) {
                                unset($list['amenities']);
                            }
                            unset($list['services']);
                            array_push($arr, $list);
                        }
                    }
                }
            }
        }
        if ($filter['amenities'] != null) {
            foreach ($filter['amenities'] as $amenitie_id) {
                foreach ($vendor_list as $list) {
                    foreach ($list['amenities'] as $amenitie) {
                        if ($amenitie_id == $amenitie['amenitie_id']) {
                            if ($filter['categories'] == null) {
                                unset($list['services']);
                            }
                            unset($list['amenities']);

                            array_push($arr, $list);
                        }
                    }
                }
            }
        }
        $arr_final = array_unique($arr, SORT_REGULAR);

        $reCreateArray = array_values($arr_final);
        return $reCreateArray;

    }

    public function NearBySalonListing(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'lat' => 'required',
                'lng' => 'required',
                'distance' => 'required',
                'sortId' => 'required',
            ],
            [
                'lat.required' => 'Latitude is Required',
                'lng.required' => 'longitude is Required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        try {

            if (Auth::user()) {

                if ($request->sortId == 3) {
                    $orderBy = 'ASC';
                } elseif ($request->sortId == 4) {
                    $orderBy = 'DESC';
                }

                $salonList = '';

                if ($request->sortId == 1) {

                    //sortBy Nearest with Id 1

                    $salonList = MerchantAddress::join('merchants', 'merchants.id', '=', 'merchant_addresses.merchant_id')
                        ->join('merchant_details', 'merchant_details.merchant_id', '=', 'merchants.id')
                        ->selectRaw('merchant_details.id as merchantDetailId, merchant_details.merchant_business_name as merchantBusinessName, merchant_details.landline as merchantBusinessLandline, merchant_addresses.id as mercahntAddressId, merchant_addresses.address1 as merchantBusinessAddress, merchant_addresses.address2 as merchantBusinessAddress2, merchant_addresses.city as merchantBusinessCity, merchant_addresses.postcode as merchantBusinessPostcode, merchants.id as merchantId, (
                                                6371 * acos(
                                                    cos(radians(?)) *
                                                    cos(radians(lat)) *
                                                    cos(radians(lng) - radians(?)) +
                                                    sin(radians(?)) * sin(radians(lat))
                                                )
                                            ) as distance', [$request->lat, $request->lng, $request->lat])
                        ->having('distance', '<', (isset($request->distance)) ? $request->distance : 5)
                        ->where('merchants.merchant_user_type', 1)
                        ->orderBy('distance')
                        ->get();
                } elseif ($request->sortId == 2) {

                    //sortBy Highest Rating with Id 2

                    $ratingCount = ReviewRating::count();
                    if (!empty($ratingCount)) {

                        $salonList = MerchantAddress::leftJoin('merchants', 'merchants.id', '=', 'merchant_addresses.merchant_id')
                            ->leftJoin('merchant_details', 'merchant_details.merchant_id', '=', 'merchants.id')
                            ->selectRaw('merchant_details.id as merchantDetailId, merchant_details.merchant_business_name as merchantBusinessName, merchant_details.landline as merchantBusinessLandline, merchant_addresses.id as mercahntAddressId, merchant_addresses.address1 as merchantBusinessAddress, merchant_addresses.address2 as merchantBusinessAddress2, merchant_addresses.city as merchantBusinessCity, merchant_addresses.postcode as merchantBusinessPostcode, merchants.id as merchantId')
                            ->where('merchants.merchant_user_type', 1)
                            ->get();
                    } else {
                        return response()->json(['message' => 'No Higest Rating salon Found'], 401);
                    }
                } elseif ($request->sortId == 3 || $request->sortId == 4) {

                    //sortBy oldest and newest merchant

                    $salonList = MerchantAddress::leftJoin('merchants', 'merchants.id', '=', 'merchant_addresses.merchant_id')
                        ->leftJoin('merchant_details', 'merchant_details.merchant_id', '=', 'merchants.id')
                        ->selectRaw('merchant_details.id as merchantDetailId, merchant_details.merchant_business_name as merchantBusinessName, merchant_details.landline as merchantBusinessLandline, merchant_addresses.id as mercahntAddressId, merchant_addresses.address1 as merchantBusinessAddress, merchant_addresses.address2 as merchantBusinessAddress2, merchant_addresses.city as merchantBusinessCity, merchant_addresses.postcode as merchantBusinessPostcode, merchants.id as merchantId')
                        ->where('merchants.merchant_user_type', 1)
                        ->orderBy('merchants.id', $orderBy)
                        ->get();
                } else {

                    //show all record nearby 50kms if incorrect sortby id found

                    $salonList = MerchantAddress::join('merchants', 'merchants.id', '=', 'merchant_addresses.merchant_id')
                        ->join('merchant_details', 'merchant_details.merchant_id', '=', 'merchants.id')
                        ->selectRaw('merchant_details.id as merchantDetailId, merchant_details.merchant_business_name as merchantBusinessName, merchant_details.landline as merchantBusinessLandline, merchant_addresses.id as mercahntAddressId, merchant_addresses.address1 as merchantBusinessAddress, merchant_addresses.address2 as merchantBusinessAddress2, merchant_addresses.city as merchantBusinessCity, merchant_addresses.postcode as merchantBusinessPostcode, merchants.id as merchantId, (
                                                6371 * acos(
                                                    cos(radians(?)) *
                                                    cos(radians(lat)) *
                                                    cos(radians(lng) - radians(?)) +
                                                    sin(radians(?)) * sin(radians(lat))
                                                )
                                            ) as distance', [$request->lat, $request->lng, $request->lat])
                        ->having('distance', '<', (isset($request->distance)) ? $request->distance : 50)
                        ->where('merchants.merchant_user_type', 1)
                        ->orderBy('distance')
                        ->get();
                }

                $salonDeatils = array();

                if (!empty(count($salonList))) {

                    foreach ($salonList as $salonLists) {

                        $reviewCount = ReviewRating::where('merchant_id', $salonLists->merchantId)->count();

                        if (!empty($reviewCount)) {
                            $reviewAvg = ReviewRating::where('merchant_id', $salonLists->merchantId)->orderByRaw('avg(rating)  ASC')->groupBy('merchant_id')->avg('rating');
                        } else {
                            $reviewAvg = 'N/A';
                        }

                        $salonLists->reviewCount = $reviewCount;
                        $salonLists->reviewAvg = $reviewAvg;
                        $salonDeatils[] = $salonLists;
                    }

                    if ($request->sortId == 2) {

                        $salonDeatils = collect($salonDeatils)->sortBy('reviewAvg')->reverse()->values();
                    }

                    return response()->json(['message' => 'Merchant Infomation', 'data' => $salonDeatils]);
                } else {
                    return response()->json(['message' => 'No Salon Found'], 401);
                }
            } else {
                return response()->json(['message' => "User doesn't exist! Please login"], 401);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 401);
        }
    }

    /*
     *
     *    search and get Salon Listing as Per keywords and lat lng
     *
     */

    public function searchSalonListingAsKeywords(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'lat' => 'required',
                'lng' => 'required',
                'keywords' => 'required',
            ],
            [
                'lat.required' => 'Latitude is Required',
                'lng.required' => 'longitude is Required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        try {

            if (Auth::user()) {

                $salonList = MerchantAddress::join('merchants', 'merchants.id', '=', 'merchant_addresses.merchant_id')
                    ->join('merchant_details', 'merchant_details.merchant_id', '=', 'merchants.id')
                    ->selectRaw('merchant_details.id as merchantDetailId, merchant_details.merchant_business_name as merchantBusinessName, merchant_details.landline as merchantBusinessLandline, merchant_addresses.id as mercahntAddressId, merchant_addresses.address1 as merchantBusinessAddress, merchant_addresses.address2 as merchantBusinessAddress2, merchant_addresses.city as merchantBusinessCity, merchant_addresses.postcode as merchantBusinessPostcode, merchants.id as merchantId, (
                                                6371 * acos(
                                                    cos(radians(?)) *
                                                    cos(radians(lat)) *
                                                    cos(radians(lng) - radians(?)) +
                                                    sin(radians(?)) * sin(radians(lat))
                                                )
                                            ) as distance', [$request->lat, $request->lng, $request->lat])
                    ->having('distance', '<', 15)
                    ->orderBy('distance')
                    ->get();
            } else {
                return response()->json(['message' => "User doesn't exist! Please login"], 401);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 401);
        }
    }

    public function categoriesListing(Request $request)
    {
        $categoryJSON = $request->serviceCategoryId;
        $categoryArray = json_decode($categoryJSON, true);
        $categorylist = array();
        foreach ($categoryArray as $key => $value) {
            $category_id = MerchantService::select('id', 'service_id', 'merchant_staff_id')->where('service_category_id', '=', $value)->where('user_type', '=', 1)->get();
            // print_r($category_id);
            // die();
            // $categorylist[] = $category_id;
            return response()->json(['message' => 'listing',
                'data' => $category_id]);
        }

    }

    public function MerchantCategoriesListing()
    {
        $user = Auth::user();
        if ($user) {

            $user = Auth::user();
            $id = Auth::id();
            $category_listing = ServiceCategory::selectRaw('id as categoryId, category_name as categoryName')->get();
            return response()->json([
                'message' => 'Merchant Category Listing',
                'data' => $category_listing,
            ], 200);
        } else {
            return response()->json(['message' => "User Not Exist"], 401);
        }
    }

    public function MerchantAmenitiesListing()
    {
        if (Auth::user()) {

            $amenities_listing = Amenitie::selectRaw('id as AmenityId, name as AmenityName')->get();
            return response()->json([
                'message' => 'Merchant Amenity Listing',
                'data' => $amenities_listing,
            ], $this->success_code);
        } else {
            return response()->json(['message' => "User Not Exist"], 401);
        }
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => "success"], $this->successStatus);
    }

    public function logout(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'push_token' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

        if (PushToken::where(['user_id' => Auth::id(), 'token' => $request->push_token, 'user_type' => 1])->delete()) {
            $token = $request->user()->token();
            $token->revoke();

            $response = response()->json(['message' => 'You have been successfully logged out!'], $this->success_code);
        } else {
            $response = response()->json(['message' => 'You have been successfully logged out!'], $this->success_code);
        }

        return $response;
    }

    public function index()
    {
        $user = Auth::user();

        $data = array(
            'id' => $user->id,
            'Merchant mobile number' => $user->mobile_number,
        );

        return response()->json(['success' => true, 'data' => $data]);
    }

    public function addUserByMerchant(Request $request)
    {

        if (Auth::guard('merchants')) {
            $user = Auth::user();
            $id = Auth::id();
            $checknumber = $user->mobile_number;

            $validator = Validator::make(
                $request->all(),
                [
                    'name' => 'required',
                    'country_code' => 'required',
                    'mobile_number' => 'required|digits:10',
                    'address1' => 'required',
                    'city' => 'required',
                    'postalcode' => 'required',
                    'gender' => 'required',
                ],
                [
                    'name.required' => ' Name is Required',
                    'mobile_number.required' => ' Mobile Number is Required',
                    'mobile_number.digits' => ' Please enter a Valid Mobile Number',
                    'address1.required' => ' Address is Required',
                    'city.required' => ' City is Required',
                    'postalcode.required' => ' Postal Code is Required',
                    'gender.required' => ' Gender is Required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 401);
            }
            $request->merge([
                'mobile_number' => str_replace(' ', '', $request->mobile_number)
            ]);
            if (!empty($user)) {
                $user = new User();
                $user->country_code = $request->country_code;
                $user->mobile_number = !empty($request->mobile_number) ? $request->mobile_number : "";
                $user->name = !empty($request->name) ? $request->name : "";
                $user->email = !empty($request->email) ? $request->email : "";
                $user->address1 = !empty($request->address1) ? $request->address1 : "";
                $user->address2 = !empty($request->address2) ? $request->address2 : "";
                $user->postalcode = !empty($request->postalcode) ? $request->postalcode : "";
                $user->city = !empty($request->city) ? $request->city : "";
                $user->added_by = $id;
                $user->gender = !empty($request->gender) ? $request->gender : "";
                $user->added_by_type = "2";
                $user->otp = "0000";

                //$request->mobile_number) ? $request->mobile_number : "";

                $check = User::where('mobile', '=', $request->mobile_number)->first();
                if (!empty($check)) {
                    $response['message'] = "Phone number is already in use";

                    return response()->json($response, 401);
                }

                // $user1->status = "1";

                // print_r($user->mobile_number);
                // die();
                $data = User::where('mobile', '=', $user->mobile_number)->first();
                // print_r($data);
                // die();
                // $d= $data->mobile_number;

                if (!empty($data)) {

                    // $update = DB::table('users')
                    //     ->where('mobile_number',  $user->mobile_number)
                    //     ->update(['mobile_number' => $user->mobile_number, 'name' => $user->name, 'email' => $user->email, 'address1' => $user->address1, 'address2' => $user->address2, 'postalcode' =>  $user->postalcode, 'city' =>  $user->city, 'added_by' => $user->added_by, 'added_by_type' =>   $user->added_by_type, 'gender' =>   $user->gender,'otp' => $user->otp,'status' => $user->status]);
                    // $user->save();

                    $response['message'] = "User Already Exist";

                    return response()->json($response, 401);
                } else {
                    $user->save();
                    $user1 = new UsersDetail();
                    $user1->name = $request->name;
                    $user1->email = $request->email;
                    $user1->user_address1 = $request->address1;
                    $user1->user_address2 = $request->address2;
                    $user1->user_postcode = $request->postalcode;
                    $user1->user_city = $request->city;
                    $user1->added_by = $id;
                    $user1->gender = $request->gender;
                    $user1->added_user_type = "2";
                    $data = User::select('id')->where('mobile', '=', $user->mobile)->first();
                    $id = $data->id;

                    $user1->user_id = $id;
                    $user1->save();
                    $response['message'] = "User Added Successfully";

                    return response()->json($response, 200);
                }
            } else {
                return response()->json(['message' => 'User Not Added.'], 401);
            }
        }
    }

    public function addUserProfilePicByMerchant(Request $request)
    {
        // if (Auth::guard('merchants'))
        // {
        $user = Auth::user();
        // print_r($user->toArray());
        // die();
        $mobile_number = $user->mobile_number;

        // die();
        $validator = Validator::make(
            $request->all(),
            [
                'profilepic' => 'required|image|mimes:jpeg,png,jpg',

            ],
            [
                'profilepic.required' => '  Image is Required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $check = DB::table('users')
            ->where('mobile', '=', $mobile_number)->get('mobile')->first();
        // print_r($check);
        // die();
        if (empty($check)) {
            return response()->json(['message' => 'Mobile Number doesnot same'], 401);
        }

        if (!empty($user)) {

            $user = new User();
            $user->pic = !empty($request->pic) ? $request->pic : "";

            $imageName = 'UserProfilePic/' . time() . '.' . $request->profilepic->getClientOriginalExtension();
            $image = $request->file('profilepic');
            $t = Storage::disk('s3')->put($imageName, file_get_contents($image), 'public');
            $imageName = Storage::disk('s3')->url($imageName);
            $user->profilepic = $imageName;
            //   print_r($mobile_number);
            // print_r($user->profilepic);
            // die();

            $update = DB::table('users')
                ->where('mobile_number', '=', $mobile_number)
                ->update(['profilepic' => $user->profilepic]);

            $response['message'] = "Image Upload Successfully";

            return response()->json($response);
        } else {
            return response()->json(['message' => 'Image Not upload Successfully.'], 401);
        }
    }

    public function userList()
    {
        if (Auth::guard('merchants')) {
            $user = Auth::user();
            $id = Auth::id();

            if (!empty($user)) {

                $user_data = DB::table('users')->select('id', 'profilepic', 'name', 'mobile_number', 'email', 'address1', 'gender', 'address2', 'postalcode', 'city', 'added_by_type')->orderBy('name')->get()->toArray();

                $response['message'] = "Client Details";
                $response['data'] = $user_data;
                return response()->json($response);
            }
            if (empty($user)) {
                $response['message'] = "Client Details";
                $response['data'] = "";
                return response()->json($response);
            }
        }
    }

    /* public function edaitUserDetails(Request $request)
    {
    if (Auth::guard('merchants'))
    {
    $user = Auth::user();
    $id =  Auth::id();
    $mobilenumber = $user->mobile_number;

    $validator = Validator::make(
    $request->all(),
    [
    'name' => 'required',
    'mobile_number' => 'required|digits:10',
    'address1' => 'required',
    'city' => 'required',
    'postalcode' => 'required',
    ],
    [
    'name.required' => ' Name is Required',
    'mobile_number.required' => ' Mobile Number is Required',
    'mobile_number.digits' => ' Please enter a Valid Mobile Number',
    'address1.required' => ' Address is Required',
    'city.required' => ' City is Required',
    'postalcode.required' => ' Postal Code is Required',

    ]
    );

    if ($validator->fails()) {
    return response()->json(['message' => $validator->errors()], 401);
    }
    if (!empty($user))
    {

    $user->email = $request->email;
    $user->user_address1 = $request->address1;
    $user->user_address2 = $request->address2;
    $user->gender = $request->gender;
    $user->postalcode = $request->postalcode;
    $user->city = $request->city;
    $user->status = "1"; // if(DB::table('users')
    // ->where('mobile_number',  $user->mobile_number))
    // {
    $data= DB::table('users')->where('mobile_number',  $user->mobile_number)->first();
    $merchantcheck = DB::table('users')->where('added_by_type',2)->where('added_by', $id)->first();
    if(!empty($merchantcheck))
    {

    if(!empty($data))
    {
    $update = DB::table('users')
    ->where('mobile_number',  $user->mobile_number)
    ->update(['mobile_number' => $user->mobile_number,'name' => $user->name, 'email' => $user->email, 'address1' => $user->address1, 'address2' => $user->address2, 'gender' => $user->gender,'postalcode' =>  $user->postalcode, 'city' =>  $user->city,'status' => $user->status]);

    $response['message'] = "User details Successfully Submitted";
    $response['data'] =  "";

    return response()->json($response);
    }
    else
    {
    $response['message'] = "Please check your mobile number";

    return response()->json($response);
    }
    }
    else
    {
    $response['message'] = "User Not added by this Merchant";

    return response()->json($response);
    }
    }
    }

    $response['message'] = "User details Successfully Submitted";
    $response['data'] =  "";

    return response()->json($response);
    //}
    //}

    //} */

    public function exsistedIds()
    {
        $merchantlist = Merchant::with([
            'address' => function ($query) {
                $query->select('merchant_id', 'id', 'address1', 'address2', 'lat', 'lng', 'city', 'postcode');
            },
            'detail' => function ($query) {
                $query->select('merchant_id', 'id', 'merchant_business_name', 'merchant_company_name', 'landline', 'merchant_name', 'merchant_service_type', 'salon_type');
            },
            'rating' => function ($query) {
                $query->select(['merchant_id', 'id', 'review', 'rating']);
            },
        ])->select('id')->get()->toArray();
        $ids = [];
        foreach ($merchantlist as $merchant) {
            if ($merchant['address'] == null || $merchant['detail'] == null) {
            } else {
                array_push($ids, $merchant['id']);
            }
        }

        return $ids;
    }

    /*
     *   Search Api for vendor / service search with salon type (Screen Search)
     */
    public function search($keyword, $type = 0, $limit, $page, $current_lat, $current_lng)
    {

        $merchants = Merchant::select('id', 'is_verified')->where('status', 1)
            ->whereHas(
                'detail', function ($q) use ($keyword, $type) {
                    $q->where('merchant_business_name', 'like', '%' . $keyword . '%');
                    if ($type != 0) {
                        $q->where('salon_type', $type);
                    }
                })
            ->orWhereHas('services.serviceDetail', function ($q) use ($keyword) {
                $q->where('service_name', 'like', '%' . $keyword . '%');
            })
            ->whereHas('address')
            ->with(['detail', 'address', 'rating'])
        // ->toSql();
            ->get()->toArray();
        $merchant_list = [];
        foreach ($merchants as $merchant) {
            if ($merchant['address'] != null) {
                $lat = $merchant['address']['lat'];
                $lng = $merchant['address']['lng'];

                $merchant['distance'] = $this->getDistanceBetweenPointsNew($current_lat, $current_lng, $lat, $lng);

                $merchant['merchantRating'] = 0;
                $merchant['merchantReview'] = null;
                $merchant['discount'] = null;

                $merchant['merchantBusinessAddress'] = $merchant['address']['address1'];
                $merchant['merchantBusinessAddress2'] = $merchant['address']['address2'];
                $merchant['merchatLat'] = (double) $lat;
                $merchant['merchatLng'] = (double) $lng;
                $merchant['mercahntAddressId'] = $merchant['address']['id'];
                $merchant['merchantBusinessCity'] = $merchant['address']['city'];
                $merchant['merchantBusinessPostcode'] = $merchant['address']['postcode'];
            }
            if ($merchant['detail'] != null) {
                $merchant['merchantDetailId'] = $merchant['detail']['id'];
                $merchant['merchantBusinessName'] = $merchant['detail']['merchant_business_name'];
                $merchant['merchantBusinessLandline'] = $merchant['detail']['landline'];
                $merchant['salonType'] = $merchant['detail']['salon_type'];
                $merchant['businessCoverImage'] = ($merchant['detail']['business_cover_image']) ? $merchant['detail']['business_cover_image'] : $this->default_image;
                $merchant['deatilImage'] = ($merchant['detail']['detail_image'] != null) ? $merchant['detail']['detail_image'] : $this->default_image;
            }
            if ($merchant['rating'] != null) {
                $r_list = $merchant['rating'];
                $rating_list = array_map(function ($r_list) {
                    return $r_list['rating'];
                }, $r_list);
                $total = array_sum($rating_list);
                $avg = $total / count($rating_list);
                $merchant['merchantRating'] = $avg;
                $merchant['merchantReview'] = count($merchant['rating']);
            }
            unset($merchant['detail']);
            unset($merchant['address']);
            unset($merchant['rating']);
            $merchant['merchantId'] = $merchant['id'];
            array_push($merchant_list, $merchant);
        }
        $final_list = array_filter($merchant_list, function ($merchantList) {
            // dump($merchantList['distance']);
            if (isset($merchantList['distance']) && $merchantList['distance'] <= 20) {
                return true;
            }
        });
        // echo json_encode($final_list);
        // die;

        $merchant_list = $final_list;
        $columns = array_column($merchant_list, 'distance');
        array_multisort($columns, SORT_ASC, $merchant_list);
        $merchants = $this->paginate($merchant_list, $limit, $page)->toArray();

        $merchants = $this->removePaginatorParam($merchants);
        $merchants['data'] = array_values($merchants['data']);
        $response = (empty($merchants)) ? ['message' => 'Empty Merchant Listing', 'data' => $merchants['data']] : ['message' => 'Merchant Listing', 'total' => $merchants['last_page'], 'data' => $merchants['data']];
        return response()->json($response);
    }

    public function getMerchantDetail(Request $req)
    {
        if (Auth::user()) {
            $merchant_id = $req->merchant_id;
            $fav_merchant = Favourity::where(['user_id' => Auth::id(), 'merchant_id' => $merchant_id])->first();
            $merchant = Merchant::whereHas('services')
                ->with([
                    // 'detail' => function($q){
                    //     $q->select('id','merchant_id','business_cover_image','merchant_business_name','merchant_company_name','merchantBusinessLandline','merchant_name','merchant_image','merchant_service_type','salon_type','email');
                    // },
                    'services.serviceDetail',
                    'portfolio' => function ($q) {
                        $q->select('merchant_id', 'image');
                    },
                    'rating' => function ($q) {
                        $q->select('merchant_id', 'rating', 'review');
                    },
                    'staff',
                    'workingHours',
                ])
                ->where('id', $merchant_id)
                ->select('id')
                ->first()->toArray();
            $merchant['favourt_status'] = ($fav_merchant != null) ? true : false;
            $list = [];
            foreach ($merchant['services'] as $service) {
                $service['name'] = $service['service_detail']['service_name'];
                $unset_vars = ['service_detail', 'created_at', 'updated_at', 'status', 'service_id'];
                foreach ($unset_vars as $key) {
                    unset($service[$key]);
                }
                if (!array_key_exists($service['service_category_id'], $list)) {
                    array_push($list, $service['service_category_id']);
                    $category = ServiceCategory::where('id', $service['service_category_id'])->first();
                    $list[$service['service_category_id']] = [
                        'id' => $category->id,
                        'name' => $category->category_name,
                        'service' => [$service],
                    ];
                } else {
                    array_push($list[$service['service_category_id']]['service'], $service);
                }
            }

            unset($list[0]);
            $list = array_values($list);
            $merchant['services'] = $list;
            $response = ['message' => 'Merchant Listing', 'data' => $merchant];
            return response()->json($response);
        } else {
            return response()->json(['message' => 'Merchant not found! Please login'], 401);
        }
    }

    public function addRemoveFav(Request $req)
    {
        if (Auth::user()) {
            $validator = Validator::make(
                $req->all(),
                [
                    'merchant_id' => 'Required',
                    'action' => 'required',
                ],
                [
                    'merchant_id.required' => 'Salon name is required',
                    'action.required' => 'Action is required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }

            $m_id = $req->merchant_id;
            $action = $req->action;
            if (Auth::user()) {
                $u_id = Auth::id();
                switch ($action) {
                    case '0':
                        Favourity::where(['user_id' => $u_id, 'merchant_id' => $m_id])->delete();
                        $response = ['message' => 'Salon Removed from Favourite List'];
                        break;
                    case '1':
                        Favourity::create(['user_id' => $u_id, 'merchant_id' => $m_id]);
                        $response = ['message' => 'Salon Added To Favourite List'];
                        break;

                    default:
                        $response = ['message' => 'invalid input'];
                        break;
                }
                return response()->json($response);
            } else {
                $response = ['message' => 'User not found! Please login'];
                return response()->json($response);
            }
        } else {
            return response()->json(['message' => 'Please login'], 401);
        }
    }

    public function getFavMerchantList(Request $req)
    {
        if (Auth::user()) {
            $validator = Validator::make(
                $req->all(),
                [
                    'location' => 'Required',
                ],
                [
                    'location.required' => 'location is Required',
                ]
            );
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }
            $merchant_ids = Favourity::where('user_id', Auth::user()->id)->pluck('merchant_id');
            $merchants = Merchant::select("id", 'is_verified')->with('detail', 'address', 'rating')->whereIn('id', $merchant_ids)->get()->toArray();

            $limit = (isset($req->limit)) ? $req->limit : 20;
            $page = (isset($req->page)) ? $req->page : 1;
            $distance = (isset($req->distance)) ? $req->distance : 5;
            $current_lat = explode(',', $req->location)[0];
            $current_lng = explode(',', $req->location)[1];

            $merchant_list = [];
            // dd($merchants);
            foreach ($merchants as $merchant) {
                $merchant['merchantRating'] = null;
                $merchant['merchantReview'] = null;

                $merchant['merchantBusinessAddress'] = null;
                $merchant['merchantBusinessAddress2'] = null;
                $merchant['merchatLat'] = null;
                $merchant['merchatLng'] = null;
                $merchant['mercahntAddressId'] = null;
                $merchant['merchantBusinessCity'] = null;
                $merchant['merchantBusinessPostcode'] = null;

                $merchant['merchantDetailId'] = null;
                $merchant['merchantBusinessName'] = null;
                $merchant['merchantBusinessLandline'] = null;
                $merchant['salonType'] = null;
                $merchant['businessCoverImage'] = null;
                $merchant['deatilImage'] = null;

                $merchant['merchantRating'] = null;
                $merchant['merchantReview'] = null;
                if ($merchant['address'] != null) {
                    $lat = $merchant['address']['lat'];
                    $lng = $merchant['address']['lng'];

                    $merchant['distance'] = $this->getDistanceBetweenPointsNew($current_lat, $current_lng, $lat, $lng);

                    $merchant['merchantRating'] = 0;
                    $merchant['merchantReview'] = null;

                    $merchant['merchantBusinessAddress'] = $merchant['address']['address1'];
                    $merchant['merchantBusinessAddress2'] = $merchant['address']['address2'];
                    $merchant['merchatLat'] = (double) $merchant['address']['lat'];
                    $merchant['merchatLng'] = (double) $merchant['address']['lng'];
                    $merchant['mercahntAddressId'] = $merchant['address']['id'];
                    $merchant['merchantBusinessCity'] = $merchant['address']['city'];
                    $merchant['merchantBusinessPostcode'] = $merchant['address']['postcode'];
                }
                if ($merchant['detail'] != null) {
                    $merchant['merchantDetailId'] = $merchant['detail']['id'];
                    $merchant['merchantBusinessName'] = $merchant['detail']['merchant_business_name'];
                    $merchant['merchantBusinessLandline'] = $merchant['detail']['landline'];
                    $merchant['salonType'] = $merchant['detail']['salon_type'];
                    $merchant['merchantServiceType'] = $merchant['detail']['merchant_service_type'];
                    $merchant['businessCoverImage'] = ($merchant['detail']['business_cover_image'] != null) ? $merchant['detail']['business_cover_image'] : $this->default_image;
                    $merchant['deatilImage'] = ($merchant['detail']['detail_image'] != null) ? $merchant['detail']['detail_image'] : $this->default_image;
                }
                if ($merchant['rating'] != null) {
                    $r_list = $merchant['rating'];
                    $rating_list = array_map(function ($r_list) {
                        return $r_list['rating'];
                    }, $r_list);
                    $total = array_sum($rating_list);
                    $avg = $total / count($rating_list);
                    $merchant['merchantRating'] = $avg;
                    $merchant['merchantReview'] = count($merchant['rating']);
                }
                unset($merchant['detail']);
                unset($merchant['address']);
                unset($merchant['rating']);
                $merchant['merchantId'] = $merchant['id'];
                array_push($merchant_list, $merchant);
            }

            $columns = array_column($merchant_list, 'merchantBusinessName');
            array_multisort($columns, SORT_ASC, $merchant_list);
            $response = (empty($merchant_list)) ? ['message' => 'Favourite Merchant List is Empty', 'data' => null] : ['message' => 'Favourite Merchant list', 'data' => $merchant_list];
            return response()->json($response);
        } else {
            return response()->json(['message' => 'Merchant not found! Please login'], 401);
        }
    }

    public function getProfile()
    {
        if (Auth::user()) {
            $user_id = Auth::id();
            $user = User::where('id', $user_id)->select('id', 'mobile', 'country_code')->with('detail')->first();
            $user_detail = $user['detail'];
            $user['email'] = $user_detail['email'];
            $user['gender'] = $user_detail['gender'];
            $user['image'] = ($user_detail['user_image'] == null) ? $this->default_profile_image : $user_detail['user_image'];
            $user['name'] = $user_detail['name'];
            unset($user['detail']);
            $response = response()->json(['message' => 'User profile', 'data' => $user], $this->success_code);
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function updateProfile(Request $req)
    {
        if (Auth::user()) {
            $validation = $this->requestValidator($req, ['name']);
            if ($validation != null) {
                return $validation;
            }

            $user_id = Auth::id();
            $data = [
                'name' => $req->name,
                'email' => $req->email,
                'gender' => $req->gender,
            ];

            if (UsersDetail::where('user_id', $user_id)->update($data)) {
                $response = response()->json(['message' => 'Profile updated successsfully'], $this->success_code);
            } else {
                $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
            }

        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function uploadImage(Request $req)
    {
        if (Auth::user()) {
            $validator = Validator::make($req->all(),
                [
                    'image' => 'required|image|mimes:jpeg,png,jpg',
                ]
            );
            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], $this->error_code);
            }
            if ($image = $this->uploadFile($req->image, 'User_profile_pictures')) {
                if (UsersDetail::where('user_id', Auth::id())->update(['user_image' => $image])) {
                    $response = response()->json(['message' => 'Image upload successsfully'], $this->success_code);
                } else {
                    $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
                }
            } else {
                $response = response()->json(['message' => 'Unable to upload image on server'], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function getSetting()
    {
        if (Auth::user()) {
            $user = User::where('id', Auth::id())->with('detail')->first();

            $setting = [
                'push_status' => ($user['detail']['push_status'] == 1 ? true : false),
                'email_push_status' => ($user['detail']['email_push_status'] == 1 ? true : false),
                'email_exist' => ($user['detail']['email'] != null) ? true : false,
                'pin_status' => ($user->pin != null) ? true : false,
            ];
            $response = response()->json(['message' => 'User setting', 'data' => $setting], $this->success_code);
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    public function saveSetting(Request $req)
    {
        if (Auth::user()) {
            $data = [
                'push_status' => $req->push_status,
                'email_push_status' => $req->email_push_status,
            ];

            $response = (UsersDetail::where('user_id', Auth::id())->update($data)) ? response()->json(['message' => 'Setting updated successsfully'], $this->success_code) : $response = response()->json(['message' => 'Something went wrong'], $this->error_code);
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;
    }

    private function key_value_pair_exists(array $haystack, $key, $value)
    {
        return array_key_exists($key, $haystack) &&
            $haystack[$key] == $value;
    }

    public function test($mins = 10)
    {
        $s_time = 1603180800000;
        $e_time = 1603206000000;
        dump($this->convertToDateTime($s_time));
        // dump($this->convertToDateTime($e_time));

        dump('start time', $s_time);
        $miliseconds = $this->convertToMiliseconds($mins);

        dump("Adding $mins : ", $this->convertToDateTime($s_time + $miliseconds));

// echo time() + (24*60*60);
        // dump(time() + (24*60*60));
        // dump($this->convertToDateTime(time()));
        // dump($this->convertToDateTime(time() + (24*60*60)));
    }

    /*
    Function 0name : addReviewSubmit
    Function Purpose : Add Review submit
    Created by : Sourabh
    Created on : 23 Oct 2020
     */

    public function addReviewSubmit(Request $request)
    {
        if (Auth::user()) {
            $user_id = Auth::id();

            $user = User::where('id', $user_id)->select('id', 'mobile')->with('detail')->first();

            $validator = Validator::make(
                $request->all(),
                [
                    'merchant_id' => 'required',
                    'appointment_id' => 'required',
                    'rating' => 'required|between:1,5',
                    'review' => 'required',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }

            $check_appointment_status = DB::table('appointments')
                ->select('id', 'user_id', 'merchant_id', 'appointment_status')
                ->where('id', '=', $request->appointment_id)
                ->where('user_id', '=', $user_id)
                ->where('merchant_id', '=', $request->merchant_id)
                ->get()->toArray();
            $data = [
                'merchant_id' => $request->merchant_id,
                'user_id' => $user_id,
                'appointment_id' => $request->appointment_id,
                'rating' => $request->rating,
                'review' => $request->review,

            ];

            if (!empty($check_appointment_status)) {

                if (ReviewRating::create($data)) {
                    $response['message'] = "Review added successfully";
                    return response()->json($response, 200);
                } else {
                    $response['message'] = "Something went wrong";
                    return response()->json($response, 401);
                }
            } else {
                $response['message'] = "You are not authenticate to submit review";
                return response()->json($response, 401);
            }

        } else {
            $response['message'] = "Please login";
            return response()->json($response, 401);
        }
    }

    public function testSMSapi()
    {
        $message = 'Hi, This is last test Message from Team Opayn';
        $recipients = '+447854858377';
        $this->sendMessage($message, $recipients);
    }

    public function favoriteListExsist()
    {
        $status = (Favourity::where('user_id', Auth::id())->first() != null) ? true : false;
        return response()->json(['status' => $status], $this->success_code);

    }

    public function authSetPin(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['pin' => 'required']
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], $this->error_code);
        }

        $response = (User::where('id', Auth::user()->id)->update(['pin' => $request->pin])) ? response()->json(['message' => 'Pin set successfully'], $this->success_code) : response()->json(['message' => 'Soemthing went wrong'], $this->success_code);
        return $response;
    }

    public function setPin(Request $request)
    {     
        $validator = Validator::make(
            $request->all(),
            [
                'country_code' => 'required',
                'mobile' => 'required',
                'pin' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        $request->merge([
            'mobile' => str_replace(' ', '', $request->mobile)
        ]);

        if (!isset($request->push_token) || $request->push_token == null) {

            $request->merge([
                'push_token' => $this->randomNo(15),
            ]);
        }
        if ((strlen($request->pin) < 4) || (strlen($request->pin) > 4)) {
            return $response = response()->json(['message' => "Please enter 4 digits pin"], $this->error_code);
        }

        $user_detail = User::with('detail')->where('mobile', $request->mobile)->first();
        $user_name = (!empty($user_detail->detail) ? $user_detail->detail['name'] : null );
        $user_id = $user_detail->id;
        $userType = 0;
        $type = 2;
       
        if (!empty($user_detail)) {
            $update_pin = User::where('mobile', $request->mobile)->update([
                'pin' => $request->pin,
            ]);

            if (empty(PushToken::where('user_id', $user_detail->id)->where('token', $request->push_token)->first())) {
                PushToken::create([
                    'user_id' => $user_detail->id,
                    'user_type' => 0,
                    'token' => $request->push_token,
                ]);
            }

            $user_name = UsersDetail::select('name')->where('user_id', $user_id)->first();
            $u_name = $user_name->name;

            $data   =   array(
                'title'     => "Welcome Onboard!",
                'body'      => "Hi " .$u_name. ", Explore and enjoy the elite services offered by Opayn",
                'user_id'  =>  $user_id,
                'user_type' => 0
            );

            if($user_detail->detail['push_status'] == 1){
                $this->pushNotification($data);
            }

            $this->saveNotification($user_id,$userType,$data['title'],$type,$ref_id = null);

            $token = $user_detail->createToken('MyApp')->accessToken;

            $response = response()->json(['message' => "Pin set successfully", 'token' => $token], $this->success_code);
        } else {
            $response = response()->json(['message' => "This number is not save in database"], $this->error_code);
        }

        return $response;

    }
    public function updatePin(Request $request)
    {

        if (Auth::user()) {
            $id = Auth::id();

            $validator = Validator::make(
                $request->all(),
                [

                    'old_pin' => 'required|max:4',
                    'new_pin' => 'required|max:4',
                ]
            );

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()], 400);
            }

            $user_detail = User::where('id', $id)->first();            
            if (!empty($user_detail)) {
                if ($user_detail->pin == $request->old_pin) {
                    if ($user_detail->pin != $request->new_pin) {
                        $update_pin = User::where('id', $id)->update([
                            'pin' => $request->new_pin,
                        ]);
                        $response = response()->json(['message' => "Pin updated successfully"], $this->success_code);
                    } else {
                        $response = response()->json(['message' => "New pin must be different from old pin"], $this->error_code);
                    }
                } else {
                    $response = response()->json(['message' => "Old pin is incorrect"], $this->error_code);
                }

            } else {
                $response = response()->json(['message' => "This number is not save in database"], $this->error_code);
            }
        } else {
            $response = response()->json(['message' => 'Please login'], $this->unauthenticate_code);
        }
        return $response;

    }
    public function forgetPin(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'country_code' => 'required',
                'mobile' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }
        $request->merge([
            'mobile' => str_replace(' ', '', $request->mobile)
        ]);
        $user_detail = User::with('detail')->where('mobile', $request->mobile)->first();
        $mobile_no = $request->country_code . $request->mobile;
        $user_name = (!empty($user_detail->detail) ? $user_detail->detail['name'] : "");

        if (!empty($user_detail)) {
            if (!(isset($request->pin) && ($request->status))) {
                // $pin = ($request->mobile == '7988512302' || $request->mobile == '9530681066' || $request->mobile == '8528809228' || $request->mobile == '7854858377') ? '0000' : $this->sendPIN($mobile_no, $user_name);

                $pin = ($request->mobile == '7988512302' || $request->mobile == '9530681066' || $request->mobile == '8528809228' || $request->mobile == '7854858377') ? $this->sendPIN($mobile_no, $user_name) : $this->sendPIN($mobile_no, $user_name);

                if (isset($pin['status']) == "0000" && $pin['status'] == 400) {
                    return response()->json(['message' => "Please enter a valid Phone number"], $this->error_code);
                }

                $response = response()->json(['message' => "Temporary pin has been sent on your registered mobile number", 'temp_pin' => $pin, 'token' => null], $this->success_code);
            } else {
                $update_pin = User::where('mobile', $request->mobile)->update(['pin' => $request->pin]);
                $token = $user_detail->createToken('MyApp')->accessToken;
                if(empty(PushToken::where('user_id', $user_detail->id)->where('token', $request->push_token)->first())) {
                    PushToken::create([
                        'user_id' => $user_detail->id,
                        'user_type' => 0,
                        'token' => $request->push_token,
                    ]);
                }
                $response = response()->json(['message' => "Pin set successfully", 'temp_pin' => null, 'token' => $token], $this->success_code);
            }
        } else {
            $response = response()->json(['message' => "This number is not save in database"], $this->error_code);
        }

        return $response;

    }

    public function verifyPin(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'pin' => 'required|digits:4',
                'country_code' => 'required',
                'mobile' => 'required',
                'user_type' => 'required',
            ]
        );

        if (!isset($request->push_token) || $request->push_token == null) {

            $request->merge([
                'push_token' => $this->randomNo(15),
            ]);
        }
        $request->merge([
            'mobile' => str_replace(' ', '', $request->mobile)
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 400);
        }

        if (!isset($request->push_token) || $request->push_token == null) {

            $request->merge([
                'push_token' => $this->randomNo(15),
            ]);
        }
        $checkmobile = User::where('mobile', $request->mobile)->first();

        if (!empty($checkmobile)) {

            $user = User::where('mobile', $request->mobile)->where('pin', $request->pin)->first();
            if (!empty($user)) {

                //     if (empty(PushToken::where('user_id', $user->id)->where('token', $request->push_token)->first())) {
                //         PushToken::create([
                //             'user_id' => $user->id,
                //             'user_type' => 0,
                //             'token' => $request->push_token,
                //         ]);
                //     }

                //     $user_detail = UsersDetail::select('name')->where('user_id', $user->id)->first();

                //     // if ($user->is_user_verified == 0) {
                //         $user_name = $user_detail->name;
                //     $data = array(
                //         'title' => "Welcome Onboard!",
                //         'body' => "Hi " . $user_name . " Explore and enjoy the elite services offered by Opayn",
                //         'user_id' => $user->id,
                //         'user_type' => 0,
                //     );

                //     $this->pushNotification($data);

                // $userinfo = User::where('mobile', $request->mobile)
                //     ->update([
                //         'otp' => '',
                //         'is_user_verified' => 1,
                //     ]);
                $success['message'] = "Pin verify successfully";
                $success['token'] = $user->createToken('MyApp')->accessToken;
                return response()->json($success, $this->successStatus);
                // } else {

                //     return response()->json(['message' => 'Please enter a valid pin'], $this->error_code);
                // }
            } else {

                return response()->json(['message' => "Please enter a valid pin"], $this->error_code);
            }
        }

    }
}
