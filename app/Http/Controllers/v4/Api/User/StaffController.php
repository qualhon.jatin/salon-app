<?php

namespace App\Http\Controllers\v4\Api\User;

use App\Merchant;
use App\Appointment;
use App\MerchantStaff;
use Illuminate\Http\Request;
use App\MerchantsWorkingHour;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Merchant_secondary_working_hour;
use Illuminate\Support\Facades\Validator;

class StaffController extends Controller
{

    private $timezone;
    public function getStaffListByServiceId(Request $req)
    {
        if (Auth::user()) {
            $validator = Validator::make(
                $req->all(),
                ['merchant_id' => 'required', 'service_id' => 'required','datetime'=>'required']
            );
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], $this->error_code);
            }

            $service_ids = explode(',', $req->service_id);
            $date = $req->datetime;
            $timezone = $req->header('timezone');

            $merchant_id = $req->merchant_id;

            $date_time = $this->getStartAndEndDate($date,$timezone);
            // ej(array_search($this->getDay($date,$timezone)), $this->days) ;
            $week_day = array_search($this->getDay($date,$timezone), $this->days);            

            $merchant = Merchant::select('id')->where('id', $merchant_id)
                ->with(['workingHours', 'staff.staffServices' => function ($q) use ($service_ids) {
                    $q->whereIn('service_id', $service_ids);
                },
                    'staff.staffDetail',
                ])->first();

            $merchant_staff = $merchant['staff']->toArray();
            $merchant_working_hours = $merchant->workingHours->toArray();

            $merchant_satff_list = array_values(array_filter($merchant_staff, function ($m_s) {
                if (!empty($m_s['staff_services']) && $m_s['staff_detail'] != null) {
                    return true;
                }
            }));

            $staff_list = array_map(function ($merchant_satff_list) use ($week_day, $date){
                
                $default_working_hours = MerchantsWorkingHour::where(['merchant_id'=>$merchant_satff_list['staff_id'],'week_day'=>$week_day])->first();
                $secondry_working_hours = Merchant_secondary_working_hour::where('merchant_id',$merchant_satff_list['staff_id'])->whereDate('date',$this->convertToDate($date))->first();


                $m_staff = $merchant_satff_list;
                $m_detail = $m_staff['staff_detail'];

                $m_staff['status'] = 0;
                
                if($default_working_hours != null && $secondry_working_hours == null){
                    $m_staff['status'] = (int)$default_working_hours['status'];
                }
                if($default_working_hours == null && $secondry_working_hours != null){
                    $m_staff['status'] = (int)$secondry_working_hours['status'];
                }
                if($default_working_hours == null && $secondry_working_hours == null){
                    $m_staff['status'] = 0;
                }

                $m_staff['staff_name'] = $m_detail['merchant_name'];
                $m_staff['staff_image'] = ($m_detail['merchant_image'] == null) ? $this->default_profile_image : $m_detail['merchant_image'];
                $m_staff['gender'] = $m_detail['gender'];

                $unset_vars = ['staff_detail', 'staff_services', 'merchant_id'];
                foreach ($unset_vars as $key) {
                    unset($m_staff[$key]);
                }

                return $m_staff;
            }, $merchant_satff_list);
            
            $data = [
                'merchant_id' => $merchant['id'],
                'staff_list' => $staff_list,
                // 'working_hours' => $this->workingHours($merchant_working_hours)
            ];
            return response()->json(['message' => 'Merchant staff list by service', 'data' => $data], $this->success_code);
        } else {
            return response()->json(['message' => 'Merchant not found! Please login'], $this->unauthenticate_code);
        }
    }

    public function getStaffWorkingHours(Request $req)
    {
        $validator = Validator::make(
            $req->all(),
            [
                'staff_id' => 'required',
                'merchant_id' => 'required',
                'timezone' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], $this->error_code);
        }

        $this->timezone = $req->timezone;
        $week_day = array_search($this->getDay($req->dateTime, $this->timezone), $this->days);

        $date = date("Y-m-d", strtotime($this->convertToDateTime($req->dateTime)));
        $start_date = date("Y-m-d", strtotime($date));

        $merchant = Merchant::where('id', $req->merchant_id)->with(['workingHours' => function ($query) use ($week_day) {
            $query->where('week_day', $week_day);
        }])->first();

        $merchant_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $req->merchant_id)->whereDate('date', $start_date)->first();
        
        $merchant_working = [];
        if (!empty($merchant)) {
            if($merchant->workingHours != null ){
                $merchant_working = $merchant->workingHours->toArray();
            }            
        }

      
        if ((!empty($merchant_temp_working_hour)) && (!empty($merchant_working))) {            
            if ($merchant_working[0]['week_day'] == $merchant_temp_working_hour->week_day) {
                $merchant_working[0]['start_time'] = $merchant_temp_working_hour->start_time;
                $merchant_working[0]['end_time'] = $merchant_temp_working_hour->end_time;
                $merchant_working[0]['break_start_time'] = $merchant_temp_working_hour->break_start_time;
                $merchant_working[0]['break_end_time'] = $merchant_temp_working_hour->break_end_time;
                $merchant_working[0]['status'] = $merchant_temp_working_hour->status;
            }
        } 
        elseif ((empty($merchant_temp_working_hour)) && (empty($merchant_working))) {            
            $merchant_working = [];
        } 
        else {
            if (!empty($merchant_temp_working_hour)) {
                
                $merchant_working[0]['week_day'] = $merchant_temp_working_hour->week_day;
                $merchant_working[0]['start_time'] = $merchant_temp_working_hour->start_time;
                $merchant_working[0]['end_time'] = $merchant_temp_working_hour->end_time;
                $merchant_working[0]['break_start_time'] = $merchant_temp_working_hour->break_start_time;
                $merchant_working[0]['break_end_time'] = $merchant_temp_working_hour->break_end_time;
                $merchant_working[0]['status'] = $merchant_temp_working_hour->status;
            }

        }      

        if ($req->staff_id != 0 && $req->staff_id != $req->merchant_id) {
            
            $staff = MerchantStaff::where(['staff_id' => $req->staff_id, 'merchant_id' => $req->merchant_id])->with(['workingHours' => function ($query) use ($week_day) {
                $query->where('week_day', $week_day);
            }])->first();

            $staff_working_hours = "";
            if (!empty($staff)) {
                if (!empty($staff->workingHours)) {
                    $staff_working_hours = $staff->workingHours->toArray();
                }
            }

            $staff_temp_working_hour = Merchant_secondary_working_hour::where('merchant_id', $req->staff_id)->whereDate('date', $start_date)->first(); 
                      
            if ((!empty($staff_working_hours)) && (!empty($staff_temp_working_hour))) {

                $staff_working_hours = $staff->workingHours->toArray();
                if ($staff_working_hours[0]['week_day'] == $staff_temp_working_hour->week_day) {
                    $staff_working_hours[0]['start_time'] = $staff_temp_working_hour->start_time;
                    $staff_working_hours[0]['end_time'] = $staff_temp_working_hour->end_time;
                    $staff_working_hours[0]['break_start_time'] = $staff_temp_working_hour->break_start_time;
                    $staff_working_hours[0]['break_end_time'] = $staff_temp_working_hour->break_end_time;
                    $staff_working_hours[0]['status'] = $staff_temp_working_hour->status;
                }
                 
            } elseif ((empty($staff_working_hours)) && (empty($staff_temp_working_hour))) {

                $staff_working_hours = [];

            } elseif ((!empty($staff_working_hours)) && (empty($staff_temp_working_hour))) {

                $staff_working_hours = $staff->workingHours->toArray();

            } elseif ((empty($staff_working_hours)) && (!empty($staff_temp_working_hour))) {

                $staff_working_hours = $staff_temp_working_hour;

            }
            
            if ($staff != null) {
                $working_hours = $working_hour_staff = [];
                if (gettype($staff_working_hours) == "object") {

                    $working_hour_staff[0] = $staff_working_hours->toArray();

                } else {                    
                    $working_hour_staff = $staff_working_hours;
                }

                if(!empty($working_hour_staff) && ($working_hour_staff[0]['status'] != "0")){
                    $working_hours = $this->workingHours($working_hour_staff, $req->dateTime);                                        
                }
                else{
                    $working_hours = [];
                }

                if (count($working_hours) > 0) {
                    $working_hours = $this->checkAppoientment($req->merchant_id, $req->staff_id, $req->dateTime, $working_hours);
                }
                                
                $response = response()->json(['message' => 'Staff working hours list', 'data' => array_values($working_hours)], $this->success_code);
            } elseif (empty($staff)) {
                $response = response()->json(['message' => 'Staff working hours list', 'data' => $working_hours], $this->success_code);
            } else {
                $response = response()->json(['message' => 'You are not authorize to access this staff member'], $this->unauthenticate_code);
            }
        } else {            
            $merchant_working_hours = "";
            if ($merchant != null) {
                $merchant_working_hours = $merchant_working;       

                if(empty($merchant_working_hours)){                    
                   return  $response = response()->json(['message' => 'Merchant working hours list', 'data' => $merchant_working_hours], $this->success_code);
                }
                if($merchant_working_hours[0]['status'] == "0"){                    
                   return  $response = response()->json(['message' => 'Merchant working hours list', 'data' => []], $this->success_code);
                }

                $working_hours = $this->workingHours($merchant_working_hours, $req->dateTime);                

                if (count($working_hours) > 0) {
                    $working_hours = $this->checkAppoientment($req->merchant_id, $req->staff_id, $req->dateTime, $working_hours);
                }                
                $response = response()->json(['message' => 'Merchant working hours list', 'data' => array_values($working_hours)], $this->success_code);
            } else {
                $response = response()->json(['message' => 'Merchant not found'], $this->error_code);
            }
        }
        if ($req->dev == 1) {
            return $this->dev_timeSlots($working_hours);
        }
        return $response;
    }

    private function dev_timeSlots($response)
    {
        $time_slots = $response;
        $wh = array_map(function ($time_slots) {
            // dump($this->convertToDateTime($time_slots));
            dump($this->convertToLocalTime($time_slots, $this->timezone));
            // $time_slots = $this->convertToLocalTime($time_slots,$this->timezone);
            return $time_slots;
        }, $time_slots);
        // return response()->json(['data' => $wh], $this->success_code);
    }

    private function workingHours($wh, $timestamp)
    {

        $timezone = $this->timezone;
        $working_hours = array_map(function ($wh) use ($timestamp, $timezone) {

            $wh['start_time'] = $this->convertToTimestamp($wh['start_time'], $timezone);
            $wh['end_time'] = $this->convertToTimestamp($wh['end_time'], $timezone);
            $wh['break_start_time'] = $this->convertToTimestamp($wh['break_start_time'], $timezone);
            $wh['break_end_time'] = $this->convertToTimestamp($wh['break_end_time'], $timezone);

            $wh['time_slots'] = $this->timeSlots($wh['week_day'], $wh['start_time'], $wh['end_time'], $wh['break_start_time'], $wh['break_end_time'], $timestamp);

            $unset_vars = ['start_time', 'end_time', 'break_end_time', 'break_start_time', 'id', 'merchant_id', 'status', 'deleted_at', 'created_at', 'updated_at'];
            foreach ($unset_vars as $key) {
                unset($wh[$key]);
            }
            $day = array_search($this->getDay($timestamp, $timezone), $this->days);

            if ($day == $wh['week_day']) {
                return $wh;
            }
        }, $wh);

        $working_hours_list = array_filter($working_hours, function ($wh) {
            if ($wh != null) {
                return true;
            }
        });

        $hours_list = [];

        $working_hours_list = array_values($working_hours_list);
        if (!empty($working_hours_list)) {
            $hours_list = $working_hours_list[0];
        }

        if (isset($working_hours_list[0]['date'])) {
            unset($working_hours_list[0]['date']);
            $hours_list = $working_hours_list[0];
        }           
        return array_values($hours_list);
    }

    private function timeSlots($day, $start_time, $end_time, $break_start_time, $break_end_time, $timestamp) {

        $requested_datetime = $this->convertToDateTime($timestamp);
        $requested_local_timestamp = $this->convertToTimestamp($requested_datetime, $this->timezone);
        
        $time = $this->convertToTime($requested_local_timestamp);

        $data = [$this->convertToDateTime($start_time, $this->timezone)];

        if ($this->days[$day] == $this->getDay($timestamp, $this->timezone)) {
            $data = [];
        }
        $data = ($this->convertToTime($timestamp) > 0) ? [] : [$start_time];

        
        $updated_time = $start_time;
        
        while ($updated_time < $end_time) {
            $updated_time = $this->addInterval($updated_time);

            if ($this->convertToTime($updated_time) < $this->convertToTime($break_start_time) || $this->convertToTime($updated_time) >= $this->convertToTime($break_end_time)) {
                
                if ($time > 0) {                         
                    if ($this->convertToTime($updated_time) >= $time) {                    
                        array_push($data, $updated_time);
                    }
                } else {                    
                    array_push($data, $updated_time);

                }
            }
            
        }        
        unset($data[count($data) - 1]);
        return $data;   
    }

    private function checkAppoientment($merchant_id, $staff_id, $timestamp, $working_hours)
    {
        $options = ($staff_id == 0) ? ['merchant_id' => $merchant_id, 'appointment_status' => 1] : ['staff_id' => $staff_id, 'appointment_status' => 1];
        $appoientments = Appointment::with('detail')->where($options)->whereDate('appointment_date_time', $this->convertToDate($timestamp))->get()->toArray();
        if (count($appoientments) > 0) {

            $working_day_time_slots = $working_hours[1];

            $updated_working_hrs = [];
            $unsetKeys = [];
            foreach ($working_day_time_slots as $key => $time_slot) {
                $working_time = $this->convertToTime($time_slot);
                foreach ($appoientments as $appoientment) {

                    $appoientment_date_time_timestamp = $this->convertToTimestamp($appoientment['appointment_date_time'], $this->timezone);
                    $end_time = 0;
                    foreach ($appoientment['detail'] as $detail) {
                        $end_time += $detail['service_duration'] + $detail['service_buffer_time'];
                    }
                    $appoientment_end_timestamp = $appoientment_date_time_timestamp + $this->minsToMiliseconds($end_time);
                    $appoientment_end_time = $this->convertToTime($appoientment_end_timestamp);
                    $appoientment_start_time = $this->convertToTime($appoientment_date_time_timestamp);

                    if ($working_time >= $appoientment_start_time && $working_time <= $appoientment_end_time) {
                        array_push($unsetKeys, $key);
                    }
                }
            }
            foreach ($unsetKeys as $key) {
                unset($working_day_time_slots[$key]);
            }

            return $working_day_time_slots;
        } else {
            return (!empty($working_hours)) ? $working_hours[1] : $working_hours;
        }
    }

    public function testUpload(Request $request)
    {
        // dd($request->image);
        // echo $this->uploadFile($request->image, 'banner_images');

    }

    private function getStartEndDate(){
        $date = date("Y-m-d", strtotime($this->convertToLocalTime($date, $timezone)));

        $start_date = date("Y-m-d", strtotime($date));
        $start = date("Y-m-d", strtotime($date));
        $start_date = $start_date . " 00:00:00";

        $end_date = $start_date;
        $end_date = strtotime($end_date);
        $end_date = strtotime("+6 day", $end_date);
        $end = date("Y-m-d", $end_date);
        $end_date = date("Y-m-d", $end_date) . " 23:59:59";

        return ['start_date'=>$start_date,'end_date'=>$end_date];
    }
}
