<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Merchant;
use App\MerchantDetail;
use Yajra\DataTables\Facades\DataTables;

class MerchantController extends Controller
{

    // protected $connection = 'mysql2';
    public function index()
    {
        return view('admin.merchant.index');
    }

    public function getMerchantListings()
    {

        $merchant = Merchant::join('merchant_addresses', 'merchant_addresses.merchant_id', '=', 'merchants.id')
            ->join('merchant_details', 'merchant_details.merchant_id', '=', 'merchants.id')
            ->select('merchants.id as merchantId', 'merchant_details.id as merchantDetailId', 'merchants.*', 'merchant_addresses.*', 'merchant_details.*')
            ->where('merchants.merchant_user_type', 1);
        return DataTables::eloquent($merchant)
        //->addColumn('action','admin.seller.action')
            ->addColumn('action', 'action')
            ->addColumn('address', function ($data) {
                return ($data->address1) . ' ' . ($data->address2) . ' ' . ($data->city);

            })
            ->rawColumns(['action'])
            ->toJson();
    }

    public function merchantList()
    {

        $count = 0;
        for ($i = 0; $i < 1; $i++) {

            $mer_data = Merchant::with('detail', 'address')->offset(11001)->limit(2000)->get();

            foreach ($mer_data->toArray() as $data) {
                // print_r($data);

                $count++;

                $check_record = MerchantTrail::where('mobile', $data['mobile'])->first();
                if (empty($check_record)) {

                    $merchants = new MerchantTrail;

                    $merchants->country_code = $data['country_code'];
                    $merchants->mobile = $data['mobile'];
                    $merchants->otp = $data['otp'];
                    $merchants->verification_level = $data['verification_level'];
                    $merchants->merchant_user_type = $data['merchant_user_type'];
                    $merchants->date_of_joining = $data['date_of_joining'];
                    $merchants->remember_token = $data['remember_token'];
                    $merchants->status = $data['status'];
                    $merchants->is_verified = $data['is_verified'];
                    $merchants->created_at = date('Y-m-d H:i:s');
                    $merchants->updated_at = date('Y-m-d H:i:s');

                    $merchants->save();
                    $insert_merchant_data = $merchants->id;

                    $merchants_details = new MerchantDetailsTrail;

                    $merchants_details->merchant_id = $insert_merchant_data;
                    $merchants_details->business_cover_image = $data['detail']['business_cover_image'];
                    $merchants_details->merchant_business_name = $data['detail']['merchant_business_name'];
                    $merchants_details->merchant_company_name = $data['detail']['merchant_company_name'];
                    $merchants_details->landline = $data['detail']['landline'];
                    $merchants_details->merchant_name = $data['detail']['merchant_name'];
                    $merchants_details->merchant_image = $data['detail']['merchant_image'];
                    $merchants_details->merchant_service_type = $data['detail']['merchant_service_type'];
                    $merchants_details->salon_type = $data['detail']['salon_type'];
                    $merchants_details->email = $data['detail']['email'];
                    $merchants_details->gender = $data['detail']['gender'];
                    $merchants_details->detail_image = $data['detail']['detail_image'];
                    $merchants_details->push_status = $data['detail']['push_status'];
                    $merchants_details->created_at = date('Y-m-d H:i:s');
                    $merchants_details->updated_at = date('Y-m-d H:i:s');

                    $merchants_details->save();

                    $merchants_address = new MerchantAddressTrail;

                    $merchants_address->merchant_id = $insert_merchant_data;
                    $merchants_address->address1 = $data['address']['address1'];
                    $merchants_address->address2 = $data['address']['address2'];
                    $merchants_address->city = $data['address']['city'];
                    $merchants_address->postcode = $data['address']['postcode'];
                    $merchants_address->lat = $data['address']['lat'];
                    $merchants_address->lng = $data['address']['lng'];
                    $merchants_address->created_at = date('Y-m-d H:i:s');
                    $merchants_address->updated_at = date('Y-m-d H:i:s');

                    $merchants_address->save();

                    $someModel = new MerchantTrail;
                    $someModel->setConnection('mysql_two');

                }

            }

        }

    }
    public function merchantBussinessName()
    {
        // echo "hy";
        // die();
        $count = 0;
        // for ($i = 0; $i < 1; $i++) {

            $mer_data = MerchantDetail::offset(10001)->limit(1000)->get();

            foreach ($mer_data->toArray() as $data) {
                if($data['merchant_business_name'] == "" || $data['merchant_business_name']){
                  $upadte_merchant_bussiness =  MerchantDetail::where('id',$data['id'])
                                                                ->update([
                        'merchant_business_name' => $data['merchant_name'],
                        'merchant_company_name' => $data['merchant_name']
                        ]);
                }
                
                // echo json_encode($data);

                // $count++;

            }

        // }

    }

    public function importMerchant()
    {
        dd('merchant listing');
    }
}
