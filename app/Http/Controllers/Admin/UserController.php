<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Yajra\DataTables\Facades\DataTables;
use DB;

class UserController extends Controller
{
    //
    public function index(){
        return view('admin.user.index');
    }

    public function getUserListings(){
        //$seller = User::select('*');
        $user = User::join('users_details','users_details.user_id', '=', 'users.id')->select('users.id as userId','users.*','users_details.*');
        return DataTables::eloquent($user)
            //->addColumn('action','admin.seller.action')
            ->addColumn('action','action')
            ->addColumn('address', function ($data) {

                return ($data->user_address1) . ' ' . ($data->user_address2) .' '. ($data->user_city);

            })
            ->rawColumns(['action'])
            ->toJson();
    }


}
