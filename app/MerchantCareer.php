<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantCareer extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_careers';

    protected $fillable = [
        'merchant_id','image','name','experience','speciality','job_description','education','career_type'
    ];

    public function merchant(){
    	$columns = ['id'];
    	return $this->hasOne('App\Merchant','id','merchant_id')->select($columns);
    }
}
