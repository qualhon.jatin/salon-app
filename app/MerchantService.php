<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantService extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_services';

    protected $fillable = [
        'service_id','merchant_staff_id','service_category_id','user_type','service_price','service_duration','service_buffer_time','service_image','status'
    ];

    public function serviceDetail(){
        $columns = ['id','service_categories_id','service_name','service_image'];
    	return $this->hasOne('App\Service','id','service_id')->select($columns);
    }

    public function category(){
    	return $this->hasOne('App\ServiceCategory','id','service_category_id');
    }

    public function offer(){
        $columns = ['service_id','discount','max_discount_price'];
        return $this->hasOne('App\MerchantOffer','id','service_id')->select($columns);
    }
    public function staffMemeber(){
        $columns = [];
        return $this->hasMany('App\MerchantStaff','staff_id','merchant_staff_id');
    }
}
