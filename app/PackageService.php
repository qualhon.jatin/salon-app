<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageService extends Model
{
    protected $guard = 'merchants';
    protected $table = 'package_services';

    protected $fillable = [
        'package_id','service_id',
    ];

    public function service(){
        $columns = ['id','service_id','merchant_staff_id','service_category_id','service_price','service_duration','service_buffer_time','service_image'];
        return $this->hasOne('App\MerchantService','service_id','service_id')->select($columns);
    }

    public function category(){        
        return $this->hasOne('App\ServiceCategory');
    }
}
