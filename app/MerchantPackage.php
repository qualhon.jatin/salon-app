<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantPackage extends Model
{
    protected $guard = 'merchants';
    protected $table = 'merchant_packages';

    protected $fillable = [
        'merchant_id','name','price','start_time','end_time','status'
    ];

    public function packageServices(){
    	$columns =  ['package_id','service_id'];
    	return $this->hasMany('App\PackageService','package_id')->select($columns);
    }

    public function detail(){
        $columns = ['merchant_id','merchant_name','merchant_business_name','merchant_image','detail_image'];
        return $this->hasOne('App\MerchantDetail','merchant_id','merchant_id')->select($columns);
    }
}
