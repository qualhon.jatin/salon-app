<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    protected $fillable = ['user_id','message','is_read','user_type','type','ref_id'];
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    // public function appoientment(){
    //     $columns = [];
    //     return $this->hasOne('App\Appointment','ref_id');
    // }


    /*
    public function staffAppointment(){
        $columns =  ['id','merchant_id','staff_id','user_id','appointment_date_time','appointment_status'];
        return $this->hasMany('App\Appointment','merchant_id')->select($columns)->where('appointment_status',1);
    }
    
    public function detail(){
        $columns = ['user_id','email','gender','user_image','name','push_status','email_push_status'];
        return $this->hasOne('App\UsersDetail','user_id')->select($columns);
    }

    public function detailMerchant(){
        $columns = ['id','merchant_id','business_cover_image','merchant_business_name','merchant_company_name','landline','merchant_name','merchant_image','merchant_service_type','salon_type','email','gender','detail_image'];
        return $this->hasOne('App\MerchantDetail','merchant_id','id')->select($columns);
    }
    */

}
