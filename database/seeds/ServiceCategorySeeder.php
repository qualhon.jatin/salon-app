<?php

use Illuminate\Database\Seeder;
use App\ServiceCategory;
use App\Service;

class ServiceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        /* $categories = [

                "Murad Facials"=>['subcats'=>['Express Facial','AHA Rapid exfoliation','Sun Undone Infusion','Clearly youthfull','Redness Therapy']],

                "Facial"=>['subcats'=>['½ Hour Facial','Full 1hr Facial','Shenaz Hussain Gold','Shenaz Hussain Perl','Shenaz Hussain Oxygen']],

                "Body Treatment"=>['subcats'=>['Bacne Blitz','Pomegranate body exfoliation','Spray Tan']],
                "
                Hair Removal Waxing"=>['subcats'=>['Lip','Full face','Chin','Face Sides','Underarm','Half Leg wax','Full Leg','Bikini line','Hollywood/Brazillian','Male hollywood','Half arm','Full arm','Chest/Back']],

                "Eye Treatments"=>['subcats'=>['Eyebrow wax / thread','Lash Tint','Brow Tint','Lash Extensions']],

                "Hands and Feet"=>['subcats'=>['Shellac','Pedicure','Manicure','Shellac removal','Male mani / pedi','Shape and Paint','Acrylic Nails','Deluxe Manicure','Deluxe Pedicure','Soak off Gel Nails']],

                "Massage"=>['subcats'=>['Back massage','Full body','Lava shell Back massage','Lava shell full body','Indian head massage','Hopi ear candling','Full Body Scrub and Moisturise']],
            ]; */
            $categories = [
                "Murad Facials"=>['subcats'=>['Express Facial','AHA Rapid exfoliation','Sun Undone Infusion','Clearly youthfull','Redness Therapy']],

                "Facial"=>['subcats'=>['½ Hour Facial','Full 1hr Facial','Shenaz Hussain Gold','Shenaz Hussain Perl','Shenaz Hussain Oxygen']],

                "Body Treatment"=>['subcats'=>['Bacne Blitz','Pomegranate body exfoliation','Spray Tan']],
                "
                Hair Removal Waxing"=>['subcats'=>['Lip','Full face','Chin','Face Sides','Underarm','Half Leg wax','Full Leg','Bikini line','Hollywood/Brazillian','Male hollywood','Half arm','Full arm','Chest/Back']],

                "Eye Treatments"=>['subcats'=>['Eyebrow wax / thread','Lash Tint','Brow Tint','Lash Extensions']],

                "Hands and Feet"=>['subcats'=>['Shellac','Pedicure','Manicure','Shellac removal','Male mani / pedi','Shape and Paint','Acrylic Nails','Deluxe Manicure','Deluxe Pedicure','Soak off Gel Nails']],

                "Massage"=>['subcats'=>['Back massage','Full body','Lava shell Back massage','Lava shell full body','Indian head massage','Hopi ear candling','Full Body Scrub and Moisturise']],

                "Threading"=>['subcats'=>['Eyebrow Threading','Forehead','Upperlip','Chin','Full Face','Eyberow and upper lip']],

                "Mehndi"=>['subcats'=>['Mehndi','Bridal Mehndi']],

                "Make- Up"=>['subcats'=>['Daytime','Evening','Bridal Trial','Full Bridal (inc. Hair and Make)']],

                "HAIR SERVICES"=>['subcats'=>['Curls','Afro Hair','Cut and Blow dry','Braiding and Extentions','Dry Cut','Blow Dry','Children’s Cut'

                ,'Fringe Trim','Conditioning treatment','Hair up','Hair Straightening','Dressing Hair up','Perm','Steam and Conditioning','Shampoo and Set','Wash & Restyle','Gents Hair cut','Colour']],


                "Root Colour"=>['subcats'=>['Full head colour','T-section Foils','T-section Foils','Half head foils','Full head foils','Colour correction','Permanent Colour','Semi Permanent Colour','Highlights'.'Lowlights']],

                ];




            foreach ($categories as $key => $categoriesss) {

                $category = new ServiceCategory;
                $category->category_name = $key;
                $category->status = "1";
                $category->save();

                foreach($categoriesss['subcats'] as $value ){

                    $service = new Service;
                    $service->service_categories_id = $category->id;
                    $service->service_name = $value;
                    $service->save();

                }

            }

    }
}
