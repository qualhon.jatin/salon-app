<?php

use Illuminate\Database\Seeder;

use App\Amenitie;

class AmenitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $amenities = ['Adult only','Children friendly','Close to bus stop','Close to train station','Cruelty free products','Free - wifi','Free parking available','Natural products','Paid parking available','Wheelchair accessible'];
        // dd($amenities)
        // for ($item = 0; $item < count($amenities); $item++)
        // {
            foreach ($amenities as $amenitie)
            {
                $amenity = new Amenitie;
                $amenity->name = $amenitie;                
                $amenity->status = "1";
                $amenity->save();                
            }
        // }
    }
}
