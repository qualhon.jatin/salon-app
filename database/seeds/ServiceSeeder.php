<?php

use Illuminate\Database\Seeder;
use App\Service;
class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = ["Express Facial", "AHA rapid exfoliation", "Clearly Youthful "];
        for ($item = 0; $item < count($categories); $item++)
        {
            foreach ($categories as $category)
            {
                $service = new Service;
                $service->service_name = $categories[$item];
                $service->service_categories_id = "1";
                $service->status = "1";
                $service->save();
                $item += 1;
            }
        }
    }

}
