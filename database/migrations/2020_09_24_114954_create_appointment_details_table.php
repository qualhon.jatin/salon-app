<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('appointment_id');
            // $table->unsignedBigInteger('appointment_service_id');
            $table->string('name');
            $table->integer('price');
            $table->integer('discount')->nullable();

            $table->integer('service_duration')->nullable();
            $table->integer('service_buffer_time')->nullable();
            $table->text('package_services')->nullable();

            // $table->integer('appoientment_type')->comment('0=>normal , 1=>package');

            $table->timestamps();
            $table->foreign('appointment_id')->references('id')->on('appointments')->onDelete('cascade');
            // $table->foreign('appointment_service_id')->references('id')->on('merchant_services')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_details');
    }
}