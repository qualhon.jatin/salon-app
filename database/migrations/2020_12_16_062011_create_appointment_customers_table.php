<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_customers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('appointment_id');
            $table->string('customer_name')->nullable();
            $table->bigInteger('customer_mobile')->nullable();
            $table->text('customer_address')->nullable();
            $table->string('customer_landmark',255)->nullable();
            $table->string('customer_postcode',255)->nullable();
            $table->double('customer_lat')->nullable();
            $table->double('customer_lng')->nullable();
            $table->foreign('appointment_id')->references('id')->on('appointments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_customers');
    }
}
