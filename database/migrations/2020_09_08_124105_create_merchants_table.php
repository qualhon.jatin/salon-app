<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchants', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('mobile')->unique();
            $table->string('otp',255)->nullable();
            $table->integer('verification_level')->length(11)->default(0)->comment('1=>step1Complete,2=>step2Complete,3=>step3Complete,4=>step4Complete,5=>step5Complete');
            $table->integer('merchant_user_type')->length(11)->default(1)->comment('1=>merchant,2=>staff');
            $table->timestamp('date_of_joining');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchants');
    }
}
