<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('service_categories_id');
            $table->string('service_name',255);
            $table->integer('service_price')->default(99);
            $table->integer('service_duration')->default(2700);
            $table->integer('service_buffer_time')->default(300);
            $table->string('service_image',255)->nullable();
            $table->integer('status')->default(1)->comment('0=>inactive,1=>active');
            $table->timestamps();
            $table->foreign('service_categories_id')->references('id')->on('service_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
