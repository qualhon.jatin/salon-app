<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnBirthdayToUsersDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_details', function (Blueprint $table) {
            $table->string('birthday',255)->nullable()->after('remember_token');
            $table->string('anniversary',255)->nullable()->after('birthday');
            $table->text('allergies',255)->nullable()->after('anniversary');
            $table->boolean('phone_preference',255)->nullable()->after('allergies');
            $table->boolean('text_preference',255)->nullable()->after('phone_preference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_details', function (Blueprint $table) {
            //
        });
    }
}
