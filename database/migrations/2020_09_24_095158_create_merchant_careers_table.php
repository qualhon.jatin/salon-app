<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantCareersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_careers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('merchant_id');
            $table->string('image',255)->nullable();
            $table->string('name',255);
            $table->string('experience',255)->nullable();
            $table->string('speciality',255)->nullable();
            $table->string('education',255)->nullable();
            $table->string('job_description',255);
            $table->integer('career_type')->default(1)->comment('0=>training,1=>vacancy');
            $table->timestamps();
            $table->foreign('merchant_id')->references('id')->on('merchants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_careers');
    }
}
