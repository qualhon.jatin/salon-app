<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMerchantDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merchant_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('merchant_id');
            $table->string('business_cover_image',255)->nullable();
            $table->string('merchant_business_name',255)->nullable();
            $table->string('merchant_company_name',255)->nullable();
            $table->string('landline',255)->nullable();
            $table->string('merchant_name',255);
            $table->string('merchant_image',255)->nullable();
            $table->integer('merchant_service_type')->nullable()->comment('1=>inhouse,2=>mobile,3=>both');
            $table->integer('salon_type')->default(3)->comment('1=>male,2=>female,3=>unisex');
            $table->string('email',255)->nullable();
            $table->timestamps();
            $table->foreign('merchant_id')->references('id')->on('merchants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant_details');
    }
}
