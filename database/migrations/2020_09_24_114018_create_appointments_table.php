<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('merchant_id');
            $table->unsignedBigInteger('staff_id');
            $table->unsignedBigInteger('user_id');
            $table->string('merchant_address');

            $table->timestamp('appointment_date_time')->nullable();
            $table->integer('appointment_type')->nullable();
            $table->integer('appointment_status')->default(1)->comment('0=>cancelled, 1=>active, 2=>completed');
            $table->float('actual_price');
            $table->integer('discount')->nullable();
            $table->float('final_price');
            $table->string('client_message',255)->nullable();            
            $table->timestamps();
            $table->foreign('merchant_id')->references('id')->on('merchants')->onDelete('cascade');
            $table->foreign('staff_id')->references('id')->on('merchants')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
