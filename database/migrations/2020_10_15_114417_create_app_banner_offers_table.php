<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppBannerOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_banner_offers', function (Blueprint $table) {
            $table->id();
            $table->string('banner_image')->default('https://images-na.ssl-images-amazon.com/images/I/61cjGXN24xL._SL1200_.jpg')->comment('offerBannerImage By admin');
            $table->string('banner_title',1048)->nullable();
            $table->string('offer_text',1048)->nullable();
            $table->integer('banner_type')->default(1)->comment('1:offer_Percent, 2: Training, 3: vacancy');
            $table->integer('discount_percent')->nullable()->comment('Discount percentage like 10,20 ect.');
            $table->integer('status')->default(1)->comment('1:Active, 0: Inactive');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_banner_offers');
    }
}
