<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('email',255)->nullable();
            $table->string('name',255);
            $table->integer('gender')->nullable();
            $table->string('user_address1',255)->nullable();
            $table->string('user_address2',255)->nullable();
            $table->string('user_city',255)->nullable();
            $table->string('user_postcode',255)->nullable();
            $table->string('user_image',255)->nullable();
            $table->integer('added_by')->nullable();
            $table->integer('added_user_type')->default(1)->comment('1=>user,2=>Merchent, 3 =>admin');
            $table->integer('push_status')->default(1)->comment('1=>active,2=>inactive');
            $table->integer('email_push_status')->default(1)->comment('1=>active,2=>inactive');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_details');
    }
}
