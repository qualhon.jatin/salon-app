<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserAdddressToAppointments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appointments', function (Blueprint $table) {
            // $table->text('user_address')->after('merchant_lat')->nullable();
            // $table->double('user_lat')->after('user_address')->nullable();
            // $table->double('user_lng')->after('user_lat')->nullable();
            $table->integer('appointment_service_type')->after('user_lng')->nullable()->comment('1=>inhouse,2=>mobile,3=>both');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appointments', function (Blueprint $table) {
            //
        });
    }
}
