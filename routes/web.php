<?php


/* --------------------- Common/User Routes START -------------------------------- */

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes([ 'verify' => true ]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('scrap-data','Admin\HomeController@scrapData');
// Route::get('/merchant/bussiness', 'MerchantController@merchantBussinessName');

/* --------------------- Common/User Routes END -------------------------------- */

/* ----------------------- Admin Routes START -------------------------------- */

Route::prefix('/admin')->name('admin.')->namespace('Admin')->group(function(){

    /**
     * Admin Auth Route(s)
     */
    Route::get('/login','LoginController@showLoginForm')->name('login');
    Route::namespace('Auth')->group(function(){

        //Login Routes
        Route::get('/login','LoginController@showLoginForm')->name('login');
        Route::post('/login','LoginController@login');
        Route::post('/logout','LoginController@logout')->name('logout');

        //Register Routes
        // Route::get('/register','RegisterController@showRegistrationForm')->name('register');
        // Route::post('/register','RegisterController@register');

        //Forgot Password Routes
        Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        //Reset Password Routes
        Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');

        // Email Verification Route(s)
        Route::get('email/verify','VerificationController@show')->name('verification.notice');
        Route::get('email/verify/{id}','VerificationController@verify')->name('verification.verify');
        Route::get('email/resend','VerificationController@resend')->name('verification.resend');





    });
    Route::get('/user', 'UserController@index')->name('user');
    Route::get('/getUserListings', 'UserController@getUserListings')->name('getUserListings');

    Route::get('/merchant', 'MerchantController@index')->name('merchant');
    Route::get('/getMerchantListings', 'MerchantController@getMerchantListings')->name('getMerchantListings');

    Route::get('/settings', 'HomeController@settings')->name('settings');
    Route::post('/updateSettings', 'HomeController@updateSettings')->name('updateSettings');

    Route::get('/dashboard','HomeController@index')->name('home');

    //Put all of your admin routes here...

    Route::get('/merchant/bussiness', 'MerchantController@merchantBussinessName')->name('merchant');

});

/* ----------------------- Admin Routes END -------------------------------- */
