
<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Version 1 api listing start

        Route::group(['middleware' => 'auth:api'], function () {
            Route::post('details', 'v1\Api\User\UserController@details');
        });

        Route::post('v1/verify', 'v1\Api\User\UserController@verify');
        Route::post('v1/login', 'v1\Api\User\UserController@login');
        Route::post('v1/re-send-otp', 'v1\MainController@reSendOTP');

        Route::group(['prefix' => 'v2', 'middleware' => 'auth:api'], function () {
            Route::post('update-mobile-number', 'v2\MainController@updateNumber');
        });

        Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function () {
            // Route::post('merchant/update-mobile-number','v1/MainController@updateNumber');

            Route::get('get-setting', 'v1\Api\User\UserController@getSetting');
            Route::post('save-setting', 'v1\Api\User\UserController@saveSetting');
            Route::get('favorite-list-status', 'v1\Api\User\UserController@favoriteListExsist');

            Route::get('get-profile', 'v1\Api\User\UserController@getProfile');
            Route::post('update-profile', 'v1\Api\User\UserController@updateProfile');
            Route::post('upload-image', 'v1\Api\User\UserController@uploadImage');
            Route::post('/userLogout', 'v1\Api\User\UserController@logout');

            // Route::post('/nearby-salon-listing','v1\Api\User\UserController@NearBySalonListing_dev')->name('nearby-salon-listing');
            // Route::post('/search-salon-listing','v1\Api\User\UserController@NearBySalonListing')->name('nearby-salon-listing');

            Route::get('/merchant_categories_listing', 'v1\Api\User\UserController@MerchantCategoriesListing')->name('merchant_categories_listing');
            Route::get('merchant_amenities_listing', 'v1\Api\User\UserController@MerchantAmenitiesListing')->name('merchant_amenities_listing');

            Route::get('vendors', 'v1\Api\User\UserController@NearBySalonListing_dev');
            // Route::get('vendor-detail','v1\Api\User\UserController@getMerchantDetail');
            Route::get('vendor-detail', 'v1\Api\MerchantsController@getMerchantDetail');
            Route::post('favorite', 'v1\Api\User\UserController@addRemoveFav');
            Route::get('fav-merchant-list', 'v1\Api\User\UserController@getFavMerchantList');

            Route::get('get-staff-by-serviceId', 'v1\Api\User\StaffController@getStaffListByServiceId');
            Route::get('get-staff-working-hours', 'v1\Api\User\StaffController@getStaffWorkingHours');

            Route::group(['prefix' => 'banner'], function () {
                Route::get('list', 'v1\Api\User\OfferBannerController@getBannerList');
                Route::get('detail', 'v1\Api\User\OfferBannerController@getBannerDetail');
            });
            Route::group(['prefix' => 'appoientment'], function () {
                Route::post('add', 'v1\Api\AppoientmentController@create');
                // Route::get('list','v1\Api\AppoientmentController@index');
                Route::get('list', 'v1\Api\AppoientmentController@userAppointmentList');
                Route::get('detail', 'v1\Api\AppoientmentController@show');
                Route::post('cancel', 'v1\Api\AppoientmentController@cancel');
                Route::post('update', 'v1\Api\AppoientmentController@update');
            });

            Route::group(['prefix' => 'notification'], function () {
                Route::get('list', 'v1\MainController@notificationList');
                Route::post('read', 'v1\MainController@notificationRead');
                Route::get('delete', 'v1\MainController@notificationDelete');
            });

            // Submit report and issue or abuse
            Route::post('report', 'v1\ReportsController@store');
            //add review submit
            Route::post('add-review', 'v1\Api\User\UserController@addReviewSubmit');

            //apply job
            Route::post('apply/job', 'v1\Api\User\OfferBannerController@applyJob');
            //   Route::post('add-review','v1\Api\User\UserController@addReviewSubmit');
            Route::get('trending-packages', 'v1\Api\Merchant\OffersPackagesController@trendingPackages');
            Route::post('update-mobile-number', 'v1\MainController@updateNumber');
        });

        // For Merchant

        Route::post('v1/merchant/login', 'v1\Api\Merchant\UserController@login');
        Route::post('v1/merchant/verify', 'v1\Api\Merchant\UserController@verify');
        Route::post('v1/merchant/logout', 'v1\Api\Merchant\UserController@logout')->middleware('auth:merchants');

        Route::post('v1/merchant/re-send-otp', 'v1\MainController@reSendOTP');

        //Route::group(['middleware' => 'auth:merchants'], function(){
        Route::group(['prefix' => 'v2/merchant/', 'middleware' => 'auth:merchants'], function () {
            Route::post('update-mobile-number', 'v2\MainController@updateNumber');
        });

        Route::group(['prefix' => 'v1/merchant/', 'middleware' => 'auth:merchants'], function () {

            Route::post('update-mobile-number', 'v1\MainController@updateNumber');
            Route::group(['prefix' => 'appoientment'], function () {
                Route::post('add', 'v1\Api\AppoientmentController@create');
                Route::get('list', 'v1\Api\AppoientmentController@index');
                Route::get('detail', 'v1\Api\AppoientmentController@show');
                Route::post('cancel', 'v1\Api\AppoientmentController@cancel');
                Route::post('update', 'v1\Api\AppoientmentController@update');
                Route::get('delete', 'v1\Api\AppoientmentController@delete');
                Route::post('complete', 'v1\Api\AppoientmentController@completeAppointment');
                Route::get('app_list', 'v1\Api\AppoientmentController@merchantAppointment');
            });

            Route::group(['prefix' => 'notification'], function () {
                Route::get('list', 'v1\MainController@notificationList');
                Route::post('read', 'v1\MainController@notificationRead');
                Route::get('notif_list', 'v1\MainController@merchantnotificationList');
            });

            //staff working hours

            Route::get('get-staff-working-hours', 'v1\Api\User\StaffController@getStaffWorkingHours');
            Route::get('get-staff-by-serviceId', 'v1\Api\User\StaffController@getStaffListByServiceId');

            // Merchant   UserController details
            Route::post('update_profile_image', 'v1\Api\Merchant\UserController@uploadProfileImage');
            Route::get('get-portfolio', 'v1\Api\Merchant\UserController@getPortfolio');
            Route::post('delete-portfolio', 'v1\Api\Merchant\UserController@deletePortfolio');
            Route::post('update_merchants_detail', 'v1\Api\Merchant\UserController@merchantsDetails');
            Route::post('update_merchants_address', 'v1\Api\Merchant\UserController@merchantsAddress');
            Route::post('update_merchants_service_type', 'v1\Api\Merchant\UserController@merchantServiceType');
            Route::get('merchant_info', 'v1\Api\Merchant\UserController@merchantInfo');

            Route::post('add_user', 'v1\Api\Merchant\UserController@addUserByMerchant');
            Route::get('user_list', 'v1\Api\Merchant\UserController@userList');
            Route::post('/user/edit_details', 'v1\Api\Merchant\UserController@editUserDetails');
            Route::get('/user/profile', 'v1\Api\Merchant\UserController@getClientProfile');

            Route::post('/findUser', 'v1\Api\Merchant\UserController@findUser')->name('find_user');

            // delete user by merchant
            Route::post('user/delete_details', 'v1\Api\Merchant\UserController@deleteUserDetails');

            // delete staff by merchant
            Route::post('staff/delete_details', 'v1\Api\Merchant\StaffController@deleteStaffDetails');

            // edit staff by merchant
            Route::post('staff/edit_details', 'v1\Api\Merchant\StaffController@editStaffDetailsByMerchant');

            // get working hour
            Route::get('get/working/hour', 'v1\Api\Merchant\ServiceController@getWorkingHour');

            //update working hour
            Route::post('update/working/hour', 'v1\Api\Merchant\ServiceController@updateWorkingHour');

            // get merchant amenties
            Route::get('get/amenties', 'v1\Api\Merchant\UserController@getAmenitiesListing');

            // set merchant amenties
            Route::post('set/amenties', 'v1\Api\Merchant\UserController@setMerchantAmenitiesListing');

            // get merchant review
            Route::get('review/list', 'v1\Api\Merchant\UserController@getMerchantReviewList');

            // Merchant   ServiceController details

            Route::get('category_info', 'v1\Api\Merchant\ServiceController@getServiceInfo');
            Route::post('update_merchant_new_services', 'v1\Api\Merchant\ServiceController@addNewService');
            Route::post('update_merchant_services', 'v1\Api\Merchant\ServiceController@merchantServices');
            Route::post('set_service_woking_time', 'v1\Api\Merchant\ServiceController@ServiceWorkingTime');
            Route::get('select_services', 'v1\Api\Merchant\ServiceController@selectMerchantService');
            Route::get('get_staff_services', 'v1\Api\Merchant\ServiceController@getStaffService');

            // Route::post('get_staff_services', 'v1\Api\Merchant\ServiceController@test');  //Pending

            // Merchant ClientController details

            // Route::post('add_client', 'v1\Api\Merchant\ClientController@addClient');

            // Merchant   StaffController details
            Route::post('add_staff', 'v1\Api\Merchant\StaffController@addStaffByMerchant');
            Route::get('select_staff_service', 'v1\Api\Merchant\StaffController@selectMerchantServiceForStaff');
            Route::get('/staff/profile', 'v1\Api\Merchant\StaffController@getStaffProfile');
            Route::get('/staff_listing', 'v1\Api\Merchant\StaffController@getstaffListing');

            Route::group(['prefix' => 'offer'], function () {
                Route::post('create', 'v1\Api\Merchant\OffersPackagesController@createOffer');
                Route::get('list', 'v1\Api\Merchant\OffersPackagesController@getOfferList');
                Route::get('delete', 'v1\Api\Merchant\OffersPackagesController@deleteOffer');
            });

            Route::group(['prefix' => 'package'], function () {
                Route::post('create', 'v1\Api\Merchant\OffersPackagesController@createPackage');
                Route::post('update', 'v1\Api\Merchant\OffersPackagesController@updatePackage');
                Route::get('list', 'v1\Api\Merchant\OffersPackagesController@getPackageList');
                Route::get('delete', 'v1\Api\Merchant\OffersPackagesController@deletePackage');
            });

            //add career & training
            Route::post('/training/vacancy', 'v1\Api\Merchant\TrainingAndVacancy@addTraining_vacancy')->name('training_vacancy');

            //get carrer and training list
            Route::get('list/training/vacancy', 'v1\Api\Merchant\TrainingAndVacancy@getTraining_vacancy')->name('training_vacancy');

            //delete carrer and training list
            Route::post('delete/training/vacancy', 'v1\Api\Merchant\TrainingAndVacancy@deleteTraining_vacancy')->name('training_vacancy');

            Route::get('get-location', 'v1\Api\Merchant\UserController@getLocation');
            Route::post('edit-location', 'v1\Api\Merchant\UserController@editLocation');
            Route::get('get-business-profile', 'v1\Api\Merchant\ServiceController@getBusinessProfile');
            Route::post('edit-business-profile', 'v1\Api\Merchant\ServiceController@editBusinessProfile');

            // merchant bussiness info

            // get merchant services list
            Route::get('get/bussiness/service/list', 'v1\Api\Merchant\ServiceController@getBussinessServiceList');

            // add_edit merchant services
            Route::post('add/edit/bussiness/service', 'v1\Api\Merchant\ServiceController@addEditBussinessService');

            // delete bussiness merchant service
            Route::post('delete/bussiness/service', 'v1\Api\Merchant\ServiceController@deleteBussinessService');
            // Route::post('update-mobile-number','MainController@updateNumber');

            //merchant setting
            Route::get('getSetting', 'v1\Api\Merchant\UserController@getSetting');
            Route::post('saveSetting', 'v1\Api\Merchant\UserController@saveSetting');

            Route::group(['prefix' => 'reports'], function () {
                Route::get('total', 'v1\Api\Merchant\MerchantReports@totalReport');
                Route::get('graph', 'v1\Api\Merchant\MerchantReports@graphReview');

            });
        });

// Version 1 api listing End

// Version 2 api listing Start
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('details', 'v2\Api\User\UserController@details');
    });

    Route::post('v2/verify', 'v2\Api\User\UserController@verify');
    Route::post('v2/login', 'v2\Api\User\UserController@login');
    Route::post('v2/re-send-otp', 'v2\MainController@reSendOTP');

    Route::group(['prefix' => 'v2', 'middleware' => 'auth:api'], function () {
        Route::post('update-mobile-number', 'v2\MainController@updateNumber');
    });

    Route::group(['prefix' => 'v2', 'middleware' => 'auth:api'], function () {
        // Route::post('merchant/update-mobile-number','v2/MainController@updateNumber');

        Route::get('get-setting', 'v2\Api\User\UserController@getSetting');
        Route::post('save-setting', 'v2\Api\User\UserController@saveSetting');
        Route::get('favorite-list-status', 'v2\Api\User\UserController@favoriteListExsist');

        Route::get('get-profile', 'v2\Api\User\UserController@getProfile');
        Route::post('update-profile', 'v2\Api\User\UserController@updateProfile');
        Route::post('upload-image', 'v2\Api\User\UserController@uploadImage');
        Route::post('/userLogout', 'v2\Api\User\UserController@logout');

        // Route::post('/nearby-salon-listing','v2\Api\User\UserController@NearBySalonListing_dev')->name('nearby-salon-listing');
        // Route::post('/search-salon-listing','v2\Api\User\UserController@NearBySalonListing')->name('nearby-salon-listing');

        Route::get('/merchant_categories_listing', 'v2\Api\User\UserController@MerchantCategoriesListing')->name('merchant_categories_listing');
        Route::get('merchant_amenities_listing', 'v2\Api\User\UserController@MerchantAmenitiesListing')->name('merchant_amenities_listing');

        Route::get('vendors', 'v2\Api\User\UserController@NearBySalonListing_dev');
        // Route::get('vendor-detail','v2\Api\User\UserController@getMerchantDetail');
        Route::get('vendor-detail', 'v2\Api\MerchantsController@getMerchantDetail');
        Route::post('favorite', 'v2\Api\User\UserController@addRemoveFav');
        Route::get('fav-merchant-list', 'v2\Api\User\UserController@getFavMerchantList');

        Route::get('get-staff-by-serviceId', 'v2\Api\User\StaffController@getStaffListByServiceId');
        Route::get('get-staff-working-hours', 'v2\Api\User\StaffController@getStaffWorkingHours');

        Route::group(['prefix' => 'banner'], function () {
            Route::get('list', 'v2\Api\User\OfferBannerController@getBannerList');
            Route::get('detail', 'v2\Api\User\OfferBannerController@getBannerDetail');
        });
        Route::group(['prefix' => 'appoientment'], function () {
            Route::post('add', 'v2\Api\AppoientmentController@create');
            // Route::get('list','v2\Api\AppoientmentController@index');
            Route::get('list', 'v2\Api\AppoientmentController@userAppointmentList');
            Route::get('detail', 'v2\Api\AppoientmentController@show');
            Route::post('cancel', 'v2\Api\AppoientmentController@cancel');
            Route::post('update', 'v2\Api\AppoientmentController@update');
        });

        Route::group(['prefix' => 'notification'], function () {
            Route::get('list', 'v2\MainController@notificationList');
            Route::post('read', 'v2\MainController@notificationRead');
            Route::get('delete', 'v2\MainController@notificationDelete');
        });

        // Submit report and issue or abuse
        Route::post('report', 'v2\ReportsController@store');
        //add review submit
        Route::post('add-review', 'v2\Api\User\UserController@addReviewSubmit');

        //apply job
        Route::post('apply/job', 'v2\Api\User\OfferBannerController@applyJob');
        //   Route::post('add-review','v2\Api\User\UserController@addReviewSubmit');
        Route::get('trending-packages', 'v2\Api\Merchant\OffersPackagesController@trendingPackages');
        Route::post('update-mobile-number', 'v2\MainController@updateNumber');
    });

    // For Merchant

    Route::post('v2/merchant/login', 'v2\Api\Merchant\UserController@login');
    Route::post('v2/merchant/verify', 'v2\Api\Merchant\UserController@verify');
    Route::post('v2/merchant/logout', 'v2\Api\Merchant\UserController@logout')->middleware('auth:merchants');

    Route::post('v2/merchant/re-send-otp', 'v2\MainController@reSendOTP');

    //Route::group(['middleware' => 'auth:merchants'], function(){
    Route::group(['prefix' => 'v2/merchant/', 'middleware' => 'auth:merchants'], function () {
        Route::post('update-mobile-number', 'v2\MainController@updateNumber');
    });

    Route::group(['prefix' => 'v2/merchant/', 'middleware' => 'auth:merchants'], function () {
        Route::post('report', 'v2\ReportsController@store');
        Route::post('update-mobile-number', 'v2\MainController@updateNumber');
        Route::group(['prefix' => 'appoientment'], function () {
            Route::post('add', 'v2\Api\AppoientmentController@create');
            Route::get('list', 'v2\Api\AppoientmentController@index');
            Route::get('detail', 'v2\Api\AppoientmentController@show');
            Route::post('cancel', 'v2\Api\AppoientmentController@cancel');
            Route::post('update', 'v2\Api\AppoientmentController@update');
            Route::get('delete', 'v2\Api\AppoientmentController@delete');
            Route::post('complete', 'v2\Api\AppoientmentController@completeAppointment');
            Route::get('app_list', 'v2\Api\AppoientmentController@merchantAppointment');
        });

        Route::group(['prefix' => 'notification'], function () {
            Route::get('list', 'v2\MainController@notificationList');
            Route::post('read', 'v2\MainController@notificationRead');
            Route::get('notif_list', 'v2\MainController@merchantnotificationList');
            Route::get('delete', 'v2\MainController@notificationDelete');
        });

        //staff working hours
        Route::get('get-staff-working-hours', 'v2\Api\User\StaffController@getStaffWorkingHours');
        Route::get('get-staff-by-serviceId', 'v2\Api\User\StaffController@getStaffListByServiceId');

        // Merchant   UserController details
        Route::post('update_profile_image', 'v2\Api\Merchant\UserController@uploadProfileImage');
        Route::get('get-portfolio', 'v2\Api\Merchant\UserController@getPortfolio');
        Route::post('delete-portfolio', 'v2\Api\Merchant\UserController@deletePortfolio');
        Route::post('update_merchants_detail', 'v2\Api\Merchant\UserController@merchantsDetails');
        Route::post('update_merchants_address', 'v2\Api\Merchant\UserController@merchantsAddress');
        Route::post('update_merchants_service_type', 'v2\Api\Merchant\UserController@merchantServiceType');
        Route::get('merchant_info', 'v2\Api\Merchant\UserController@merchantInfo');

        Route::post('add_user', 'v2\Api\Merchant\UserController@addUserByMerchant');
        Route::get('user_list', 'v2\Api\Merchant\UserController@userList');
        Route::post('/user/edit_details', 'v2\Api\Merchant\UserController@editUserDetails');
        Route::get('/user/profile', 'v2\Api\Merchant\UserController@getClientProfile');

        Route::post('/findUser', 'v2\Api\Merchant\UserController@findUser')->name('find_user');

        // delete user by merchant
        Route::post('user/delete_details', 'v2\Api\Merchant\UserController@deleteUserDetails');

        // delete staff by merchant
        Route::post('staff/delete_details', 'v2\Api\Merchant\StaffController@deleteStaffDetails');

        // edit staff by merchant
        Route::post('staff/edit_details', 'v2\Api\Merchant\StaffController@editStaffDetailsByMerchant');

        // get working hour
        Route::get('get/working/hour', 'v2\Api\Merchant\ServiceController@getWorkingHour');

        //update working hour
        Route::post('update/working/hour', 'v2\Api\Merchant\ServiceController@updateWorkingHour');

        // get merchant amenties
        Route::get('get/amenties', 'v2\Api\Merchant\UserController@getAmenitiesListing');

        // set merchant amenties
        Route::post('set/amenties', 'v2\Api\Merchant\UserController@setMerchantAmenitiesListing');

        // get merchant review
        Route::get('review/list', 'v2\Api\Merchant\UserController@getMerchantReviewList');

        // Merchant   ServiceController details

        Route::get('category_info', 'v2\Api\Merchant\ServiceController@getServiceInfo');
        Route::post('update_merchant_new_services', 'v2\Api\Merchant\ServiceController@addNewService');
        Route::post('update_merchant_services', 'v2\Api\Merchant\ServiceController@merchantServices');
        Route::post('set_service_woking_time', 'v2\Api\Merchant\ServiceController@ServiceWorkingTime');
        Route::get('select_services', 'v2\Api\Merchant\ServiceController@selectMerchantService');
        Route::get('get_staff_services', 'v2\Api\Merchant\ServiceController@getStaffService');

        // Route::post('get_staff_services', 'v2\Api\Merchant\ServiceController@test');  //Pending

        // Merchant   ClientController details

        // Route::post('add_client', 'v2\Api\Merchant\ClientController@addClient');

        // Merchant   StaffController details
        Route::post('add_staff', 'v2\Api\Merchant\StaffController@addStaffByMerchant');
        Route::get('select_staff_service', 'v2\Api\Merchant\StaffController@selectMerchantServiceForStaff');
        Route::get('/staff/profile', 'v2\Api\Merchant\StaffController@getStaffProfile');
        Route::get('/staff_listing', 'v2\Api\Merchant\StaffController@getstaffListing');

        Route::group(['prefix' => 'offer'], function () {
            Route::post('create', 'v2\Api\Merchant\OffersPackagesController@createOffer');
            Route::get('list', 'v2\Api\Merchant\OffersPackagesController@getOfferList');
            Route::get('delete', 'v2\Api\Merchant\OffersPackagesController@deleteOffer');
        });

        Route::group(['prefix' => 'package'], function () {
            Route::post('create', 'v2\Api\Merchant\OffersPackagesController@createPackage');
            Route::post('update', 'v2\Api\Merchant\OffersPackagesController@updatePackage');
            Route::get('list', 'v2\Api\Merchant\OffersPackagesController@getPackageList');
            Route::get('delete', 'v2\Api\Merchant\OffersPackagesController@deletePackage');
        });

        //add career & training
        Route::post('/training/vacancy', 'v2\Api\Merchant\TrainingAndVacancy@addTraining_vacancy')->name('training_vacancy');

        //get carrer and training list
        Route::get('list/training/vacancy', 'v2\Api\Merchant\TrainingAndVacancy@getTraining_vacancy')->name('training_vacancy');

        //delete carrer and training list
        Route::post('delete/training/vacancy', 'v2\Api\Merchant\TrainingAndVacancy@deleteTraining_vacancy')->name('training_vacancy');

        Route::get('get-location', 'v2\Api\Merchant\UserController@getLocation');
        Route::post('edit-location', 'v2\Api\Merchant\UserController@editLocation');
        Route::get('get-business-profile', 'v2\Api\Merchant\ServiceController@getBusinessProfile');
        Route::post('edit-business-profile', 'v2\Api\Merchant\ServiceController@editBusinessProfile');

        // merchant bussiness info

        // get merchant services list
        Route::get('get/bussiness/service/list', 'v2\Api\Merchant\ServiceController@getBussinessServiceList');

        // add_edit merchant services
        Route::post('add/edit/bussiness/service', 'v2\Api\Merchant\ServiceController@addEditBussinessService');

        // delete bussiness merchant service
        Route::post('delete/bussiness/service', 'v2\Api\Merchant\ServiceController@deleteBussinessService');
        // Route::post('update-mobile-number','MainController@updateNumber');

        //merchant setting
        Route::get('getSetting', 'v2\Api\Merchant\UserController@getSetting');
        Route::post('saveSetting', 'v2\Api\Merchant\UserController@saveSetting');

        Route::group(['prefix' => 'reports'], function () {
            Route::get('total', 'v2\Api\Merchant\MerchantReports@totalReport');
            Route::get('graph', 'v2\Api\Merchant\MerchantReports@graphReview');

        });
    });
// Version 2 api listing End

// Version 3 api listing Start

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('details', 'v3\Api\User\UserController@details');
    });

    Route::post('v3/verify', 'v3\Api\User\UserController@verify');
    Route::post('v3/login', 'v3\Api\User\UserController@login');
    Route::post('v3/guest-login/{id?}', 'v3\Api\User\UserController@guestLogin');
    Route::post('v3/re-send-otp', 'v3\MainController@reSendOTP');

    Route::group(['prefix' => 'v3', 'middleware' => 'auth:api'], function () {
        Route::post('update-mobile-number', 'v3\MainController@updateNumber');
    });

    Route::group(['prefix' => 'v3', 'middleware' => 'auth:api'], function () {
        // Route::post('merchant/update-mobile-number','v3/MainController@updateNumber');
        Route::get('get-setting', 'v3\Api\User\UserController@getSetting');
        Route::post('save-setting', 'v3\Api\User\UserController@saveSetting');
        Route::get('favorite-list-status', 'v3\Api\User\UserController@favoriteListExsist');

        Route::get('get-profile', 'v3\Api\User\UserController@getProfile');
        Route::post('update-profile', 'v3\Api\User\UserController@updateProfile');
        Route::post('upload-image', 'v3\Api\User\UserController@uploadImage');
        Route::post('/userLogout', 'v3\Api\User\UserController@logout');

        // Route::post('/nearby-salon-listing','v3\Api\User\UserController@NearBySalonListing_dev')->name('nearby-salon-listing');
        // Route::post('/search-salon-listing','v3\Api\User\UserController@NearBySalonListing')->name('nearby-salon-listing');

        Route::get('/merchant_categories_listing', 'v3\Api\User\UserController@MerchantCategoriesListing')->name('merchant_categories_listing');
        Route::get('merchant_amenities_listing', 'v3\Api\User\UserController@MerchantAmenitiesListing')->name('merchant_amenities_listing');

        Route::get('vendors', 'v3\Api\User\UserController@NearBySalonListing_dev');
        // Route::get('vendor-detail','v3\Api\User\UserController@getMerchantDetail');
        Route::get('vendor-detail', 'v3\Api\MerchantsController@getMerchantDetail');
        Route::post('favorite', 'v3\Api\User\UserController@addRemoveFav');
        Route::get('fav-merchant-list', 'v3\Api\User\UserController@getFavMerchantList');

        Route::get('get-staff-by-serviceId', 'v3\Api\User\StaffController@getStaffListByServiceId');
        Route::get('get-staff-working-hours', 'v3\Api\User\StaffController@getStaffWorkingHours');

        Route::group(['prefix' => 'banner'], function () {
            Route::get('list', 'v3\Api\User\OfferBannerController@getBannerList');
            Route::get('detail', 'v3\Api\User\OfferBannerController@getBannerDetail');
        });
        Route::group(['prefix' => 'appoientment'], function () {
            Route::post('add', 'v4\Api\AppoientmentController@create');
            // Route::get('list','v3\Api\AppoientmentController@index');
            Route::get('list', 'v4\Api\AppoientmentController@userAppointmentList');
            Route::get('detail', 'v4\Api\AppoientmentController@show');
            Route::post('cancel', 'v4\Api\AppoientmentController@cancel');
            Route::post('update', 'v4\Api\AppoientmentController@update');
        });

        Route::group(['prefix' => 'notification'], function () {
            Route::get('list', 'v3\MainController@notificationList');
            Route::post('read', 'v3\MainController@notificationRead');
            Route::post('read-all', 'v3\MainController@notificationReadAll');
            Route::get('delete', 'v3\MainController@notificationDelete');
        });

        // Submit report and issue or abuse
        Route::post('report', 'v3\ReportsController@store');
        //add review submit
        Route::post('add-review', 'v3\Api\User\UserController@addReviewSubmit');

        //apply job
        Route::post('apply/job', 'v3\Api\User\OfferBannerController@applyJob');
        //   Route::post('add-review','v3\Api\User\UserController@addReviewSubmit');
        Route::get('trending-packages', 'v3\Api\Merchant\OffersPackagesController@trendingPackages');
        Route::post('update-mobile-number', 'v3\MainController@updateNumber');
    });

    // For Merchant

    Route::post('v3/merchant/login', 'v3\Api\Merchant\UserController@login');
    Route::post('v3/merchant/verify', 'v3\Api\Merchant\UserController@verify');
    Route::post('v3/merchant/logout', 'v3\Api\Merchant\UserController@logout')->middleware('auth:merchants');

    Route::post('v3/merchant/re-send-otp', 'v3\MainController@reSendOTP');

    //Route::group(['middleware' => 'auth:merchants'], function(){
    Route::group(['prefix' => 'v3/merchant/', 'middleware' => 'auth:merchants'], function () {
        Route::post('update-mobile-number', 'v3\MainController@updateNumber');
    });

    Route::group(['prefix' => 'v3/merchant/', 'middleware' => 'auth:merchants'], function () {
        Route::post('report', 'v3\ReportsController@store');
        Route::post('update-mobile-number', 'v3\MainController@updateNumber');
        Route::group(['prefix' => 'appoientment'], function () {
            Route::post('add', 'v4\Api\AppoientmentController@create');
            Route::get('list', 'v4\Api\AppoientmentController@index');
            Route::get('detail', 'v4\Api\AppoientmentController@show');
            Route::post('cancel', 'v4\Api\AppoientmentController@cancel');
            Route::post('update', 'v4\Api\AppoientmentController@update');
            Route::get('delete', 'v4\Api\AppoientmentController@delete');
            Route::post('complete', 'v4\Api\AppoientmentController@completeAppointment');
            Route::get('app_list', 'v4\Api\AppoientmentController@merchantAppointment');    
            Route::get('calender_list', 'v4\Api\AppoientmentController@merchantAppCalender');          
        });

        Route::group(['prefix' => 'notification'], function () {
            Route::get('list', 'v3\MainController@notificationList');
            Route::post('read', 'v3\MainController@notificationRead');
            Route::get('notif_list', 'v3\MainController@merchantnotificationList');
            Route::get('delete', 'v3\MainController@notificationDelete');
            Route::post('read-all', 'v3\MainController@notificationReadAll');
        });

        //staff working hours
        Route::get('get-staff-working-hours', 'v3\Api\User\StaffController@getStaffWorkingHours');
        Route::get('get-staff-by-serviceId', 'v3\Api\User\StaffController@getStaffListByServiceId');

        // Merchant   UserController details
        Route::post('update_profile_image', 'v3\Api\Merchant\UserController@uploadProfileImage');
        Route::get('get-portfolio', 'v3\Api\Merchant\UserController@getPortfolio');
        Route::post('delete-portfolio', 'v3\Api\Merchant\UserController@deletePortfolio');
        Route::post('update_merchants_detail', 'v3\Api\Merchant\UserController@merchantsDetails');
        Route::post('update_merchants_address', 'v3\Api\Merchant\UserController@merchantsAddress');
        Route::post('update_merchants_service_type', 'v3\Api\Merchant\UserController@merchantServiceType');
        Route::get('merchant_info', 'v3\Api\Merchant\UserController@merchantInfo');

        Route::post('add_user', 'v3\Api\Merchant\UserController@addUserByMerchant');
        Route::get('user_list', 'v3\Api\Merchant\UserController@userList');
        Route::post('/user/edit_details', 'v3\Api\Merchant\UserController@editUserDetails');
        Route::get('/user/profile', 'v3\Api\Merchant\UserController@getClientProfile');

        Route::post('/findUser', 'v3\Api\Merchant\UserController@findUser')->name('find_user');

        // delete user by merchant
        Route::post('user/delete_details', 'v3\Api\Merchant\UserController@deleteUserDetails');

        // delete staff by merchant
        Route::post('staff/delete_details', 'v3\Api\Merchant\StaffController@deleteStaffDetails');

        // edit staff by merchant
        Route::post('staff/edit_details', 'v3\Api\Merchant\StaffController@editStaffDetailsByMerchant');

        // get working hour
        Route::get('get/working/hour', 'v3\Api\Merchant\ServiceController@getWorkingHour');

        //update working hour
        Route::post('update/working/hour', 'v3\Api\Merchant\ServiceController@updateWorkingHour');

        // get merchant amenties
        Route::get('get/amenties', 'v3\Api\Merchant\UserController@getAmenitiesListing');

        // set merchant amenties
        Route::post('set/amenties', 'v3\Api\Merchant\UserController@setMerchantAmenitiesListing');

        // get merchant review
        Route::get('review/list', 'v3\Api\Merchant\UserController@getMerchantReviewList');

        // Merchant   ServiceController details

        Route::get('category_info', 'v3\Api\Merchant\ServiceController@getServiceInfo');
        Route::post('update_merchant_new_services', 'v3\Api\Merchant\ServiceController@addNewService');
        Route::post('update_merchant_services', 'v3\Api\Merchant\ServiceController@merchantServices');
        Route::post('set_service_woking_time', 'v3\Api\Merchant\ServiceController@ServiceWorkingTime');
        Route::get('select_services', 'v3\Api\Merchant\ServiceController@selectMerchantService');
        Route::get('get_staff_services', 'v3\Api\Merchant\ServiceController@getStaffService');

        // Route::post('get_staff_services', 'v3\Api\Merchant\ServiceController@test');  //Pending

        // Merchant   ClientController details

        // Route::post('add_client', 'v3\Api\Merchant\ClientController@addClient');

        // Merchant   StaffController details
        Route::post('add_staff', 'v3\Api\Merchant\StaffController@addStaffByMerchant');
        Route::get('select_staff_service', 'v3\Api\Merchant\StaffController@selectMerchantServiceForStaff');
        Route::get('/staff/profile', 'v3\Api\Merchant\StaffController@getStaffProfile');
        Route::get('/staff_listing', 'v3\Api\Merchant\StaffController@getstaffListing');

        Route::group(['prefix' => 'offer'], function () {
            Route::post('create', 'v3\Api\Merchant\OffersPackagesController@createOffer');
            Route::get('list', 'v3\Api\Merchant\OffersPackagesController@getOfferList');
            Route::get('delete', 'v3\Api\Merchant\OffersPackagesController@deleteOffer');
        });

        Route::group(['prefix' => 'package'], function () {
            Route::post('create', 'v3\Api\Merchant\OffersPackagesController@createPackage');
            Route::post('update', 'v3\Api\Merchant\OffersPackagesController@updatePackage');
            Route::get('list',    'v3\Api\Merchant\OffersPackagesController@getPackageList');
            Route::get('delete',  'v3\Api\Merchant\OffersPackagesController@deletePackage');
        });

        //add career & training
        Route::post('/training/vacancy', 'v3\Api\Merchant\TrainingAndVacancy@addTraining_vacancy')->name('training_vacancy');

        //get carrer and training list
        Route::get('list/training/vacancy', 'v3\Api\Merchant\TrainingAndVacancy@getTraining_vacancy')->name('training_vacancy');

        //delete carrer and training list
        Route::post('delete/training/vacancy', 'v3\Api\Merchant\TrainingAndVacancy@deleteTraining_vacancy')->name('training_vacancy');

        Route::get('get-location', 'v3\Api\Merchant\UserController@getLocation');
        Route::post('edit-location', 'v3\Api\Merchant\UserController@editLocation');
        Route::get('get-business-profile', 'v3\Api\Merchant\ServiceController@getBusinessProfile');
        Route::post('edit-business-profile', 'v3\Api\Merchant\ServiceController@editBusinessProfile');

        // merchant bussiness info

        // get merchant services list
        Route::get('get/bussiness/service/list', 'v3\Api\Merchant\ServiceController@getBussinessServiceList');

        // add_edit merchant services
        Route::post('add/edit/bussiness/service', 'v3\Api\Merchant\ServiceController@addEditBussinessService');

        // delete bussiness merchant service
        Route::post('delete/bussiness/service', 'v3\Api\Merchant\ServiceController@deleteBussinessService');
        // Route::post('update-mobile-number','MainController@updateNumber');

        //merchant setting
        Route::get('getSetting', 'v3\Api\Merchant\UserController@getSetting');
        Route::post('saveSetting', 'v3\Api\Merchant\UserController@saveSetting');

        Route::group(['prefix' => 'reports'], function () {
            Route::get('total', 'v3\Api\Merchant\MerchantReports@totalReport');
            Route::get('graph', 'v3\Api\Merchant\MerchantReports@graphReview');

        });
    });

    Route::get('v3/update_app_detail', 'v3\Api\AppoientmentController@userAppointmentDetailUpdate');
// Version 3 api listing End

//version 4 api start
    //user route
    
    Route::group(['prefix' => 'v4'], function () {
        Route::post('register', 'v4\Api\User\UserController@register');
        Route::post('login', 'v4\Api\User\UserController@login');
        Route::post('verify', 'v4\Api\User\UserController@verify');
        Route::post('verify_pin', 'v4\Api\User\UserController@verifyPin');
        Route::post('set_pin', 'v4\Api\User\UserController@setPin');
        Route::post('forget_pin', 'v4\Api\User\UserController@forgetPin');
        Route::get('get-staff-working-hours', 'v4\Api\User\StaffController@getStaffWorkingHours');
        Route::group(['middleware' => 'auth:api'], function () {
            Route::post('auth_set_pin', 'v4\Api\User\UserController@authSetPin');
            Route::post('update_pin', 'v4\Api\User\UserController@updatePin');
            Route::get('get-setting', 'v4\Api\User\UserController@getSetting');
            Route::get('get-staff-by-serviceId', 'v4\Api\User\StaffController@getStaffListByServiceId');
            Route::group(['prefix'=>'trader'],function(){
                Route::get('list','v4\Api\TraderController@index');
            });
        });   
    });

    // Merchant route
    Route::group(['prefix' => 'v4/merchant'], function () {
        Route::post('login', 'v4\Api\Merchant\UserController@login');
        Route::post('verify', 'v4\Api\Merchant\UserController@verify');
        Route::post('verify_pin', 'v4\Api\Merchant\UserController@verifyPin');
        Route::post('set_pin', 'v4\Api\Merchant\UserController@setPin');
        Route::post('forget_pin', 'v4\Api\Merchant\UserController@forgetPin');
        Route::get('get-staff-working-hours', 'v3\Api\User\StaffController@getStaffWorkingHours');
        Route::group(['middleware' => 'auth:merchants'], function () {
            Route::get('get-staff-by-serviceId', 'v4\Api\User\StaffController@getStaffListByServiceId');
            Route::post('auth_set_pin', 'v4\Api\Merchant\UserController@authSetPin');
            Route::post('update_pin', 'v4\Api\Merchant\UserController@updatePin');            
            Route::get('user_list', 'v4\Api\Merchant\UserController@userList');
            Route::get('working_hour', 'v4\Api\Merchant\UserController@getWorkingHours');
            Route::post('update_working_hour', 'v4\Api\Merchant\UserController@updateWorkingHours');
            Route::get('getSetting', 'v4\Api\Merchant\UserController@getSetting');
            Route::post('check_mobile', 'v4\Api\Merchant\UserController@checkMobileExist');
            Route::get('get-staff-working-hours', 'v4\Api\User\StaffController@getStaffWorkingHours');
            Route::post('staff/edit_details', 'v4\Api\Merchant\StaffController@editStaffDetailsByMerchant');
            Route::get('/staff_listing', 'v4\Api\Merchant\StaffController@getstaffListing');
            Route::group(['prefix'=>'services'],function(){
                Route::post('update','v4\Api\Merchant\ServiceController@updateServices');
            });

            Route::group(['prefix' => 'offer'], function () {
                Route::post('update', 'v4\Api\Merchant\OffersPackagesController@updateOffer');                
            });
            Route::group(['prefix'=>'working_hour_secondary'],function(){               
                Route::post('set','v4\Api\Merchant\ServiceController@updateWorkingHourSecondary');                
                Route::get('week_wise','v4\Api\Merchant\ServiceController@getWorkingHourWeekwise');
            });

        });        
    });    

//version 4 api end


// VERSION 5 API START HERE       

    Route::group(['prefix' => 'v5'], function () {

        Route::post('guest-login/{id?}', 'v5\Api\User\UserController@guestLogin');
        Route::post('register', 'v5\Api\User\UserController@register');
        Route::post('login', 'v5\Api\User\UserController@login');
        Route::post('re-send-otp', 'v5\MainController@reSendOTP');
        Route::post('verify', 'v5\Api\User\UserController@verify');
        Route::post('verify_pin', 'v5\Api\User\UserController@verifyPin');
        Route::post('set_pin', 'v5\Api\User\UserController@setPin');
        Route::post('forget_pin', 'v5\Api\User\UserController@forgetPin');
        Route::group(['middleware' => 'auth:api'], function () {
            Route::post('auth_set_pin', 'v5\Api\User\UserController@authSetPin');
            Route::post('update_pin', 'v5\Api\User\UserController@updatePin');
            Route::get('get-setting', 'v5\Api\User\UserController@getSetting');
            Route::get('get-staff-by-serviceId', 'v5\Api\User\StaffController@getStaffListByServiceId');
            Route::group(['prefix'=>'trader'],function(){
                Route::get('list','v5\Api\TraderController@index');
            });
            Route::get('get-staff-working-hours', 'v5\Api\User\StaffController@getStaffWorkingHours');

            Route::post('save-setting', 'v5\Api\User\UserController@saveSetting');
            Route::get('favorite-list-status', 'v5\Api\User\UserController@favoriteListExsist');

            Route::get('get-profile', 'v5\Api\User\UserController@getProfile');
            Route::post('update-profile', 'v5\Api\User\UserController@updateProfile');
            Route::post('upload-image', 'v5\Api\User\UserController@uploadImage');
            Route::post('/userLogout', 'v5\Api\User\UserController@logout');
            Route::post('details', 'v5\Api\User\UserController@details');
            Route::post('update-mobile-number', 'v5\MainController@updateNumber');        

            Route::get('/merchant_categories_listing', 'v5\Api\User\UserController@MerchantCategoriesListing')->name('merchant_categories_listing');
            Route::get('merchant_amenities_listing', 'v5\Api\User\UserController@MerchantAmenitiesListing')->name('merchant_amenities_listing');

            Route::get('vendors', 'v5\Api\User\UserController@NearBySalonListing_dev');
            
            Route::get('vendor-detail', 'v5\Api\MerchantsController@getMerchantDetail');
            Route::post('favorite', 'v5\Api\User\UserController@addRemoveFav');
            Route::get('fav-merchant-list', 'v5\Api\User\UserController@getFavMerchantList');

            Route::group(['prefix' => 'banner'], function () {
                Route::get('list', 'v5\Api\User\OfferBannerController@getBannerList');
                Route::get('detail', 'v5\Api\User\OfferBannerController@getBannerDetail');
            });
            Route::group(['prefix' => 'appoientment'], function () {
                Route::post('add', 'v5\Api\AppoientmentController@create');            
                Route::get('list', 'v5\Api\AppoientmentController@userAppointmentList');
                Route::get('detail', 'v5\Api\AppoientmentController@show');
                Route::post('cancel', 'v5\Api\AppoientmentController@cancel');
                Route::post('update', 'v5\Api\AppoientmentController@update');
            });

            Route::group(['prefix' => 'notification'], function () {
                Route::get('list', 'v5\MainController@notificationList');
                Route::post('read', 'v5\MainController@notificationRead');
                Route::post('read-all', 'v5\MainController@notificationReadAll');
                Route::get('delete', 'v5\MainController@notificationDelete');
            });

            // Submit report and issue or abuse
            Route::post('report', 'v5\ReportsController@store');
            //add review submit
            Route::post('add-review', 'v5\Api\User\UserController@addReviewSubmit');

            //apply job
            Route::post('apply/job', 'v5\Api\User\OfferBannerController@applyJob');
            //   Route::post('add-review','v5\Api\User\UserController@addReviewSubmit');
            Route::get('trending-packages', 'v5\Api\Merchant\OffersPackagesController@trendingPackages');
            Route::post('update-mobile-number', 'v5\MainController@updateNumber');
        });
        // Route::get('get-setting', 'v3\Api\User\UserController@getSetting');
       
        // Route::get('get-staff-by-serviceId', 'v3\Api\User\StaffController@getStaffListByServiceId');
        // Route::get('get-staff-working-hours', 'v3\Api\User\StaffController@getStaffWorkingHours');        
    });

    // For Merchant

    // Route::post('v3/merchant/login', 'v3\Api\Merchant\UserController@login');
    // Route::post('v3/merchant/verify', 'v3\Api\Merchant\UserController@verify');
    // Route::post('v3/merchant/logout', 'v3\Api\Merchant\UserController@logout')->middleware('auth:merchants');

    Route::post('v3/merchant/re-send-otp', 'v3\MainController@reSendOTP');

    //Route::group(['middleware' => 'auth:merchants'], function(){
    Route::group(['prefix' => 'v3/merchant/', 'middleware' => 'auth:merchants'], function () {
        Route::post('update-mobile-number', 'v3\MainController@updateNumber');
    });


    Route::group(['prefix' => 'v5/merchant'], function () {
        Route::post('login', 'v5\Api\Merchant\UserController@login');
        Route::post('re-send-otp', 'v5\MainController@reSendOTP');
        Route::post('verify', 'v5\Api\Merchant\UserController@verify');
        Route::post('verify_pin', 'v5\Api\Merchant\UserController@verifyPin');
        Route::post('set_pin', 'v5\Api\Merchant\UserController@setPin');
        Route::post('forget_pin', 'v5\Api\Merchant\UserController@forgetPin');
        // Route::get('get-staff-working-hours', 'v3\Api\User\StaffController@getStaffWorkingHours');
        Route::group(['middleware' => 'auth:merchants'], function () {
            Route::get('get-staff-by-serviceId', 'v5\Api\User\StaffController@getStaffListByServiceId');
            Route::post('auth_set_pin', 'v5\Api\Merchant\UserController@authSetPin');
            Route::post('update_pin', 'v5\Api\Merchant\UserController@updatePin');            
            Route::get('user_list', 'v5\Api\Merchant\UserController@userList');
            Route::get('working_hour', 'v5\Api\Merchant\UserController@getWorkingHours');
            Route::post('update_working_hour', 'v5\Api\Merchant\UserController@updateWorkingHours');
            Route::get('getSetting', 'v5\Api\Merchant\UserController@getSetting');
            Route::post('check_mobile', 'v5\Api\Merchant\UserController@checkMobileExist');
            Route::get('get-staff-working-hours', 'v5\Api\User\StaffController@getStaffWorkingHours');
            Route::post('staff/edit_details', 'v5\Api\Merchant\StaffController@editStaffDetailsByMerchant');
            Route::get('/staff_listing', 'v5\Api\Merchant\StaffController@getstaffListing');
            Route::group(['prefix'=>'services'],function(){
                Route::post('update','v5\Api\Merchant\ServiceController@updateServices');
            });

            Route::group(['prefix' => 'offer'], function () {
                Route::post('update', 'v5\Api\Merchant\OffersPackagesController@updateOffer');                
            });
            Route::group(['prefix'=>'working_hour_secondary'],function(){               
                Route::post('set','v5\Api\Merchant\ServiceController@updateWorkingHourSecondary');                
                Route::get('week_wise','v5\Api\Merchant\ServiceController@getWorkingHourWeekwise');
            });

            Route::post('report', 'v5\ReportsController@store');
            Route::post('update-mobile-number', 'v5\MainController@updateNumber');
            Route::group(['prefix' => 'appoientment'], function () {
                Route::post('add', 'v5\Api\AppoientmentController@create');
                Route::get('list', 'v5\Api\AppoientmentController@index');
                Route::get('detail', 'v5\Api\AppoientmentController@show');
                Route::post('cancel', 'v5\Api\AppoientmentController@cancel');
                Route::post('update', 'v5\Api\AppoientmentController@update');
                Route::get('delete', 'v5\Api\AppoientmentController@delete');
                Route::post('complete', 'v5\Api\AppoientmentController@completeAppointment');
                Route::get('app_list', 'v5\Api\AppoientmentController@merchantAppointment');    
                Route::get('calender_list', 'v5\Api\AppoientmentController@merchantAppCalender');          
            });

            Route::group(['prefix' => 'notification'], function () {
                Route::get('list', 'v5\MainController@notificationList');
                Route::post('read', 'v5\MainController@notificationRead');
                Route::get('notif_list', 'v5\MainController@merchantnotificationList');
                Route::get('delete', 'v5\MainController@notificationDelete');
                Route::post('read-all', 'v5\MainController@notificationReadAll');
            });

            //staff working hours
            Route::get('get-staff-working-hours', 'v5\Api\User\StaffController@getStaffWorkingHours');
            Route::get('get-staff-by-serviceId', 'v5\Api\User\StaffController@getStaffListByServiceId');

            // Merchant   UserController details
            Route::post('update_profile_image', 'v5\Api\Merchant\UserController@uploadProfileImage');
            Route::get('get-portfolio', 'v5\Api\Merchant\UserController@getPortfolio');
            Route::post('delete-portfolio', 'v5\Api\Merchant\UserController@deletePortfolio');
            Route::post('update_merchants_detail', 'v5\Api\Merchant\UserController@merchantsDetails');
            Route::post('update_merchants_address', 'v5\Api\Merchant\UserController@merchantsAddress');
            Route::post('update_merchants_service_type', 'v5\Api\Merchant\UserController@merchantServiceType');
            Route::get('merchant_info', 'v5\Api\Merchant\UserController@merchantInfo');

            Route::post('add_user', 'v5\Api\Merchant\UserController@addUserByMerchant');
            Route::get('user_list', 'v5\Api\Merchant\UserController@userList');
            Route::post('/user/edit_details', 'v5\Api\Merchant\UserController@editUserDetails');
            Route::get('/user/profile', 'v5\Api\Merchant\UserController@getClientProfile');

            Route::post('/findUser', 'v5\Api\Merchant\UserController@findUser')->name('find_user');

            // delete user by merchant
            Route::post('user/delete_details', 'v5\Api\Merchant\UserController@deleteUserDetails');

            // delete staff by merchant
            Route::post('staff/delete_details', 'v5\Api\Merchant\StaffController@deleteStaffDetails');

            // edit staff by merchant
            Route::post('staff/edit_details', 'v5\Api\Merchant\StaffController@editStaffDetailsByMerchant');

            // get working hour
            Route::get('get/working/hour', 'v5\Api\Merchant\ServiceController@getWorkingHour');

            //update working hour
            Route::post('update/working/hour', 'v5\Api\Merchant\ServiceController@updateWorkingHour');

            // get merchant amenties
            Route::get('get/amenties', 'v5\Api\Merchant\UserController@getAmenitiesListing');

            // set merchant amenties
            Route::post('set/amenties', 'v5\Api\Merchant\UserController@setMerchantAmenitiesListing');

            // get merchant review
            Route::get('review/list', 'v5\Api\Merchant\UserController@getMerchantReviewList');

            // Merchant   ServiceController details

            Route::get('category_info', 'v5\Api\Merchant\ServiceController@getServiceInfo');
            Route::post('update_merchant_new_services', 'v5\Api\Merchant\ServiceController@addNewService');
            Route::post('update_merchant_services', 'v5\Api\Merchant\ServiceController@merchantServices');
            Route::post('set_service_woking_time', 'v5\Api\Merchant\ServiceController@ServiceWorkingTime');
            Route::get('select_services', 'v5\Api\Merchant\ServiceController@selectMerchantService');
            Route::get('get_staff_services', 'v5\Api\Merchant\ServiceController@getStaffService');
            

            // Merchant   StaffController details
            Route::post('add_staff', 'v5\Api\Merchant\StaffController@addStaffByMerchant');
            Route::get('select_staff_service', 'v5\Api\Merchant\StaffController@selectMerchantServiceForStaff');
            Route::get('/staff/profile', 'v5\Api\Merchant\StaffController@getStaffProfile');
            Route::get('/staff_listing', 'v5\Api\Merchant\StaffController@getstaffListing');

            Route::group(['prefix' => 'offer'], function () {
                Route::post('create', 'v5\Api\Merchant\OffersPackagesController@createOffer');
                Route::get('list', 'v5\Api\Merchant\OffersPackagesController@getOfferList');
                Route::get('delete', 'v5\Api\Merchant\OffersPackagesController@deleteOffer');
            });

            Route::group(['prefix' => 'package'], function () {
                Route::post('create', 'v5\Api\Merchant\OffersPackagesController@createPackage');
                Route::post('update', 'v5\Api\Merchant\OffersPackagesController@updatePackage');
                Route::get('list',    'v5\Api\Merchant\OffersPackagesController@getPackageList');
                Route::get('delete',  'v5\Api\Merchant\OffersPackagesController@deletePackage');
            });

            //add career & training
            Route::post('/training/vacancy', 'v5\Api\Merchant\TrainingAndVacancy@addTraining_vacancy')->name('training_vacancy');

            //get carrer and training list
            Route::get('list/training/vacancy', 'v5\Api\Merchant\TrainingAndVacancy@getTraining_vacancy')->name('training_vacancy');

            //delete carrer and training list
            Route::post('delete/training/vacancy', 'v5\Api\Merchant\TrainingAndVacancy@deleteTraining_vacancy')->name('training_vacancy');

            Route::get('get-location', 'v5\Api\Merchant\UserController@getLocation');
            Route::post('edit-location', 'v5\Api\Merchant\UserController@editLocation');
            Route::get('get-business-profile', 'v5\Api\Merchant\ServiceController@getBusinessProfile');
            Route::post('edit-business-profile', 'v5\Api\Merchant\ServiceController@editBusinessProfile');

            // merchant bussiness info

            // get merchant services list
            Route::get('get/bussiness/service/list', 'v5\Api\Merchant\ServiceController@getBussinessServiceList');

            // add_edit merchant services
            Route::post('add/edit/bussiness/service', 'v5\Api\Merchant\ServiceController@addEditBussinessService');

            // delete bussiness merchant service
            Route::post('delete/bussiness/service', 'v5\Api\Merchant\ServiceController@deleteBussinessService');
            // Route::post('update-mobile-number','MainController@updateNumber');

            //merchant setting
            Route::get('getSetting', 'v5\Api\Merchant\UserController@getSetting');
            Route::post('saveSetting', 'v5\Api\Merchant\UserController@saveSetting');

            Route::group(['prefix' => 'reports'], function () {
                Route::get('total', 'v5\Api\Merchant\MerchantReports@totalReport');
                Route::get('graph', 'v5\Api\Merchant\MerchantReports@graphReview');
            });
            Route::post('logout', 'v5\Api\Merchant\UserController@logout');
        });        
    });   

    Route::get('v5/update_app_detail', 'v5\Api\AppoientmentController@userAppointmentDetailUpdate');
// VERSION 5 API END HERE





Route::post('user/add_user_profilepic', 'v1\Api\Merchant\UserController@addUserProfilePicByMerchant')->middleware('auth:merchants');
Route::get('/user_info', 'v1\Api\Merchant\UserController@index')->middleware('auth:merchants');

Route::get('send-mail', 'v1\MainController@sendMail');

Route::get('cron-test', 'v5\MainController@notificationCron');
Route::get('notification-test', 'v2\MainController@sampleNotification');

// Route::get('dev-services/{merchant_id}','v1\Api\MerchantsController@devServices');
// Route::get('test/{mins?}','v1\Api\User\UserController@test');
Route::get('send-sms', 'v5\Api\User\UserController@testSMSapi');
// Route::post('file-upload', 'v1\Api\User\StaffController@testUpload');
// Route::get('timezone','v1\MainController@timestampToLocal');
// Route::get('convert-seconds','v1\MainController@convertSeconds');
