<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title></title>
<style type="text/css">
* {
	-webkit-font-smoothing: antialiased;
}
body {
	Margin: 0;
	padding: 0;
	min-width: 100%;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
	mso-line-height-rule: exactly;
}
table {
	border-spacing: 0;
	color: #333333;
	font-family: Arial, sans-serif;
}
img {
	border: 0;
}
.wrapper {
	width: 100%;
	table-layout: fixed;
	-webkit-text-size-adjust: 100%;
	-ms-text-size-adjust: 100%;
}
.webkit {
	max-width: 600px;
}
.outer {
	Margin: 0 auto;
	width: 100%;
	max-width: 600px;
}
.full-width-image img {
	width: 100%;
	max-width: 600px;
	height: auto;
}
.inner {
	padding: 10px;
}
p {
	Margin: 0;
	padding-bottom: 10px;
}
.h1 {
	font-size: 21px;
	font-weight: bold;
	Margin-top: 15px;
	Margin-bottom: 5px;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
}
.h2 {
	font-size: 18px;
	font-weight: bold;
	Margin-top: 10px;
	Margin-bottom: 5px;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
}
.one-column .contents {
	text-align: left;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
}
.one-column p {
	font-size: 14px;
	Margin-bottom: 10px;
	font-family: Arial, sans-serif;
	-webkit-font-smoothing: antialiased;
}
.two-column {
	text-align: center;
	font-size: 0;
}
.two-column .column {
	width: 100%;
	max-width: 300px;
	display: inline-block;
	vertical-align: top;
}
.contents {
	width: 100%;
}
.two-column .contents {
	font-size: 14px;
	text-align: left;
}
.two-column img {
	width: 100%;
	max-width: 280px;
	height: auto;
}
.two-column .text {
	padding-top: 10px;
}
.three-column {
	text-align: center;
	font-size: 0;
	padding-top: 10px;
	padding-bottom: 10px;
}
.three-column .column {
	width: 100%;
	max-width: 200px;
	display: inline-block;
	vertical-align: top;
}
.three-column .contents {
	font-size: 14px;
	text-align: center;
}
.three-column img {
	width: 100%;
	max-width: 180px;
	height: auto;
}
.three-column .text {
	padding-top: 10px;
}
.img-align-vertical img {
	display: inline-block;
	vertical-align: middle;
}
@media only screen and (max-device-width: 480px) {
table[class=hide], img[class=hide], td[class=hide] {
	display: none !important;
}
.contents1 {
	width: 100%;
}
.contents1 {
	width: 100%;
}
</style>
<!--[if (gte mso 9)|(IE)]>
	<style type="text/css">
		table {border-collapse: collapse !important;}
	</style>
	<![endif]-->
</head>

<body style="Margin:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;min-width:100%;background-color:#f3f2f0;">
<center class="wrapper" style="width:100%;table-layout:fixed;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#f3f2f0;">
  <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:#f3f2f0;" bgcolor="#f3f2f0;">
    <tr>
      <td width="100%"><div class="webkit" style="max-width:600px;Margin:0 auto;">

          <!-- ======= start main body ======= -->
          <table class="outer" align="center" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0;Margin:0 auto;width:100%;max-width:600px;">
            <tr>
              <td style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">

				  <!-- ======= start header ======= -->

                <table border="0" width="100%" cellpadding="0" cellspacing="0"  >
                  <tr>
                    <td><table style="width:100%;" cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                          <tr>
                            <td align="center"><center>
                                <table border="0" align="center" width="100%" cellpadding="0" cellspacing="0" style="Margin: 0 auto;">
                                  <tbody>
                                    <tr>
                                      <td class="one-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" bgcolor="#FFFFFF">
										  <!-- ======= start header ======= -->

                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="#d6d6d6" style="border-bottom: 5px solid #9b9b9b;">
                                          <tr>
                                            <td class="two-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:left;font-size:0;" >

                                              <div class="column" style="width:100%;max-width:80px;display:inline-block;vertical-align:top;">
                                                <table class="contents" style="border-spacing:0; width:100%"  >
                                                  <tr>
                                                    <td style="padding-top:10px;padding-bottom:15px;padding-right:0;padding-left:5px;" align="left">
														<a href="#" target="_blank">
															<img src="images/opayn-logo.png" alt="" width="60" height="60" style="border-width:0; max-width:73px;height:auto; display:block" align="left"/>
														</a>
													</td>
                                                  </tr>
                                                </table>
                                              </div>

                                              <div class="column" style="width:100%;max-width:518px;display:inline-block;vertical-align:top;">
                                                <table width="100%" style="border-spacing:0" cellpadding="0" cellspacing="0" border="0" >
                                                  <tr>
                                                    <td class="inner" style="padding-top:0px;padding-bottom:10px; padding-right:10px;padding-left:10px;"><table class="contents" style="border-spacing:0; width:100%" cellpadding="0" cellspacing="0" border="0">
                                                        <tr>
                                                          <td align="left" valign="top">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                          <td  align="right" valign="top"><font style="font-size:11px; text-decoration:none; color:#474b53; font-family: Verdana, Geneva, sans-serif; text-align:left; line-height:16px; padding-bottom:30px"><a href="#"  style="color:#474b53; text-decoration:none;font-weight: 600;">Order #xxxxxxxx</a>
															  <br>
															  <a href="#" target="_blank" style="color:#474b53; text-decoration:none;font-weight: 600;">August,20, 2020, 6:09 AM PST</a>
															</font>
														   </td>
                                                        </tr>
                                                      </table></td>
                                                  </tr>
                                                </table>
                                              </div>

                                              <!--[if (gte mso 9)|(IE)]>
													</td>
													</tr>
													</table>
													<![endif]--></td>
                                          </tr>
                                        </table></td>
                                    </tr>

									  <tr>
									  <td>
										  <table class="one-column" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0;" bgcolor="#FFFFFF">
                  <tbody><tr>
                    <td align="center" style="padding:40px 50px 30px 50px">
						<img src="images/tick.png">

						<p style="color:#262626; font-size:20px;margin-top:10px; text-align:center; font-family: Verdana, Geneva, sans-serif"><strong>Thank you for placing your order<br>
							with InTouch Photos!</strong></p>
                      <p style="color:#008fc4; font-size:14px; text-align:center; font-family: Verdana, Geneva, sans-serif; line-height:24px ">Your order will be shipped to:<br>
						  John Paul Smith # 04456-087,<br><br>P.O. BOX 15330<br>
						  FORT WORTH, TX 76119<br><br>Estimated Arrival to Facility: October 3rd, 2020 to October 8th, 2020 <br>
						  To track your order, please click here:
                      </p>

                      </td>
                  </tr>
                </tbody></table>
										  </td>
									  </tr>
									  <tr>
									  <td style="padding:0 30px 20px;background:#fff;border:none">
										   <table class="one-column" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0;" bgcolor="#FFFFFF">
											    <tr style="background: #000;color:#fff;">
											  <th  style="padding: 12px 6px 12px 80px;;text-align: left;">Products</th>
											  <th  style="padding:12px 30px 12px 6px;;text-align: left;">Price</th>
											  </tr>
											  <tr>
											  <td style="color:#666;text-align: left;padding:14px 6px 10px 80px">5 (4x6) high-quality photos </td>
											  <td style="color:#000;text-align: left;padding:14px 30px 10px 6px">$4.99</td>
											  </tr>
											    <tr>
											  <td style="color:#666;text-align: left;padding:10px 6px 10px 80px">Shipping (via First Class USPS) </td>
											  <td style="color:#000;text-align: left;padding:10px 30px 10px 6px">Free</td>
											  </tr>
											    <tr>
											  <td style="color:#666;text-align: left;padding:10px 6px 10px 80px">Sales tax </td>
											  <td style="color:#000;text-align: left;padding:10px 30px 10px 6px">$0.00</td>
											  </tr>
											     <tr>
											  <td style="color:#000;text-align: left;padding: 15px 6px 15px 80px;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc">Total</td>
											  <td style="color:#000;text-align: left;padding: 15px 30px 15px 6px;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc">$4.99</td>
											  </tr>
										  </table>

										  </td>

									  </tr>

									  <table cellpadding="0" cellspacing="0" border="0" width="100%" bgcolor="white">
                  <tbody>
                  <tr>
                    <td class="two-column" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;text-align:center;font-size:0;"><!--[if (gte mso 9)|(IE)]>
													<table width="100%" style="border-spacing:0" >
													<tr>
													<td width="60%" valign="top" style="padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;" >
													<![endif]-->

                    	<div class="" style="width:100%;display:inline-block;vertical-align:top;">
                        	<table class="contents" style="border-spacing:0;width:100%;padding-left: 30px;padding-right: 30px;">
                          		<tbody style="background: #0092c7;padding: 10px 0;">
								  	<tr>
										<td  align="center" style="padding-top:10px;padding-bottom:10px;padding-right:10px;padding-left:0;">
											<h2 style="color:#fff;padding-right: 30px;display: inline-block;margin-right: 70px;margin: 0px 70px 0 0;vertical-align: middle;">Follow us on</h2>

											<a href="#" target="_blank" style="display: inline-block;vertical-align: middle;">
												<img src="images/fb.png" alt="facebook" width="36" height="36" border="0" style="border-width:0; max-width:36px;height:auto; display:block; max-height:36px;    margin: 0 5px;">
											</a>
											<a href="#" target="_blank" style="display: inline-block;vertical-align: middle;">
												<img src="images/whatsapp.png" alt="whats app" width="36" height="36" border="0" style="border-width:0; max-width:36px;height:auto; display:block; max-height:36px;    margin: 0 5px;">
											</a>
											<a href="#" target="_blank" style="display: inline-table;vertical-align: middle;">
												<img src="images/twiter.png" alt="twitter" width="36" height="36" border="0" style="border-width:0; max-width:36px;height:auto; display:block; max-height:36px;    margin: 0 5px;">
											</a>
										</td>
									</tr>
                        		</tbody>
							</table>
                    	</div>
                    </td>
                </tr>

            </tbody>
		</table>
		<tr>
			<td>
				<table class="one-column" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-spacing:0;padding:0 30px 20px;background:#fff;border:none" bgcolor="#FFFFFF">
					<tbody>
						<tr>
							<td>
								<p style="color:#000;font-size:12px;text-align:center;font-family: Verdana, Geneva, sans-serif;line-height:22px;padding: 10px 0 0;border-top: 1px solid #aeaeae;margin-top: 0px;">Copyright © 2020 Opayn, All rights reserved.</p>
							</td>
						</tr>

					</tbody>
				</table>
			</td>
		</tr>

	</tbody>
</table>
</center>
			</td>
		</tr>
                        </tbody>
                      </table></td>
        </tr>
                </table>

                <!-- ======= end header ======= -->



            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
					</td>
				</tr>
			</table>
			<![endif]-->
        </div></td>
    </tr>
  </table>
</center>
</body>
</html>
